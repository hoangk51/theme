const mix = require('laravel-mix');

mix.sass('resources/sass/front.scss', 'public/assets/default/css/app.css')
   .sass('resources/sass/back.scss', 'public/assets/admin/css/app.css')
   .js('resources/js/front.js', 'public/assets/default/js/app.js')
   .js('resources/js/back.js', 'public/assets/admin/js/app.js');
