<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\Paginator;
use Illuminate\Database\Connection;
use App\Database\MySqlConnection;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Connection::resolverFor('mysql', function ($connection, $database, $prefix, $config) {
            return new MySqlConnection($connection, $database, $prefix, $config);
        });
    }




    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if(!app()->runningInConsole() && config('app.installed') === true)
        {
          Paginator::defaultView('vendor.pagination.semantic-ui');

          Paginator::defaultSimpleView('vendor.pagination.simple-semantic-ui');

          $settings = (object)array_map(function($val)
                      {
                        return json_decode($val, true);
                      },\App\Models\Setting::first()->getAttributes() ?? []);

          config([
            'services'   => array_merge(config('services'), $settings->social_login),
            'payments'   => array_merge(config('payments'), $settings->payments),
            'mail'       => array_merge(config('mail'), $settings->mailer['mail'] ?? []),
            'Imap'       => $settings->mailer['imap'],
            'app'        => array_merge(config('app'), $settings->general, $settings->search_engines),
            'filehosts'  => array_merge(config('filehosts'), $settings->files_host),
            'categories' => \App\Models\Category::products(),
            'pages'      => \App\Models\Page::useIndex('active')->select('name', 'slug', 'deletable')->where('active', 1)->get(),
            'popular_categories' => \App\Models\Category::popular(),
            'adverts'    => $settings->adverts
          ]);

          if(config('app.timezone'))
            date_default_timezone_set(config('app.timezone'));
        }
    }
}
