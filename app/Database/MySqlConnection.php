<?php

namespace App\Database;

use App\Database\Query\Grammars\MySqlGrammar;

/**
 * Class MySqlConnection
 * @package App\Database\Connection
 */
class MySqlConnection extends \Illuminate\Database\MySqlConnection
{
    /**
     * @return MySqlGrammar
     */
    protected function getDefaultQueryGrammar()
    {
        return $this->withTablePrefix(new MySqlGrammar()); // here comes our custom Grammar object
    }
}