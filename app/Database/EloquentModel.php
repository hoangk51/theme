<?php

namespace App\Database;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;


class EloquentModel extends Model 
{
	
  protected static function get_auto_increment(): ?int
  {
    $var       = explode('\\', get_called_class());
  	$table     = Str::plural(strtolower(array_pop($var)));

  	$statement = DB::select("SHOW TABLE STATUS LIKE '$table'");

		return $statement[0]->Auto_increment;
  }



  protected static function useIndex($indexes = [])
  {
    $indexes = implode(', ', is_array($indexes) ? $indexes : func_get_args());

    if(!$indexes)
      throw new \Exception('No indexes defined for the modal.');

    $model = new static();

    $model->setTable(\DB::raw($model->getTable() . " USE INDEX({$indexes})"));

    return $model;
  }


  protected static function insertIgnore(array $attributes = [])
  {
      $model = new static($attributes);

      if ($model->usesTimestamps()) {
          $model->updateTimestamps();
      }

      $attributes = $model->getAttributes();

      $query = $model->newBaseQueryBuilder();
      $processor = $query->getProcessor();
      $grammar = $query->getGrammar();

      $table = $grammar->wrapTable($model->getTable());
      $keyName = $model->getKeyName();
      $columns = $grammar->columnize(array_keys($attributes));
      $values = $grammar->parameterize($attributes);

      $sql = "insert ignore into {$table} ({$columns}) values ({$values})";

      $id = $processor->processInsertGetId($query, $sql, array_values($attributes));

      $model->setAttribute($keyName, $id);

      return $model;
  }

}