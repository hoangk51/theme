<?php

namespace App\Models;

use App\Database\EloquentModel as Model;

class Newsletter_Subscriber extends Model
{
		protected $table = 'newsletter_subscribers';
    protected $fillable = ['email'];
}
