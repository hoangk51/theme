<?php

namespace App\Models;

use App\Database\EloquentModel as Model;

class Setting extends Model
{
    protected $fillable = ['social_login', 'files_host', 'payments', 'mailer'];
}
