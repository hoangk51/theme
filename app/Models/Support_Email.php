<?php

namespace App\Models;

use App\Database\EloquentModel as Model;

class Support_Email extends Model
{
    protected $table 		= 'support_email';
    public $timestamps 	= false;
    protected $fillable = ['email'];
}
