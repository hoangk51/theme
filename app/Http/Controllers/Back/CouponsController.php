<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Coupon;
use Illuminate\Support\Facades\Validator;



class CouponsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $validator =  Validator::make($request->all(),
                    [
                      'orderby' => ['regex:/^(code|used_by|amount|start_at|expire_at|updated_at)$/i', 'required_with:order'],
                      'order'   => ['regex:/^(asc|desc)$/i', 'required_with:orderby']
                    ]);

      if($validator->fails()) abort(404);

      $base_uri = [];

      if($keywords = $request->keywords)
      {
        $base_uri = ['keywords' => $keywords];

        $coupons = Coupon::useIndex('primary')
                          ->selectRaw("coupons.id, code, value, is_percentage, starts_at, expires_at, 
                                       coupons.updated_at, products.name AS item_name, products.slug AS item_slug,
                                       IF(used_by IS NOT NULL, 
                                       CHAR_LENGTH(used_by) - CHAR_LENGTH( REPLACE ( used_by, ',', '') ) + 1, 0) as used_by")
                          ->leftJoin('products', 'products.id', 'coupons.product_id')
                          ->where('code', 'like', "%{$keywords}%")
                          ->orWhere('products.name', 'like', "%{$keywords}%")
                          ->orWhere('products.slug', 'like', "%{$keywords}%")
                          ->orderBy('id', 'DESC');
      }
      else
      {
        if($request->orderby)
        {
          $base_uri = ['orderby' => $request->orderby, 'order' => $request->order];
        }

        $coupons = Coupon::useIndex($request->orderby ?? 'primary')
                          ->selectRaw("coupons.id, code, value, is_percentage, starts_at, expires_at, updated_at,
                                      IF(used_by IS NOT NULL, 
                                      CHAR_LENGTH(used_by) - CHAR_LENGTH( REPLACE ( used_by, ',', '') ) + 1, 0) as used_by")
                          ->orderBy($request->orderby ?? 'id', $request->order ?? 'DESC');
      }

      $coupons = $coupons->paginate(15);

      $items_order = $request->order === 'desc' ? 'asc' : 'desc';

      return View('back.coupons.index', [ 'title' => 'Coupons',
                                          'coupons' => $coupons,
                                          'items_order' => $items_order,
                                          'base_uri' => $base_uri]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('back.coupons.create', ['title' => 'Create coupon']);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {      
      $request->validate([
        'code'          => 'required|max:255|unique:coupons,code|bail',
        'value'         => 'required|gt:0',
        'is_percentage' => ['required', 'regex:/^(0|1)$/'],
        'product_id'    => 'nullable|numeric',
        'users_ids'     => ['nullable', 'regex:/^([\d,?]+)$/'],
        'starts_at'     => 'required|date_format:Y-m-d\TH:i',
        'expires_at'    => 'required|date_format:Y-m-d\TH:i',
      ]);

      $coupon = new Coupon;

      $coupon->code           = $request->code;
      $coupon->value          = $request->value;
      $coupon->is_percentage  = $request->is_percentage;
      $coupon->product_id     = $request->product_id;
      $coupon->users_ids      = $request->users_ids
                                ? implode(',', array_map('wrap_str', explode(',', $request->users_ids)))
                                : '';
      $coupon->starts_at      = date_format(new \DateTime($request->starts_at), 'Y-m-d H:i:s');
      $coupon->expires_at     = date_format(new \DateTime($request->expires_at), 'Y-m-d H:i:s');

      $coupon->save();

      return redirect()->route('coupons');
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      if(!$coupon = Coupon::find($id))
        abort(404);

      $coupon->starts_at = str_ireplace(' ', 'T', $coupon->starts_at);
      $coupon->expires_at = str_ireplace(' ', 'T', $coupon->expires_at);

      return View('back.coupons.edit', ['title' => "Edit Coupon - {$coupon->code}",
                                        'coupon' => $coupon]);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $request->validate([
        'code'          => "required|max:255|unique:coupons,code,{$id}|bail",
        'value'         => 'required|gt:0',
        'is_percentage' => ['required', 'regex:/^(0|1)$/'],
        'product_id'    => 'nullable|numeric',
        'users_ids'     => ['nullable', 'regex:/^([\d,?]+)$/'],
        'starts_at'     => 'required|date_format:Y-m-d\TH:i:s',
        'expires_at'    => 'required|date_format:Y-m-d\TH:i:s',
      ]);

      $coupon = Coupon::find($id);

      $coupon->code           = $request->code;
      $coupon->value          = $request->value;
      $coupon->is_percentage  = $request->is_percentage;
      $coupon->product_id     = $request->product_id;
      $coupon->users_ids      = $request->users_ids
                                ? implode(',', array_map('wrap_str', explode(',', $request->users_ids)))
                                : '';
      $coupon->starts_at      = date_format(new \DateTime($request->starts_at), 'Y-m-d H:i:s');
      $coupon->expires_at     = date_format(new \DateTime($request->expires_at), 'Y-m-d H:i:s');

      $coupon->save();

      return redirect()->route('coupons');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $ids
     * @return \Illuminate\Http\Response
     */
    public function destroy($ids)
    {
      Coupon::destroy(explode(',', $ids));

      return redirect()->route('coupons');
    }



    /**
    * Generate coupon
    * @param int $limit (max 40)
    * @return String
    */
    public function generate($limit = 12)
    {
      $limit = ($limit > 40) ? 12 : $limit;

      $arr = array_merge(range('A', 'Z'), range(0, 15));
    
      shuffle($arr);
      
      $coupon = implode('', array_slice($arr, 0, $limit));

      while(Coupon::where('code', $coupon)->count() === 1)
      {
        $this->generate($limit);
      }

      return response()->json(['code' => $coupon]);
    }
}
