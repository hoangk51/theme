<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\User;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $validator =  Validator::make($request->all(),
                    [
                      'orderby' => ['regex:/^(name|email|verified|created_at|purchases|total_purchases)$/i', 
                                    'required_with:order'],
                      'order' => ['regex:/^(asc|desc)$/i', 'required_with:orderby']
                    ]);

      if($validator->fails()) abort(404);

      $base_uri = [];

      if($keywords = $request->keywords)
      {
        $base_uri = ['keywords' => $keywords];

        $users = User::useIndex('user')
                      ->selectRaw("users.id, IFNULL(users.name, '-') as `name`, users.email, 
                        users.created_at, 
                        IF(users.email_verified_at IS NOT NULL, 1, 0) as verified,
                        (select ifnull(sum(items_count), 0) from transactions where user_id = users.id) as purchases,
                        (select ifnull(round(sum(amount), 2), 0) from transactions where user_id = users.id) as total_purchases
                      ")
                      ->where('users.name', 'like', "%{$keywords}%")
                      ->orWhere('users.email', 'like', "%{$keywords}%")
                      ->orderBy('id', 'DESC');
      }
      else
      {
        if($request->orderby)
        {
          $base_uri = ['orderby' => $request->orderby, 'order' => $request->order];
        }

        $index  = preg_match('/^(purchases|total_purchases|)$/i', $request->orderby) ? 'primary' : $request->orderby;

        $users = User::useIndex($index)
                      ->selectRaw("users.id, IFNULL(users.name, '-') as `name`, users.email, 
                        users.created_at,
                        IF(users.email_verified_at IS NOT NULL, 1, 0) as verified,
                        (select ifnull(sum(items_count), 0) from transactions where user_id = users.id) as purchases,
                        (select ifnull(round(sum(amount), 2), 0) from transactions where user_id = users.id) as total_purchases
                      ")
                      ->orderBy($request->orderby ?? 'id', $request->order ?? 'desc');
      }

      $users = $users->paginate(15);

      $items_order = $request->order === 'desc' ? 'asc' : 'desc';

      return View('back.users', [ 'title' => 'Users',
                                  'users' => $users,
                                  'items_order' => $items_order,
                                  'base_uri' => $base_uri]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $ids
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $ids)
    {
      $ids = explode(',', $ids);

      if(User::whereIn('id', $ids)->Where('role', '!=', 'admin')->delete())
      {
        foreach($ids as $id)
        {
          $avatar = glob(storage_path("app/public/avatars/{$id}.*"))[0] ?? NULL;

          if($avatar) unlink($avatar);
        }
      }

      return redirect()->route('users');
    }
}
