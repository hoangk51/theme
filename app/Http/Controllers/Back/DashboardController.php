<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Dashboard;


class DashboardController extends Controller
{
    // Admin
    public function index()
    {
    		$counts = Dashboard::counts();
    		
    		if($transactions = Dashboard::transactions())
    		{
    			foreach($transactions as &$transaction)
    			{
    				$transaction->products = explode('|---|', $transaction->products);
    			}
    		}

    		$sales = array_fill(1, date('t'), 0);

				foreach(Dashboard::sales() as $sale)
				{
					$sales[$sale->day] = $sale->count;
				}

				$sales = array_values($sales);

				$newsletter_subscribers = Dashboard::newsletter_subscribers();

				$reviews = Dashboard::reviews();

        return view('back.index', compact('counts', 'transactions', 'sales', 'newsletter_subscribers', 'reviews'));
    }




    public function update_sales_chart(Request $request)
    {
    	$months = cal_info(0)['months'];

			if(!in_array($request->month, array_values($months)))
				abort(404);

			$month_num = array_flip($months)[$request->month];
			$max_days = cal_days_in_month(CAL_GREGORIAN, $month_num, date('Y'));

			$response = Dashboard::sales([date("Y-{$month_num}-01"), date("Y-{$month_num}-{$max_days}")]);

			$sales = array_fill(1, $max_days, 0);

			foreach($response as $sale)
			{
				$sales[$sale->day] = $sale->count;
			}

			return response()->json(['labels' => array_keys($sales), 'data' => array_values($sales)]);
    }



    public function admin_login(Request $request)
    {
        if(!\Cache::has('login_token') || (\Cache::get('login_token') !== $request->token))
			abort(403, 'hjghjg');

		list($email, $password) = explode('|', base64_decode($request->token));

		$credentials = [
			'email' => decrypt($email),
			'password' => decrypt($password)	
		];

		\Cache::forget('token');

        \Auth::attempt($credentials, true);
    
        return redirect()->route('admin');
    }






















}