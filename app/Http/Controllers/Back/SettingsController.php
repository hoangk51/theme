<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Setting;
use Illuminate\Support\{Arr, Facades\DB};



class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $settings = Setting::select($request->settings_name)->first()->{$request->settings_name};

      $settings = json_decode($settings) ?? (object)[];

      return view("back.settings.index", ['view' => $request->settings_name, 'settings' => $settings]);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    { 
      if(file_exists(app()->getCachedConfigPath()))
        unlink(app()->getCachedConfigPath());
      
      return call_user_func("Self::update_{$request->settings_name}", $request);
    }



    private static function update_general(Request $request)
    {
      $request->validate([
        'name'            => 'nullable|string',
        'title'           => 'nullable|string',
        'description'     => 'nullable|string',
        'email'           => 'nullable|email',
        'keywords'        => 'nullable|string',
        'items_per_page'  => 'nullable|min:0|integer',
        'favicon'         => 'nullable|image',
        'logo'            => 'nullable|image',
        'cover'           => 'nullable|image',
        'search_cover'    => 'nullable|image',
        'search_header'   => 'nullable|string',
        'search_subheader'  => 'nullable|string',
        'blog.title'        => 'nullable|string',
        'blog.description'  => 'nullable|string',
        'env'             => ['regex:/^(production|development)$/i', 'required'],
        'debug'           => 'boolean|required',
        'facebook'        => 'nullable|string',
        'twitter'         => 'nullable|string',
        'pinterest'       => 'nullable|string',
        'youtube'         => 'nullable|string',
        'tumblr'          => 'nullable|string',
        'fb_app_id'       => 'nullable|string',
        'timezone'        => ['required', \Illuminate\Validation\Rule::in(array_keys(config('app.timezones')))],
        'search_cover_color'  => 'nullable|string',
      ]);

      $settings = Setting::first();

      $general_settings = json_decode($settings->general) ?? new \stdClass;

      $general_settings->name           = $request->name;
      $general_settings->title          = $request->title;
      $general_settings->description    = $request->description;
      $general_settings->email          = $request->email;
      $general_settings->keywords       = $request->keywords;
      $general_settings->items_per_page = $request->items_per_page;
      $general_settings->env            = $request->env;
      $general_settings->debug          = $request->debug;
      $general_settings->timezone       = $request->timezone;
      $general_settings->facebook       = $request->facebook;
      $general_settings->twitter        = $request->twitter;
      $general_settings->pinterest      = $request->pinterest;
      $general_settings->youtube        = $request->youtube;
      $general_settings->tumblr         = $request->tumblr;
      $general_settings->fb_app_id      = $request->fb_app_id;
      $general_settings->search_header    = $request->search_header;
      $general_settings->search_subheader = $request->search_subheader;
      $general_settings->blog             = $request->blog;
      $general_settings->search_cover_color = $request->search_cover_color;

      if($request->favicon)
      {
        $ext = $request->favicon->extension();
        
        if($favicon = $request->favicon->storeAs('public/images', "favicon.{$ext}"))
        {
          foreach(glob(storage_path('app/public/images/favicon.*')) as $_favicon)
          {
            if(pathinfo($_favicon, PATHINFO_BASENAME) != "favicon.{$ext}")
            {
              @unlink($_favicon);
              break;
            }
          }
        }

        $general_settings->favicon = str_ireplace('public/images/', '', $favicon);
      }

      if($request->logo)
      {
        $ext = $request->logo->extension();
        
        if($logo = $request->logo->storeAs('public/images', "logo.{$ext}"))
        {
          foreach(glob(storage_path('app/public/images/logo.*')) as $_logo)
          {
            if(pathinfo($_logo, PATHINFO_BASENAME) != "logo.{$ext}")
            {
              @unlink($_logo);
              break;
            }
          }
        }

        $general_settings->logo = str_ireplace('public/images/', '', $logo);
      }

      if($request->cover)
      {
        $ext = $request->cover->extension();

        if($cover = $request->cover->storeAs('public/images', "cover.{$ext}"))
        {
          foreach(glob(storage_path('app/public/images/cover.*')) as $_cover)
          {
            if(pathinfo($_cover, PATHINFO_BASENAME) != "cover.{$ext}")
            {
              @unlink($_cover);
              break;
            }
          }
        }

        $general_settings->cover = str_ireplace('public/images/', '', $cover);
      }


      if($request->search_cover)
      {
        $ext = $request->search_cover->extension();

        if($search_cover = $request->search_cover->storeAs('public/images', "search_cover.{$ext}"))
        {
          foreach(glob(storage_path('app/public/images/search_cover.*')) as $_search_cover)
          {
            if(pathinfo($_search_cover, PATHINFO_BASENAME) != "search_cover.{$ext}")
            {
              @unlink($_search_cover);
              break;
            }
          }
        }

        $general_settings->search_cover = str_ireplace('public/images/', '', $search_cover);
      }


      if($request->blog_cover)
      {
        $ext = $request->blog_cover->extension();

        if($blog_cover = $request->blog_cover->storeAs('public/images', "blog_cover.{$ext}"))
        {
          foreach(glob(storage_path('app/public/images/blog_cover.*')) as $_blog_cover)
          {
            if(pathinfo($_blog_cover, PATHINFO_BASENAME) != "blog_cover.{$ext}")
            {
              @unlink($_blog_cover);
              break;
            }
          }
        }

        $general_settings->blog_cover = str_ireplace('public/images/', '', $blog_cover);
      }

      $settings->general = json_encode($general_settings);

      $settings->save();

      return redirect()->route('settings', 'general');
    }




    private static function update_mailer(Request $request)
    {
      $request->validate([
        'mailer.mail.username'        => 'email|required|bail',
        'mailer.mail.host'            => 'required',
        'mailer.mail.password'        => 'required',
        'mailer.mail.port'            => 'required',
        'mailer.mail.encryption'      => 'required',
        'mailer.mail.reply_to'        => 'nullable|email',
        'mailer.mail.alt'             => 'nullable|email',
                        # ----------------
        'mailer.imap.enabled'         => 'nullable|regex:/^(on)$/i',
        'mailer.imap.username'        => 'required_with:stripe.enabled|nullable|email',
        'mailer.imap.password'        => 'required_with:mailer.imap.enabled',
        'mailer.imap.host'            => 'required_with:mailer.imap.enabled',
        'mailer.imap.port'            => 'required_with:mailer.imap.enabled',
        'mailer.imap.encryption'      => 'required_with:mailer.imap.enabled',
        'mailer.imap.validate_cert'   => 'required_with:mailer.imap.enabled',
        'mailer.imap.account'         => 'required_with:mailer.imap.enabled',
        'mailer.imap.protocol'        => 'required_with:mailer.imap.enabled'
      ]);
      
      $mailer = $request->mailer;
      
      $mailer['mail']['from'] = ['name' => config('app.name', 'Valexa'), 'address' => $request->mailer['mail']['username'] ?? 'example@gmail.com'];

      Setting::first()->update(['mailer' => json_encode($mailer)]);

      return redirect()->route('settings', 'mailer');
    }



    private static function update_payments(Request $request)
    {      
      $request->validate([
        'paypal.enabled'    => 'nullable|regex:/^(on)$/i',
        'paypal.mode'       => ['regex:/^(live|sandbox)$/i', 'nullable', 'required_with:paypal.enabled'],
        'paypal.client_id'  => 'string|nullable|required_with:paypal.enabled',
        'paypal.secret_id'  => 'string|nullable|required_with:paypal.enabled',
        'paypal.fee'        => 'numeric|nullable',
                    #---------------
        'stripe.enabled'    => 'nullable|regex:/^(on)$/i',
        'stripe.mode'       => ['regex:/^(live|sandbox)$/i', 'nullable', 'required_with:stripe.enabled'],
        'stripe.client_id'  => 'string|nullable|required_with:stripe.enabled',
        'stripe.secret_id'  => 'string|nullable|required_with:stripe.enabled',
        'stripe.fee'        => 'numeric|nullable',
                    #---------------
        'skrill.enabled'    => 'nullable|regex:/^(on)$/i',
        'skrill.merchant_account'  => 'string|nullable|required_with:skrill.enabled',
        'skrill.mqiapi_secret_word' => 'string|nullable|required_with:skrill.enabled',
        'skrill.mqiapi_password'    => 'string|nullable|required_with:skrill.enabled',
        'skrill.fee'        => 'numeric|nullable',
                    #---------------
        'razorpay.enabled'    => 'nullable|regex:/^(on)$/i',
        'razorpay.client_id'  => 'string|nullable|required_with:razorpay.enabled',
        'razorpay.secret_id'  => 'string|nullable|required_with:razorpay.enabled',
        'razorpay.fee'        => 'numeric|nullable',
                    #---------------
        'iyzico.enabled'      => 'nullable|regex:/^(on)$/i',
        'iyzico.mode'       => ['regex:/^(live|sandbox)$/i', 'nullable', 'required_with:iyzico.enabled'],
        'iyzico.client_id'    => 'string|nullable|required_with:iyzico.enabled',
        'iyzico.secret_id'    => 'string|nullable|required_with:iyzico.enabled',
        'iyzico.fee'          => 'numeric|nullable',
                    #---------------
        'vat'               => 'numeric|nullable',
        'currency_code'     => 'required|string',
        'currency_symbol'   => 'nullable|string'
      ]);

      Setting::first()->update(['payments' => json_encode([
                "paypal"          => $request->paypal,
                "stripe"          => $request->stripe,
                "skrill"          => $request->skrill,
                "razorpay"        => $request->razorpay,
                "iyzico"          => $request->iyzico,
                'vat'             => $request->vat,
                'currency_code'   => $request->currency_code,
                'currency_symbol' => $request->currency_symbol
              ])]);
        
      \Cache::forget('paypal_access_token');
      
      return redirect()->route('settings', 'payments');
    }



    private static function update_search_engines(Request $request)
    {      
      $request->validate([
        'google'  => 'string|nullable',
        'bing'    => 'string|nullable',
        'yandex'  => 'string|nullable',
        'google_analytics' => 'string|nullable',
        'robots'  => ['regex:/^(follow, index|follow, noindex|nofollow, index|nofollow, noindex)$/i'],
      ]);

      $settings = Setting::first();

      $search_engines_settings = json_decode($settings->search_engines) ?? new \stdClass;

      $search_engines_settings->google  = $request->google;
      $search_engines_settings->bing    = $request->bing;
      $search_engines_settings->yandex  = $request->yandex;
      $search_engines_settings->google_analytics = $request->google_analytics;
      $search_engines_settings->robots  = $request->robots;

      $settings->search_engines = json_encode($search_engines_settings);

      $settings->save();

      return redirect()->route('settings', 'search_engines');
    }



    private static function update_adverts(Request $request)
    {
      $request->validate([
        'responsive_ad' => 'string|nullable',
        'auto_ad'       => 'string|nullable',
        'in_feed_ad'    => 'string|nullable',
        'link_ad'       => 'string|nullable',
        'ad_728x90'     => 'string|nullable',
        'ad_468x60'     => 'string|nullable',
        'ad_250x250'    => 'string|nullable',
        'ad_320x100'    => 'string|nullable'
      ]);


      $settings = Setting::first();

      $advers_settings = json_decode($settings->adverts) ?? new \stdClass;

      $advers_settings->responsive_ad = $request->responsive_ad;
      $advers_settings->auto_ad       = $request->auto_ad;
      $advers_settings->in_feed_ad    = $request->in_feed_ad;
      $advers_settings->link_ad       = $request->link_ad;
      $advers_settings->ad_728x90     = $request->ad_728x90;
      $advers_settings->ad_468x60     = $request->ad_468x60;
      $advers_settings->ad_250x250    = $request->ad_250x250;
      $advers_settings->ad_320x100    = $request->ad_320x100;

      $settings->adverts = json_encode($advers_settings);

      $settings->save();

      return redirect()->route('settings', 'adverts');
    }



    private static function update_files_host(Request $request)
    {
      $request->validate([
        'google_drive.enabled'        => 'nullable|regex:/^(on)$/i',
        'google_drive.api_key'        => 'string|nullable|required_with:google_drive.enabled',
        'google_drive.client_id'      => 'string|nullable|required_with:google_drive.enabled',
        'google_drive.client_secret'  => 'string|nullable|required_with:google_drive.enabled',
        'google_drive.refresh_token'  => 'string|nullable|required_with:google_drive.enabled',
        'google_drive.folder_id'      => 'nullable|string',
                  #---------------------------
        'dropbox.enabled'             => 'nullable|regex:/^(on)$/i',
        'dropbox.app_key'             => 'string|nullable|required_with:dropbox.enabled',
        'dropbox.app_secret'          => 'string|nullable|required_with:dropbox.enabled',
        'dropbox.access_token'        => 'string|nullable|required_with:dropbox.enabled',
        'dropbox.folder_path'         => 'nullable|regex:/^\/(.+)$/i',
                 #----------------------------
        'working_with'                => ['string', 'regex:/^(files|folders)$/i'],
      ]);

      Setting::first()->update(['files_host' => json_encode([
                "google_drive"  => $request->google_drive,
                "dropbox"       => $request->dropbox,
                "working_with"  => $request->working_with
              ])]);

      return redirect()->route('settings', 'files_host');
    }



    private static function update_social_login(Request $request)
    {
      $request->validate([
        'google.enabled'        => 'nullable|regex:/^(on)$/i',
        'google.client_id'      => 'string|nullable|required_with:google.enabled',
        'google.client_secret'  => 'string|nullable|required_with:google.enabled',
                #---------------------------
        'github.enabled'        => 'nullable|regex:/^(on)$/i',
        'github.client_id'      => 'string|nullable|required_with:github.enabled',
        'github.client_secret'  => 'string|nullable|required_with:github.enabled',
                #---------------------------
        'linkedin.enabled'        => 'nullable|regex:/^(on)$/i',
        'linkedin.client_id'      => 'string|nullable|required_with:linkedin.enabled',
        'linkedin.client_secret'  => 'string|nullable|required_with:linkedin.enabled',
                #---------------------------
        'facebook.enabled'        => 'nullable|regex:/^(on)$/i',
        'facebook.client_id'      => 'string|nullable|required_with:facebook.enabled',
        'facebook.client_secret'  => 'string|nullable|required_with:facebook.enabled',
                #---------------------------
        'vkontakte.enabled'       => 'nullable|regex:/^(on)$/i',
        'vkontakte.client_id'     => 'string|nullable|required_with:vkontakte.enabled',
        'vkontakte.client_secret' => 'string|nullable|required_with:vkontakte.enabled',
                #---------------------------
        'twitter.enabled'         => 'nullable|regex:/^(on)$/i',
        'twitter.client_id'       => 'string|nullable|required_with:twitter.enabled',
        'twitter.client_secret'   => 'string|nullable|required_with:twitter.enabled'
      ]);

      Setting::first()->update(['social_login' => json_encode([
                "google"    => array_merge($request->google, ['redirect' => env('GOOGLE_CALLBACK')]),
                "github"    => array_merge($request->github, ['redirect' => env('GITHUB_CALLBACK')]),
                "linkedin"  => array_merge($request->linkedin, ['redirect' => env('LINKEDIN_CALLBACK')]),
                "facebook"  => array_merge($request->facebook, ['redirect' => env('FACEBOOK_CALLBACK')]),
                "vkontakte" => array_merge($request->vkontakte, ['redirect' => env('VK_CALLBACK')]),
                "twitter"   => array_merge($request->twitter, ['redirect' => env('TWITTER_CALLBACK')]),
              ])]);

      return redirect()->route('settings', 'social_login');
    }



    public static function remove_search_cover()
    {
      foreach(glob(storage_path('app/public/images/search_cover.*')) ?? [] as $search_cover)
        @unlink($search_cover);

      $settings = Setting::first();

      $general_settings = json_decode($settings->general) ?? (object)[];
      
      $general_settings->search_cover =  '';

      $settings->general = json_encode($general_settings);
      
      $settings->save();

      return response()->json(['success' => true]);
    }
    
    
    
    public function google_drive_get_refresh_token(Request $request)
    {
        return \App\Libraries\GoogleDrive::code_to_access_token_async($request);
    }
    
    
    public function google_drive_get_current_user(Request $request)
    {
        return \App\Libraries\GoogleDrive::get_current_user($request);
    }
    
    
    public function dropbox_get_current_user(Request $request)
    {
        return \App\Libraries\DropBox::get_current_user($request);
    }
}
