<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Newsletter_Subscriber as Subscriber;
use Illuminate\Support\Facades\{Mail, Validator};


class SubscribersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $validator =  Validator::make($request->all(),
                    [
                      'orderby' => ['regex:/^(email|updated_at)$/i', 'required_with:order'],
                      'order' => ['regex:/^(asc|desc)$/i', 'required_with:orderby']
                    ]);

      if($validator->fails()) abort(404);

      $base_uri = [];

      if($request->orderby)
      {
        $base_uri = ['orderby' => $request->orderby, 'order' => $request->order];
      }

      $subscribers = Subscriber::useIndex($request->orderby ?? 'primary')
                                ->select('id', 'email', 'updated_at')
                                ->orderBy($request->orderby ?? 'id', $request->order ?? 'desc')->paginate(15);

      $items_order = $request->order === 'desc' ? 'asc' : 'desc';

      return View('back.newsletters.subscribers', ['title' => 'Newsletter Subscribers',
                                                   'subscribers' => $subscribers,
                                                   'items_order' => $items_order,
                                                   'base_uri' => $base_uri]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

      $subscribers = Subscriber::select('email')->get();
      $products = \App\Models\Product::useIndex('active')->select('id', 'name', 'slug', 'cover', 'price')->where('active', 1)->get();
      $posts = \App\Models\Post::useIndex('active')->select('id', 'name', 'slug', 'cover');


      return View('back.newsletters.create', ['title' => 'Create and send newsletter',
                                              'subscribers' => $subscribers,
                                              'products' => $products,
                                              'posts' => $posts]);
    }

    /**
     * Send a newsletter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function send(Request $request)
    {
      $request->validate([
        'newsletter' => 'required',
        'subject' => 'required'
      ]);

      $newsletter = new \App\Mail\Newsletter(['message' => $request->newsletter, 'md5_email' => null]);

      $newsletter->subject = $request->subject;

      if(isset($_POST['preview']))
      {
        return $newsletter->render();
      }

      if(!$emails = array_filter(explode(',', $request->emails)))
      {
        if($emails = Subscriber::select('email')->get())
          $emails = array_column($emails->toArray(), 'email');
      }
      else
      {
        foreach($emails as $key => &$email)
        {
          $email = trim($email);

          if(!filter_var($email, FILTER_VALIDATE_EMAIL))
            unset($emails[$key]);
        }
      }

      if(!$emails)
      {
        $validator = Validator::make($request->all(), [])
                      ->errors()->add('Emails', 'Invalid emails input');

        return redirect()->back()->withErrors($validator)->withInput();
      }

      Mail::to($emails)->send($newsletter);

      $request->session()->flash('newsletter_sent', 'newsletter sent successfully');

      return redirect()->route('subscribers.newsletter.create');
    }

    /**
     * Export ell resources.
     *
     * @return null
     */
    public function export()
    {
        if($emails = Subscriber::select('email')->get())
        {
          return  response()->streamDownload(function() use($emails)
                              {
                                echo implode(',',array_column($emails->toArray(), 'email'));
                              },'Newsletter_Subscribers_Emails.csv');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $ids
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $ids)
    {
        Subscriber::destroy(explode(',', $ids));

        return redirect()->route('subscribers');
    }
}
