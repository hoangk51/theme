<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{DB, Validator};
use App\Models\{Support_Email, Support};
use Webklex\IMAP\Client as ImapClient;


class SupportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $validator =  Validator::make($request->all(),
                    [
                      'orderby' => ['regex:/^(subject|email|read|created_at)$/i', 'required_with:order'],
                      'order'   => ['regex:/^(asc|desc)$/i', 'required_with:orderby']
                    ]);

      if($validator->fails()) abort(404);

      $base_uri = [];

      if($keywords = $request->keywords)
      {
        $base_uri = ['keywords' => $keywords];

        $support_messages = Support::useIndex('search', 'email_id')
                                    ->select('support.id', 'support.subject', 'support.message AS content', 
                                             'support.created_at', 'support.read', 'support_email.email AS email')
                                    ->leftJoin('support_email', 'support_email.id', '=', 'support.email_id')
                                    ->where('email', 'like', "%{$keywords}%")
                                    ->where('parent', '=', null)
                                    ->orWhere('subject', 'like', "%{$keywords}%")
                                    ->orWhere('support.message', 'like', "%{$keywords}%")
                                    ->orderBy('id', 'DESC');
      }
      else
      {
        if($request->orderby)
        {
          $base_uri = ['orderby' => $request->orderby, 'order' => $request->order];
        }

        $support_messages = Support::useIndex($request->orderby ?? 'primary')
                                    ->select('support.id', 'support.subject', 'support.message AS content', 'support.created_at', 
                                             'support.read', 'support_email.email AS email')
                                    ->leftJoin('support_email', 'support_email.id', '=', 'support.email_id')
                                    ->where('parent', '=', null)
                                    ->orderBy($request->orderby ?? 'id', $request->order ?? 'desc');
      }

      $support_messages = $support_messages->paginate(15);

      $items_order = $request->order === 'desc' ? 'asc' : 'desc';

      return View('back.support.index', ['title' => 'Support messages',
                                         'support_messages' => $support_messages,
                                         'items_order' => $items_order,
                                         'base_uri' => $base_uri]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(int $id)
    {
      $messages = Support::useIndex('parent', 'primary')
      ->select('support.id', 'support.subject', 'support.parent', 'support.message', 
               'support.created_at', 'support_email.email as email')
      ->leftJoin('support_email', 'support_email.id', '=', 'support.email_id')
      ->where('support.id', $id)
      ->orWhere('parent', $id)->get();

      $parent   = $messages->where('parent', '=', null)->first();
      $children = $messages->where('parent', '!=', null);

      Support::where('id', $id)->update(['read' => 1]);

      return view('back.support.create', compact('parent', 'children'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
        'message' => 'required|bail|string',
        'parent' => 'required|integer',
        'email' => 'required|email'
      ]);

      if(\App\Models\Newsletter::useIndex('email')->where('email', $request->email)->count())
        $md5_email = md5($request->email);

      $mail_support = new \App\Mail\Support(['message' => $request->message, 'md5_email' => $md5_email ?? null]);

      // return $mail_support->render();

      \Illuminate\Support\Facades\Mail::to($request->email)
                                        ->send($mail_support);

      Support::insert(['message'  => $request->message, 
                       'email_id' => $request->user()->id, 
                       'parent'   => $request->parent, 
                       'read'     => 1]);


      $request->session()->flash('response', 'Message sent');

      return redirect()->route('support.create', ['id' => $request->parent]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Support::destroy(explode(',', $request->ids));

        $to_route = $request->to_route ?? ['support'];

        return redirect()->route(...$to_route);
    }


    public function status(Request $request)
    {
      $res = DB::update("UPDATE support USE INDEX(primary) SET `read` = 1 WHERE id = ?", 
                        [(int)$request->id]);

      return response()->json(['success' => (bool)$res ?? false]);
    }




    public function load_unseen(Request $request)
    {
      if(!config('Imap.enabled'))
      {
        $request->session()->flash('unseen_messages', 'IMAP is not enabled/configured');
        return response()->json(['success' => true]);
      }

      if($_emails = Support_Email::select('id', 'email')->get())
      {
        $emails = $_emails->toArray();
      }

      require_once(app_path("htmlDomParser.php"));

      $new_messages = new \Illuminate\Support\Collection();

      foreach(config('Imap') as $k => $v)
      {
        if($k === 'port')
          config(["Imap.{$k}" => (int)$v]);

        if($k === 'validate_cert')
          config(["Imap.{$k}" => filter_var($v, FILTER_VALIDATE_BOOLEAN)]);
      }
      
      $oClient = new ImapClient(config('Imap'));

      $oClient->connect();

      $messages = $oClient->getFolder('INBOX')->getUnseenMessages();

      foreach($messages as $message)
      {
        $body = str_get_html($message->getHtmlBody());

        if($ltr = $body->find('div[dir="ltr"]'))
        {
          $new_messages->push((object)['email'   => $message->getFrom()[0]->mail, 
                                     'subject' => str_ireplace('re: ', '', $message->getSubject()),
                                     'message' => strip_tags(str_replace('<br>', "\n\r", $ltr[0]->outertext))]);
        }
        else
        {
          $new_messages->push((object)['email'   => $message->getFrom()[0]->mail, 
                                     'subject' => str_ireplace('re: ', '', $message->getSubject()),
                                     'message' => strip_tags($body->outertext)]);
        }
      }

      $emails_ids = [];

      if($_emails = Support_Email::select('id', 'email')->get())
      {
        $emails_ids = $_emails->toArray();
      }

      $emails = array_map('strtolower', array_column($emails_ids, 'email'));

      $filtered_messages =  $new_messages->reject(function($item) use($emails)
                            {
                              return !in_array(strtolower($item->email), $emails);
                            });

      if($filtered_messages->count())
      {
        $emails_ids = array_combine($emails, array_column($emails_ids, 'id'));

        $filtered_messages->map(function(&$item) use($emails_ids)
                            {
                              $item->email_id = $emails_ids[strtolower($item->email)];

                              unset($item->email);
                            });

        Support::insert(json_decode($filtered_messages->toJson(), true));

        $request->session()->flash('unseen_messages', 'New messages have been loaded');
      }
      else
      {
        $request->session()->flash('unseen_messages', 'Nothing found!');
      }

      return response()->json(['success' => true]);
    }

}
