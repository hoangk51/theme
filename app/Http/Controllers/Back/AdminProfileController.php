<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Hash;


class AdminProfileController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Request $request)
	{
		$user = User::find($request->user()->id);

		return view('back.profile', ['title' => 'Edit profile',
																 'user' => $user]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request)
	{		
		$id = $request->user()->id;

		$request->validate([
			'name' 			=> 'required|max:255|nullable',
			'email' 		=> "required|max:255|unique:users,email,{$id}",
			'password' 	=> 'nullable|max:255',
			'avatar' 		=> 'nullable|image|max:2048',
			'firstname' => 'nullable|max:255',
			'lastname' 	=> 'nullable|max:255'
		]);

		$user = User::find($id);

		$user->name = $request->name;
		$user->email = $request->email;
		$user->firstname = $request->firstname;
		$user->lastname = $request->lastname;


		if($request->file('avatar'))
		{
			$ext  	= $request->file('avatar')->extension();
	    $avatar = $request->file('avatar') 
	                    ->storeAs('public/avatars', "{$id}.{$ext}");

	    $user->avatar = str_ireplace('public/avatars/', '', $avatar);	
		}
		
		if($request->password)
		{
			$user->password = Hash::make($request->password);
		}

		$user->save();

		return redirect()->route('profile.edit');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
