<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{Post, Category};
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\{DB, Validator};


class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      if(!file_exists(public_path('posts.xml')))
        \App\Libraries\Sitemap::create('posts');

      $validator =  Validator::make($request->all(),
                    [
                      'orderby' => ['regex:/^(name|active|views|updated_at)$/i', 'required_with:order'],
                      'order' => ['regex:/^(asc|desc)$/i', 'required_with:orderby']
                    ]);

      if($validator->fails()) abort(404);

      $base_uri = [];

      if($keywords = $request->keywords)
      {
        $base_uri = ['keywords' => $keywords];

        $posts = Post::useIndex('search')
                      ->select('posts.id', 'posts.name', 'posts.slug', 'posts.updated_at', 'posts.active', 'posts.views')
                      ->where('posts.name', 'like', "%{$keywords}%")
                      ->orWhere('posts.slug', 'like', "%{$keywords}%")
                      ->orWhere('posts.short_description', 'like', "%{$keywords}%")
                      ->orWhere('posts.content', 'like', "%{$keywords}%")
                      ->orWhere('posts.tags', 'like', "%{$keywords}%")
                      ->orderBy('id', 'DESC');
      }
      else
      {
        if($request->orderby)
        {
          $base_uri = ['orderby' => $request->orderby, 'order' => $request->order];
        }

        $posts = Post::useIndex($request->orderby ?? 'primary')
                      ->select('posts.id', 'posts.name', 'posts.slug', 'posts.updated_at', 'posts.active', 'posts.views')
                      ->orderBy($request->orderby ?? 'id', $request->order ?? 'desc');
      }

      $posts = $posts->paginate(15);

      $items_order = $request->order === 'desc' ? 'asc' : 'desc';

      return View('back.posts.index', ['title' => 'posts',
                                       'posts' => $posts,
                                       'items_order' => $items_order,
                                       'base_uri' => $base_uri]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::useIndex('`for`')->select('id', 'name')->where('categories.for', 0)->get();

        return view('back.posts.create', ['title' => 'Create post', 'categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
          'name' => 'bail|required|max:255|unique:posts',
          'content' => 'required',
          'short_description' => 'required',
          'cover' => 'required|image'
        ]);

        $category_id = null;

        if($request->input('new_category'))
        {
          $category_id = $this->add_new_category($request);
        }
        else
        {
          $request->validate([
            'category' => ['numeric', 'required', 
                            function ($attribute, $value, $fail) 
                            {                              
                              if(!Category::where(['id' => $value, 'for' => 0])->exists())
                                  $fail($attribute.' does not exist.');
                            }
                          ]
          ]);

          $category_id = $request->input('category');
        }

        $post  = new Post;

        $post_id = Post::get_auto_increment();

        $post->name = $request->name;
        $post->slug = Str::slug($request->name, '-');
        $post->short_description = $request->short_description;
        $post->content = $request->content;
        $post->tags = $request->tags;
        $post->category = $category_id;

        $ext    = $request->file('cover')->extension();
        $cover  = $request->file('cover')
                          ->storeAs('public/posts', "{$post_id}.{$ext}");

        $post->cover = str_ireplace('public/posts/', '', $cover);

        $post->save();

        $sitemap_url_data = ['loc' => route('home.post', $post->slug), 'lastmod' => $post->updated_at];

        \App\Libraries\Sitemap::append($sitemap_url_data, 'posts');

        return redirect()->route('posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!$post = Post::find($id))
        abort(404);
        
        $categories = Category::useIndex('`for`')->select('id', 'name')->where('categories.for', 0)->get();

        return view('back.posts.edit', ['title' => "Edit post - {$post->name}",
                                        'post' => $post,
                                        'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $request->validate([
          'name' => ['bail', 'required', 'max:255', Rule::unique('posts')->ignore($id)],
          'content' => 'required',
          'short_description' => 'required'
        ]);

        $category_id = null;

        if($request->input('new_category'))
        {
          $category_id = $this->add_new_category($request);
        }
        else
        {
          $request->validate([
            'category' => ['numeric', 'required', 
                            function ($attribute, $value, $fail) 
                            {                              
                              if(!Category::where(['id' => $value, 'for' => 0])->exists())
                                  $fail($attribute.' does not exist.');
                            }
                          ]
          ]);

          $category_id = $request->input('category');
        }

        $post  = Post::find($id);

        $post->name = $request->name;
        $post->slug = Str::slug($request->name, '-');
        $post->category = $category_id;
        $post->short_description = $request->short_description;
        $post->content = $request->content;
        $post->tags = $request->tags;

        if($cover = $request->file('cover'))
        {
          $ext    = $cover->extension();
          $cover  = $cover->storeAs('public/posts', "{$id}.{$ext}");

          $post->cover = str_ireplace('public/posts/', '', $cover);
        }

        $post->updated_at = date('Y-m-d H:i:s');
        
        $post->save();

        $sitemap_url_data = ['loc' => route('home.post', $post->slug), 'lastmod' => $post->updated_at];

        \App\Libraries\Sitemap::update($sitemap_url_data, 'posts', $post->slug);

        return redirect()->route('posts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $ids
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $ids)
    {
      $ids   = explode(',', $ids);
      $slugs = Post::useIndex('primary')->selectRaw("CONCAT(?, '/', `slug`) slug", [route('home.post', '')])
                                        ->whereIn('id', $ids)->get();

      if($slugs)
      {
        \App\Libraries\Sitemap::delete('posts', array_column($slugs->toArray(), 'slug'));

        if(Post::destroy($ids))
        {
          foreach($ids as $id)
          {
            if(!$file = glob(storage_path("app/public/posts/{$id}.*"))[0] ?? null)
              continue;

            if(file_exists($file))
              unlink($file);
          }
        }
      }

      return redirect()->route('posts');
    }



    public function status(Request $request)
    {      
      $res = DB::update("UPDATE posts USE INDEX(primary) SET {$request->status} = IF({$request->status} = 1, 0, 1) WHERE id = ?", 
                      [$request->id]);

      if($request->status === 'active')
      {
        $post = Post::find($request->id);

        if(! $post->active)
        {
          \App\Libraries\Sitemap::delete('posts', $post->slug);
        }
        else
        {
          $sitemap_url_data = ['loc' => route('home.post', $post->slug), 'lastmod' => date('Y-m-d H:i:s')];
          
          \App\Libraries\Sitemap::append($sitemap_url_data, 'posts');
        }
      }

      return response()->json(['success' => (bool)$res ?? false]);
    }



    private function add_new_category($request)
    {
      $request->validate([
        'new_category' => ['required', 'max:255', 
                            function ($attribute, $value, $fail) 
                            {                              
                              if(Category::where(['name' => $value, 'for' => 0])->exists())
                                  $fail($attribute.' already exists.');
                            }
                          ]
      ]);

      $category = new Category;

      $category->name         = $request->input('new_category');
      $category->slug         = Str::slug($category->name, '-');
      $category->for          = 0;

      $category->save();

      return $category->id;
    }

}
