<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\{Transaction, Product};
use Illuminate\Support\Facades\Validator;
use Iyzipay;
use App\Libraries\{Stripe, PayPalCheckout, Skrill, Razorpay, IyzicoLib};


class TransactionsController extends Controller
{
    /**
     * Display a listing of transactions.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $validator =  Validator::make($request->all(),
                    [
                      'orderby' => ['regex:/^(product|buyer|amount|processor|refunded|refund|updated_at)$/i', 'required_with:order'],
                      'order' => ['regex:/^(asc|desc)$/i', 'required_with:orderby']
                    ]);

      if($validator->fails()) abort(404);

      $base_uri = [];

      if($keywords = $request->keywords)
      {
        $base_uri = ['keywords' => $keywords];

        $transactions = Transaction::useIndex('search')
                                    ->select('transactions.id', 'transactions.reference_id', 'transactions.transaction_id', 
                                      'transactions.cs_token', 'users.email as buyer', 'transactions.amount', 'transactions.refunded', 
                                      'transactions.refund', 'transactions.updated_at', 'transactions.processor')
                                    ->leftjoin('users use index(primary)', 'transactions.user_id', '=', 'users.id')
                                    ->where('users.email', 'like', "%{$keywords}%")
                                    ->orWhere('reference_id', 'like', "%{$keywords}%")
                                    ->orWhere('processor', 'like', "%{$keywords}%")
                                    ->orWhere('order_id', 'like', "%{$keywords}%")
                                    ->orWhere('transaction_id', 'like', "%{$keywords}%")
                                    ->orderBy('id', 'DESC');
      }
      else
      {
        if($request->orderby)
        {
          $base_uri = ['orderby' => $request->orderby, 'order' => $request->order];
        }

        $index = ($request->orderby === 'buyer') ? 'user_id' : ($request->orderby ?? 'primary');

        $transactions = Transaction::useIndex($index)
                                    ->select('transactions.id', 'transactions.reference_id', 'transactions.transaction_id', 
                                      'transactions.cs_token', 'users.email as buyer', 'transactions.amount', 'transactions.refunded', 
                                      'transactions.refund', 'transactions.updated_at', 'transactions.processor')
                                    ->leftjoin('users use index(primary)', 'transactions.user_id', '=', 'users.id')
                                    ->orderBy($request->orderby ?? 'id', $request->order ?? 'desc');
      }

      $transactions = $transactions->paginate(15);

      foreach($transactions as &$transaction)
      {
        if($transaction->processor === 'stripe')
        {
          $transaction->transaction_id = $transaction->cs_token;
        }
      }

      $items_order = $request->order === 'desc' ? 'asc' : 'desc';

      return View('back.transactions.index', ['title'         => 'Transactions',
                                              'transactions'  => $transactions,
                                              'items_order'   => $items_order,
                                              'base_uri'      => $base_uri]);
    }




    /**
     * Display the specified transaction details.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {      
      // Get transaction ID
      if(!$transaction = Transaction::where('transactions.id', '=', $id)->first())
        abort(404);

      $data = [
        'processor'   => $transaction->processor,
        'amount'      => $transaction->amount,
        'discount'    => $transaction->discount,
        'created_at'  => $transaction->created_at,
        'updated_at'  => $transaction->updated_at,
        'handling'    => 0,
        'fee'         => 0,
        'tax'         => 0
      ];

      switch($transaction->processor)
      {
        case 'paypal':
          if(!$details = json_decode((new PayPalCheckout)->order_details($transaction->order_id)))
            abort(404);

          break;
        
        case 'skrill':
          if(!$details = Skrill::transaction_details($transaction->transaction_id))
            abort(404);

          settype($details, 'object');

          $details->id = $details->transaction_id;

          break;
          
        case 'stripe':
          if(!$details = json_decode((new Stripe)->get_checkout_session($transaction->cs_token)))
            abort(404);

          $balance_transaction_token = $details->payment_intent->charges->data[0]->balance_transaction;

          if(!$balance_transaction = json_decode((new Stripe)->get_balance_transaction($balance_transaction_token)))
            abort(404);

          $items = ['names' => [], 'items' => []];

          foreach($details->display_items as $v)
          {
            if(!preg_match('/^(Fee|VAT)$/', $v->custom->name))
            {
              if(!in_array($v->custom->name, $items['names']))
              {
                $items['names'][] = $v->custom->name;
                $items['items'][] = $v;
              }
              else
              {
                $k = array_search($v->custom->name, $items['names']);

                $items['items'][$k]->quantity += 1;

                if($v->amount > $items['items'][$k]->amount)
                {
                  $items['items'][$k]->amount = $v->amount;
                }
              }
            }
          }

          unset($items['names']);

          $exchange_rate           = $balance_transaction->exchange_rate; /*USD to EUR*/
          $data['fee']             = round($balance_transaction->fee*(1/$exchange_rate) / 100, 2);
          $data['net_amount']      = round($balance_transaction->net*(1/$exchange_rate) / 100, 2);
          $data['items']           = $items; 
          $data['amount_refunded'] = $details->payment_intent->charges->data[0]->amount_refunded;
          $data['refunded']        = $details->payment_intent->charges->data[0]->refunded;
          $data['handling']        = find_one($details->display_items, ['custom', 'name'], 'Fee')->amount ?? 0;
          $data['tax']             = find_one($details->display_items, ['custom', 'name'], 'VAT')->amount ?? 0;
          $data['amount']          =  array_reduce($details->display_items, function($ac, $item)
                                      {
                                        return $ac += !preg_match('/^(Fee|VAT)$/', $item->custom->name)
                                                      ? $item->amount/100
                                                      : 0;
                                      }, 0);
          break;

        case 'razorpay':
          if(!$invoice = Razorpay::fetch_invoice($transaction->order_id))
            abort(404);

          if(!$payment = Razorpay::fetch_payment($transaction->transaction_id))
            abort(404);

          $details = (object)[
            'invoice' => $invoice,
            'payment' => $payment,
            'id'      => $payment->id
          ];

          $items = [];
          $data['amount'] = 0;

          foreach($invoice->line_items as $item)
          {
            if(preg_match('/^Handling Fee$/i', $item->name))
            {
              $data['handling'] = round($item->amount / 100, 2);
            }
            elseif(preg_match('/^VAT \d+(\.\d+)?%$/i', $item->name))
            {
              $data['tax'] = round($item->amount / 100, 2);
            }
            else
            {
              $data['amount'] += round($item->amount / 100, 2);

              $item->amount = round($item->amount / 100, 2);

              $items[] = $item;
            }
          }

          $data['fee']             = round($payment->fee / 100, 2);
          $data['items']           = $items;
          $data['amount_refunded'] = round($payment->amount_refunded / 100, 2);
          $data['refunded']        = $payment->refund_status;

          break;

        case 'iyzico':
          if(!$payment = IyzicoLib::getPaymentRequest($transaction->transaction_id))
            abort(404);

          $details  = (object)[
                        'payment' => $payment,
                        'id'      => $transaction->transaction_id
                      ];

          if($error = $payment->getErrorMessage())
            exists_or_abort(null, $error);

          $paymentItems = $payment->getPaymentItems();

          $items          = [];
          $data['amount'] = 0;
          $items_ids      = [];

          foreach($paymentItems as $paymentItem)
          {
            $paymentId = $paymentItem->getItemId();

            if($paymentId === 'FEE1')
            {
              $data['handling'] = $paymentItem->getPaidPrice();
            }
            elseif(preg_match('/^(VAT\d+)$/i', $paymentId))
            {
              $data['tax'] = $paymentItem->getPaidPrice();
            }
            else
            {
              $items_ids[$paymentId] = $paymentId;

              $data['amount'] += $paymentItem->getPaidPrice();

              $items[] = $paymentItem;
            }
          }

          $items_names = Product::useIndex('primary')->select('id', 'name')->whereIn('id', $items_ids)->get();

          if($items_names->count() !== count($items_ids))
          {
            exists_or_abort(null, __('ids count does not match names count. It is probably due to removal of a sold product.'));
          }

          $items_names = $items_names->toArray();

          $items_names = array_combine(array_column($items_names, 'id'), array_column($items_names, 'name'));

          foreach($items as &$item)
          {
            $item->name = $items_names[$item->getItemId()] ?? '';
          }

          $data['fee']             = number_format($payment->getIyziCommissionRateAmount() + $payment->getIyziCommissionFee(), 2);
          $data['items']           = $items;
          $data['amount_refunded'] = round($transaction->refund, 2);
          $data['net_amount']      = number_format($payment->getPaidPrice() - $data['fee'], 2);

          break;
      }


      $data['title']   = "Transaction - {$details->id}";
      $data['details'] = $details;

      return view('back.transactions.show', $data);
    }




    /**
     * Show the form for preparing the specified transaction
     * for the refund.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function refund(Request $request)
    {
      $validator =  Validator::make($request->all(),
                    [
                      'transaction_id' => 'required|bail',
                      'amount_to_refund' => 'numeric|nullable',
                      'processor' => ['required', 'regex:/^(paypal|stripe|razorpay)$/']
                    ]);

      if($validator->fails())
      {
        return response()->json(['response' => 'error', 'message' => $validator->errors()->all()], 404);
      }

      $trasaction_id = '';

      if(preg_match('/^paypal|razorpay$/i', $request->processor))
      {
        $trasaction_id = 'transactions.transaction_id';
      }
      elseif($request->processor === 'stripe')
      {
        $trasaction_id = 'transactions.cs_token';
      }

      $sql_cond = [$trasaction_id, '=', $request->transaction_id];

      $transaction = Transaction::where(...$sql_cond)->first();

      if($transaction->amount === $transaction->refund)
      {
        return response()->json(['response' => 'Error'], 404);
      }
      
      if($request->processor === 'paypal')
      {
        $payload = (float)$request->amount_to_refund
                   ?  ['amount' => [
                          'currency_code' => env('CURRENCY_CODE', 'USD'), 
                          'value' => $request->amount_to_refund
                        ]
                      ]
                    : [];

        if(!json_decode((new PayPalCheckout)->refund_order($request->transaction_id, $payload)))
        {
          return response()->json(['response' => 'Error'], 404);
        }
        
        $new_details = json_decode((new PayPalCheckout)->order_details($transaction->order_id));

        $refund = array_reduce($new_details->purchase_units[0]->payments->refunds, 
                                            function($ac, $cv)
                                            {
                                              return $ac + $cv->amount->value;
                                            }, 0);
      }
      elseif($request->processor === 'stripe')
      {
        if(!$details = json_decode((new Stripe)->get_checkout_session($request->transaction_id)))
            abort(404);

        $ch_id  = $details->payment_intent->charges->data[0]->id;
        $amount = (float)$request->amount_to_refund;

        if(!$res = json_decode((new Stripe)->refund_transaction($ch_id, $amount)))
          abort(404);

        if(property_exists($res, 'error'))
          return response()->json(['response' => 'error', 'message' => $res->error->message]);

        $refund = number_format($res->amount/100 + $transaction->refund, 2);
      }
      elseif($request->processor === 'razorpay')
      {
        Razorpay::fetch_payment($request->transaction_id);

        if(!$res = Razorpay::refund_payment($request->transaction_id, (float)$request->amount_to_refund))
          abort(404);

        if(property_exists($res, 'error'))
          return response()->json(['response' => 'error', 'message' => "{$res->error->code} : {$res->error->description}"]);

        $payment = Razorpay::fetch_payment($request->transaction_id);

        $refund = round($payment->amount_refunded/100);
      }

      $transaction->refunded = 1;
      $transaction->refund = $refund;
      $transaction->updated_at = date('Y-m-d H:i:s');

      $transaction->save();

      return response()->json(['response' => 'Done', 'refund' => $refund]);
    }



    public  function refund_iyzico(Request $request)
    {
      if(!$payment = IyzicoLib::getPaymentRequest($request->payment_id))
        abort(404);

      if($error = $payment->getErrorMessage())
        exists_or_abort(null, $error);


      if($request->isMethod('post'))
      {
        $items =  array_filter($request->post('item') ?? [], function($v, $k)
                  {
                    return $v;
                  }, ARRAY_FILTER_USE_BOTH);

        $refund_total = 0;

        foreach($items as $paymentTransactionId => $amount_to_refund)
        {
          $iyzipay_request = new \Iyzipay\Request\CreateRefundRequest();

          $iyzipay_request->setLocale(strtoupper(config('app.locale')));
          $iyzipay_request->setPaymentTransactionId($paymentTransactionId);
          $iyzipay_request->setPrice((double)$amount_to_refund);
          $iyzipay_request->setCurrency(config('payments.currency_code'));

          $refund = \Iyzipay\Model\Refund::create($iyzipay_request, IyzicoLib::getOptions());

          if(! $error = $refund->getErrorMessage())
          {
            $refund_total += $amount_to_refund;
          }
          else
          {
            Transaction::where('transaction_id', $request->payment_id)->increment('refund', $refund_total);
            exists_or_abort(null, $error);
          }
        }

        Transaction::where('transaction_id', $request->payment_id)->increment('refund', $refund_total);

        \Session::flash('refund_response', __('Done!'));

        return redirect()->route('transactions.refund_iyzico', $request->payment_id);
      }

      $paymentItems = $payment->getPaymentItems();
      $items_ids    = [];
      $items        = [];

      foreach($paymentItems as $paymentItem)
      {
        $paymentId = $paymentItem->getItemId();

        if(ctype_digit($paymentId))
        {
          $items_ids[$paymentId] = $paymentId;
          $items[] = $paymentItem;
        }
      }

      $items_names = Product::useIndex('primary')->select('id', 'name')->whereIn('id', $items_ids)->get();

      if($items_names->count() !== count($items_ids))
      {
        exists_or_abort(null, __('ids count does not match names count. It is probably due to removal of a sold product.'));
      }

      $items_names = $items_names->toArray();

      $items_names = array_combine(array_column($items_names, 'id'), array_column($items_names, 'name'));

      foreach($items as &$paymentItem)
      {
        $paymentItem->name = $items_names[$paymentItem->getItemId()] ?? '';
      }

      $title = __('Refund payment items ') . $request->payment_id;

      return view('back.transactions.iyzico_refund', compact('items', 'title'));
    }



    /**
     * Proceed to the refund of the specified transaction.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      
    }



    /**
     * Remove the specified transaction.
     *
     * @param  string  $ids
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $ids)
    {
      Transaction::destroy(explode(',', $ids));

      return redirect()->route('transactions');
    }
}
