<?php

namespace App\Http\Middleware;

use Closure;

class ValidateCheckoutSessions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(\Illuminate\Http\Request $request, Closure $next)
    {
      if(!$request->session()->has(['products_ids', 'payment_processor', 'cart']))
        abort(404);

      return $next($request);
    }
}
