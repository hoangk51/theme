<?php

	namespace App\Libraries;


	class PayPalCheckout 
	{

		public function __construct()
		{
			exists_or_abort(config('payments.paypal.enabled'), 'PayPal is not enabled');
            
            //dd(cache('paypal_access_token'));
			if(!cache('paypal_access_token'))
			{				
				\Illuminate\Support\Facades\Cache::put('paypal_access_token', $this->access_token(), now()->addMinutes(55));
			}
		}




		private function access_token()
		{
			$ch 		 = curl_init();
			$api_url = 'https://api.sandbox.paypal.com/v1/oauth2/token';

			if(config('payments.paypal.mode') === 'live')
				$api_url = 'https://api.paypal.com/v1/oauth2/token';

			$header = ['Accept: application/json',
								 'Accept-Language: en_US'];

			curl_setopt($ch, CURLOPT_URL, $api_url); 
			curl_setopt($ch, CURLOPT_USERPWD, config('payments.paypal.client_id') . ':' . config('payments.paypal.secret_id'));
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_POSTFIELDS, 'grant_type=client_credentials');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			$result = curl_exec($ch);
			
			if(curl_errno($ch))
			{
				$error_msg = curl_error($ch);

				curl_close($ch);

				exists_or_abort(null, $error_msg);
			}

			return json_decode($result)->access_token;
		}




		public function create_order(array $cart = [], 
																 array $discount)
		{
			exists_or_abort($cart, 'Order payload is missing');

			$ch 			= curl_init();
			$api_url 	= 'https://api.sandbox.paypal.com/v2/checkout/orders';

			if(config('payments.paypal.mode') === 'live')
				$api_url = 'https://api.paypal.com/v2/checkout/orders';

			$handling   = config('payments.paypal.fee');
			$item_total = number_format(array_reduce($cart, function($carry, $prod)
                    {                                
                      return $carry + ($prod->quantity * $prod->price);
                    }, 0), 2);

      $tax_total =  number_format(config('payments.vat')
                          ? (number_format(($item_total - $discount['value']) * config('payments.vat') / 100, 2))
                          : 0, 2);
      
      $amount = number_format($item_total + $handling + $tax_total - $discount['value'], 2);

			$payload = [
            'intent' => 'CAPTURE',
            'application_context' => [
            	'return_url' => route('home.checkout.save'),
            	'cancel_url' => route('home.checkout'),
            	'shipping_preference' => 'NO_SHIPPING'
            ],
            'purchase_units' => [
              [
                'reference_id' => auth()->user()->id.'_'.time().'_REF',
                //'description' => config('paypal.transaction_description') ?? '',
                'amount' => [
                  'currency_code' => config('payments.currency_code'),
                  'value' => $amount,
                  'breakdown' => [
                    'item_total' => [
                      'currency_code' => config('payments.currency_code'),
                      'value' => $item_total
                    ],
                    'handling' => [
                      'currency_code' => config('payments.currency_code'),
                      'value' => $handling
                    ],
                    'tax_total' => [
                      'currency_code' => config('payments.currency_code'),
                      'value' => $tax_total
                    ],
                    'discount' => [
                      'currency_code' => config('payments.currency_code'),
                      'value' => $discount['value']
                    ],
                  ]
                ],
                'items' =>  array_map(function($prod)
                            {
                              return [
                                        'name' => $prod->name,
                                        'description' => $prod->category,
                                        'unit_amount' => [
                                          'currency_code' => config('payments.currency_code'),
                                          'value' => $prod->price,
                                        ],
                                        'quantity' => $prod->quantity,
                                        'category' => 'DIGITAL_GOODS'
                                      ];
                            }, $cart)
              ]
            ]
        ];

      $headers = [
				'Content-Type: application/json',
				'Authorization: Bearer ' . cache('paypal_access_token'),
			];

			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));
			curl_setopt($ch, CURLOPT_URL, $api_url);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			$result = curl_exec($ch);

			curl_close($ch);
			
			return $result;
		}




		public function capture_order(string $order_id = NULL)
		{
			exists_or_abort($order_id, 'Order id is missing');

			$ch 		 = curl_init();
			$api_url = "https://api.sandbox.paypal.com/v1/checkout/orders/{$order_id}/capture";

			if(config('payments.paypal.mode') === 'live')
				$api_url = "https://api.paypal.com/v1/checkout/orders/{$order_id}/capture";

			$headers = [
				'Content-Type: application/json',
				'Authorization: Bearer ' . cache('paypal_access_token'),
			];

			curl_setopt($ch, CURLOPT_URL, $api_url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			$result = curl_exec($ch);

			curl_close($ch);
			
			return $result;
		}




		public function order_details(string $order_id = null)
		{
			exists_or_abort($order_id, 'Order id is missing');

			$ch 		 = curl_init();
			$api_url = "https://api.sandbox.paypal.com/v2/checkout/orders/{$order_id}";

			if(config('payments.paypal.mode') === 'live')
				$api_url = "https://api.paypal.com/v2/checkout/orders/{$order_id}";

			$headers = [
				'Content-Type: application/json',
				'Authorization: Bearer ' . cache('paypal_access_token'),
			];

			curl_setopt($ch, CURLOPT_URL, $api_url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HTTPGET, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			$result = curl_exec($ch);

			curl_close($ch);
			
			return $result;
		}




		public function refund_order(string $order_id = null, array $payload = [])
		{
			exists_or_abort($order_id, 'Order id is missing');

			$ch 		 	 = curl_init();
			$api_url   = "https://api.sandbox.paypal.com/v2/payments/captures/{$order_id}/refund";

			if(config('payments.paypal.mode') === 'live')
				$api_url = "https://api.paypal.com/v2/payments/captures/{$order_id}/refund";


			$headers = [
				'Content-Type: application/json',
				'Authorization: Bearer ' . cache('paypal_access_token'),
			];

			curl_setopt($ch, CURLOPT_URL, $api_url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			if($payload)
			{
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));
			}

			$result = curl_exec($ch);

			curl_close($ch);
			
			return $result;
		}
	}