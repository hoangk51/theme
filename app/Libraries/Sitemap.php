<?php

	namespace App\Libraries;

	use Illuminate\Support\Facades\DB;


	class Sitemap 
	{

		/**
		*	Add new url to a given Sitemap file
		*
		* @param array  $sitemap_url_data (assoc array with 'loc' and 'lastmod' keys)
		* loc corresponds to item's url, lastmod corresponds to item's updated at date (Y-m-d H:i:s)
		* @param string  $file_name, if null the $table_name will be used instead
		* @return void
		**/
		public static function append(array $sitemap_url_data = [], string $file_name)
		{
			extract($sitemap_url_data);

			if(!isset($loc, $lastmod))
				exists_or_abort(null, "sitemap_url_data array passed to Sitemap::update doesn't have 'loc' & 'lastmod' keys");

			$file = public_path("{$file_name}.xml");

			$sitemap_url = preg_replace("/(\n|\r|\t)+/", '', "<url><loc>{$loc}</loc><lastmod>{$lastmod}</lastmod></url>");

			if(file_exists($file))
      {
        $sitemap = file_get_contents($file);
        $sitemap = str_replace("</urlset>", "{$sitemap_url}</urlset>", $sitemap);
        $sitemap = preg_replace("/(\n|\r|\t)+/", "", $sitemap);

        file_put_contents($file, $sitemap);
      }
      else
      {
        $sitemap = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'.$sitemap_url.'</urlset>';

        file_put_contents($file, $sitemap);
      }
		}



		/**
		*	Update a given Sitemap file
		*
		* @param array  $sitemap_url_data - (assoc array with 'loc' and 'lastmod' keys)
		* loc corresponds to item's url, lastmod corresponds to item's updated at date (Y-m-d H:i:s)
		* @param string  $file_name - if null the $table_name will be used instead
		* @param string $item_url
		* @return void
		**/
		public static function update(array $sitemap_url_data = [], string $file_name, string $item_url)
		{
			extract($sitemap_url_data);

			if(!isset($loc, $lastmod))
				exists_or_abort(null, "sitemap_url_data array passed to Sitemap::update doesn't have 'loc' & 'lastmod' keys");

			$file = public_path("{$file_name}.xml");

			$sitemap_obj 	= simplexml_load_file($file);

			if($sitemap_obj)
      {
        $sitemap_arr 	= array_merge(...array_values((array)$sitemap_obj));
        $locs         = array_column($sitemap_arr, 'loc');
        $sitemap_arr	= array_combine($locs, $sitemap_arr);

      	$sitemap_arr[$item_url] = simplexml_load_string("<url><loc>{$loc}</loc><lastmod>{$lastmod}</lastmod></url>");

        $sitemap = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

        foreach($sitemap_arr as $url)
        {
          $sitemap .= "<url><loc>{$url->loc}</loc><lastmod>{$url->lastmod}</lastmod></url>";
        }

        $sitemap .= '</urlset>'; 

        $sitemap = preg_replace("/(\n|\r|\t)+/", "", $sitemap);

        file_put_contents($file, $sitemap);
      }
      else
      {
        file_put_contents($file, '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'.$sitemap_url.'</urlset>');
      }
		}



		/**
		*	Create Sitemap file
		*
		* @param string $table_name
		* @param string $file_name, if null the $table_name will be used instead
		* @return void
		**/
		public static function create(string $table_name, $file_name = null)
		{
			$file_name = $file_name ?? $table_name;

      if($sitemap_urls =  Self::get_urls($table_name))
      {
          $sitemap_urls = array_column($sitemap_urls, 'url');

          $xml = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'.implode('', $sitemap_urls).'</urlset>';

          $file = public_path("{$file_name}.xml");

          file_put_contents($file, $xml);
      }
		}




		/**
		*	Update a given Sitemap file
		*
		* @param array  $sitemap_url_data - (assoc array with 'loc' and 'lastmod' keys)
		* loc corresponds to item's url, lastmod corresponds to item's updated at date (Y-m-d H:i:s)
		* @param string  $file_name - if null the $table_name will be used instead
		* @param mixed array|string $item_url
		* @return void
		**/
		public static function delete(string $file_name, $item_url)
		{
			$file	= public_path("{$file_name}.xml");

			if(! file_exists($file)) return;

			$sitemap_obj 	= simplexml_load_file($file);
			$item_url 		= is_array($item_url) ? $item_url : [$item_url];

			foreach($item_url as &$url)
			{
				$url = route('home.'.rtrim($file_name, 's'), $url);
			}

			if($sitemap_obj)
      {
      	$sitemap_arr = json_decode(json_encode($sitemap_obj), true)['url'];

      	if(key_exists('loc', $sitemap_arr))
      	{
      		$sitemap_arr = [$sitemap_arr];
      	}

    		$locs	= array_column($sitemap_arr, 'loc');

				$sitemap_arr	= array_combine($locs, $sitemap_arr);

				$sitemap_arr  = array_diff_key($sitemap_arr, array_flip($item_url));				

        $sitemap = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

        foreach($sitemap_arr as $url)
        {
          $sitemap .= "<url><loc>{$url['loc']}</loc><lastmod>{$url['lastmod']}</lastmod></url>";
        }

        $sitemap .= '</urlset>'; 

        $sitemap = preg_replace("/(\n|\r|\t)+/", "", $sitemap);

        file_put_contents($file, $sitemap);
      }
		}




		/**
		*	Generate sitemap urls from database
		* Each table must have (slug, created_at, updated_at) columns
		* Each table must have (slug, active) indexes
		*
		* @param string $table_name
		* @return array	
		**/
		public static function get_urls(string $table_name)
    {
    	$base_url = '';

    	switch($table_name)
    	{
    		case 'products':
    			$base_url = url('/item').'/';
    			break;
    		case 'posts':
    			$base_url = url('/blog').'/';
    			break;
    		case 'pages':
    			$base_url = url('/page').'/';
    			break;
    		default:
    			exists_or_abort(null, 'Unsupported table name.');
    			break;
    	}

    	return DB::select("SELECT CONCAT('<url>', 
															      		CONCAT('<loc>', CONCAT(?, slug), '</loc>'),
															      		CONCAT('<lastmod>', IFNULL(updated_at, created_at), '</lastmod>'),
															      	'</url>') AS url 
												FROM $table_name USE INDEX(slug, active) WHERE active = 1", [$base_url]);
    }
	}