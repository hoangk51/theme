<?php

	namespace App\Libraries;


	class Skrill 
	{

		public function __construct()
		{

		}


		public static function checkout_session_id(array $cart = [], array $discount = [], $user_id = null)
		{
			$ch 		 = curl_init();
			$api_url = 'https://pay.skrill.com';

			$handling   = number_format(config('payments.skrill.fee'), 2);
			$item_total = number_format(array_reduce($cart, function($carry, $prod)
                    {                                
                      return $carry + ($prod->quantity * $prod->price);
                    }, 0), 2);

			$tax_total =  number_format(config('payments.vat')
                          ? (number_format(($item_total - $discount['value']) * config('payments.vat') / 100, 2))
                          : 0, 2);
      
			$amount = number_format($item_total + $handling + $tax_total - $discount['value'], 2);

			$header = ['Accept-Language: en_US'];

			$payload = [
			  "pay_to_email" => config('payments.skrill.merchant_account'),
			  "prepare_only" => 1,
			  "status_url" => route('home.checkout.save_skrill'),
			  "return_url" => route('home.checkout.save_skrill_complete'),
			  "return_url_text" => "Return",
			  "return_url_target" => "1",
			  "cancel_url" => route('home.checkout'),
			  "cancel_url_target" => "1",
			  "dynamic_descriptor" => "Descriptor",
			  "merchant_fields" => "user_id",
			  "user_id" => $user_id,
			  "language" => "EN",
			  "logo_url" => asset("storage/images/".config('app.logo')),
			  "amount" => number_format($amount, 2),
			  "currency" => config('payments.currency_code'),
			  "amount2_description" => "Gross Price : ",
			  "amount2" => number_format(($item_total - $discount['value']), 2),
			  "amount3_description" => "Handling Fees : ",
			  "amount3" => $handling,
			  "amount4_description" => "VAT (".config('payments.vat')."%) : ",
			  "amount4" => number_format($tax_total, 2),
			  "detail1_description" => "ID : ",
			  "detail1_text" => 'TR'.time(),
			  "submit_id" => "Submit",
			  "Pay" => "Pay",
			  "payment_methods" => "ACC"
			];

			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($ch, CURLOPT_URL, $api_url); 
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payload));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			$result = curl_exec($ch);
			
			if(curl_errno($ch))
			{
				$error_msg = curl_error($ch);

				curl_close($ch);

				exists_or_abort(null, $error_msg);
			}

			if(json_decode($result))
			{
				exists_or_abort(null, json_decode($result));
			}

			return $result;
		}


        
    public static function transaction_details(string $transaction_id)
    {
    	$transaction =  \DB::select("SELECT transactions.*, skrill_transactions.* FROM transactions USE INDEX(primary)
    							 								 LEFT JOIN skrill_transactions USE INDEX(transaction_id) ON skrill_transactions.transaction_id = transactions.transaction_id WHERE transactions.transaction_id = ?", [$transaction_id])[0] ?? [];

    	$transaction->items = [];

    	if($transaction)
    	{
    		$items = \DB::select("SELECT DISTINCT `name`, price FROM products USE INDEX(primary) WHERE ? REGEXP CONCAT('(\'', products.id, '\')')",
    													[$transaction->products_ids]);

    		$transaction->items = $items;
    	}

    	return $transaction;
    }
	}