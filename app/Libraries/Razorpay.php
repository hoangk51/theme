<?php

	namespace App\Libraries;

  use App\User;


	class Razorpay 
	{

		public function __construct()
		{

		}

    public static function create_payment_link(array $cart = [], array $discount = [], User $user)
    {
      /*
        DOC : https://razorpay.com/docs/payment-links/api/create/#sample-code
        ---------------------------------------------------------------------
        curl -u rzp_test_f8ONjSq19XZhLH:M6NbpABbYBmKz6noDAYOTTxZ \
        -X POST https://api.razorpay.com/v1/invoices/ \
        -H 'Content-type: application/json' \
        -d '{
          "customer": {
            "name": "Acme Enterprises",
            "email": "foxtinez@gmail.com"
          },
          "type": "link",
          "view_less": 1,
          "line_items": [
            {
              "name": "Book / English August",
              "description": "Generic - Dark Responsive MyBB Theme.",
              "unit_amount": 100000,
              "currency": "INR",
              "quantity": 2,
              "tax_rate": 20
            },
            {
              "name": "Book / English August",
              "description": "KingFire - Responsive MyBB Theme.",
              "unit_amount": 560000,
              "currency": "INR",
              "quantity": 3,
              "tax_rate": 20
            },
            {
              "name": "Fee",
              "description": "Handling Fee",
              "unit_amount": 100000,
              "currency": "INR",
              "quantity": 1
            }
          ],
          "description": "Item 1\r\nItem 2\r\nItem3",
          "currency": "INR",
          "sms_notify": 0,
          "email_notify": 0,
          "expire_by": 1793630556,
          "callback_url": "https://uma.co/checkout/save",
          "callback_method": "get"
        }'
      */

      exists_or_abort(config('payments.razorpay.enabled'), 'Razorpay is not enabled');

      $ch 		 = curl_init();
			$api_url = 'https://api.razorpay.com/v1/invoices';

      $key_id      = config('payments.razorpay.client_id');
      $key_secret  = config('payments.razorpay.secret_id');
      
      $item_total = round(array_reduce($cart, function($carry, $prod)
                    {                                
                      return $carry + ($prod->quantity * $prod->price);
                    }, 0), 2);

      $tax_total =  round(config('payments.vat')
                          ? ($item_total - $discount['value']) * (config('payments.vat') / 100)
                          : 0, 2);

      $total_quantity = array_reduce($cart, function($carry, $prod)
		                    {                                
		                      return $carry + $prod->quantity;
		                    }, 0);

      $line_items 		= [];
      $items_names 		= [];
      $_cart 					= [];

      foreach($cart as $prod)
      {
      	if($prod->quantity > 1)
      	{
      		for($i=0; $i<$prod->quantity; $i++)
  				{	
  					array_push($_cart, $prod);
  				}
      	}
      	else
      	{
      		array_push($_cart, $prod);
      	}
      }

      foreach($_cart as &$prod)
      {
      	$items_names[] = $prod->name;
      	$quantity = $prod->quantity;
      	$amount 	= $prod->price;
      	$item 		= [
                      'name' => $prod->name,
                      'description' => $prod->category,
                      'unit_amount' => round($amount*100),
                      'quantity' => 1,
                      'currency' => config('payments.currency_code')
		                ];

		    if($discount['value'])
		    {
		    	if(!isset($discount['product']))
		    	{
            $item['unit_amount'] = round(($amount - ($discount['value']/$total_quantity))*100);
		    	}
		    	else
		    	{
		    		if($prod->id == $discount['product'])
        		{
        			if($quantity > 1 && in_array($prod->name, $items_names))
        			{
        				if(isset($prod->coupon_applied))
        				{
                  $item['unit_amount'] = round($amount*100);
        				}
        				else
        				{
        					$prod->coupon_applied = true;
        					$item['unit_amount'] = round(($amount - $discount['value'])*100);
        				}
        			}
        			else
        			{
        				$item['unit_amount'] = round(($amount - $discount['value'])*100);
        			}
        		}
          }
        }
        
		    $line_items[] = $item;
      }

      if($handling_fee = config('payments.razorpay.fee'))
      {
        $line_items[] = [
          "name" => __('Handling Fee'),
          "description" => __('Handling Fee'),
          "unit_amount" => round($handling_fee*100),
          "currency" => config('payments.currency_code'),
          "quantity" => 1
        ];
      }

      if($tax_total)
		  {
		  	$line_items[] = [
		  		'name' => 'VAT '.config('payments.vat').'%',
		  		'description' => config('payments.vat').'%',
          "unit_amount" => round($tax_total*100),
          'currency' => config('payments.currency_code'),
          "quantity" => 1
		  	];
		  }
      
			$payload = [
			  "customer" => [
          "name" => $user->name,
          "email" => $user->email
        ],
        "type" => "link",
        "view_less" => 1,
        "line_items" => $line_items,
        "description" => '- '.implode("\r\n- ", array_column($line_items, 'name')),
        "currency" => config('payments.currency_code'),
        "sms_notify" => 0,
        "email_notify" => 0,
        "callback_url" => route('home.checkout.save'),
        "callback_method" => "get"
      ];
      
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_USERPWD, "{$key_id}:{$key_secret}");
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($ch, CURLOPT_URL, $api_url); 
			curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-type: application/json']);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			$result = curl_exec($ch);
			
			if(curl_errno($ch) || !json_decode($result))
			{
				$error_msg = curl_error($ch);

				curl_close($ch);

				exists_or_abort(null, $error_msg);
      }
      
      curl_close($ch);

      $result = json_decode($result);

      if(property_exists($result, 'error'))
      {
        exists_or_abort(null, $result->error->code.' : '.$result->error->description);
      }

			return urldecode($result->short_url);
    }



    public static function fetch_payment(string $razorpay_payment_id)
    {
      /*
        DOC : https://razorpay.com/docs/api/payments/#fetch-a-payment
        --------------------------------------------------------------
        curl -u rzp_test_f8ONjSq19XZhLH:M6NbpABbYBmKz6noDAYOTTxZ \
        -X GET https://api.razorpay.com/v1/payments/pay_ELaVWiGiOQMkOB
      */

      exists_or_abort(config('payments.razorpay.enabled'), 'Razorpay is not enabled');

      $ch 		 = curl_init();
			$api_url = "https://api.razorpay.com/v1/payments/{$razorpay_payment_id}";

      $key_id      = config('payments.razorpay.client_id');
      $key_secret  = config('payments.razorpay.secret_id');

      curl_setopt($ch, CURLOPT_USERPWD, "{$key_id}:{$key_secret}");
			curl_setopt($ch, CURLOPT_URL, $api_url); 
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			$result = curl_exec($ch);
			
			if(curl_errno($ch))
			{
				$error_msg = curl_error($ch);

				curl_close($ch);

				exists_or_abort(null, $error_msg);
      }
      
      curl_close($ch);

      $result = json_decode($result);

      if(property_exists($result, 'error'))
      {
        exists_or_abort(null, $result->error->code.' : '.$result->error->description);
      }

      return $result;
    }



    public static function fetch_invoice(string $razorpay_invoice_id)
    {
      /*
        DOC : https://razorpay.com/docs/api/invoices/#fetch-an-invoice
        --------------------------------------------------------------
        curl -u rzp_test_32hsbEKriO6ai4:SC6d7z4FcgX2wJj49obRRT4M \
        https://api.razorpay.com/v1/invoices/inv_gHQwerty123ggd
      */

      exists_or_abort(config('payments.razorpay.enabled'), 'Razorpay is not enabled');

      $ch 		 = curl_init();
			$api_url = "https://api.razorpay.com/v1/invoices/{$razorpay_invoice_id}";

      $key_id      = config('payments.razorpay.client_id');
      $key_secret  = config('payments.razorpay.secret_id');

      curl_setopt($ch, CURLOPT_USERPWD, "{$key_id}:{$key_secret}");
			curl_setopt($ch, CURLOPT_URL, $api_url); 
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			$result = curl_exec($ch);
			
			if(curl_errno($ch))
			{
				$error_msg = curl_error($ch);

				curl_close($ch);

				exists_or_abort(null, $error_msg);
      }
      
      curl_close($ch);

      $result = json_decode($result);

      if(property_exists($result, 'error'))
      {
        exists_or_abort(null, $result->error->code.' : '.$result->error->description);
      }

      return $result;
    }



    public static function refund_payment(string $payment_id, int $amount)
    {
      /*
        DOC : https://razorpay.com/docs/api/refunds/#refund-a-payment
        ---------------------------------------------------------------
        curl -u <YOUR_KEY_ID>:<YOUR_KEY_SECRET> \
          -X POST \
          https://api.razorpay.com/v1/payments/pay_29QQoUBi66xm2f/refund
          -H 'Content-Type: application/json' \
          -d '{
            "amount": 20000"
        }'
      */

      exists_or_abort(config('payments.razorpay.enabled'), 'Razorpay is not enabled');

      $ch 		 = curl_init();
			$api_url = "https://api.razorpay.com/v1/payments/{$payment_id}/refund";

      $key_id      = config('payments.razorpay.client_id');
      $key_secret  = config('payments.razorpay.secret_id');

      curl_setopt($ch, CURLOPT_USERPWD, "{$key_id}:{$key_secret}");
      curl_setopt($ch, CURLOPT_URL, $api_url);
      curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-type: application/json']);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');

      if($amount)
      {
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(['amount' => round($amount*100)]));
      }

			$result = curl_exec($ch);

			if(curl_errno($ch))
			{
				$error_msg = curl_error($ch);

				curl_close($ch);

				exists_or_abort(null, $error_msg);
      }

      curl_close($ch);

      return json_decode($result);
    }
	}