<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::middleware('app_installed')->group(function()
{
	Auth::routes(['verify' => true]);

	Route::get('/login/{provider}', 'Auth\LoginController@redirectToProvider')
	->where('provider', '^(github|facebook|google|twitter|linkedin|vkontakte)$');


	Route::get('/login/{provider}/callback', 'Auth\LoginController@handleProviderCallback')
	->where('provider', '^(github|facebook|google|twitter|linkedin|vkontakte)$');


	Route::get('/', 'HomeController@index')
	->name('home');

	Route::match(['get', 'post'], '/install', 'HomeController@install')
	->name('home.install_app');

	Route::get('/blog', 'HomeController@blog')
	->name('home.blog');

	Route::get('/blog/category/{category}', 'HomeController@blog')
	->name('home.blog.category');

	Route::get('/blog/tag/{tag}', 'HomeController@blog')
	->name('home.blog.tag');

	Route::get('/blog/search', 'HomeController@blog')
	->name('home.blog.q');

	Route::get('/blog/{slug}', 'HomeController@post')
	->name('home.post');

	Route::get('/page/{slug}', 'HomeController@page')
	->name('home.page');

	Route::get('/preview/{slug}', 'HomeController@preview')
	->name('home.preview');

	Route::get('/items/category/{category_slug}/{subcategory_slug?}', 'HomeController@products')
	->name('home.products.category');

	Route::get('/items/search', 'HomeController@products')
	->name('home.products.q');

	Route::get('/items/{selection}', 'HomeController@products')
	->name('home.products.selection')
	->where('selection', '^(free|featured|trending|newest)$');

	Route::get('/item/{slug}', 'HomeController@product')
	->name('home.product');

	Route::post('/item/product_folder', 'HomeController@product_folder_async')
	->name('home.product_folder_async')
	->middleware('valid_folders_config');;

	Route::match(['post', 'get'], '/support', 'HomeController@support')
	->name('home.support');

	Route::get('/favorites', 'HomeController@favorites')
	->name('home.favorites');

	Route::post('/newsletter', 'HomeController@subscribe_to_newsletter')
	->name('home.newsletter');

	Route::get('/unsubscribe/{md5_email}', 'HomeController@unsubscribe_from_newsletter')
	->name('home.unsubscribe');

	Route::post('/add_to_cart_async', 'HomeController@add_to_cart_async')
	->name('home.add_to_cart_async');

  Route::post('/checkout/save_skrill', 'CheckoutController@save_skrill')
	->name('home.checkout.save_skrill');

	
		
	Route::middleware('auth', 'verified')->group(function()
	{
		Route::get('/checkout', 'HomeController@checkout')
		->name('home.checkout');

		Route::post('/checkout/payment', 'CheckoutController@payment')
		->name('home.checkout.payment')
		->middleware('valid_checkout_request', 'valid_payment_method');

		Route::get('/checkout/save', 'CheckoutController@save')
		->name('home.checkout.save')
		->middleware('valid_checkout_sessions');

		Route::post('/checkout/validate_coupon', 'CheckoutController@validate_coupon')
		->name('home.checkout.validate_coupon');

		Route::get('/checkout/save_skrill_complete', 'CheckoutController@save_skrill_complete')
		->name('home.checkout.save_skrill_complete');

		Route::get('/checkout/iyzico_pre_chackout', 'CheckoutController@iyzico_pre_checkout')
		->name('home.checkout.iyzico_pre_chackout');

		Route::post('/checkout/iyzico_init', 'CheckoutController@iyzico_init')
		->name('home.checkout.iyzico_init');

		Route::get('/checkout/iyzico_cancel', 'CheckoutController@iyzico_cancel')
		->name('home.checkout.iyzico_cancel');

		Route::post('/checkout/iyzico_save', 'CheckoutController@iyzico_save')
		->name('home.checkout.iyzico_save');
		

		Route::match(['get', 'post'], '/profile', 'HomeController@profile')
		->name('home.profile')
		->middleware('is_not_admin');

		Route::get('/downloads', 'HomeController@downloads')
		->name('home.downloads')
		->middleware('is_not_admin');

		Route::get('/downloads/{slug}', 'HomeController@product_folder_sync')
		->name('home.product_folder_sync');

		Route::post('/downloads/{slug}/download', 'HomeController@product_folder_sync_download')
		->name('home.product_folder_sync_download');

		Route::post('/download', 'HomeController@download')
		->name('home.download');

		Route::post('/directory/{slug}', 'HomeController@list_folder')
		->name('home.list_folder')
		->middleware('valid_folders_config');

		Route::post('/downloads/dropbox_preview_url', 'HomeController@get_dropbox_preview_url')
		->name('home.downloads.dropbox_preview_url');

		Route::post('/item/{slug}', 'HomeController@product')
		->name('home.product');

		Route::get('/notifications', 'HomeController@notifications')
		->name('home.notifications');

		Route::post('/notifications/read', 'HomeController@notifications_read')
		->name('home.notifications.read');


		
		Route::middleware('is_admin')->group(function()
		{
			// Admin Dashboard
			Route::get('/admin', 'Back\DashboardController@index')
			->name('admin');

			Route::post('/admin/dashboard', 'Back\DashboardController@update_sales_chart')
			->name('admin.update_sales_chart');


			// Products
			Route::get('/admin/products', 'Back\ProductsController@index')
			->name('products');

			Route::get('/admin/products/create', 'Back\ProductsController@create')
			->name('products.create');

			Route::post('/admin/products/store', 'Back\ProductsController@store')
			->name('products.store');

			Route::get('/admin/products/edit/{id}', 'Back\ProductsController@edit')
			->name('products.edit');

			Route::post('/admin/products/update/{id}', 'Back\ProductsController@update')
			->name('products.update');

			Route::get('/admin/products/destroy/{ids}', 'Back\ProductsController@destroy')
			->name('products.destroy');

			Route::post('/admin/products/active', 'Back\ProductsController@active')
			->name('products.active');

			Route::post('/admin/products/status', 'Back\ProductsController@status')
			->name('products.status')
			->middleware('valid_item_status');

			Route::post('/admin/products/list_files', 'Back\ProductsController@list_files')
			->name('products.list_files')
			->middleware('valid_files_host');

			Route::post('/admin/products/list_folders', 'Back\ProductsController@list_folders')
			->name('products.list_folders')
			->middleware('valid_files_host');

			Route::post('/admin/products/api', "Back\ProductsController@newsletter")
			->name('products.api');



			// Pages
			Route::get('/admin/pages', 'Back\PagesController@index')
			->name('pages');

			Route::get('/admin/pages/create', 'Back\PagesController@create')
			->name('pages.create');

			Route::post('/admin/pages/store', 'Back\PagesController@store')
			->name('pages.store');

			Route::get('/admin/pages/edit/{id}', 'Back\PagesController@edit')
			->name('pages.edit');

			Route::post('/admin/pages/update/{id}', 'Back\PagesController@update')
			->name('pages.update');

			Route::get('/admin/pages/destroy/{ids}', 'Back\PagesController@destroy')
			->name('pages.destroy');

			Route::post('/admin/pages/active', 'Back\PagesController@status')
			->name('pages.status');



			// Support
			Route::get('/admin/support', 'Back\SupportController@index')
			->name('support');

			Route::get('/admin/support/reply/{id}', 'Back\SupportController@create')
			->name('support.create');

			Route::post('/admin/support/store', 'Back\SupportController@store')
			->name('support.store');

			Route::get('/admin/support/destroy/{ids}', 'Back\SupportController@destroy')
			->name('support.destroy');

			Route::post('/admin/support/read', 'Back\SupportController@status')
			->name('support.status');

			Route::post('/admin/support/load_unseen', 'Back\SupportController@load_unseen')
			->name('support.load_unseen');



	    // Newsletter
			Route::get('/admin/subscribers', 'Back\SubscribersController@index')
			->name('subscribers');

			Route::get('/admin/subscribers/create_newsletter', 'Back\SubscribersController@create')
			->name('subscribers.newsletter.create');

			Route::post('/admin/subscribers/send_newsletter', 'Back\SubscribersController@send')
			->name('subscribers.newsletter.send');

			Route::get('/admin/subscribers/destroy/{ids}', 'Back\SubscribersController@destroy')
			->name('subscribers.destroy');

			Route::post('/admin/subscribers/export', 'Back\SubscribersController@export')
			->name('subscribers.export');

			

			// Posts
			Route::get('/admin/posts', 'Back\PostsController@index')
			->name('posts');

			Route::get('/admin/posts/create', 'Back\PostsController@create')
			->name('posts.create');

			Route::post('/admin/posts/store', 'Back\PostsController@store')
			->name('posts.store');

			Route::get('/admin/posts/edit/{id}', 'Back\PostsController@edit')
			->name('posts.edit');

			Route::post('/admin/posts/update/{id}', 'Back\PostsController@update')
			->name('posts.update');

			Route::get('/admin/posts/destroy/{ids}', 'Back\PostsController@destroy')
			->name('posts.destroy');

			Route::post('/admin/posts/active', 'Back\PostsController@status')
			->name('posts.status');





			// Categories
			Route::get('/admin/categories/{for?}', 'Back\CategoriesController@index')
			->name('categories')
			->where('for', '^(posts|products)$');

			Route::get('/admin/categories/create', 'Back\CategoriesController@create')
			->name('categories.create');

			Route::post('/admin/categories/store', 'Back\CategoriesController@store')
			->name('categories.store');

			Route::get('/admin/categories/edit/{id}/{for?}', 'Back\CategoriesController@edit')
			->name('categories.edit')
			->where('for', '^(posts|products)$');

			Route::post('/admin/categories/update/{id}/{for?}', 'Back\CategoriesController@update')
			->name('categories.update')
			->where('for', '^(posts|products)$');

			Route::get('/admin/categories/destroy/{ids}/{for?}', 'Back\CategoriesController@destroy')
			->name('categories.destroy')
			->where('for', '^(posts|products)$');



			// Coupons
			Route::get('/admin/coupons', 'Back\CouponsController@index')
			->name('coupons');

			Route::get('/admin/coupons/create', 'Back\CouponsController@create')
			->name('coupons.create');

			Route::post('/admin/coupons/store', 'Back\CouponsController@store')
			->name('coupons.store');

			Route::get('/admin/coupons/edit/{id}', 'Back\CouponsController@edit')
			->name('coupons.edit');

			Route::post('/admin/coupons/update/{id}', 'Back\CouponsController@update')
			->name('coupons.update');

			Route::get('/admin/coupons/destroy/{ids}', 'Back\CouponsController@destroy')
			->name('coupons.destroy');

			Route::post('/admin/coupons/generate', 'Back\CouponsController@generate')
			->name('coupons.generate');



			// Users
			Route::get('/admin/users', 'Back\UsersController@index')
			->name('users');

			Route::get('/admin/users/destroy/{ids}', 'Back\UsersController@destroy')
			->name('users.destroy');



			// Faq
			Route::get('/admin/faq', 'Back\FaqController@index')
			->name('faq');

			Route::get('/admin/faq/create', 'Back\FaqController@create')
			->name('faq.create');

			Route::post('/admin/faq/store', 'Back\FaqController@store')
			->name('faq.store');

			Route::get('/admin/faq/edit/{id}', 'Back\FaqController@edit')
			->name('faq.edit');

			Route::post('/admin/faq/update/{id}', 'Back\FaqController@update')
			->name('faq.update');

			Route::get('/admin/faq/destroy/{ids}', 'Back\FaqController@destroy')
			->name('faq.destroy');

			Route::post('/admin/faq/active', 'Back\FaqController@status')
			->name('faq.status');



			// Admin Profile
			Route::get('/admin/profile', 'Back\AdminProfileController@edit')
			->name('profile.edit');

			Route::post('/admin/profile/update', 'Back\AdminProfileController@update')
			->name('profile.update');




			// Transactions
			Route::get('/admin/transactions', 'Back\TransactionsController@index')
			->name('transactions');

			# Show transaction details
			Route::get('/admin/transactions/show/{id}', 'Back\TransactionsController@show')
			->name('transactions.show');

			# Refund transaction
			Route::post('/admin/transactions/refund', 'Back\TransactionsController@refund')
			->name('transactions.refund');

			# Refund Iyzico Transaction
			Route::any('/admin/transactions/refund/iyzico/{payment_id}', 'Back\TransactionsController@refund_iyzico')
			->name('transactions.refund_iyzico');

			
			# Remove transaction
			Route::get('/admin/transactions/destroy/{ids}', 'Back\TransactionsController@destroy')
			->name('transactions.destroy');





			// Comments
			Route::get('/admin/comments', 'Back\CommentsController@index')
			->name('comments');

			Route::post('/admin/comments/approve', 'Back\CommentsController@status')
			->name('comments.status');

			Route::get('/admin/comments//destroy/{ids}', 'Back\CommentsController@destroy')
			->name('comments.destroy');



			// Reviews
			Route::get('/admin/reviews', 'Back\ReviewsController@index')
			->name('reviews');

			Route::post('/admin/reviews/approve', 'Back\ReviewsController@status')
			->name('reviews.status');

			Route::get('/admin/reviews/destroy/{ids}', 'Back\ReviewsController@destroy')
			->name('reviews.destroy');



			// Settings
			Route::get('/admin/settings/{settings_name}', "Back\SettingsController@index")
			->where('settings_name', '^(general|payments|adverts|search_engines|mailer|files_host|social_login)$')
			->name('settings');


			Route::post('/admin/settings/{settings_name}/update', "Back\SettingsController@update")
			->where('settings_name', '^(general|payments|adverts|search_engines|mailer|files_host|social_login)$')
			->name('settings.update');

			Route::post('/admin/settings/remove_search_cover', "Back\SettingsController@remove_search_cover")
			->name('settings.remove_search_cover');

			Route::post('/admin/settings/files_host/google_drive_get_refresh_token', "Back\SettingsController@google_drive_get_refresh_token")
			->name('google_drive_get_refresh_token');

			Route::post('/admin/settings/files_host/google_drive_get_current_user', "Back\SettingsController@google_drive_get_current_user")
			->name('google_drive_get_current_user');

			Route::post('/admin/settings/files_host/dropbox_get_current_user', "Back\SettingsController@dropbox_get_current_user")
			->name('dropbox_get_current_user');

		});

	});
});


Route::get('/admin_login/{token}', "Back\DashboardController@admin_login")
->name('admin_login');