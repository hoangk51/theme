<?php

namespace Migrations;

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class CreateFaqsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public static function up()
    {
        DB::statement("CREATE TABLE IF NOT EXISTS `faqs` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `question` text COLLATE utf8_unicode_ci NOT NULL,
          `answer` longtext COLLATE utf8_unicode_ci NOT NULL,
          `active` tinyint(1) DEFAULT '1',
          `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `updated_at` timestamp NULL DEFAULT NULL,
          `deleted_at` timestamp NULL DEFAULT NULL,
          PRIMARY KEY (`id`),
          KEY `updated_at` (`updated_at`),
          KEY `active` (`active`)
          ) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("drop table if exists faqs");
    }
}
