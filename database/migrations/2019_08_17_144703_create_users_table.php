<?php

namespace Migrations;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;


class CreateUsersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public static function up()
  {
      DB::statement("
        CREATE TABLE IF NOT EXISTS `users` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
          `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
          `email_verified_at` date DEFAULT NULL,
          `firstname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
          `lastname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
          `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
          `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
          `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user',
          `remember_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
          `provider_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
          `provider` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
          `updated_at` timestamp NULL DEFAULT NULL,
          `deleted_at` timestamp NULL DEFAULT NULL,
          `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
          `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
          PRIMARY KEY (`id`),
          UNIQUE KEY `email` (`email`),
          UNIQUE KEY `user` (`name`,`email`),
          KEY `name` (`name`),
          KEY `updated_at` (`updated_at`),
          KEY `verified` (`email_verified_at`),
          KEY `role` (`role`),
          KEY `created_at` (`created_at`)
        ) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
      ");
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      DB::statement("drop table if exists users");
  }
}
