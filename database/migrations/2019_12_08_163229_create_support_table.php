<?php

namespace Migrations;

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class CreateSupportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public static function up()
    {
        DB::statement("CREATE TABLE IF NOT EXISTS `support` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `email_id` int(11) NOT NULL,
          `subject` text COLLATE utf8_unicode_ci,
          `message` text COLLATE utf8_unicode_ci NOT NULL,
          `read` tinyint(1) DEFAULT '0',
          `parent` tinyint(4) DEFAULT NULL,
          `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `updated_at` timestamp NULL DEFAULT NULL,
          `deleted_at` timestamp NULL DEFAULT NULL,
          PRIMARY KEY (`id`),
          KEY `email_id` (`email_id`),
          KEY `updated_at` (`updated_at`),
          KEY `read` (`read`),
          KEY `parent` (`parent`),
          FULLTEXT KEY `search` (`subject`,`message`)
        ) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP TABLE IF EXISTS support');
    }
}
