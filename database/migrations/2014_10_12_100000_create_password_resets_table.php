<?php

namespace Migrations;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePasswordResetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public static function up()
    {
      DB::statement("
        CREATE TABLE IF NOT EXISTS `password_resets` (
          `email` varchar(255) not null,
          `token` varchar(255) not null,
          `created_at` timestamp default current_timestamp not null,
          KEY `password_resets_email_index` (`email`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
      ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement("DROP TABLE IF EXISTS `password_resets`");
    }
}
