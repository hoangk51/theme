<?php

namespace Migrations;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public static function up()
    {
        DB::statement("
          CREATE TABLE IF NOT EXISTS `pages` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
            `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
            `short_description` text COLLATE utf8_unicode_ci,
            `content` longtext COLLATE utf8_unicode_ci NOT NULL,
            `tags` text COLLATE utf8_unicode_ci,
            `views` int(11) DEFAULT '0',
            `deletable` tinyint(1) DEFAULT '1',
            `active` tinyint(1) DEFAULT '1',
            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `updated_at` timestamp NULL DEFAULT NULL,
            `deleted_at` timestamp NULL DEFAULT NULL,
            PRIMARY KEY (`id`),
            UNIQUE KEY `name` (`name`),
            UNIQUE KEY `slug` (`slug`),
            KEY `updated_at` (`updated_at`),
            KEY `active` (`active`),
            KEY `views` (`views`),
            KEY `deletable` (`deletable`),
            FULLTEXT KEY `description` (`name`,`slug`,`short_description`,`content`,`tags`)
          ) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
        ");
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement("drop table if exists pages");
    }
}
