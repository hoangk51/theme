<?php

namespace Migrations;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public static function up()
    {
        DB::statement("
          CREATE TABLE IF NOT EXISTS `categories` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
            `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
            `description` text COLLATE utf8_unicode_ci,
            `parent` int(11) DEFAULT NULL,
            `range` int(11) DEFAULT '1',
            `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
            `updated_at` timestamp NULL DEFAULT NULL,
            `deleted_at` timestamp NULL DEFAULT NULL,
            `for` tinyint(1) DEFAULT '1' COMMENT '0 for posts / 1 for products',
            PRIMARY KEY (`id`),
            UNIQUE KEY `name` (`name`,`parent`,`slug`,`for`),
            KEY `range` (`range`),
            KEY `for` (`for`),
            KEY `parent` (`parent`),
            KEY `slug` (`slug`),
            FULLTEXT KEY `description` (`description`)
          ) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement("DROP TABLE IF EXISTS categories");
    }
}
