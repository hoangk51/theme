<?php

namespace Migrations;

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InitPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public static function up()
    {
        \DB::insert("INSERT IGNORE INTO pages (name, slug, content) VALUES ('Terms and conditions', 'terms-and-conditions', '-'),
                    ('Privacy policy', 'privacy-policy', '-')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public static function down()
    {
        \DB::delete("DELETE FROM pages WHERE slug IN ('terms-and-conditions', 'privacy-policy')");
    }
}
