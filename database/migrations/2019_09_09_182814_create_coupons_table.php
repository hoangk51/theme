<?php

namespace Migrations;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public static function up()
    {
        DB::statement("
          CREATE TABLE IF NOT EXISTS `coupons` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
            `value` float NOT NULL,
            `is_percentage` tinyint(1) DEFAULT '0',
            `product_id` int(11) DEFAULT NULL,
            `users_ids` text COLLATE utf8_unicode_ci,
            `starts_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `expires_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `used_by` text COLLATE utf8_unicode_ci,
            `apply` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'globally',
            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
            `deleted_at` timestamp NULL DEFAULT NULL,
            PRIMARY KEY (`id`),
            UNIQUE KEY `id` (`id`),
            UNIQUE KEY `code` (`code`),
            KEY `value` (`value`),
            KEY `product_id` (`product_id`),
            KEY `starts_at` (`starts_at`),
            KEY `expires_at` (`expires_at`),
            KEY `apply` (`apply`),
            KEY `updated_at` (`updated_at`),
            KEY `deleted_at` (`deleted_at`),
            FULLTEXT KEY `users_ids` (`users_ids`),
            FULLTEXT KEY `used_by` (`used_by`)
          ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
      ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP TABLE IF EXISTS coupons");
    }
}
