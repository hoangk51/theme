<?php

namespace Migrations;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;


class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public static function up()
    {
        DB::statement("
          CREATE TABLE IF NOT EXISTS `transactions` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `processor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
            `products_ids` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
            `user_id` int(11) NOT NULL,
            `coupon_id` int(11) DEFAULT NULL,
            `reference_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
            `order_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
            `transaction_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
            `cs_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
            `amount` float NOT NULL,
            `items_count` int(11) not null,
            `discount` float DEFAULT NULL,
            `refunded` tinyint(1) DEFAULT '0',
            `refund` float DEFAULT '0',
            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `updated_at` timestamp NULL DEFAULT NULL,
            `deleted_at` timestamp NULL DEFAULT NULL,
            PRIMARY KEY (`id`),
            KEY `products_ids` (`products_ids`),
            KEY `coupon_id` (`coupon_id`),
            KEY `refunded` (`refunded`),
            KEY `refund` (`refund`),
            KEY `processor` (`processor`),
            KEY `created_at` (`created_at`),
            KEY `updated_at` (`updated_at`),
            KEY `deleted_at` (`deleted_at`),
            KEY `amount` (`amount`),
            KEY `user_id` (`user_id`),
            KEY `items_count` (`items_count`),
            KEY `search` (`processor`,`order_id`,`transaction_id`,`amount`,`reference_id`)
          ) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP TABLE IF EXISTS transactions");
    }
}
