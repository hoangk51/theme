<?php

namespace Migrations;

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public static function up()
    {
        DB::statement("CREATE TABLE IF NOT EXISTS `posts` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
          `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
          `short_description` text COLLATE utf8_unicode_ci NOT NULL,
          `content` longtext COLLATE utf8_unicode_ci NOT NULL,
          `cover` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
          `tags` text COLLATE utf8_unicode_ci,
          `active` tinyint(1) DEFAULT '1',
          `views` int(11) DEFAULT '0',
          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
          `deleted_at` timestamp NULL DEFAULT NULL,
          `category` int(11) NOT NULL,
          PRIMARY KEY (`id`),
          UNIQUE KEY `name` (`id`,`name`,`slug`),
          KEY `updated_at` (`updated_at`),
          KEY `active` (`active`),
          KEY `views` (`views`),
          KEY `slug` (`slug`),
          KEY `category` (`category`),
          FULLTEXT KEY `search` (`name`,`short_description`,`content`,`tags`),
          FULLTEXT KEY `tags` (`tags`)
        ) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP TABLE IF EXISTS posts");
    }
}
