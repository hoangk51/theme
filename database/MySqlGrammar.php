<?php

namespace App\Database\Query\Grammars;

use \Illuminate\Database\Query\Grammars\MySqlGrammar as BaseMySqlGrammar;
/**
 * Class MySqlGrammar
 * @package App\Database\Query\Grammar
 */
class MySqlGrammar extends BaseMySqlGrammar
{
    /**
     * Wrap a table in keyword identifiers.
     *
     * @param  \Illuminate\Database\Query\Expression|string  $table
     * @return string
     */
    public function wrapTable($table)
    {        
        return $table;
    }
}