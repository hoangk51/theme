<tr>
	<td class="footer-nav-1" align="center">
		<nav>
			<a href="{{ config('app.twitter') }}" target="_blank">
				<img src="https://drive.google.com/uc?export=view&id=1tLj4eXeTNH5a1F1Rej8fWWnFEKfQDGAC" alt="twitter">
			</a>
			<a href="{{ config('app.facebook') }}" target="_blank">
				<img src="https://drive.google.com/uc?export=view&id=1b95pRaIA48ep-iDATmDvL-7KUcKM2M-N" alt="facebook">
			</a>
			<a href="{{ config('app.pinterest') }}" target="_blank">
				<img src="https://drive.google.com/uc?export=view&id=1HKtG9UnotKMxyf2NGzUItBXWljlDr3Rq" alt="pinterest">
			</a>
			<a href="{{ config('app.tumblr') }}" target="_blank">
				<img src="https://drive.google.com/uc?export=view&id=19cPF7Jdn5j-79i3vnX4QgEKt3YaD2mXB" alt="tumblr">
			</a>
			<a href="{{ config('app.youtube') }}" target="_blank">
				<img src="https://drive.google.com/uc?export=view&id=1uqaUgi00liT-fNnLF1i3Ej52_qharYhm" alt="youtube">
			</a>
		</nav>
	</td>
</tr>
<tr>
	<td class="footer-nav-2" align="center">
		<nav>
			<a href="{{ $url }}" target="_blank">Home</a>
			<a href="{{ "{$url}/blog" }}">Blog</a>
			<a href="{{ "{$url}/login" }}" target="_blank">Account</a>
			<a href="{{ "{$url}/support" }}" target="_blank">Support</a>
			@if(isset($md5_email))
			<a href="{{ "{$url}/unsubscribe/{$md5_email}" }}" target="_blank">Unsubscribe</a>
			@endif
		</nav>
	</td>
</tr>
<tr class="footer">
  <td class="content-cell" align="center">
      {{ Illuminate\Mail\Markdown::parse($slot) }}
  </td>
</tr>
