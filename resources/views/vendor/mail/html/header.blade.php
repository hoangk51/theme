<tr>
    <td class="header">
        <a href="{{ $url }}">
            {{ $slot }}
        </a>
    </td>
</tr>
<tr>
	<td class="header-nav" align="center">
		<nav>
			<a href="{{ "{$url}/items/featured" }}">Featured</a>
			<a href="{{ "{$url}/items/trending" }}">Trending</a>
			<a href="{{ "{$url}/items/free" }}">Free</a>
			<a href="{{ "{$url}/items/newest" }}">New</a>
			<a href="{{ "{$url}/blog" }}">Blog</a>
		</nav>
	</td>
</tr>
