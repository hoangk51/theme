@extends('auth.master')

@section('additional_head_tags')
<title>{{ __('Login') }}</title>
@include('front.default.partials.meta_data')
@endsection

@section('title', 'Login to your account')

@section('content')
<div class="content">
  @if(array_filter(array_column(config('services'), 'enabled')))
  <div class="ui floating dropdown right labeled icon fluid basic button">
    <div class="text">{{ __('With your social account') }}</div>
    <i class="dropdown icon"></i>
    <div class="menu">
      @if(config('services.facebook.enabled'))
      <a href="{{ secure_url('login/facebook') }}" class="item">
        <i class="facebook icon"></i>
        Facebook
      </a>
      @endif

      @if(config('services.google.enabled'))
      <a href="{{ secure_url('login/google') }}" class="item">
        <i class="google icon"></i>
        Google
      </a>
      @endif
      
      @if(config('services.github.enabled'))
      <a href="{{ secure_url('login/github') }}" class="item">
        <i class="github icon"></i>
        Github
      </a>
      @endif

      @if(config('services.twitter.enabled'))
      <a href="{{ secure_url('login/twitter') }}" class="item">
        <i class="twitter icon"></i>
        Twitter
      </a>
      @endif

      @if(config('services.linkedin.enabled'))
      <a href="{{ secure_url('login/linkedin') }}" class="item">
        <i class="linkedin icon"></i>
        Linkedin
      </a>
      @endif

      @if(config('services.vkontakte.enabled'))
      <a href="{{ secure_url('login/vkontakte') }}" class="item">
        <i class="vk icon"></i>
        Vkontakte (VK)
      </a>
      @endif
    </div>
  </div>

  <div class="ui horizontal divider">Or</div>
  @endif
  
  <form class="ui form" action="{{ route('login', ['redirect' => request()->redirect ?? '/']) }}" method="post">
    @csrf 

    <div class="field">
      <label>Email</label>
      <input type="email" placeholder="..." name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

      @error('email')
        <div class="ui negative message">
          <strong>{{ $message }}</strong>
        </div>
      @enderror
    </div>

    <div class="field">
      <label>Password</label>
      <input type="password" placeholder="..." name="password" required autocomplete="current-password">

      @error('password')
        <div class="ui negative message">
          <strong>{{ $message }}</strong>
        </div>
      @enderror
    </div>

    <div class="field">
      <div class="ui checkbox">
        <input type="checkbox" name="remember">
        <label>Remember me</label>
      </div>
    </div>

    <div class="field mb-0">
      <button class="ui yellow fluid button" type="submit">Login</button>
    </div>

    <div class="field">
      <div class="ui text menu my-0">
        <a class="item right aligned" href="{{ route('password.request') }}">Forgot password</a>
      </div>
    </div>
  </form>
</div>

<div class="content center aligned">
  <p>Don't have an account ?</p>
  <a href="{{ route('register') }}" class="ui fluid button">Create an account</a>
</div>
@endsection
