<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="UTF-8">
    <meta name="language" content="{{ str_replace('_', '-', app()->getLocale()) }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{{ asset_("storage/images/".config('app.favicon'))}}">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <!-- App CSS -->
    <link rel="stylesheet" href="{{ asset_('assets/default/css/app.css') }}">
    
    @yield('additional_head_tags')
    
    <script>
      'use strict';
      
      window.props = {
        propduct: {},
        checkoutRoute: '{{ route('home.checkout') }}',
        currentRouteName: '{{ Route::currentRouteName() }}'
      }
    </script>
    
    <style>
      body, html {
        height: 100vh !important;
      }
      
      .main.container {
        height: 100%;
        display: contents;
        padding-top: 0 !important;
      }

      .grid {
        height: 100%;
      }

      .form.column {
        width: 350px !important;
      }
    </style>

  </head>

  <body>
    <div class="ui main fluid container pt-0" id="app">
      <div class="ui one column celled middle aligned grid m-0 shadowless">
        <div class="form column mx-auto">
          <div class="ui fluid card">

            <div class="content center aligned p-0">
              <a href="/">
                <img class="ui image mx-auto" src="{{ asset_("storage/images/".config('app.logo')) }}" alt="{{ config('app.name') }}">
              </a>
            </div>

            <div class="content center aligned">
              <h2>@yield('title')</h2>
            </div>

            @yield('content')
          </div>
        </div>
      </div>
    </div>

    <!-- App JS -->
    <script type="application/javascript" src="{{ asset_('assets/default/js/app.js') }}"></script>
  </body>
</html>