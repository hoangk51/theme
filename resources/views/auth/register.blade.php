@extends('auth.master')

@section('additional_head_tags')
<title>{{ __('Register') }}</title>
@include('front.default.partials.meta_data')
@endsection


@section('title', 'Create an account')

@section('content')
<div class="content">
  <form class="ui form" method="post" action="{{ route('register') }}">
    @csrf 

    <div class="two fields">
      <div class="field  @error('firstname') error @enderror">
        <label>{{ __('First name') }}</label>
        <input type="text" name="firstname" placeholder="..." value="{{ old('firstname') }}" required autofocus>
      </div>

      <div class="field @error('lastname') error @enderror">
        <label>{{ __('Last name') }}</label>
        <input type="text" name="lastname" placeholder="..." value="{{ old('lastname') }}" required >
      </div>
    </div>

    <div class="field @error('name') error @enderror">
      <label>{{ __('Name') }}</label>
      <input type="text" name="name" placeholder="..." value="{{ old('name') }}" required>
      
      @error('name')
      <div class="ui negative message">
        {{ $message }}
      </div>
      @enderror
    </div>
    
    <div class="field @error('email') error @enderror">
      <label>{{ __('E-Mail Address') }}</label>
      <input type="email" name="email" placeholder="..." value="{{ old('email') }}" required>

      @error('email')
      <div class="ui negative message">
        {{ $message }}
      </div>
      @enderror
    </div>

    <div class="field @error('password') error @enderror">
      <label>{{ __('Password') }}</label>
      <input type="password" name="password" placeholder="..." value="{{ old('password') }}" required>

      @error('password')
      <div class="ui negative message">
        {{ $message }}
      </div>
      @enderror
    </div>
    
    <div class="field">
      <label>{{ __('Confirm Password') }}</label>
      <input type="password" name="password_confirmation" placeholder="..." value="{{ old('password_confirmation') }}" required>
    </div>

    <div class="field mb-0">
      <button class="ui yellow fluid button" type="submit">Create account</button>
    </div>
  </form>
</div>

<div class="content center aligned">
  <p>Have an account ?</p>
  <a href="{{ route('login') }}" class="ui fluid button">Login</a>
</div>

@endsection
