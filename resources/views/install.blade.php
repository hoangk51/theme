<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="language" content="en-us">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Installation</title>
		<link rel="icon" href="{{ asset_("assets/images/favicon.png") }}">
		
		<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
		
		<!-- jQuery -->
		<script type="application/javascript" src="{{ asset_('assets/jquery/jquery-3.4.1.min.js') }}"></script>
		
		<!-- Semantic-UI -->	
		<link rel="stylesheet" href="{{ asset_('assets/semantic-ui/semantic.min.2.4.2.css') }}">
		<script src="{{ asset_('assets/semantic-ui/semantic.min.2.4.2.js') }}"></script>

		<script>
			'use strict';

			window.props = {}
		</script>

		<style>
			#app .grid {
				margin: 1rem 0;
			}

			#app .attached.segment {
				width: 100% !important;
				max-width: 100%;
			}

			#app .steps {
				padding: 0;
			}

			#app input[type="file"] {
				display: none;
			}

			.fields {
				width: 100%;
    		margin: 1rem .5rem !important;
			}

			.shadowless {
				box-shadow: none !important;
			}

			.bordered {
				border: 1px solid #eaeaea !important;
			}

			button[type="submit"] {
				display: block;
				float: left;
				margin-top: 1rem !important;
				width: 120px;
			}
		</style>
	</head>
	<body>
		
		<div class="ui main container" id="app">
			<div class="ui one column grid">

				<div class="column">
					@if($errors->any())
				    @foreach ($errors->all() as $error)
						<div class="ui negative fluid small message">
							<i class="times icon close"></i>
							{{ $error }}
						</div>
				    @endforeach
					@endif

					<form class="ui grid form" method="post" action="{{ route('home.install_app') }}" enctype="multipart/form-data">
						
						<div class="ui five top attached unstackable tabular steps">
						  <a class="step active" data-tab="1">
						    <i class="cog icon"></i>
						    <div class="content">
						      <div class="title">General</div>
						      <div class="description">General settings</div>
						    </div>
						  </a>
						  <a class="step" data-tab="2">
						    <i class="database icon"></i>
						    <div class="content">
						      <div class="title">Database</div>
						      <div class="description">Database settings</div>
						    </div>
						  </a>
						  <a class="step" data-tab="3">
						    <i class="user icon"></i>
						    <div class="content">
						      <div class="title">Admin access</div>
						      <div class="description">cPanel access settings</div>
						    </div>
						  </a>
						  <a class="step" data-tab="4">
						    <i class="credit card outline icon"></i>
						    <div class="content">
						      <div class="title">Payment processors</div>
						      <div class="description">Payment settings</div>
						    </div>
						  </a>
						  <a class="step" data-tab="5">
						    <i class="envelope outline icon"></i>
						    <div class="content">
						      <div class="title">Email</div>
						      <div class="description">Email settings</div>
						    </div>
						  </a>
						</div>

						<div class="ui attached segment tab active" data-tab="1">
						 	<div class="field">
						  	<label>Site name</label>
						  	<input type="text" name="site[name]" value="{{ old('site.name') }}">
						  </div>

						  <div class="field">
						  	<label>Site title</label>
						  	<input type="text" name="site[title]" value="{{ old('site.title') }}">
						  </div>

						  <div class="field">
						  	<label>Site description</label>
						  	<textarea name="site[description]" cols="30" rows="5">{{ old('site.description') }}</textarea>
						  </div>

						  <div class="field">
						  	<label>Site items per page</label>
						  	<input type="number" name="site[items_per_page]" value="{{ old('site.items_per_page') }}">
						  </div>
							
							<div class="field">
						  	<label>Site timezone</label>
						  	<div class="ui selection dropdown">
						  		<input type="hidden" name="site[timezone]" value="{{ old('site.timezone') }}">
						  		<i class="dropdown icon"></i>
						  		<div class="default text">Select timezone</div>
									<div class="menu">
										@foreach(config('app.timezones') as $key => $val)
										<div class="item" data-value="{{ $key }}">{{ $key }} - {{ $val }}</div>
										@endforeach
									</div>
						  	</div>
						  </div>

							<table class="ui celled unstackable table">
								<thead>
									<tr>
										<th class="center aligned">Favicon</th>
										<th class="center aligned">Logo</th>
										<th class="center aligned">Cover</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											<button class="ui basic fluid button" type="button" onclick="this.nextElementSibling.click()">Browse</button>
											<input type="file" name="site[favicon]" accept="image/*">
										</td>
										<td>
											<button class="ui basic fluid button" type="button" onclick="this.nextElementSibling.click()">Browse</button>
											<input type="file" name="site[logo]" accept="image/*">
										</td>
										<td>
											<button class="ui basic fluid button" type="button" onclick="this.nextElementSibling.click()">Browse</button>
											<input type="file" name="site[cover]" accept="image/*">
										</td>
									</tr>
								</tbody>
							</table>
						</div>

						<div class="ui attached segment tab" data-tab="2">
							<div class="field">
						  	<label>Database host</label>
						  	<input type="text" name="database[host]" value="{{ old('database.host') }}">
						  </div>

							<div class="field">
						  	<label>Database username</label>
						  	<input type="text" name="database[username]" value="{{ old('database.username') }}">
						  </div>

						  <div class="field">
						  	<label>Database password</label>
						  	<input type="text" name="database[password]" value="{{ old('database.password') }}">
						  </div>

						  <div class="field">
						  	<label>Database name</label>
						  	<input type="text" name="database[database]" value="{{ old('database.database') }}">
						  </div>
						</div>

						<div class="ui attached segment tab" data-tab="3">
						  <div class="field">
						  	<label>Admin username</label>
						  	<input type="text" name="admin[username]" value="{{ old('admin.username') }}">
						  </div>

						  <div class="field">
						  	<label>Admin email</label>
						  	<input type="email" name="admin[email]" value="{{ old('admin.email') }}">
						  </div>

						  <div class="field">
						  	<label>Admin password</label>
						  	<input type="text" name="admin[password]" value="{{ old('admin.password') }}">
						  </div>

						  <div class="field">
						  	<label>Admin avatar</label>
						  	<button class="ui basic fluid button" type="button" onclick="this.nextElementSibling.click()">Browse</button>
								<input type="file" name="admin[avatar]" accept="image/*">
						  </div>
						</div>

						<div class="ui attached segment tab" data-tab="4">
						  <div class="ui two cards">
						  	<div class="card shadowless bordered">
						  		<div class="content">
						  			<div class="field">
							  			<div class="ui checkbox @if(old('payments.paypal.enabled')) checked @endif">
											  <input type="checkbox" name="payments[paypal][enabled]">
											  <label>Enable <strong>PayPal</strong> Payment Processor</label>
											</div>
										</div>
						  		</div>
						  		<div class="content">
						  			<div class="field">
									  	<label>PayPal Client ID</label>
									  	<input type="text" name="payments[paypal][client_id]" value="{{ old('payments.paypal.client_id') }}">
									  </div>
									  <div class="field">
									  	<label>PayPal Secret ID</label>
									  	<input type="text" name="payments[paypal][secret_id]" value="{{ old('payments.paypal.secret_id') }}">
									  </div>
									  <div class="field">
									  	<label>PayPal Transaction Fee</label>
									  	<input type="text" name="payments[paypal][fee]" value="{{ old('payments.paypal.secret_id') }}">
									  </div>
						  		</div>
						  	</div>

						  	<div class="card shadowless bordered">
						  		<div class="content">
						  			<div class="field">
							  			<div class="ui checkbox @if(old('payments.stripe.enabled')) checked @endif">
											  <input type="checkbox" name="payments[stripe][enabled]">
											  <label>Enable <strong>Stripe</strong> Payment Processor</label>
											</div>
										</div>
						  		</div>
						  		<div class="content">
						  			<div class="field">
									  	<label>Stripe Client ID</label>
									  	<input type="text" name="payments[stripe][client_id]" value="{{ old('payments.stripe.client_id') }}">
									  </div>
									  <div class="field">
									  	<label>Stripe Secret ID</label>
									  	<input type="text" name="payments[stripe][secret_id]" value="{{ old('payments.stripe.secret_id') }}">
									  </div>
									  <div class="field">
									  	<label>Stripe Transaction Fee</label>
									  	<input type="text" name="payments[stripe][fee]" value="{{ old('payments.stripe.secret_id') }}">
									  </div>
						  		</div>
						  	</div>

						  	<div class="three fields">
						  		<div class="field">
						  			<label>VAT (%)</label>
						  			<input type="number" name="payments[vat]" step="0.01" value="{{ old('payments.vat') }}">
						  		</div>

						  		<div class="field">
						  			<label>Currency code</label>
						  			<input type="text" name="payments[currency_code]" value="{{ old('payments.currency_code') }}">
						  		</div>

						  		<div class="field">
						  			<label>Currency symbol</label>
						  			<input type="text" name="payments[currency_symbol]" value="{{ old('payments.currency_symbol') }}">
						  		</div>
						  	</div>
						  </div>
						</div>

						<div class="ui attached segment tab" data-tab="5">
							<div class="ui two cards">
						  	<div class="card shadowless bordered">
						  		<div class="content" style="max-height:47px">
						  			Mail
						  		</div>
						  		<div class="content">
						  			<div class="field">
									  	<label>Mail username</label>
									  	<input type="text" name="mailer[mail][username]" value="{{ old('mailer.mail.username') }}">
									  </div>
									  <div class="field">
									  	<label>Mail password</label>
									  	<input type="text" name="mailer[mail][password]" value="{{ old('mailer.mail.password') }}">
									  </div>
									  <div class="field">
									  	<label>Mail host</label>
									  	<input type="text" name="mailer[mail][host]" value="{{ old('mailer.mail.password') }}">
									  </div>
									  <div class="field">
									  	<label>Mail port</label>
									  	<input type="text" name="mailer[mail][port]" value="{{ old('mailer.mail.port') }}">
									  </div>
									  <div class="field">
									  	<label>Mail encryption</label>
									  	<input type="text" name="mailer[mail][encryption]" value="{{ old('mailer.mail.encryption') }}">
									  </div>
									  <div class="field">
									  	<label>Mail reply to</label>
									  	<input type="email" name="mailer[mail][reply_to]" value="{{ old('mailer.mail.reply_to') }}">
									  </div>
						  		</div>
						  	</div>

						  	<div class="card shadowless bordered">
						  		<div class="content">
						  			<div class="field">
							  			<div class="ui checkbox @if(old('mailer.imap.enabled')) checked @endif">
											  <input type="checkbox" name="mailer[imap][enabled]">
											  <label>Enable <strong>IMAP</strong></label>
											</div>
										</div>
						  		</div>
						  		<div class="content">
						  			<div class="field">
									  	<label>IMAP username</label>
									  	<input type="text" name="mailer[imap][username]" value="{{ old('mailer.imap.username') }}">
									  </div>
									  <div class="field">
									  	<label>IMAP password</label>
									  	<input type="text" name="mailer[imap][password]" value="{{ old('mailer.imap.password') }}">
									  </div>
									  <div class="field">
									  	<label>IMAP host</label>
									  	<input type="text" name="mailer[imap][host]" value="{{ old('mailer.imap.host') }}">
									  </div>
									  <div class="field">
									  	<label>IMAP port</label>
									  	<input type="text" name="mailer[imap][port]" value="{{ old('mailer.imap.port', '143') }}">
									  </div>
									  <div class="field">
									  	<label>IMAP encryption</label>
									  	<input type="text" name="mailer[imap][encryption]" value="{{ old('mailer.imap.encryption', 'tls') }}">
									  </div>
									  <div class="field">
									  	<label>IMAP validate cert</label>
									  	<div class="ui selection dropdown">
									  		<input type="hidden" name="mailer[imap][validate_cert]" value="{{ old('mailer.imap.validate_cert', 'false') }}">
									  		<i class="dropdown icon"></i>
									  		<div class="default text">False</div>
									  		<div class="menu">
									  			<a class="item" data-value="true">True</a>
									  			<a class="item" data-value="false">False</a>
									  		</div>
									  	</div>
									  </div>
									  <div class="field">
									  	<label>IMAP account</label>
									  	<input type="text" name="mailer[imap][account]" value="{{ old('mailer.imap.account', 'default') }}">
									  </div>
									  <div class="field">
									  	<label>IMAP protocol</label>
									  	<input type="text" name="mailer[imap][protocol]" value="{{ old('mailer.imap.protocol', 'imap') }}">
									  </div>
						  		</div>
						  	</div>
						  </div>
						</div>
						
						<button class="ui basic button" type="submit">Submit</button>
					</form>
				</div>
				
			</div>
		</div>

<script>
	'use strict';
	
	$('.tabular.steps .step').tab()
	$('.ui.dropdown').dropdown()
	$('.ui.checkbox').checkbox()

	$(document).on('click', '.ui.message i.close', function()
	{
		$(this).closest('.ui.message').hide();
	})

	$('input').on('keypress', function(e)
	{
		if(e.keyCode === 13)
		{
			e.preventDefault();
			return false;
		}
	})
</script>
	</body>
</html>