@extends('back.master')

@section('title', $title)


@section('content')

<div class="row main" id="subscribers">
	<div class="ui menu shadowless">		
		<a id="bulk-delete" @click="deleteItems" :href="route+ids.join()" class="item" :class="{disabled: isDisabled}">Delete</a>
		<a onclick="$('form', this).submit()" class="item">
			Export CSV
			<form action="{{ route('subscribers.export') }}" method="post" target="_blank"></form>
		</a>
		<div class="right menu">
			<a href="{{ route('subscribers.newsletter.create') }}" class="item ml-1">Create newsletter</a>
		</div>
	</div>
	
	<div class="table wrapper">
		<table class="ui unstackable celled basic table">
			<thead>
				<tr>
					<th>
						<div class="ui fitted checkbox">
						  <input type="checkbox" @change="selectAll">
						  <label></label>
						</div>
					</th>
					<th>
						<a href="{{ route('subscribers', ['orderby' => 'email', 'order' => $items_order]) }}">Email</a>
					</th>
					<th>
						<a href="{{ route('subscribers', ['orderby' => 'updated_at', 'order' => $items_order]) }}">Updated at</a>
					</th>
					<th>Delete</th>
				</tr>
			</thead>
			<tbody>
				@foreach($subscribers as $subscriber)
				<tr>
					<td class="center aligned">
						<div class="ui fitted checkbox select">
						  <input type="checkbox" value="{{ $subscriber->id }}" @change="toogleId({{ $subscriber->id }})">
						  <label></label>
						</div>
					</td>
					<td>{{ ucfirst($subscriber->email) }}</td>
					<td class="center aligned">{{ $subscriber->updated_at }}</td>
					<td class="center aligned one column wide">
						<a @click="deleteItem($event)" href="{{ route('subscribers.destroy', $subscriber->id) }}" class="ui small basic labeled icon button"><i class="trash alternate outline icon"></i>Delete</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	
	<div class="ui fluid divider"></div>

	{{ $subscribers->appends($base_uri)->onEachSide(1)->links() }}
	{{ $subscribers->appends($base_uri)->links('vendor.pagination.simple-semantic-ui') }}

</div>

<script>
	'use strict';

	var app = new Vue({
	  el: '#subscribers',
	  data: {
	  	route: '{{ route('subscribers.destroy', "") }}/',
	    ids: [],
	    isDisabled: true
	  },
	  methods: {
	  	toogleId: function(id)
	  	{
	  		if(this.ids.indexOf(id) >= 0)
	  			this.ids.splice(this.ids.indexOf(id), 1);
	  		else
	  			this.ids.push(id);
	  	},
	  	selectAll: function()
	  	{
	  		$('#subscribers tbody .ui.checkbox.select').checkbox('toggle')
	  	},
	  	deleteItems: function(e)
	  	{
	  		var confirmationMsg = 'Are you sure you want to delete the selected subscribers(s) ?';

	  		if(!this.ids.length || !confirm(confirmationMsg))
	  		{
	  			e.preventDefault();
	  			return false;
	  		}
	  	},
	  	deleteItem: function(e)
	  	{
	  		if(!confirm('Are you sure you want to delete this subscriber ?'))
  			{
  				e.preventDefault();
  				return false;
  			}
	  	}
	  },
	  watch: {
	  	ids: function(val)
	  	{
	  		this.isDisabled = !val.length;
	  	}
	  }
	})
</script>
@endsection