@extends('back.master')

@section('title', $title)

@section('additional_head_tags')
<link href="{{ asset_('assets/admin/css/summernote-lite-0.8.12.css') }}" rel="stylesheet">
<script src="{{ asset_('assets/admin/js/summernote-lite-0.8.12.js') }}"></script>
@endsection

@section('content')

<form class="ui form" method="post" action="{{ route('subscribers.newsletter.send') }}" id="newsletter" spellcheck="false">
	@csrf

	<div class="field">
		<button type="submit" name="preview" class="ui basic icon button mx-0" title="Preview" 
						onclick="$(this).closest('form').attr('target', '_blank')">
		  <i class="eye icon mx-0"></i>
		</button>
		<button type="submit" class="ui basic labeled icon button mx-0"
						onclick="$(this).closest('form').attr('target', '_self')">
		  <i class="save outline icon mx-0"></i>
		  Send
		</button>
		<a href="{{ route('subscribers') }}" class="ui basic right labeled icon button mx-0">
		  <i class="times icon mx-0"></i>
		  Cancel
		</a>
	</div>

	<div class="ui fluid divider"></div>
	
	@if($errors->any())
    @foreach ($errors->all() as $error)
		<div class="ui negative fluid small message">
			<i class="times icon close"></i>
			{{ $error }}
		</div>
    @endforeach
	@endif

	@if(session('newsletter_sent'))
	<div class="ui positive fluid small bold message">
		<i class="times icon close"></i>
		{{ session('newsletter_sent') }}
	</div>
	@endif
	
	<div class="field">
		<div class="ui multiple selection search fluid dropdown">
			<input type="hidden" name="emails" value="{{ old('emails') }}">
			<div class="default text">Select emails</div>
			<i class="dropdown icon"></i>
			<div class="menu">
				@foreach($subscribers as $subscriber)
				<a data-value="{{ $subscriber->email }}" class="item">{{ $subscriber->email }}</a>
				@endforeach
			</div>
		</div>
	</div>

	<div class="field">
		<label>Subject</label>
		<input type="text" name="subject" required value="{{ old('subject') }}">
	</div>

	<div class="ui fluid divider"></div>

	<div class="ui grid">
		<div class="left column">
			<h4 class="center aligned">Using HTML editor</h4>
			<div class="field">
				<textarea name="newsletter" id="summernote" spellcheck="false" cols="30">{{ old('newsletter') }}</textarea>
			</div>
		</div>

		<div class="right column" id="selections">
			<h4 class="center aligned">Using selections</h4>
			
			<div class="items-selection">
				<input type="text" class="selection-title" placeholder="Selection title...">
				<div class="ui multiple selection search fluid dropdown mt-1-hf items-search">
					<input type="hidden" name="selection_ids" value="">
					<div class="default text">Search products...</div>
					<i class="dropdown icon"></i>
					<div class="menu"></div>
				</div>
			</div>
			
			<button type="button" id="add-selection" class="ui basic icon button small mt-1"><i class="plus icon mx-0"></i></button>
    
			<div id="selections-html" class="d-none">
				<table v-for="(items, key) in products">
				  <tbody>
				    <tr>
				      <th colspan="2">
				        <h2 v-if="(titles[key] || '').trim().length > 0" style="color: #565656;font-size: 16px;font-weight: bold;margin-top: 0;text-align: center;margin: 0 0 .5rem;text-transform: uppercase;display: inline-block;padding: .5rem 1rem;border: 1px solid #ebebeb;border-radius: .25rem;">
					        @{{ titles[key] }}
					      </h2>
				      </th>
				    </tr>
				    <tr v-for="item in chunkArr(Object.values(items), 2)">
				      <td v-for="product in item" style="width: 50%!important">
				        <a :href="productBaseUrl+'/'+product.slug" 
				           target="_blank" 
				           style="display:block;text-decoration:none;color:#000;position:relative;">
				          <img :src="coversBase+'/'+product.cover" 
				               :alt="product.name" 
				               width="100%" height="auto" style="display: block">
				        </a>
			        	<span style="bottom: .5rem;right: .5rem;font-size:.8rem;display: block;padding: .25rem .5rem;background: #232323;border-radius: .25rem;color: #fff;width: 100px;text-align: center;margin-top: .25rem;margin-left: auto;">
			            @{{ currency+''+product.price }} Buy now
			          </span>
				        <h4 align="center" style="margin: .5rem 0;height: 50px;overflow:hidden;">@{{ product.name }}</h4>
				        <p style="text-align:center;height:50px;margin:.5rem 0;color:grey;overflow:hidden">@{{ product.short_description }}</p>
				      </td>
				    </tr>
				  </tbody>
				</table>
			</div>

		</div>
	</div>
</form>

<script>
	'use strict';
	
	var products = {},
			titles = {},
			productsSelections = {};

	var app = new Vue({
		el: '#newsletter',
		data: {
			products: {},
			titles: {},
			coversBase: '{{ asset_('storage/covers') }}',
			productBaseUrl: '{{ route('home.product', '') }}',
			currency: '{{ config('payments.currency_symbol') ?? config('payments.currency_code') }}',
		},
		methods: {
			updateApp: function()
			{
				this.products = productsSelections;
				this.titles 	= titles;

				this.$forceUpdate();
			}
		},
		watch: {
			
		}
	});


	$.fn.itemsDropdown = function()
	{
		this.dropdown({
			onAdd: function(addedValue, addedText, $addedChoice)
			{
				var keys = Object.keys(productsSelections);
				var i = $('.items-search').index($(this)).toString();

				if(keys.indexOf(i) < 0)
				{
					productsSelections[i] = {};
				}
				
				productsSelections[i][addedValue] = products[addedValue];
			},
			onRemove: function(removedValue, removedText, $removedChoice)
			{
				var keys = Object.keys(productsSelections);
				var i = $('.items-search').index($(this)).toString();

				if(keys.indexOf(i) >= 0)
				{
					var items = productsSelections[i];

					items = Object.keys(items).reduce(function(carry, key)
									{
										if(key != removedValue)
											carry[key] = items[key];

										return carry;
									}, {});
					
					if(!Object.keys(items).length)
					{
						productsSelections = 	Object.keys(productsSelections).reduce(function(carry, key)
																	{
																		if(key != i)
																			carry[key] = productsSelections[key];

																		return carry;
																	}, {});
					}
					else
					{
						productsSelections[i] = items;
					}
				}
			},
			onHide: function()
			{
				app.updateApp();
			}
		})
	};


	$(function()
	{
		$('.items-search').itemsDropdown();


		$(document).on('keyup', '.items-search input.search', debounce(function(e)
		{
			var _this = $(e.target);

			var val = _this.val().trim();

			if(!val.length)
				return;

			$.post('{{ route('products.api') }}', {'keywords': val}, null, 'json')
			.done(function(res)
			{
				var items = res.products.reduce(function(carry, item)
										{
											carry.push({'value': item.id, 'name': item.name});
											return carry;
										}, []);

				_this.closest('.items-search').dropdown('setup menu', {'values': items});

				if(items.length)
				{
					items = res.products.reduce(function(carry, item)
					{
						carry[item.id] = item;
						return carry;
					}, {});

					products = Object.assign(products, items);
				}
			})
			.fail(function()
			{
				alert('Request failed')
			})
		}, 200));


		$('#summernote').summernote({
	    placeholder: '...',
	    tabsize: 2,
	    height: 450
	  })


		$(document).on('change', '.selection-title', function()
		{
			var i = $('.selection-title').index($(this));

			titles[i] = $(this).val().trim();

			app.updateApp();
		})


		$('#add-selection').on('click', function()
		{
			$('<div class="items-selection mt-1">\
					<input type="text" class="selection-title" placeholder="Selection title...">\
					<div class="ui multiple selection search fluid dropdown mt-1-hf items-search">\
						<input type="hidden" name="selection_ids" value="">\
						<div class="default text">Search products...</div>\
						<i class="dropdown icon"></i>\
						<div class="menu"></div>\
					</div>\
				</div>').insertAfter('.items-selection:last');
			
			$('.items-search:last').itemsDropdown();
		})

		$('#newsletter').on('submit', function(e)
		{
			$('#summernote').summernote('reset');
			$('#summernote').summernote('pasteHTML', $('#selections-html').html());
		})
	})
</script>

@endsection