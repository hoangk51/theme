<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta charset="UTF-8">
		<meta name="language" content="{{ str_replace('_', '-', app()->getLocale()) }}">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="icon" href="{{ asset_("storage/images/".config('app.favicon')) }}">
		
		<title>@yield('title')</title>

		<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
		
		<!-- App CSS -->
		<link rel="stylesheet" href="{{ asset_('assets/admin/css/app.css') }}">
		
		<!-- App Javascript -->
		<script type="application/javascript" src="{{ asset_('assets/admin/js/app.js') }}"></script>

		@yield('additional_head_tags')
	</head>

	<body>

		<div class="ui main fluid container">
			<div class="ui celled grid m-0 shadowless">
				<div class="row" id="content">
					
					<div class="l-side-wrapper column">
						@include('back.includes.left_sidebar')
					</div>

					<div id="vertical-divider"></div>

					<div class="r-side-wrapper column">
						<div class="ui unstackable secondary menu px-1" id="top-menu">
							@include('back.includes.top_menu')
						</div>

						<div class="ui text menu breadcrumb mb-0">
							<div class="item header">
								@yield('title')
							</div>
						</div>

						<div class="ui hidden divider mt-0"></div>

						@yield('content')

						<footer class="row">
							@include('back.includes.footer')
						</footer>
					</div>
				</div>

			</div>
		</div>
	</body>
</html>
