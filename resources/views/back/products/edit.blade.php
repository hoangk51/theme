@extends('back.master')

@section('title', $title)

@section('additional_head_tags')

<link href="{{ asset_('assets/admin/css/summernote-lite-0.8.12.css') }}" rel="stylesheet">
<script src="{{ asset_('assets/admin/js/summernote-lite-0.8.12.js') }}"></script>

@endsection


@section('content')
<form class="ui form" method="post" enctype="multipart/form-data" action="{{ route('products.update', $product->id) }}">
	@csrf

	<div class="field">
		<button class="ui icon labeled basic button" type="submit">
		  <i class="save outline icon"></i>
		  Update
		</button>
		<a class="ui icon labeled basic button" href="{{ route('products') }}">
			<i class="times icon"></i>
			Cancel
		</a>
		<button class="ui basic button right floated" type="button" id="toggle-r-column">
		  <i class="angle left icon mx-0"></i>
		</button>
	</div>
	
	@if($errors->any())
      @foreach ($errors->all() as $error)
         <div class="ui negative fluid small message">
         	<i class="times icon close"></i>
         	{{ $error }}
         </div>
      @endforeach
	@endif

	<div class="ui fluid divider"></div>

	<div class="ui two columns grid" id="product">
		<div class="left column">
			<div class="field inline">
				<div class="ui toggle checkbox">
					<input type="checkbox" name="notify_buyers">
					<label>Notify buyers about this update</label>
				</div>
			</div>
			<div class="field">
				<label>Name</label>
				<input type="text" name="name" placeholder="..." value="{{ old('name', $product->name) }}" autofocus required>
			</div>
			<div class="field">
				<label>Short description</label>
				<textarea name="short_description" cols="30" rows="5">{{ old('short_description', $product->short_description) }}</textarea>
			</div>
			<div class="field">
				<label>Overview</label>
				<textarea name="overview" class="summernote" id="overview" cols="30" rows="10">{{ old('overview', $product->overview) }}</textarea>
			</div>
			<div class="field">
				<label>Features</label>
				<textarea name="features" class="summernote" cols="30" rows="10">{{ old('features', $product->features) }}</textarea>
			</div>
			<div class="field">
				<label>Requirements</label>
				<textarea name="requirements" class="summernote" cols="30" rows="10">{{ old('requirements', $product->requirements) }}</textarea>
			</div>
			<div class="field">
				<label>Instuctions</label>
				<textarea name="instructions" class="summernote" cols="30" rows="10">{{ old('instructions', $product->instructions) }}</textarea>
			</div>
			<div class="field">
				<label>Notes</label>
				<textarea name="notes" placeholder="one note per line" cols="30" rows="5">{{ old('notes', $product->notes) }}</textarea>
			</div>
			<div class="field">
				<label>FAQ</label>
				<textarea name="faq" placeholder="Q:...&#10;A:...&#10;Q:...&#10;A:..." cols="30" rows="5">{{ old('faq', $product->faq) }}</textarea>
			</div>
		</div>

		<div class="right column">
			<div class="field">
				<label>Price</label>
				<div class="ui labeled fluid input">
				  <input type="text" name="price" v-model="price" placeholder="...">
				</div>
			</div>

			<div class="field" v-if="freeForLimitedTime">
				<label>Free download time</label>
				<div class="ui labeled fluid input">
					<div class="ui label px-1-hf">From</div>
				  <input type="date" name="free[from]" value="{{ old('free.from', $product->free->from ?? null) }}">
				</div>
				<div class="ui labeled fluid input mt-1">
					<div class="ui label px-1-hf">To</div>
				  <input type="date" name="free[to]" value="{{ old('free.to', $product->free->to ?? null) }}">
				</div>
			</div>

			<div class="field">
				<label>File</label>
				<button class="ui basic blue small fluid button" type="button" @click="browserMainFile('local')">Browse</button>
				<input type="file" name="file" class="d-none" accept=".zip,.rar,.7z" id="local">
				
				<input type="hidden" name="file_host" value="{{ old('file_host') }}" :value="selectedDrive" class="d-none">
					
				<div class="ui fluid small info message p-1" v-if="fileId" v-cloak>
					<i class="close icon" @click="removeSelectedFile"></i>
					@{{ fileId }}
				</div>

				<input type="hidden" name="file_name" value="{{ old('file_name') }}" :value="fileId" class="d-none">
				
				@if(config('filehosts.google_drive.enabled'))
				<button class="ui basic blue small fluid button mt-1" type="button" @click="browserMainFile('google')">From Google Drive</button>
				@endif

				@if(config('filehosts.dropbox.enabled'))
				<button class="ui basic blue small fluid button mt-1" type="button" @click="browserMainFile('dropbox')">From DropBox</button>
				@endif
			</div>

			<div class="field">
				<label>Cover</label>
				<button class="ui basic blue small fluid button" type="button" onclick="this.nextElementSibling.click()">Browse</button>
				<input type="file" name="cover" class="d-none" accept="image/*" multiple>
			</div>

			<div class="field">
				<label>Thumbnail</label>
				<button class="ui basic blue small fluid button" type="button" onclick="this.nextElementSibling.click()">Browse</button>
				<input type="file" name="thumbnail" class="d-none" accept="image/*" multiple>
			</div>

			<div class="field">
				<label>Screenshots</label>
				<button class="ui basic blue small fluid button" type="button" onclick="this.nextElementSibling.click()">Browse</button>
				<input type="file" name="screenshots[]" class="d-none" accept="image/*" multiple>
			</div>

			<div class="field">
				<label>Category</label>
				<div class="ui selection dropdown" id="category">
				  <input type="hidden" name="category" value="{{ $product->category }}">
				  <i class="dropdown icon"></i>
				  <div class="default text">Select category</div>
				  <div class="menu">
				  	@foreach($category_parents as $category_parent)
						<div class="item" data-value="{{ $category_parent->id }}">
							{{ ucfirst($category_parent->name) }}
						</div>
						@endforeach
				  </div>
				</div>
			</div>
			<div class="field">
				<label>Subcategories</label>
				<div class="ui multiple selection dropdown" id="subcategories">
					<input type="hidden" name="subcategories" value="{{ $product->subcategories }}">
					<i class="dropdown icon"></i>
					<div class="default text">Select subcategory</div>
					<div class="menu">
						@foreach($category_children[$product->category] ?? [] as $category_child)
						<div class="item" data-value="{{ $category_child->id }}">
							{{ ucfirst($category_child->name) }}
						</div>
						@endforeach
					</div>
				</div>
			</div>

			<div class="field">
				<label>Tags</label>
				<input type="text" name="tags" value="{{ old('tags', $product->tags) }}" placeholder="...">
			</div>
			<div class="field">
				<label>Version</label>
				<input type="text" name="version" value="{{ old('version', $product->version)  }}" placeholder="...">
			</div>
			<div class="field">
				<label>Release date</label>
				<input type="date" name="release_date" value="{{ old('release_date', $product->release_date) }}" placeholder="...">
			</div>
			<div class="field">
				<label>Latest update</label>
				<input type="date" name="last_update" value="{{ old('last_update', $product->last_update) }}" placeholder="...">
			</div>
			<div class="field">
				<label>Preview</label>
				<input type="text" name="preview" value="{{ old('preview', $product->preview) }}" placeholder="...">
			</div>
			<div class="field">
				<label>Included files</label>
				<input type="text" name="included_files" value="{{ old('included_files', $product->included_files) }}" placeholder="...">
			</div>
			<div class="field">
				<label>Tools used <i class="exclamation circle icon" title="languages, libraries, frameworks..."></i></label>
				<input type="text" name="software" value="{{ old('software', $product->software) }}" placeholder="...">
			</div>
			<div class="field">
				<label>Database used <i class="exclamation circle icon" title="MongoDB, MySQL, SQLite..."></i></label>
				<input type="text" name="database" value="{{ old('database', $product->db) }}" placeholder="...">
			</div>
			<div class="field">
				<label>Compatible browsers</label>
				<input type="text" name="compatible_browsers" value="{{ old('compatible_browsers', $product->compatible_browsers) }}" placeholder="...">
			</div>
			<div class="field">
				<label>Compatible OS</label>
				<input type="text" name="compatible_os" value="{{ old('compatible_os', $product->compatible_os) }}" placeholder="...">
			</div>
			<div class="field">
				<label>High resolution</label>
				<select name="high_resolution" class="ui dropdown">
					@foreach(['False', 'True'] as $k => $v)
						<option value="{{ $k }}" @if($product->high_resolution === $v) selected @endif>{{ $v }}</option>
					@endforeach
				</select>
			</div>
		</div>

		<div class="ui modal" id="files-list">
			<div class="content head p-1">
				<h3>@{{ drivesTitles[selectedDrive] }}</h3>

				<div class="ui icon input">
				  <input type="text" placeholder="Folder..." v-model="parentFolder" spellcheck="false">
				  <i class="paper plane outline link icon" @click="setFolder"></i>
				</div>
			</div>

			<div class="content body" v-if="selectedDrive">
				<div class="ui six cards">

					<a href="javascript:void(0)" 
						 class="ui card" 
						 v-for="item in mainFilesList[selectedDrive]" 
						 :title="item.name"
						 @click="setSelectedFile(item.id)">
						<div class="image">
					    <img :src="getFileExtension(item)">
					  </div>
					  <div class="content p-0">
					  	<h4 class="header">
					  		@{{ item.name }}
					  	</h4>
					  </div>
					</a>

				</div>
			</div>

			<div class="actions">
				<div class="ui icon input">
				  <input type="text" placeholder="Search..." v-model="searchFile" spellcheck="false">
				  <i class="search link icon" @click="searchFiles"></i>
				</div>

				<button v-if="googleDriveNextPageToken && selectedDrive === 'google'" 
								class="ui basic button" 
								type="button"
								@click="googleDriveLoadMore($event)">
					Load more files
				</button>

				<button v-if="dropBoxCursor && selectedDrive === 'dropbox'" 
								class="ui basic button"
								type="button"
								@click="dropBoxDriveLoadMore($event)">
					Load more files
				</button>

				<button class="ui basic button"type="button" @click="closeDriveModal">Close</button>
			</div>
		</div>
	</div>
</form>

<script>
	'use strict';

  var app = new Vue({
  	el: '#product',
  	data: {
  		mainFilesList: {google: [], dropbox: []},
  		selectedDrive: '',
  		googleDriveNextPageToken: null,
  		dropBoxCursor: null,
  		drivePageSize: 20,
  		drivesTitles: {google: 'Google Drive', dropbox: 'DropBox'},
  		searchFile: null,
  		parentFolder: null,
  		fileId: '{{ $product->file_name }}',
  		price: '{{ old('price', $product->price) }}',
  		freeForLimitedTime: false,
  	},
  	methods: {
  		browserMainFile: function(from)
  		{
  			this.selectedDrive = from;

  			if(from === 'local')
  			{  				
  				$('input[name="file"]').click();
  			}
  			else if(from === 'google')
  			{
  				this.googleDriveInit();
  			}
  			else if(from === 'dropbox')
  			{
  				this.dropboxDriveInit();
  			}

  			if(/^(google|dropbox)$/i.test(from))
  			{
  				$('#files-list').modal('show')
  			}
  		},
  		googleDriveLoadMore: function(e)
  		{
  			var e = e;

  			e.target.disabled = true;

  			if(this.googleDriveNextPageToken)
  			{
  				var payload = {
  					'files_host': 'GoogleDrive', 
						'page_size': this.drivePageSize, 
						'nextPageToken': this.googleDriveNextPageToken
					};

  				$.post('{{ route('products.list_files') }}', payload, null, 'json')
  				.done(function(res)
  				{
  					if(!res.files_list)
  						return;

						app.googleDriveNextPageToken = res.files_list.nextPageToken || null;
  					
  					e.target.disabled = app.googleDriveNextPageToken ? false : true;

  					Vue.set(app.mainFilesList, 'google', 
  								  app.mainFilesList.google.concat(res.files_list.files || []));
  				})
  			}
  		},
  		dropBoxDriveLoadMore: function(e)
  		{
  			var e = e;

  			e.target.disabled = true;

  			var payload = {
  				'files_host': 'DropBox', 
	  			'cursor': this.dropBoxCursor, 
	  			'limit': this.drivePageSize
	  		};

				$.post('{{ route('products.list_files') }}', payload, null, 'json')
				.done(function(res)
				{
					if(!res.files_list)
						return;

					res.files_list.cursor

					app.dropBoxCursor = res.files_list.has_more ? res.files_list.cursor : null;

					e.target.disabled = res.files_list.has_more ? false : true;

					Vue.set(app.mainFilesList, 'dropbox', 
  								app.mainFilesList.dropbox.concat(res.files_list.files || []));
				})
  		},
  		setFolder: function()
  		{
  			if(this.selectedDrive === 'google')
  			{  				
  				this.googleDriveInit();
  			}
  			else if(this.selectedDrive === 'dropbox')
  			{
  				this.dropboxDriveInit();
  			}
  		},
  		googleDriveInit: function()
  		{
				var payload = {
					'files_host': 'GoogleDrive', 
					'page_size': this.drivePageSize, 
					'parent': this.parentFolder,
					'keyword': this.searchFile
				};

				$.post('{{ route('products.list_files') }}', payload, null, 'json')
				.done(function(res)
				{
					try
					{
						if(!res.files_list.files.length || null)
						{
							Vue.set(app.mainFilesList, 'google', []);
							return;
						}	
					}
					catch(error){}

					app.googleDriveNextPageToken = res.files_list.nextPageToken || null;
					
					Vue.set(app.mainFilesList, 'google', res.files_list.files);
				})
  		},
  		dropboxDriveInit: function()
  		{
  			var payload = {
  				'files_host': 'DropBox', 
  				'limit': this.drivePageSize,
  				'path': this.parentFolder,
  				'keyword': this.searchFile
  			};

				$.post('{{ route('products.list_files') }}', payload, null, 'json')
				.done(function(res)
				{
					try
					{
						if(!res.files_list.files.length || null)
						{
							Vue.set(app.mainFilesList, 'dropbox', []);
							return;
						}
					}
					catch(error){}

					app.dropBoxCursor = (res.files_list || {}).hasOwnProperty('has_more') ? res.files_list.cursor : null;
  				
  				Vue.set(app.mainFilesList, 'dropbox', res.files_list.files);
				})
  		},
  		searchFiles: function()
  		{
  			if(this.selectedDrive === 'google')
  			{  				
  				this.googleDriveInit();
  			}
  			else if(this.selectedDrive === 'dropbox')
  			{
  				this.dropboxDriveInit();
  			}
  		},
  		setSelectedFile: function(fileId)
  		{
  			this.fileId = fileId;

  			$('#files-list').modal('hide');
  		},
  		removeSelectedFile: function()
  		{
  			this.selectedDrive = '';
  			this.fileId 			 = null;
  		},
  		getFileExtension(item)
  		{
  			var baseUrl = '{{ asset_('assets/images/') }}/';

  			if(this.selectedDrive === 'dropbox')
  			{
	  			var sufx = item.name.slice(-4);
	  			
	  			if(/\.zip/i.test(sufx))
  					baseUrl += 'zip';
  				else if(/\.rar/i.test(sufx))
  					baseUrl += 'rar';
  				else if(/\.7z/i.test(sufx))
  					baseUrl += '7z';
  				else
  					baseUrl += 'file';
  			}
  			else if(this.selectedDrive === 'google')
  			{
  				var mt = item.mimeType;

  				if(/zip/i.test(mt))
  					baseUrl += 'zip';
  				else if(/rar/i.test(mt))
  					baseUrl += 'rar';
  				else if(/7z/i.test(mt))
  					baseUrl += '7z';
  				else
  					baseUrl += 'file';
  			}

  			return baseUrl + '.png';
  		},
  		closeDriveModal: function()
  		{
  			$('#files-list').modal('hide')
  		}
  	},
  	watch: {
  		price: function(nv, pv)
  		{
  			this.freeForLimitedTime = (nv > 0);

  			if(nv < 0 || nv === '')
  			{
  				this.price = 0;
  			}
  		}
  	},
  	mounted: function()
  	{
  		this.freeForLimitedTime = (this.price > 0);
  	}
	})

	$(function()
  {
  	$('.summernote').summernote({
	    placeholder: '...',
	    tabsize: 2,
	    height: 150
	  });
	  	
		var subcategories = <?= json_encode($category_children) ?>;

  	$('input[name="category"]').on('change', function()
		{
			var values 					  = [];
			var parentCategoryId 	= $(this).val();

			if(Object.keys(subcategories).length)
			{
				for(var k in (subcategories[parentCategoryId] || []))
				{
					var subcategory = subcategories[parentCategoryId][k];

					values.push({name: subcategory.name, value: subcategory.id});
				}	
			}

			$('#subcategories')
			.dropdown('clear')
			.dropdown({values: values});
		})

		$('#files-list').modal({
			closable  : false
		})

		$('input[name="file"]').on('change', function()
		{
			app.fileId = null;
		})
  })
</script>

@endsection