@extends('back.master')

@section('title', $title)

@section('content')

<script type="application/javascript">
	'use strict';
	var parents_categories = {"0": {!! json_encode((object)$parents_posts) !!}, "1": {!! json_encode((object)$parents_products) !!}};
</script>

<form class="ui form" id="category" method="post" action={{ route('categories.store') }}>
	@csrf
	
	<div class="field">
		<button class="ui icon labeled basic button" type="submit">
		  <i class="save outline icon"></i>
		  Create
		</button>
		<a class="ui icon labeled basic button" href="{{ route('categories') }}">
			<i class="times icon"></i>
			Cancel
		</a>
	</div>

	@if($errors->any())
      @foreach ($errors->all() as $error)
         <div class="ui negative fluid small message">
         	<i class="times icon close"></i>
         	{{ $error }}
         </div>
      @endforeach
	@endif

	<div class="ui fluid divider"></div>

	<div class="field">
		<label>Name</label>
		<input type="text" name="name" required autofocus value="{{ old('name') }}">
	</div>
	
	<div class="field">
		<label>For</label>
		<select name="for" class="ui dropdown">
			<option selected>...</option>
			@foreach(['posts', 'products'] as $key => $for)
			<option value="{{ $key }}">{{ ucfirst($for) }}</option>
			@endforeach
		</select>
	</div>

	<div class="field">
		<label>Parent</label>
		<select name="parent" class="ui dropdown">
			<option value="0">-</option>
		</select>
	</div>

	<div class="field">
		<label>Range</label>
		<input type="number" name="range" value="{{ old('range') ?? 0}}">
	</div>

	<div class="field">
		<label>Description</label>
		<textarea name="description" cols="30" rows="5">{{ old('old') }}</textarea>
	</div>

</form>

<script>
	$(function()
	{
		'use strict';

		$('#category input[name="range"]').on('change', function()
		{
			if($(this).val() < 0)
				$(this).val('0');
		})

		$('select[name="for"]').on('change', function()
		{
			var parents = parents_categories[$(this).val()] || [];
			var options = '';

			for(var k in parents)
			{
				options += '<option value="'+ parents[k].id +'">'+ parents[k].name +'</option>';
			}

			$('select[name="parent"]').html(options).dropdown();
		})
	})
</script>
@endsection