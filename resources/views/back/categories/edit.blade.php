@extends('back.master')

@section('title', $title)

@section('content')

<script type="application/javascript">
	'use strict';
	var parents_categories = {"0": {!! json_encode((object)$parents_posts) !!}, "1": {!! json_encode((object)$parents_products) !!}};
</script>

<form class="ui form" id="category" method="post" 
			action={{ route('categories.update', [$category->id, request()->for]  ) }}>
	@csrf
	
	<div class="field">
		<button class="ui icon labeled basic button" type="submit">
		  <i class="save outline icon"></i>
		  Update
		</button>
		<a class="ui icon labeled basic button" href="{{ route('categories') }}">
			<i class="times icon"></i>
			Cancel
		</a>
	</div>

	@if($errors->any())
      @foreach ($errors->all() as $error)
         <div class="ui negative fluid small message">
         	<i class="times icon close"></i>
         	{{ $error }}
         </div>
      @endforeach
	@endif

	<div class="ui fluid divider"></div>

	<div class="field">
		<label>Name</label>
		<input type="text" name="name" value="{{ $category->name }}" required>
	</div>
	
	<div class="field">
		<label>For</label>
		<select name="for" class="ui dropdown">
			@foreach(['posts', 'products'] as $key => $for)
			<option value="{{ $key }}" @if($category->for == $key) selected @endif>
				{{ ucfirst($for) }}
			</option>
			@endforeach
		</select>
	</div>

	<div class="field">
		<label>Parent</label>
		<select name="parent" class="ui dropdown">
			<option value="0">-</option>
			@if($category->for == 0)
				@foreach($parents_posts as $parent)
				<option value="{{ $parent['id'] }}" @if($category->parent === $parent['id']) selected @endif>
					{{ ucfirst($parent['name']) }}
				</option>
				@endforeach
			@else
				@foreach($parents_products as $parent)
			<option value="{{ $parent['id'] }}" @if($category->parent === $parent['id']) selected @endif>
				{{ ucfirst($parent['name']) }}
			</option>
			@endforeach
			@endif
		</select>
	</div>

	<div class="field">
		<label>Range</label>
		<input type="number" name="range" value="{{ $category->range }}">
	</div>

	<div class="field">
		<label>Description</label>
		<textarea name="description" cols="30" rows="5">{{ nl2br($category->description) }}</textarea>
	</div>

</form>

<script>
	$(function()
	{
		'use strict';

		$('#category input[name="range"]').on('change', function()
		{
			if($(this).val() < 0)
				$(this).val('0');
		})

		$('select[name="for"]').on('change', function()
		{
			var parents = parents_categories[$(this).val()] || [];
			var options = '<option value="0">-</option>';

			for(var k in parents)
			{
				options += '<option value="'+ parents[k].id +'">'+ parents[k].name +'</option>';
			}

			$('select[name="parent"]').html(options).dropdown();
		})
	})
</script>
@endsection