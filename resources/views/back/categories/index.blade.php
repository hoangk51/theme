@extends('back.master')

@section('title', 'Categories')


@section('content')

<div class="row main" id="categories">
	
	<div class="ui menu shadowless">
		<a id="bulk-delete" @click="deleteItems" :href="route+ids.join()" class="item" :class="{disabled: isDisabled}">Delete</a>
		
		<div class="ui pointing dropdown link item">
			<span class="default text">{{ ucfirst(request()->for ?? 'Filter') }}</span>
			<i class="dropdown icon"></i>
			<div class="menu">
				<a href="{{ route('categories') }}" class="item">All</a>
				<a href="{{ route('categories', 'posts') }}" class="item">Posts</a>
				<a href="{{ route('categories', 'products') }}" class="item">Products</a>
			</div>
		</div>

		<div class="right menu">
			<a href="{{ route('categories.create') }}" class="item">Add</a>
		</div>
	</div>
	
	<table class="ui unstackable celled basic table">
		<thead>
			<tr>
				<th>
					<div class="ui fitted checkbox">
					  <input type="checkbox" @change="selectAll">
					  <label></label>
					</div>
				</th>
				<th>ID</th>
				<th class="five columns wide">Name</th>
				<th>Position</th>
				<th>Parent</th>
				<th>Created at</th>
				<th>Updated at</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			@foreach($categories as $category)
			<tr>
				<td class="center aligned">
					<div class="ui fitted checkbox select">
					  <input type="checkbox" value="{{ $category->id }}" @change="toogleId({{ $category->id }})">
					  <label></label>
					</div>
				</td>
				<td class="center aligned">{{ $category->id }}</td>
				<td>{{ ucfirst($category->name) }}</td>
				<td class="center aligned">{{ $category->range }}</td>
				<td class="center aligned">{{ $category->parent_name ?? '-' }}</td>
				<td class="center aligned">{{ $category->created_at }}</td>
				<td class="center aligned">{{ $category->updated_at }}</td>
				<td class="center aligned one column wide">
					<div class="ui dropdown">
						<i class="bars icon mx-0"></i>
						<div class="menu dropdown left">
							<a href="{{ route('categories.edit', ['id' => $category->id, 'for' => $category->for]) }}" class="item">Edit</a>
							<a href="{{ route('categories.destroy', ['ids' => $category->id, 'for' => $category->for]) }}" class="item">Delete</a>
						</div>
					</div>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>

</div>

<script>
	'use strict';
	
	var app = new Vue({
	  el: '#categories',
	  data: {
	  	route: '{{ route('categories.destroy', "") }}/',
	    ids: [],
	    isDisabled: true
	  },
	  methods: {
	  	toogleId: function(id)
	  	{
	  		if(this.ids.indexOf(id) >= 0)
	  			this.ids.splice(this.ids.indexOf(id), 1);
	  		else
	  			this.ids.push(id);
	  	},
	  	selectAll: function()
	  	{
	  		$('#categories tbody .ui.checkbox.select').checkbox('toggle')
	  	},
	  	deleteItems: function(e)
	  	{
	  		var confirmationMsg = 'Are you sure you want to delete the selected categorie(s) ?';

	  		if(!this.ids.length || !confirm(confirmationMsg))
	  		{
	  			e.preventDefault();
	  			return false;
	  		}
	  	}
	  },
	  watch: {
	  	ids: function(val)
	  	{
	  		this.isDisabled = !val.length;
	  	}
	  }
	})
</script>
@endsection