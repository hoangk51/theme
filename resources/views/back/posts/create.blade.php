@extends('back.master')

@section('title', $title)

@section('additional_head_tags')

<link href="{{ asset_('assets/admin/css/summernote-lite-0.8.12.css') }}" rel="stylesheet">
<script src="{{ asset_('assets/admin/js/summernote-lite-0.8.12.js') }}"></script>

@endsection


@section('content')
<form class="ui form" method="post" action="{{ route('posts.store') }}" enctype="multipart/form-data">
	@csrf

	<div class="field">
		<button class="ui icon labeled basic button" type="submit">
		  <i class="save outline icon"></i>
		  Create
		</button>
		<a class="ui icon labeled basic button" href="{{ route('posts') }}">
			<i class="times icon"></i>
			Cancel
		</a>
	</div>
	
	@if($errors->any())
      @foreach ($errors->all() as $error)
         <div class="ui negative fluid small message">
         	<i class="times icon close"></i>
         	{{ $error }}
         </div>
      @endforeach
	@endif

	<div class="ui fluid divider"></div>

	<div class="one column grid" id="post">
		<div class="column">
			<div class="field">
				<label>Name</label>
				<input type="text" name="name" placeholder="..." value="{{ old('name') }}" autofocus required>
			</div>
			<div class="field">
				<label>Cover</label>
				<div class="ui placeholder rounded-corner" onclick="this.children[1].click()">
					<div class="image">
						<img src="">
					</div>
					<input type="file" class="d-none" name="cover">
				</div>
			</div>
			<div class="field">
				<label>Category</label>
				<div class="ui selection dropdown">
				  <input type="hidden" name="category" value="{{ old('category') }}">
				  <i class="dropdown icon"></i>
				  <div class="default text">-</div>
				  <div class="menu">
				  	@foreach($categories as $category)
						<div class="item" data-value="{{ $category->id }}">
							{{ ucfirst($category->name) }}
						</div>
				  	@endforeach
				  </div>
				</div>
				<input class="mt-1" type="text" name="new_category" placeholder="Add new category ..." value="{{ old('new_category') }}">
			</div>
			<div class="field">
				<label>Short description</label>
				<textarea name="short_description" cols="30" rows="5">{{ old('short_description') }}</textarea>
			</div>
			<div class="field">
				<label>Content</label>
				<textarea name="content" required class="summernote" cols="30" rows="20">{{ old('content') }}</textarea>
			</div>
			<div class="field">
				<label>Tags</label>
				<input type="text" name="tags" value="{{ old('tags') }}">
			</div>
		</div>
	</div>
</form>

<script type="application/javascript">
	'use strict';
	
  $('.summernote').summernote({
    placeholder: '...',
    tabsize: 2,
    height: 300
  });
</script>

@endsection