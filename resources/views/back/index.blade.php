@extends('back.master')

@section('title', 'Dashboard')

@section('additional_head_tags')
<script src="{{ asset_('assets/admin/js/chart.bundle.2.9.3.min.js') }}"></script>
@endsection

@section('content')

<div class="row main" id="dashboard">

	<div class="ui four doubling cards general">

		<div class="card fluid">
			<div class="content top">
				<h3 class="header">Items</h3>
			</div>
			<div class="content bottom px-0">
				<div class="l-side">
					<div class="ui image">
						<img src="{{ asset_('assets/images/items.png') }}">
					</div>
				</div>
				<div class="r-side">
					<h3>{{ $counts->products }}</h3>
				</div>
			</div>
		</div>

		<div class="card fluid">
			<div class="content top">
				<h3 class="header">Orders</h3>
			</div>
			<div class="content bottom px-0">
				<div class="l-side">
					<div class="ui image">
						<img src="{{ asset_('assets/images/cart.png') }}">
					</div>
				</div>
				<div class="r-side">
					<h3>{{ $counts->orders }}</h3>
				</div>
			</div>
		</div>

		<div class="card fluid">
			<div class="content top">
				<h3 class="header">Earnings</h3>
			</div>
			<div class="content bottom px-0">
				<div class="l-side">
					<div class="ui image">
						<img src="{{ asset_('assets/images/dollar.png') }}">
					</div>
				</div>
				<div class="r-side">
					<h3>{{ config('payments.currency_code') .' '. number_format($counts->earnings, 2) }}</h3>
				</div>
			</div>
		</div>

		<div class="card fluid">
			<div class="content top">
				<h3 class="header">Users</h3>
			</div>
			<div class="content bottom px-0">
				<div class="l-side">
					<div class="ui image">
						<img src="{{ asset_('assets/images/users.png') }}">
					</div>
				</div>
				<div class="r-side">
					<h3>{{ $counts->users }}</h3>
				</div>
			</div>
		</div>

		<div class="card fluid">
			<div class="content top">
				<h3 class="header">Comments</h3>
			</div>
			<div class="content bottom px-0">
				<div class="l-side">
					<div class="ui image">
						<img src="{{ asset_('assets/images/comments.png') }}">
					</div>
				</div>
				<div class="r-side">
					<h3>{{ $counts->comments }}</h3>
				</div>
			</div>
		</div>

		<div class="card fluid">
			<div class="content top">
				<h3 class="header">Subscribers</h3>
			</div>
			<div class="content bottom px-0">
				<div class="l-side">
					<div class="ui image">
						<img src="{{ asset_('assets/images/subscribers.png') }}">
					</div>
				</div>
				<div class="r-side">
					<h3>{{$counts->newsletter_subscribers  }}</h3>
				</div>
			</div>
		</div>

		<div class="card fluid">
			<div class="content top">
				<h3 class="header">Categories</h3>
			</div>
			<div class="content bottom px-0">
				<div class="l-side">
					<div class="ui image">
						<img src="{{ asset_('assets/images/tag.png') }}">
					</div>
				</div>
				<div class="r-side">
					<h3>{{ $counts->categories }}</h3>
				</div>
			</div>
		</div>


		<div class="card fluid">
			<div class="content top">
				<h3 class="header">Posts</h3>
			</div>
			<div class="content bottom px-0">
				<div class="l-side">
					<div class="ui image">
						<img src="{{ asset_('assets/images/pages.png') }}">
					</div>
				</div>
				<div class="r-side">
					<h3>{{ $counts->posts }}</h3>
				</div>
			</div>
		</div>
	</div>

	<div class="ui fluid divider"></div>

	<div class="latest transactions">
		<table class="ui celled table">
			<thead>
				<tr>
					<th colspan="5" class="left aligned header"><h3>Latest transactions</h3></th>
				</tr>
				<tr>
					<th class="left aligned w-auto">Products</th>
					<th class="left aligned">Buyer</th>
					<th class="left aligned">Amount ({{ config('payments.currency_code') }})</th>
					<th class="left aligned">Processor</th>
					<th class="left aligned">Date</th>
				</tr>
			</thead>
			<tbody>
				@foreach($transactions as $transaction)
				<tr>
					<td>
						<div class="ui bulleted list">
							@foreach($transaction->products as $product)
							<a target="_blank" href="{{ route('home.product', \Illuminate\Support\Str::slug($product)) }}" class="item">
								{{ $product }}
							</a>
							@endforeach
						</div>
					</td>
					<td class="left aligned">{{ $transaction->buyer_name ?? $transaction->buyer_email }}</td>
					<td class="left aligned">{{ number_format($transaction->amount, 2) }}</td>
					<td class="left aligned">{{ $transaction->processor }}</td>
					<td class="left aligned">{{ $transaction->date }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>

	<div class="ui fluid divider"></div>

	<div class="sales chart">
		<div class="ui fluid card">
			<div class="content top">
			  <img class="left floated mini ui image mb-0" src="{{ asset_('assets/images/chart.png') }}">
			  <select class="ui dropdown right floated" id="sales-months">
					@foreach(cal_info(0)['months'] as $month)
					<option value="{{ $month }}" @if($month === date('F')) selected @endif>{{ $month }}</option>
					@endforeach
				</select>
			  <div class="header">Sales</div>
			  <div class="meta">
			    <span class="date">Sales evolution per month</span>
			  </div>
			</div>
			<div class="content">
				<div><canvas id="sales-chart" height="320" width="1284" min-width="1284"></canvas></div>
			</div>
		</div>
	</div>

	<div class="ui fluid divider"></div>

	<div class="ui two stackable cards latest">
		<div class="card fluid">
			<table class="ui celled table borderless">
				<thead>
					<tr>
						<th class="left aligned" colspan="2">Latest newsletter subscribers</th>
					</tr>
					<tr>
						<th class="left aligned w-auto">Email</th>
						<th class="left aligned">Date</th>
					</tr>
				</thead>
				<tbody>
					@foreach($newsletter_subscribers as $newsletter_subscriber)
					<tr>
						<td class="capitalize">{{ $newsletter_subscriber->email }}</td>
						<td>{{ $newsletter_subscriber->created_at }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>

		<div class="card fluid">
			<table class="ui celled table borderless">
				<thead>
					<tr>
						<th class="left aligned" colspan="3">Latest reviews</th>
					</tr>
					<tr>
						<th class="left aligned w-auto">Product</th>
						<th class="left aligned">Review</th>
						<th>Date</th>
					</tr>
				</thead>
				<tbody>
					@foreach($reviews as $review)
					<tr>
						<td><a href="{{ route('home.product', $review->product_slug.'#reviews') }}">{{ $review->product_name }}</a></td>
						<td><div class="ui star small rating" data-rating="{{ $review->rating }}" data-max-rating="5"></div></td>
						<td>{{ $review->created_at }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

	<script type="application/javascript">
		'use strict';
		
		var ctx1 = document.getElementById("sales-chart").getContext('2d');

		window.chart = new Chart(ctx1, {
			type: 'bar',
			responsive: false,
			data: {
				labels: {!! json_encode(range(1, date('t'))) !!}, // days
				datasets: [{
					label: 'Sales',
					backgroundColor: '#EDEDED',
					data: {!! json_encode($sales) !!}, // Sales
					borderWidth: 0
				}]
			},
			options: {
				tooltips: {
					mode: 'index',
					intersect: false,
					backgroundColor: '#fff',
					cornerRadius: 0,
					bodyFontColor: '#000',
					titleFontColor: '#000',
					legendColorBackground: '#000'
				},
				legend: {
					display: false,
				},
				responsive: false,
				maintainAspectRatio: false,
				scales: {
					xAxes: [{
						stacked: true,
					}],
					yAxes: [{
						stacked: false,
						ticks: {
	            stepSize: 1,
	            min: 0
	          }
					}]
				}
			}
		});

		$('#sales-months').on('change', function()
		{
			$.post('{{ route('admin.update_sales_chart') }}', {month: $(this).val()}, null, 'json')
			.done(function(res)
			{
				chart.data.labels = res.labels;
				chart.data.datasets[0]['data'] = res.data;

				chart.update();
			})
			.fail(function()
			{
				alert('Failed to update sales chart')
			})
		})
	</script>

</div>

@endsection