@extends('back.master')

@section('title', 'General settings')


@section('content')

<form class="ui form" method="post" spellcheck="false" action="{{ route('settings.update', 'general') }}" enctype="multipart/form-data">


	<div class="field">
		<button type="submit" class="ui basic labeled icon button mx-0">
		  <i class="save outline icon mx-0"></i>
		  Update
		</button>
	</div>

	@if($errors->any())
      @foreach ($errors->all() as $error)
         <div class="ui negative fluid small message">
         	<i class="times icon close"></i>
         	{{ $error }}
         </div>
      @endforeach
	@endif

	<div class="ui fluid divider"></div>

	<div class="one column grid" id="settings">
		<div class="column">
			<div class="field">
				<label>Name</label>
				<input type="text" name="name" placeholder="..." value="{{ old('name', $settings->name ?? null) }}">
			</div>

			<div class="field">
				<label>Title</label>
				<input type="text" name="title" placeholder="..." value="{{ old('title', $settings->title ?? null) }}">
			</div>
		
			<div class="field">
				<label>Description</label>
				<textarea name="description" cols="30" rows="5">{{ old('description', $settings->description ?? null) }}</textarea>
			</div>

			<div class="field">
				<label>Email</label>
				<input type="email" name="email" placeholder="..." value="{{ old('email', $settings->email ?? null) }}">
			</div>

			<div class="field">
				<label>Keywords</label>
				<input type="text" name="keywords" placeholder="..." value="{{ old('keywords', $settings->keywords ?? null) }}">
			</div>

			<div class="field">
				<label>Items per page</label>
				<input type="number" name="items_per_page" value="{{ old('items_per_page', $settings->items_per_page ?? null) }}">
			</div>

			<div class="field">
				<label>Blog</label>
				<input type="text" name="blog[title]" placeholder="blog title" value="{{ old('blog.title', $settings->blog->title ?? null) }}">
				<textarea name="blog[description]" cols="30" rows="5" class="mt-1">{{ old('blog.description', $settings->blog->description ?? null) }}</textarea>
			</div>

			<div class="field">
				<label>Search panel headers</label>
				<input type="text" name="search_header" placeholder="Header..." value="{{ old('search_header', $settings->search_header ?? null) }}">
				<input type="text" name="search_subheader" placeholder="Subheader..." value="{{ old('search_subheader', $settings->search_subheader ?? null) }}" class="mt-1">
			</div>
			
			<div class="field">
				<label>Facebook APP ID</label>
				<input type="text" name="fb_app_id" placeholder="Header..." value="{{ old('fb_app_id', $settings->fb_app_id ?? null) }}">
			</div>

			<div class="field">
				<label>Timezone</label>
				<div class="ui dropdown selection">
					<input type="hidden" name="timezone" value="{{ old('timezone', $settings->timezone ?? 'UTC') }}">
					<div class="default text">...</div>
					<div class="menu">
						@foreach(config('app.timezones') as $key => $val)
						<div class="item" data-value="{{ $key }}">{{ $key }} - {{ $val }}</div>
						@endforeach
					</div>
				</div>
			</div>

			<table class="ui celled unstackable table">
				<thead>
					<tr>
						<th colspan="2">Social links</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="one column wide">Facebook</td>
						<td><input type="text" name="facebook" value="{{ old('facebook', $settings->facebook ?? null ) }}"></td>
					</tr>
					<tr>
						<td class="one column wide">Twitter</td>
						<td><input type="text" name="twitter" value="{{ old('twitter', $settings->twitter ?? null ) }}"></td>
					</tr>
					<tr>
						<td class="one column wide">Pinterest</td>
						<td><input type="text" name="pinterest" value="{{ old('pinterest', $settings->pinterest ?? null ) }}"></td>
					</tr>
					<tr>
						<td class="one column wide">Youtube</td>
						<td><input type="text" name="youtube" value="{{ old('youtube', $settings->youtube ?? null ) }}"></td>
					</tr>
					<tr>
						<td class="one column wide">Tumblr</td>
						<td><input type="text" name="tumblr" value="{{ old('tumblr', $settings->tumblr ?? null ) }}"></td>
					</tr>
				</tbody>
			</table>
		
			<table class="ui celled unstackable table">
				<thead>
					<tr>
						<th>Favicon</th>
						<th>Logo</th>
						<th>Website Cover</th>
						<th>Search Cover</th>
						<th>Blog Cover</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<button class="ui basic fluid button" type="button" onclick="this.nextElementSibling.click()">Browse</button>
							<input type="file" class="d-none" name="favicon" accept="image/*">
						</td>
						<td>
							<button class="ui basic fluid button" type="button" onclick="this.nextElementSibling.click()">Browse</button>
							<input type="file" class="d-none" name="logo" accept="image/*">
						</td>
						<td>
							<button class="ui basic fluid button" type="button" onclick="this.nextElementSibling.click()">Browse</button>
							<input type="file" class="d-none" name="cover" accept="image/*">
						</td>
						<td>
							<div id="search-cover">
								<button class="ui basic button" type="button" onclick="this.nextElementSibling.click()">Browse</button>
								<input type="file" class="d-none" name="search_cover" accept="image/*">

								@if($settings->search_cover)
								<button class="ui basic icon button mr-0" type="button" id="delete-search-cover" title="Delete search cover">
									<i class="trash alternate outline icon mx-0"></i>
								</button>
								@endif
							</div>
							<input class="mt-1" type="text" name="search_cover_color" placeholder="HEX or RGBA color" value="{{ old('search_cover_color', $settings->search_cover_color ?? null ) }}">
						</td>
						<td>
							<button class="ui basic fluid button" type="button" onclick="this.nextElementSibling.click()">Browse</button>
							<input type="file" class="d-none" name="blog_cover" accept="image/*">
						</td>
					</tr>
				</tbody>
			</table>
			
			<div class="field">
				<label>Environment</label>
				<div class="ui dropdown selection">
					<input type="hidden" name="env" value="{{ old('env', $settings->env ?? 'production') }}">
					<div class="default text">...</div>
					<div class="menu">
						<div class="item" data-value="production">Production</div>
						<div class="item" data-value="development">Development</div>
					</div>
				</div>
			</div>

			<div class="field">
				<label>Mode Debug</label>
				<div class="ui dropdown selection">
					<input type="hidden" name="debug" value="{{ old('debug', $settings->debug ?? '1') }}">
					<div class="default text">...</div>
					<div class="menu">
						<div class="item" data-value="1">On</div>
						<div class="item" data-value="0">Off</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

<script type="application/javascript">
	'use strict';

	$(function()
	{
		$('#delete-search-cover').click(function()
		{
			if(!confirm('Are you sure you want to delete the search cover ?'))
				return;

			var _this = $(this);

			$.post('{{ route('settings.remove_search_cover') }}', {}, null, 'json')
			.done(function(res)
			{
				if(res.success)
				{
					_this.remove();
				}
			})
		})
	})
</script>
@endsection