@extends('back.master')

@section('title', 'Mailer settings')


@section('content')

<form class="ui form" method="post" spellcheck="false" action="{{ route('settings.update', 'mailer') }}">

	<div class="field">
		<button type="submit" class="ui basic labeled icon button mx-0">
		  <i class="save outline icon mx-0"></i>
		  Update
		</button>
	</div>

	@if($errors->any())
    @foreach ($errors->all() as $error)
      <div class="ui negative fluid small message">
       	<i class="times icon close"></i>
       	{{ $error }}
      </div>
    @endforeach
	@endif

	<div class="ui fluid divider"></div>

	<div class="one column grid" id="settings">
		<div class="ui two stackable cards mt-1">
			<div class="ui card mt-0">
				<div class="content">
					<h3 class="header">
						<i class="circular blue envelope outline icon mr-1" title="Sender"></i>MAIL
					</h3>
				</div>
				
				<div class="content">
					<div class="field">
						<label>User</label>
						<input type="text" name="mailer[mail][username]" placeholder="..." value="{{ old('mailer.mail.username', $settings->mail->username ?? null) }}">
					</div>
				
					<div class="field">
						<label>Password</label>
						<input type="text" name="mailer[mail][password]" placeholder="..." value="{{ old('mailer.mail.password', $settings->mail->password ?? null) }}">
					</div>

					<div class="field">
						<label>Host</label>
						<input type="text" name="mailer[mail][host]" placeholder="..." value="{{ old('mailer.mail.host', $settings->mail->host ?? null) }}">
					</div>

					<div class="field">
						<label>Port</label>
						<input type="text" name="mailer[mail][port]" placeholder="..." value="{{ old('mailer.mail.port', $settings->mail->port ?? null) }}">
					</div>
					
					<div class="field">
						<label>Encryption</label>
						<input type="text" name="mailer[mail][encryption]" placeholder="..." value="{{ old('mailer.mail.encryption', $settings->mail->encryption ?? null) }}">
					</div>

					<div class="field">
						<label>Reply to <i class="exclamation circle icon" title="Optional email address you want users to send their messages to when they want to reply to your messages."></i></label>
						<input type="email" name="mailer[mail][reply_to]" placeholder="example@gmail" value="{{ old('mailer.mail.reply_to', $settings->mail->reply_to ?? null) }}">
					</div>

					<div class="field">
						<label>Email alt <i class="exclamation circle icon" title="Optional email address to which you want to receive copies of contact messages. It is not nessecary if you are already using Gmail smtp server or other smtp server where you get notified about new messages."></i></label>
						<input type="email" name="mailer[mail][alt]" placeholder="example@gmail" value="{{ old('mailer.mail.alt', $settings->mail->alt ?? null) }}">
					</div>
				</div>
			</div>


			<div class="ui card mt-0">
				<div class="content">
					<h3 class="header">
						<i class="circular blue inbox icon mr-1" title="Inbox"></i>IMAP

						<div class="checkbox-wrapper">
							<div class="ui fitted toggle checkbox">
						    <input 
						    	type="checkbox" 
						    	name="mailer[imap][enabled]"
						    	@if(!empty(old('mailer.imap.enabled')))
									{{ old('mailer.imap.enabled') ? 'checked' : '' }}
									@else
									{{ ($settings->imap->enabled ?? null) ? 'checked' : '' }}
						    	@endif
						    >
						    <label></label>
						  </div>
						</div>
					</h3>
				</div>
				
				<div class="content">
					<div class="field">
						<label>User</label>
						<input type="text" name="mailer[imap][username]" placeholder="..." value="{{ old('mailer.imap.username', $settings->imap->username ?? null) }}">
					</div>
				
					<div class="field">
						<label>Password</label>
						<input type="text" name="mailer[imap][password]" placeholder="..." value="{{ old('mailer.imap.password', $settings->imap->password ?? null) }}">
					</div>

					<div class="field">
						<label>Host</label>
						<input type="text" name="mailer[imap][host]" placeholder="..." value="{{ old('mailer.imap.host', $settings->imap->host ?? null) }}">
					</div>

					<div class="field">
						<label>Port</label>
						<input type="text" name="mailer[imap][port]" placeholder="..." value="{{ old('mailer.imap.port', $settings->imap->port ?? null) }}">
					</div>
					
					<div class="field">
						<label>Encryption</label>
						<input type="text" class="uppercase" name="mailer[imap][encryption]" placeholder="ssl/tls" value="{{ old('mailer.imap.encryption', $settings->imap->encryption ?? null) }}">
					</div>

					<div class="field">
						<label>Validate Cert</label>
						<div  class="ui selection dropdown">
							<input type="hidden" name="mailer[imap][validate_cert]" value="{{ old('mailer.imap.validate_cert', $settings->imap->validate_cert ?? "false") }}">
							<div class="default text">...</div>
							<div class="menu">
								<div class="item" data-value="true" default>True</div>
								<div class="item" data-value="false">False</div>
							</div>
						</div>
					</div>

					<div class="field">
						<label>Account</label>
						<input type="text" class="capitalize" name="mailer[imap][account]" placeholder="..." value="{{ old('mailer.imap.account', $settings->imap->account ?? 'default') }}">
					</div>

					<div class="field">
						<label>Protocol</label>
						<input type="text" class="uppercase" name="mailer[imap][protocol]" placeholder="..." value="{{ old('mailer.imap.protocol', $settings->imap->protocol ?? 'imap') }}">
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

@endsection