@extends('back.master')

@section('title', 'Files host settings')



@section('additional_head_tags')

<script type="application/javascript" src="https://apis.google.com/js/platform.js" async defer></script>

<script type="application/javascript">
	'use strict';
	
  var googleDriveCode2AcessTokenRoute = '{{ route('google_drive_get_refresh_token') }}';
  var googleDriveCurrentUserRoute     = '{{ route('google_drive_get_current_user') }}';
  var dropboxCurrentAccount 					= '{{ route('dropbox_get_current_user') }}';
  var dropBoxRedirectUri 							= '{{ url()->current() }}';
</script>

@endsection



@section('content')

<form class="ui form" method="post" spellcheck="false" action="{{ route('settings.update', 'files_host') }}">

	<div class="field">
		<button type="submit" class="ui basic labeled icon button mx-0">
		  <i class="save outline icon mx-0"></i>
		  Update
		</button>
	</div>

	@if($errors->any())
      @foreach ($errors->all() as $error)
         <div class="ui negative fluid small message">
         	<i class="times icon close"></i>
         	{{ $error }}
         </div>
      @endforeach
	@endif

	<div class="ui fluid divider mb-0"></div>
	
	<div class="one column grid" id="settings">
		<div class="ui fluid card mt-1">
			<div class="content">
				<div class="header">{{ __('Working with') }}</div>
			</div>
			<div class="content">
				<div class="field">
		      <div class="ui radio checkbox">
		        <input type="radio" name="working_with" @if($settings->working_with === 'files') checked @endif value="files">
		        <label>{{ __('Files') }}</label>
		      </div>
		    </div>
		    <div class="field">
		      <div class="ui radio checkbox">
		        <input type="radio" name="working_with" @if($settings->working_with === 'folders') checked @endif value="folders">
		        <label>{{ __('Folders') }}</label>
		      </div>
		    </div>
			</div>
		</div>

		<div class="ui two stackable cards mt-1">
			<div class="ui card mt-0">

				<div class="content googledrive">
					<h3 class="header">
						<i class="circular blue google drive icon mr-1"></i>Google Drive

						<div class="checkbox-wrapper">
							<div class="ui fitted toggle checkbox">
						    <input 
						    	type="checkbox" 
						    	name="google_drive[enabled]"
						    	@if(!empty(old('google_drive.enabled')))
									{{ old('google_drive.enabled') ? 'checked' : '' }}
									@else
									{{ ($settings->google_drive->enabled ?? null) ? 'checked' : '' }}
						    	@endif
						    >
						    <label></label>
						  </div>
						</div>
					</h3>
				</div>
				
				<div class="content">
					@if($settings->google_drive->connected_email ?? null)
					<div class="ui basic green message p-1-hf">
						<span class="ui basic green label">{{ ucfirst($settings->google_drive->connected_email ?? 'Null') }}</span>	
					</div>
					@endif
					
					<div class="field">
						<label>App Default Folder ID (Optional)</label>
						<input type="text" name="google_drive[folder_id]" value="{{ old('google_drive.folder_id', $settings->google_drive->folder_id ?? null) }}" placeholder="E.g. 1FREVJPb_R_cdNbpsfo2plpYlzerfEGV9">
					</div>

					<div class="field">
						<label>API key</label>
						<input type="text" name="google_drive[api_key]" placeholder="..." value="{{ old('google_drive.api_key', $settings->google_drive->api_key ?? null) }}">
					</div>

					<div class="field">
						<label>Client ID</label>
						<input type="text" name="google_drive[client_id]" placeholder="..." value="{{ old('google_drive.client_id', $settings->google_drive->client_id ?? null) }}">
					</div>

					<div class="field">
						<label>Secret ID</label>
						<input type="text" name="google_drive[client_secret]" placeholder="..." value="{{ old('google_drive.client_secret', $settings->google_drive->client_secret ?? null) }}">
					</div>

					<div class="field">
						<label>Select account</label>
						<button class="ui fluid grey button" type="button" onclick="authorizeGooglDriveApp()">Connect</button>
					</div>

					<div class="field">
						<label>Refresh token</label>
						<input type="text" name="google_drive[refresh_token]" readonly value="{{ old('google_drive.refresh_token', $settings->google_drive->refresh_token ?? null) }}">

						<input type="hidden" name="google_drive[connected_email]" readonly value="{{ old('google_drive.connected_email', $settings->google_drive->connected_email ?? null) }}">
						<input type="hidden" name="google_drive[id_token]" readonly value="{{ old('google_drive.id_token', $settings->google_drive->id_token ?? null) }}">
					</div>
				</div>
			</div>

			<div class="ui card mt-0">
				<div class="content dropbox">
					<h3 class="header">
						<i class="circular blue dropbox icon mr-1"></i>DropBox

						<div class="checkbox-wrapper">
							<div class="ui fitted toggle checkbox">
						    <input 
						    	type="checkbox" 
						    	name="dropbox[enabled]"
						    	@if(!empty(old('dropbox.enabled')))
									{{ old('dropbox.enabled') ? 'checked' : '' }}
									@else
									{{ ($settings->dropbox->enabled ?? null) ? 'checked' : '' }}
						    	@endif
						    >
						    <label></label>
						  </div>
						</div>

					</h3>
				</div>

				<div class="content">
					@if($settings->dropbox->current_account ?? null)
					<div class="ui basic green message p-1-hf">
						<span class="ui basic green label">{{ ucfirst($settings->dropbox->current_account ?? 'Null') }}</span>	
					</div>
					@endif


					<div class="field">
						<label>App Default Folder Path (Optional)</label>
						<input type="text" name="dropbox[folder_path]" value="{{ old('dropbox.folder_path', $settings->dropbox->folder_path ?? null) }}" placeholder="/PATH">
					</div>

					<div class="field">
						<label>App key</label>
						<input type="text" name="dropbox[app_key]" placeholder="..." value="{{ old('dropbox.app_key', $settings->dropbox->app_key ?? null) }}">
					</div>

					<div class="field">
						<label>App secret</label>
						<input type="text" name="dropbox[app_secret]" placeholder="..." value="{{ old('dropbox.app_secret', $settings->dropbox->app_secret ?? null) }}">
					</div>

					<div class="field">
						<label>Select account</label>
						<button class="ui grey button fluid" type="button" onclick="authorizeDropBoxApp()">Connect</button>
					</div>

					<div class="field">
						<label>Access token</label>
						<input type="text" name="dropbox[access_token]" value="{{ old('dropbox.access_token', $settings->dropbox->access_token ?? null) }}">

						<input type="hidden" name="dropbox[current_account]" value="{{ old('dropbox.current_account', $settings->dropbox->current_account ?? null) }}">
					</div>
				</div>
			</div>
		</div>

	</div>
</form>

<script>
	'use strict';

	$('#settings .ui.checkbox').checkbox();
</script>
@endsection