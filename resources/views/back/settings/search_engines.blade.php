@extends('back.master')

@section('title', 'Search engines settings')


@section('content')

<form class="ui form" method="post" spellcheck="false" action="{{ route('settings.update', 'search_engines') }}">


	<div class="field">
		<button type="submit" class="ui basic labeled icon button mx-0">
		  <i class="save outline icon mx-0"></i>
		  Update
		</button>
	</div>

	@if($errors->any())
      @foreach ($errors->all() as $error)
         <div class="ui negative fluid small message">
         	<i class="times icon close"></i>
         	{{ $error }}
         </div>
      @endforeach
	@endif

	<div class="ui fluid divider"></div>

	<div class="one column grid" id="settings">
		<div class="column">

			<div class="field">
				<label>Site verification</label>

				<input type="text" name="google" placeholder="Gogle code..." value="{{ old('google', $settings->google ?? null) }}">

				<input class="mt-1" type="text" name="bing" placeholder="Bing code..." value="{{ old('bing', $settings->bing ?? null) }}">

				<input class="mt-1" type="text" name="yandex" placeholder="Yandex code..." value="{{ old('yandex', $settings->yandex ?? null) }}">
			</div>

			<div class="field">
				<label>Google analytics</label>
				<textarea name="google_analytics" cols="30" rows="5" placeholder="...">{{ old('google_analytics', $settings->google_analytics ?? null) }}</textarea>
			</div>
		
			<div class="field">
				<label>Robots</label>
				<div class="ui dropdown selection">
					<input type="hidden" name="robots" value="{{ old('robots', $settings->robots ?? 'follow, index') }}">
					<div class="default text">...</div>
					<div class="menu">
						<div class="item" data-value="follow, index">Follow and Index</div>
						<div class="item" data-value="follow, noindex">Follow but do not Index</div>
						<div class="item" data-value="nofollow, index">Do not Follow but Index</div>
						<div class="item" data-value="nofollow, noindex">Do not Follow and do not Index</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</form>

@endsection