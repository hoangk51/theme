@extends('back.master')

@section('title', 'Payments settings')


@section('content')

<form class="ui form" method="post" spellcheck="false" action="{{ route('settings.update', 'payments') }}">

	<div class="field">
		<button type="submit" class="ui basic labeled icon button mx-0">
		  <i class="save outline icon mx-0"></i>
		  Update
		</button>
	</div>

	@if($errors->any())
      @foreach ($errors->all() as $error)
         <div class="ui negative fluid small message">
         	<i class="times icon close"></i>
         	{{ $error }}
         </div>
      @endforeach
	@endif

	<div class="ui fluid divider mb-0"></div>
	
	<div class="one column grid" id="settings">

		<!-- PAYPAL -->
		<div class="ui fluid card mt-1">
			<div class="content">
				<h3 class="header">
					<img src="{{ asset_('assets/images/paypal_icon.png') }}" alt="PayPal" class="ui small avatar mr-1">PayPal
					
					<div class="checkbox-wrapper">
						<div class="ui fitted toggle checkbox">
					    <input 
					    	type="checkbox" 
					    	name="paypal[enabled]"
					    	@if(!empty(old('paypal.enabled')))
								{{ old('paypal.enabled') ? 'checked' : '' }}
								@else
								{{ ($settings->paypal->enabled ?? null) ? 'checked' : '' }}
					    	@endif
					    >
					    <label></label>
					  </div>
					</div>
				</h3>
			</div>

			<div class="content">
				<div class="field">
					<label>Mode</label>
					<div  class="ui selection dropdown">
						<input type="hidden" name="paypal[mode]" value="{{ old('paypal_mode', $settings->paypal->mode ?? 'sandbox') }}">
						<div class="default text">Select Mode</div>
						<div class="menu">
							<div class="item" data-value="sandbox" default>Sandbox</div>
							<div class="item" data-value="live">Live</div>
						</div>
					</div>
				</div>

				<div class="field">
					<label>Client ID</label>
					<input type="text" name="paypal[client_id]" placeholder="..." value="{{ old('paypal.client_id', $settings->paypal->client_id ?? null) }}">
				</div>

				<div class="field">
					<label>Secret ID</label>
					<input type="text" name="paypal[secret_id]" placeholder="..." value="{{ old('paypal.secret_id', $settings->paypal->secret_id ?? null) }}">
				</div>

				<div class="field">
					<label>Transaction Fee</label>
					<input type="number" step="0.01" name="paypal[fee]" placeholder="..." value="{{ old('paypal.fee', $settings->paypal->fee ?? null) }}">
				</div>
			</div>
		</div>

		<!-- STRIPE -->
		<div class="ui fluid card">
			<div class="content">
				<h3 class="header">
					<i class="circular blue inverted stripe s icon mr-1"></i>Stripe

					<div class="checkbox-wrapper">
						<div class="ui fitted toggle checkbox">
					    <input 
					    	type="checkbox" 
					    	name="stripe[enabled]"
					    	@if(!empty(old('stripe.enabled')))
								{{ old('stripe.enabled') ? 'checked' : '' }}
								@else
								{{ ($settings->stripe->enabled ?? null) ? 'checked' : '' }}
					    	@endif
					    >
					    <label></label>
					  </div>
					</div>

				</h3>
			</div>

			<div class="content">
				<div class="field">
					<label>Mode</label>
					<div  class="ui selection dropdown">
						<input type="hidden" name="stripe[mode]" value="{{ old('stripe.mode', $settings->stripe->mode ?? 'sandbox') }}">
						<div class="default text">Select Mode</div>
						<div class="menu">
							<div class="item" data-value="sandbox" default>Sandbox</div>
							<div class="item" data-value="live">Live</div>
						</div>
					</div>
				</div>

				<div class="field">
					<label>Client ID</label>
					<input type="text" name="stripe[client_id]" placeholder="..." value="{{ old('stripe.client_id', $settings->stripe->client_id ?? null) }}">
				</div>

				<div class="field">
					<label>Secret ID</label>
					<input type="text" name="stripe[secret_id]" placeholder="..." value="{{ old('stripe.secret_id', $settings->stripe->secret_id ?? null) }}">
				</div>

				<div class="field">
					<label>Transaction Fee</label>
					<input type="number" step="0.01" name="stripe[fee]" placeholder="..." value="{{ old('stripe.fee', $settings->stripe->fee ?? null) }}">
				</div>
			</div>
		</div>

		<!-- RAZOPAY -->
		<div class="ui fluid card mt-1">
			<div class="content">
				<h3 class="header">
					<img src="{{ asset_('assets/images/razorpay_icon.png') }}" alt="razorpay" class="ui small avatar mr-1">Razorpay
					
					<div class="checkbox-wrapper">
						<div class="ui fitted toggle checkbox">
					    <input 
					    	type="checkbox" 
					    	name="razorpay[enabled]"
					    	@if(!empty(old('razorpay.enabled')))
								{{ old('razorpay.enabled') ? 'checked' : '' }}
								@else
								{{ ($settings->razorpay->enabled ?? null) ? 'checked' : '' }}
					    	@endif
					    >
					    <label></label>
					  </div>
					</div>
				</h3>
			</div>

			<div class="content">
				<div class="field">
					<label>Key ID</label>
					<input type="text" name="razorpay[client_id]" placeholder="..." value="{{ old('razorpay.client_id', $settings->razorpay->client_id ?? null) }}">
				</div>

				<div class="field">
					<label>Key Secret</label>
					<input type="text" name="razorpay[secret_id]" placeholder="..." value="{{ old('razorpay.secret_id', $settings->razorpay->secret_id ?? null) }}">
				</div>

				<div class="field">
					<label>Transaction Fee</label>
					<input type="number" step="0.01" name="razorpay[fee]" placeholder="..." value="{{ old('razorpay.fee', $settings->razorpay->fee ?? null) }}">
				</div>
			</div>
		</div>


		<!-- IYZICO -->
		<div class="ui fluid card mt-1">
			<div class="content">
				<h3 class="header">
					<img src="{{ asset_('assets/images/iyzico_icon.png') }}" alt="iyzico" class="ui small avatar mr-1">Iyzico
					
					<div class="checkbox-wrapper">
						<div class="ui fitted toggle checkbox">
					    <input 
					    	type="checkbox" 
					    	name="iyzico[enabled]"
					    	@if(!empty(old('iyzico.enabled')))
								{{ old('iyzico.enabled') ? 'checked' : '' }}
								@else
								{{ ($settings->iyzico->enabled ?? null) ? 'checked' : '' }}
					    	@endif
					    >
					    <label></label>
					  </div>
					</div>
				</h3>
			</div>

			<div class="content">
				<div class="field">
					<label>Mode</label>
					<div  class="ui selection dropdown">
						<input type="hidden" name="iyzico[mode]" value="{{ old('iyzico', $settings->iyzico->mode ?? 'sandbox') }}">
						<div class="default text">Select Mode</div>
						<div class="menu">
							<div class="item" data-value="sandbox" default>Sandbox</div>
							<div class="item" data-value="live">Live</div>
						</div>
					</div>
				</div>

				<div class="field">
					<label>Key ID</label>
					<input type="text" name="iyzico[client_id]" placeholder="..." value="{{ old('iyzico.client_id', $settings->iyzico->client_id ?? null) }}">
				</div>

				<div class="field">
					<label>Key Secret</label>
					<input type="text" name="iyzico[secret_id]" placeholder="..." value="{{ old('iyzico.secret_id', $settings->iyzico->secret_id ?? null) }}">
				</div>

				<div class="field">
					<label>Transaction Fee</label>
					<input type="number" step="0.01" name="iyzico[fee]" placeholder="..." value="{{ old('iyzico.fee', $settings->iyzico->fee ?? null) }}">
				</div>
			</div>
		</div>


		<!-- SKRILL -->
		<div class="ui fluid card">
				<div class="content">
					<h3 class="header">
						<img src="{{ asset_('assets/images/skrill_icon.png') }}" alt="Skrill" class="ui small avatar mr-1">Skrill

						<div class="checkbox-wrapper">
							<div class="ui fitted toggle checkbox">
						    <input 
						    	type="checkbox" 
						    	name="skrill[enabled]"
						    	@if(!empty(old('skrill.enabled')))
									{{ old('skrill.enabled') ? 'checked' : '' }}
									@else
									{{ ($settings->skrill->enabled ?? null) ? 'checked' : '' }}
						    	@endif
						    >
						    <label></label>
						  </div>
						</div>

					</h3>
				</div>

				<div class="content">
					<div class="field">
						<label>Merchant account</label>
						<input type="text" name="skrill[merchant_account]" placeholder="..." value="{{ old('skrill.merchant_account', $settings->skrill->merchant_account ?? null) }}">
					</div>

					<div class="field">
						<label>MQI/API secret word</label>
						<input type="text" name="skrill[mqiapi_secret_word]" placeholder="..." value="{{ old('skrill.mqiapi_secret_word', $settings->skrill->mqiapi_secret_word ?? null) }}">
					</div>

					<div class="field">
						<label>MQI/API password</label>
						<input type="text" name="skrill[mqiapi_password]" placeholder="..." value="{{ old('skrill.mqiapi_password', $settings->skrill->mqiapi_password ?? null) }}">
					</div>

					<div class="field">
						<label>Transaction Fee</label>
						<input type="number" step="0.01" name="skrill[fee]" placeholder="..." value="{{ old('skrill.fee', $settings->skrill->fee ?? null) }}">
					</div>
				</div>
		</div>
		
		<div class="three fields mt-1">
			<div class="field">
				<label>VAT (%)</label>
				<input type="number" step="0.01" name="vat" value="{{ old('vat', $settings->vat ?? null) }}">
			</div>

			<div class="field">
				<label>Currency code</label>
				<input type="text" name="currency_code" value="{{ old('currency_code', $settings->currency_code ?? null) }}">
			</div>

			<div class="field">
				<label>Currency symbol</label>
				<input type="text" name="currency_symbol" value="{{ old('currency_symbol', $settings->currency_symbol ?? null) }}">
			</div>
		</div>
	</div>
</form>

@endsection