@extends('back.master')

@section('title', 'Adverts settings')


@section('content')

<form class="ui form" spellcheck="false" method="post" action="{{ route('settings.update', 'adverts') }}">

	<div class="field">
		<button type="submit" class="ui basic labeled icon button mx-0">
		  <i class="save outline icon mx-0"></i>
		  Update
		</button>
	</div>

	@if($errors->any())
      @foreach ($errors->all() as $error)
         <div class="ui negative fluid small message">
         	<i class="times icon close"></i>
         	{{ $error }}
         </div>
      @endforeach
	@endif

	<div class="ui fluid divider"></div>

	<div class="one column grid" id="settings">
		<div class="column">

			<div class="field">
				<label>Responsive ad unit</label>
				<textarea name="responsive_ad" cols="30" rows="3" placeholder="...">{{ old('responsive_ad', $settings->responsive_ad ?? null) }}</textarea>
			</div>

			<div class="field">
				<label>Auto ad unit</label>
				<textarea name="auto_ad" cols="30" rows="3" placeholder="...">{{ old('auto_ad', $settings->auto_ad ?? null) }}</textarea>
			</div>
		
			<div class="field">
				<label>In feed ad unit</label>
				<textarea name="in_feed_ad" cols="30" rows="3" placeholder="...">{{ old('in_feed_ad', $settings->in_feed_ad ?? null) }}</textarea>
			</div>

			<div class="field">
				<label>Link ad unit</label>
				<textarea name="link_ad" cols="30" rows="3" placeholder="...">{{ old('link_ad', $settings->link_ad ?? null) }}</textarea>
			</div>

			<div class="field">
				<label>728x90 ad unit</label>
				<textarea name="ad_728x90" cols="30" rows="3" placeholder="...">{{ old('ad_728x90', $settings->ad_728x90 ?? null) }}</textarea>
			</div>

			<div class="field">
				<label>468x60 ad unit</label>
				<textarea name="ad_468x60" cols="30" rows="3" placeholder="...">{{ old('ad_468x60', $settings->ad_468x60 ?? null) }}</textarea>
			</div>

			<div class="field">
				<label>250x250 ad unit</label>
				<textarea name="ad_250x250" cols="30" rows="3" placeholder="...">{{ old('ad_250x250', $settings->ad_250x250 ?? null) }}</textarea>
			</div>

			<div class="field">
				<label>320x100 ad unit</label>
				<textarea name="ad_320x100" cols="30" rows="3" placeholder="...">{{ old('ad_320x100', $settings->ad_320x100 ?? null) }}</textarea>
			</div>

		</div>
	</div>
</form>

@endsection