@extends('back.master')

@section('title', $title)

@section('content')

<div id="transactions" class="srtipe">

	<div class="ui two stackable cards">
		<div class="ui fluid card shadowless">
			<div class="content">
				<h3 class="header">{{ __('Transaction details') }}</h3>
			</div>
			<div class="content bg-ghostwhite">
				<strong class="status">Status :</strong> 
					<span class="ui basic @if($amount_refunded) red @else green @endif label">
						@if($amount_refunded) 
						{{ strtoupper(($amount_refunded < $net_amount ? __('Partial') : __('Full')) .' - '.__('Refund')) }}
						@else
						{{ __('Complete') }}
						@endif
					</span>
			</div>
			<div class="content bg-ghostwhite">
				<strong>{{ __('ID') }} :</strong> 
				<span>{{ $details->id }}</span>
			</div>
			<div class="content bg-ghostwhite">
				<strong>@if($created_at == $updated_at) Created @else Updated @endif :</strong> 
				<span>{{ (new DateTime($updated_at))->format("d F Y \a\\t h:i:s A") }}</span>
			</div>
			<div class="content">
				<strong>{{ __('Total items') }} :</strong> 
				<span>
					<strong>
						{{ $amount + $discount.' '.config('payments.currency_code') }} 
					</strong>
				</span>
			</div>
			<div class="content">
				<strong>{{ __('Discount') }} :</strong> 
				<span class="minus">
					{{ $discount.' '.config('payments.currency_code') }}
				</span>
			</div>
			<div class="content">
				<strong>{{ __('Handling') }} :</strong> 
				<span class="plus">
					{{ $handling.' '.config('payments.currency_code')  }}
				</span>
			</div>
			<div class="content">
				<strong>{{ __('Tax') }} :</strong> 
				<span class="plus">
					{{ $tax.' '.config('payments.currency_code') }}
				</span>
			</div>
			<div class="content">
				<strong>{{ __('Gross amount') }} :</strong> 
				<span>
					<strong>
						{{ number_format($amount + $handling + $tax, 2).' '.config('payments.currency_code') }}
					</strong>
				</span>
      </div>
      <div class="content">
				<strong>{{ __('Iyzico Fee') }} :</strong> 
				<span class="minus">
					{{ $fee.' '.config('payments.currency_code') }}
				</span>
			</div>
			<div class="content">
				<strong>{{ __('Net amount') }} :</strong> 
				<span>
					<strong>
						{{ $net_amount.' '.config('payments.currency_code') }}
					</strong>
				</span>
			</div>
			
			<div class="content bg-ghostwhite">
				<strong>{{ __('Refunds') }} :</strong> 
				<span>
					<strong>
						{{ $amount_refunded.' '.config('payments.currency_code') }}
					</strong>
				</span>
			</div>

		</div>

		<div class="ui fluid card shadowless">
			<div class="content">
				<h3 class="header">Items</h3>
			</div>

      @foreach($items as $k => $item)
        @php $i = $k + 1; @endphp

        <div class="content title">
          <h4>{{ $i }}. {{ $item->name }}</h4>
        </div>
        <div class="content">
          <strong>{{ __('Unit amount') }} :</strong> 
          <span>{{ $item->getPaidPrice().' '.config('payments.currency_code') }}</span>
        </div>
        <div class="content">
          <strong>{{ __('Quantity') }} :</strong> 
          <span>1</span>
        </div>

			@endforeach
		</div>
	</div>
</div>

@endsection