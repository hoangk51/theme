@extends('back.master')

@section('title', $title)

@section('content')

<form class="ui fluid form" action="{{ route('transactions.refund_iyzico', request()->payment_id) }}" method="post">
	<div class="field">
		<button class="ui icon labeled basic button" type="submit">
		  <i class="save outline icon"></i>
		  Refund
		</button>
		<a class="ui icon labeled basic button" href="{{ route('transactions') }}">
			<i class="times icon"></i>
			Cancel
		</a>
	</div>
	
	@if($errors->any())
      @foreach ($errors->all() as $error)
         <div class="ui negative fluid small message">
         	<i class="times icon close"></i>
         	{{ $error }}
         </div>
      @endforeach
	@endif

	@if(session('refund_response'))
		<div class="ui positive fluid small message">
			<i class="times icon close"></i>
			{{ session('refund_response') }}
		</div>
	@endif

	<div class="ui two stackable cards">
		@foreach($items as $item)
		<div class="ui fluid card">
			<div class="content">
				<h3 class="header">{{ $item->name }}</h3>
			</div>
			<div class="content">
				{{ __('Price') }} : {{ config('payments.currency_code') .' '. $item->getPaidPrice() }}
			</div>
			<div class="content">
				<input type="number" step="0.01" 
							 name="item[{{ $item->getPaymentTransactionId() }}]"
							 placeholder="Amount to refund">
			</div>
		</div>
		@endforeach

	</div>
</form>

@endsection