@extends('back.master')

@section('title', $title)

@section('content')

@php
	$amount = $details->purchase_units[0]->amount; 
	$seller_receivable_breakdown = $details->purchase_units[0]->payments->captures[0]->seller_receivable_breakdown;
	$status = $details->purchase_units[0]->payments->captures[0]->status;
@endphp


<div id="transactions" class="paypal">

	<div class="ui two stackable cards">
		<div class="ui fluid card shadowless">
			<div class="content">
				<h3 class="header">Transaction details</h3>
			</div>
			<div class="content bg-ghostwhite">
				<strong class="status">Status :</strong> 
					<span class="ui basic {{ ($status === 'COMPLETED') ? 'green' : 'red' }} label">
						{{ $status }}
					</span>
			</div>
			<div class="content bg-ghostwhite">
				<strong>ID :</strong> 
				<span>{{ $details->purchase_units[0]->payments->captures[0]->id }}</span>
			</div>
			<div class="content bg-ghostwhite">
				<strong>@if(isset($details->create_time)) Created @else Updated @endif :</strong> 
				<span>{{ (new DateTime($details->create_time ?? $details->update_time))->format("d F Y \a\\t h:i:s A") }}</span>
			</div>
			<div class="content">
				<strong>Total items :</strong> 
				<span>
					<strong>
						{{ $amount->breakdown->item_total->value }} 
						{{ $amount->breakdown->item_total->currency_code }}
					</strong>
				</span>
			</div>
			<div class="content">
				<strong>Discount :</strong> 
				<span class="minus">
					@if(property_exists($amount->breakdown, 'discount'))
					{{ $amount->breakdown->discount->value.' '.$amount->breakdown->discount->currency_code }}
					@else
					0.00 {{ cache('currency_code') }}
					@endif
				</span>
			</div>
			<div class="content">
				<strong>Handling :</strong> 
				<span class="plus">{{ $amount->breakdown->handling->value }} {{ $amount->breakdown->handling->currency_code }}</span>
			</div>
			<div class="content">
				<strong>Tax :</strong> 
				<span class="plus">{{ $amount->breakdown->tax_total->value }} {{ $amount->breakdown->tax_total->currency_code }}</span>
			</div>
			<div class="content">
				<strong>Gross amount :</strong> 
				<span>
					<strong>
						{{ $seller_receivable_breakdown->gross_amount->value }} 
						{{ $seller_receivable_breakdown->gross_amount->currency_code }}
					</strong>
				</span>
			</div>
			<div class="content">
				<strong>PayPal Fee :</strong> 
				<span class="minus">
					{{ $seller_receivable_breakdown->paypal_fee->value }} 
					{{ $seller_receivable_breakdown->paypal_fee->currency_code }}
				</span>
			</div>
			<div class="content">
				<strong>Net amount :</strong> 
				<span>
					<strong>
						{{ $seller_receivable_breakdown->net_amount->value }} 
						{{ $seller_receivable_breakdown->net_amount->currency_code }}
					</strong>
				</span>
			</div>
			
			@if(property_exists($details->purchase_units[0]->payments, 'refunds'))
			<div class="content bg-ghostwhite">
				<strong>Refunds :</strong> 
				<span>
					<strong>
						{{ array_reduce($details->purchase_units[0]->payments->refunds, function($ac, $cv)
							{
								return $ac + $cv->amount->value;
							}, 0) }}
						{{ cache('currency_code') }}
					</strong>
				</span>
			</div>
			@endif

		</div>

		<div class="ui fluid card shadowless">
			<div class="content">
				<h3 class="header">Items</h3>
			</div>

			@foreach($details->purchase_units[0]->items as $k => $item)

			<div class="content title">
				<h4>{{ 1+$k }}. {{ $item->name }}</h4>
			</div>
			<div class="content">
				<strong>Unit amount :</strong> 
				<span>{{ $item->unit_amount->value.' '. $item->unit_amount->currency_code }}</span>
			</div>
			<div class="content">
				<strong>Quantity :</strong> 
				<span>{{ $item->quantity }}</span>
			</div>
			<div class="content">
				<strong>Description :</strong> 
				<span>{{ $item->description }}</span>
			</div>

			@endforeach
		</div>
	</div>
		
	<div class="ui fluid card shadowless">
		<div class="content">
			<h3 class="header">Buyer</h3>
		</div>
		<div class="content">
			<strong>Name :</strong> 
			<span>{{ implode(' ', array_values((array)$details->payer->name)) }}</span>
		</div>
		<div class="content">
			<strong>Email :</strong> 
			<span>{{ $details->payer->email_address }}</span>
		</div>
	</div>
</div>

@endsection