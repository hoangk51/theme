@extends('back.master')

@section('title', $title)

@section('content')

<div id="transactions" class="srtipe">

	<div class="ui two stackable cards">
		<div class="ui fluid card shadowless">
			<div class="content">
				<h3 class="header">Transaction details</h3>
			</div>
			<div class="content bg-ghostwhite">
				<strong class="status">Status :</strong> 
					<span class="ui basic @if($amount_refunded) red @else green @endif label">
						@if($refunded) 
						Refunded
						@elseif(!$refunded && $amount_refunded)
						Partially refunded
						@else
						Complete
						@endif
					</span>
			</div>
			<div class="content bg-ghostwhite">
				<strong>ID :</strong> 
				<span>{{ $details->id }}</span>
			</div>
			<div class="content bg-ghostwhite">
				<strong>@if($created_at == $updated_at) Created @else Updated @endif :</strong> 
				<span>{{ (new DateTime($updated_at))->format("d F Y \a\\t h:i:s A") }}</span>
			</div>
			<div class="content">
				<strong>Total items :</strong> 
				<span>
					<strong>
						{{ number_format($amount + $discount, 2).' '.cache('currency_code') }} 
					</strong>
				</span>
			</div>
			<div class="content">
				<strong>Discount :</strong> 
				<span class="minus">
					{{ number_format($discount, 2).' '.cache('currency_code') }}
				</span>
			</div>
			<div class="content">
				<strong>Handling :</strong> 
				<span class="plus">
					{{ number_format($handling/100, 2).' '.cache('currency_code')  }}
				</span>
			</div>
			<div class="content">
				<strong>Tax :</strong> 
				<span class="plus">
					{{ number_format($tax/100, 2).' '.cache('currency_code') }}
				</span>
			</div>
			<div class="content">
				<strong>Gross amount :</strong> 
				<span>
					<strong>
						{{ number_format($details->payment_intent->amount/100, 2).' '.cache('currency_code') }}
					</strong>
				</span>
			</div>
			<div class="content">
				<strong>Stripe Fee :</strong> 
				<span class="minus">
					{{ number_format($fee, 2).' '.cache('currency_code') }}
				</span>
			</div>
			<div class="content">
				<strong>Net amount :</strong> 
				<span>
					<strong>
						{{ number_format($net_amount, 2).' '.cache('currency_code') }}
					</strong>
				</span>
			</div>
			
			<div class="content bg-ghostwhite">
				<strong>Refunds :</strong> 
				<span>
					<strong>
						{{ number_format($amount_refunded/100, 2).' '.cache('currency_code') }}
					</strong>
				</span>
			</div>

		</div>

		<div class="ui fluid card shadowless">
			<div class="content">
				<h3 class="header">Items</h3>
			</div>

			@foreach(array_shift($items) as $k => $item)

			<div class="content title">
				<h4>{{ $k }}. {{ $item->custom->name }}</h4>
			</div>
			<div class="content">
				<strong>Unit amount :</strong> 
				<span>{{ round($item->amount/100, 2).' '.cache('currency_code') }}</span>
			</div>
			<div class="content">
				<strong>Quantity :</strong> 
				<span>{{ $item->quantity }}</span>
			</div>
			<div class="content">
				<strong>Description :</strong> 
				<span>{{ $item->custom->description }}</span>
			</div>

			@endforeach
		</div>
	</div>
		
	<div class="ui fluid card shadowless">
		<div class="content">
			<h3 class="header">Buyer</h3>
		</div>
		<div class="content">
			<strong>Name :</strong> 
			<span>{{ $details->payment_intent->charges->data[0]->billing_details->name }}</span>
		</div>
		<div class="content">
			<strong>Email :</strong> 
			<span>{{ $details->payment_intent->charges->data[0]->billing_details->email }}</span>
		</div>
	</div>
</div>

@endsection