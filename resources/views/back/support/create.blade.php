@extends('back.master')

@section('title', "Reply to {$parent->email}")


@section('additional_head_tags')
<link href="{{ asset_('assets/admin/css/summernote-lite-0.8.12.css') }}" rel="stylesheet">
<script src="{{ asset_('assets/admin/js/summernote-lite-0.8.12.js') }}"></script>
@endsection

@section('content')

<div class="row" id="supports">
	@if($errors->any())
      @foreach ($errors->all() as $error)
         <div class="ui negative fluid small message">
         	<i class="times icon close"></i>
         	{{ $error }}
         </div>
      @endforeach
	@endif

	@if(session('response'))
	<div class="ui positive message bold">
		<i class="close icon mx-0"></i>
		{{ session('response') }}
	</div>
	@endif

	<fieldset class="mb-1">
		<legend class="ui blue label">Subject</legend>
		<p>{{ $parent->subject }}</p>
	</fieldset>
	
	<fieldset class="mb-1">
		<legend class="ui grey label">Message</legend>
		<p>{{ $parent->message }}</p>
	</fieldset>

	<fieldset class="mb-1">
		<legend class="ui grey label">Response</legend>
		<form action="{{ route('support.store') }}" id="response" method="post" class="ui form mt-1-hf">
			<div class="field">
				<textarea name="message" required autofocus class="summernote" cols="30" rows="20">{{ old('message') }}</textarea>

				<input type="hidden" class="d-none" name="parent" value="{{ $parent->id }}">
				<input type="hidden" class="d-none" name="email" value="{{ $parent->email }}">
			</div>
			<div class="field">
				<button class="ui basic button" type="submit">Submit</button>
				<a href="{{ route('support') }}" class="ui basic button">Cancel</a>
			</div>
		</form>
	</fieldset>
	
	<div class="table wrapper">
		<table class="ui unstackable celled basic table">
			<thead>
				<tr>
					<th>&nbsp;</th>
					<th>Message</th>
					<th>Sent at</th>
				</tr>
			</thead>
			<tbody>
				@foreach($children as $message)
				<tr>
					<td class="center aligned one column wide">
						<a class="ui small basic labeled icon button mx-0" 
							 @click="deleteItem($event)" 
							 href="{{ route('support.destroy', ['ids' => $message->id, 'to_route' => ['support.create', $parent->id]]) }}">
							<i class="trash red alternate outline icon mx-0"></i>
							Delete</a>
					</td>
					<td>{!! nl2br($message->message) !!}</td>
					<td class="center aligned two column wide">{{ $message->created_at }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>

</div>

<script>
	'use strict';
	
	var app = new Vue({
	  el: '#supports',
	  methods: {
	  	deleteItem: function(e)
	  	{
	  		if(!confirm('Are you sure you want to delete this support ?'))
  			{
  				e.preventDefault();
  				return false;
  			}
	  	}
	  }
	})

	$(function()
	{
		$('#response').on('submit', function(e)
		{
			if(!$('textarea', this).val().trim().length)
			{
				e.preventDefault();
				return false;
			}
		})

		$('.summernote').summernote({
	    placeholder: '...',
	    tabsize: 2,
	    height: 350
	  })
	})

</script>
@endsection