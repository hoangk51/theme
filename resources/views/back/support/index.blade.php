@extends('back.master')

@section('title', 'Support')


@section('content')

<div class="row main" id="supports">
	
	@if(session('unseen_messages'))
	<div class="ui positive message bold">
		<i class="close icon mx-0"></i>
		{{ session('unseen_messages') }}
	</div>
	@endif

	<div class="ui menu shadowless">		
		<a id="bulk-delete" @click="deleteItems" :href="route+ids.join()" class="item" :class="{disabled: isDisabled}">Delete</a>

		<div class="right menu">
			<form action="{{ route('support') }}" method="get" id="search" class="ui transparent icon input item mr-1">
        <input class="prompt" type="text" name="keywords" placeholder="Search ..." required>
        <i class="search link icon" onclick="$('#search').submit()"></i>
      </form>
      @if(config('Imap.enabled'))
      <a @click="loadUnseenMessages($event)" class="item">Load Unseen</a>
      @endif
		</div>
	</div>
	
	<div class="table wrapper">
		<table class="ui unstackable celled basic table">
			<thead>
				<tr>
					<th>
						<div class="ui fitted checkbox">
						  <input type="checkbox" @change="selectAll">
						  <label></label>
						</div>
					</th>
					<th>
						<a href="{{ route('support', ['orderby' => 'email', 'order' => $items_order]) }}">Email</a>
					</th>
					<th class="five columns wide">Subject</th>
					<th>
						<a href="{{ route('support', ['orderby' => 'read', 'order' => $items_order]) }}">Read</a>
					</th>
					<th>
						<a href="{{ route('support', ['orderby' => 'created_at', 'order' => $items_order]) }}">Created at</a>
					</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				@foreach($support_messages as $message)
				<tr>
					<td class="center aligned">
						<div class="ui fitted checkbox select">
						  <input type="checkbox" value="{{ $message->id }}" @change="toogleId({{ $message->id }})">
						  <label></label>
						</div>
					</td>
					<td>{{ ucfirst($message->email) }}</td>
					<td>{{ ucfirst($message->subject) }}</td>
					<td class="center aligned">
						<span class="ui tiny basic @if(!$message->read) blue @endif label support-message" 
							data-id="{{ $message->id }}" 
							data-html="{{ nl2br($message->content) }}">
							Read
						</span>
					</td>
					<td class="center aligned">{{ $message->created_at }}</td>
					<td class="center aligned one column wide">
						<div class="ui dropdown">
							<i class="bars icon mx-0"></i>
							<div class="menu dropdown left">
								<a href="{{ route('support.create', $message->id) }}" class="item">Reply</a>
								<a @click="deleteItem($event)" href="{{ route('support.destroy', $message->id) }}" class="item">Delete</a>
							</div>
						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	
	<div class="ui fluid divider"></div>

	{{ $support_messages->appends($base_uri)->onEachSide(1)->links() }}
	{{ $support_messages->appends($base_uri)->links('vendor.pagination.simple-semantic-ui') }}

</div>

<script>
	'use strict';
	
	var app = new Vue({
	  el: '#supports',
	  data: {
	  	route: '{{ route('support.destroy', "") }}/',
	    ids: [],
	    isDisabled: true
	  },
	  methods: {
	  	toogleId: function(id)
	  	{
	  		if(this.ids.indexOf(id) >= 0)
	  			this.ids.splice(this.ids.indexOf(id), 1);
	  		else
	  			this.ids.push(id);
	  	},
	  	selectAll: function()
	  	{
	  		$('#supports tbody .ui.checkbox').checkbox('toggle')
	  	},
	  	deleteItems: function(e)
	  	{
	  		var confirmationMsg = 'Are you sure you want to delete the selected supports(s) ?';

	  		if(!this.ids.length || !confirm(confirmationMsg))
	  		{
	  			e.preventDefault();
	  			return false;
	  		}
	  	},
	  	deleteItem: function(e)
	  	{
	  		if(!confirm('Are you sure you want to delete this support ?'))
  			{
  				e.preventDefault();
  				return false;
  			}
	  	},
	  	loadUnseenMessages: function(e)
	  	{
	  		var _this = $(e.target);

	  		_this.toggleClass('loading', true).text('Loading');

	  		$.post('{{ route('support.load_unseen') }}', {}, null, 'json')
	  		.done(function(res)
	  		{
	  			if(res.success)
					{
						window.location.href = '{{ route('support') }}';
					}
	  		})
	  		.fail(function()
	  		{
	  			alert('Failed loading new messages');

	  			_this.toggleClass('loading', false).text('load unseen');
	  		})
	  	}
	  },
	  watch: {
	  	ids: function(val)
	  	{
	  		this.isDisabled = !val.length;
	  	}
	  }
	})

	$(function()
	{
		$('.support-message').popup({
	    on: 'click',
	    onShow: function(el)
	    {
	    	var id = $(el).data('id');

	    	if($(el).hasClass('blue'))
	    	{
	    		$.post('{{ route('support.status') }}', {id: id}, null, 'json')
	    		.done(function()
	    		{
	    			$(el).removeClass('blue')
	    		})
	    		.fail(function()
	    		{
	    			alert('Failed to update read status');
	    		})
	    	}
	    }
	  })

	  $('#search').on('submit', function(event)
		{
			if(!$('input', this).val().trim().length)
			{
				e.preventDefault();
				return false;
			}
		})
	})

</script>
@endsection