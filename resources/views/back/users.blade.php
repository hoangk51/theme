@extends('back.master')

@section('title', $title)


@section('content')

<div class="row" id="users">

	<div class="ui menu shadowless">		
		<a id="bulk-delete" @click="deleteItems" :href="route+ids.join()" class="item" :class="{disabled: isDisabled}">Delete</a>

		<div class="right menu mr-1">
			<form action="{{ route('users') }}" method="get" id="search" class="ui transparent icon input item">
        <input class="prompt" type="text" name="keywords" placeholder="Search ..." required>
        <i class="search link icon" onclick="$('#search').submit()"></i>
      </form>
		</div>
	</div>

	<div class="table wrapper">
		<table class="ui unstackable celled basic table">
			<thead>
				<tr>
					<th>&nbsp;</th>
					
					<th>
						<a href="{{ route('users', ['orderby' => 'email', 'order' => $items_order]) }}">Email</a>
					</th>
					<th>
						<a href="{{ route('users', ['orderby' => 'verified', 'order' => $items_order]) }}">Verified</a>
					</th>
					<th>
						<a href="{{ route('users', ['orderby' => 'purchases', 'order' => $items_order]) }}">Purchased items</a>
					</th>
					<th>
						<a href="{{ route('users', ['orderby' => 'total_purchases', 'order' => $items_order]) }}">Total expenses</a>
					</th>
					<th>
						<a href="{{ route('users', ['orderby' => 'created_at', 'order' => $items_order]) }}">Created at</a>
					</th>
				</tr>
			</thead>
			<tbody>
				@foreach($users as $user)
				<tr>

					<td class="center aligned">
						<div class="ui fitted checkbox">
						  <input type="checkbox" value="{{ $user->id }}" @change="toogleId({{ $user->id }})">
						  <label></label>
						</div>
					</td>

					<td>{{ ucfirst($user->email) }}</td>

					<td class="center aligned">
						<i class="circle @if($user->verified) green @else red @endif icon mx-0"></i>
					</td>

					<td class="center aligned">{{ $user->purchases }}</td>

					<td class="center aligned">{{ config('payments.currency_code').' '.$user->total_purchases }}</td>

					<td class="center aligned">{{ $user->created_at }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	
	<div class="ui fluid divider"></div>

	{{ $users->appends($base_uri)->onEachSide(1)->links() }}
	{{ $users->appends($base_uri)->links('vendor.pagination.simple-semantic-ui') }}

</div>

<script>
	'use strict';
	
	var app = new Vue({
	  el: '#users',
	  data: {
	  	route: '{{ route('users.destroy', "") }}/',
	    ids: [],
	    isDisabled: true
	  },
	  methods: {
	  	toogleId: function(id)
	  	{
	  		if(this.ids.indexOf(id) >= 0)
	  			this.ids.splice(this.ids.indexOf(id), 1);
	  		else
	  			this.ids.push(id);
	  	},
	  	deleteItems: function(e)
	  	{
	  		if(!this.ids.length)
	  		{
	  			e.preventDefault();
	  			return false;
	  		}
	  	}
	  },
	  watch: {
	  	ids: function(val)
	  	{
	  		this.isDisabled = !val.length;
	  	}
	  }
	})
</script>
@endsection