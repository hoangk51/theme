<div class="right menu">
    <a class="item ui small yellow button" href="/">PREVIEW</a>
    
    <div class="item ui dropdown user">
        <i class="user outline icon mx-0"></i>
        <div class="left menu">
            <a class="item" href="{{ url('admin/profile') }}">
                <i class="user outline icon"></i>
                Profile
            </a>
            <div class="item">
                <i class="cog icon"></i>
                Settings
                <div class="menu settings left">
                    <a href="{{ url('admin/settings/general') }}" class="item">General</a>
                    <a href="{{ url('admin/settings/search_engines') }}" class="item">Search engines</a>
                    <a href="{{ url('admin/settings/payments') }}" class="item">Payments</a>
                    <a href="{{ url('admin/settings/advert') }}" class="item d-none">Advertisement</a>
                    <a href="{{ url('admin/settings/social_login') }}" class="item">Social Login</a>
                    <a href="{{ url('admin/settings/mailer') }}" class="item">Mailer</a>
                    <a href="{{ url('admin/settings/files_host') }}" class="item">Files host</a>
                </div>
            </div>
            <a class="item" href="{{ route('admin') }}">
                <i class="chart area icon"></i>
                Dashboard
            </a>
            <a class="item logout">
                <i class="sign out alternate icon"></i>
                Logout
            </a>
        </div>
    </div>

	<a class="header item pl-0 pr-1 mobile-only" id="mobile-menu-toggler">
		<i class="bars large icon mr-0 ml-1"></i>
	</a>
</div>