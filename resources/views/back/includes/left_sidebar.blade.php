<div class="ui header p-0">
	<a href="/admin">
		<img class="ui image mx-auto" src="{{ asset_("storage/images/".config('app.logo')) }}" alt="logo">
	</a>
</div>

<div class="ui vertical fluid menu">

	<a class="item parent" href="{{ route('admin') }}">
		<img src="{{ asset_('assets/images/left_menu_icons/dashboard.png') }}">
		Dashboard
	</a>

	<div class="dropdown">
		<div class="item parent">
			<img src="{{ asset_('assets/images/left_menu_icons/product.png') }}">
			Products
			<i class="circle outline icon mx-0"></i>
		</div>
		<div class="children">
			<a class="item" href="{{ route('products.create') }}"><span>Create</span></a>
			<a class="item" href="{{ route('products') }}"><span>All</span></a>
		</div>
	</div>

	<div class="dropdown">
		<div class="item parent">
			<img src="{{ asset_('assets/images/left_menu_icons/categories.png') }}">
			Categories
			<i class="circle outline icon mx-0"></i>
		</div>
		<div class="children">
			<a class="item" href="{{ route('categories.create') }}"><span>Create</span></a>
			<a class="item" href="{{ route('categories') }}"><span>All</span></a>
		</div>
	</div>
	
	<a class="item parent" href="{{ route('transactions') }}">
		<img src="{{ asset_('assets/images/left_menu_icons/cart.png') }}">
		Transactions
	</a>

	<div class="dropdown">
		<div class="item parent">
			<img src="{{ asset_('assets/images/left_menu_icons/coupons.png') }}">
			Coupons
			<i class="circle outline icon mx-0"></i>
		</div>
		<div class="children">
			<a class="item" href="{{ route('coupons.create') }}"><span>Create</span></a>
			<a class="item" href="{{ route('coupons') }}"><span>All</span></a>
		</div>
	</div>
	
	<div class="dropdown">
		<div class="item parent">
			<img src="{{ asset_('assets/images/left_menu_icons/posts.png') }}">
			Posts
			<i class="circle outline icon mx-0"></i>
		</div>
		<div class="children">
			<a class="item" href="{{ route('posts.create') }}"><span>Create</span></a>
			<a class="item" href="{{ route('posts') }}"><span>All</span></a>
		</div>
	</div>

	<div class="dropdown">
		<div class="item parent">
			<img src="{{ asset_('assets/images/left_menu_icons/pages.png') }}">
			Pages
			<i class="circle outline icon mx-0"></i>
		</div>
		<div class="children">
			<a class="item" href="{{ route('pages.create') }}"><span>Create</span></a>
			<a class="item" href="{{ route('pages') }}"><span>All</span></a>
		</div>
	</div>

	<a class="item parent" href="{{ route('comments') }}">
		<img src="{{ asset_('assets/images/left_menu_icons/comments.png') }}">
		Comments
	</a>

	<a class="item parent" href="{{ route('users') }}">
		<img src="{{ asset_('assets/images/left_menu_icons/user.png') }}">
		Users
	</a>

	<a class="item parent" href="{{ route('reviews') }}">
		<img src="{{ asset_('assets/images/left_menu_icons/reviews.png') }}">
		Reviews
	</a>
	
	<div class="dropdown">
		<div class="item parent">
			<img src="{{ asset_('assets/images/left_menu_icons/emails.png') }}">
			Newsletter
			<i class="circle outline icon mx-0"></i>
		</div>
		<div class="children">
			<a class="item" href="{{ route('subscribers.newsletter.create') }}"><span>Create</span></a>
			<a class="item" href="{{ route('subscribers') }}"><span>Subscribers</span></a>
		</div>
	</div>

	<div class="dropdown">
		<div class="item parent">
			<img src="{{ asset_('assets/images/left_menu_icons/question-mark.png') }}">
			FAQ
			<i class="circle outline icon mx-0"></i>
		</div>
		<div class="children">
			<a class="item" href="{{ route('faq.create') }}"><span>Create</span></a>
			<a class="item" href="{{ route('faq') }}"><span>All</span></a>
		</div>
	</div>
	
	<a class="item parent logout" href="{{ route('support') }}">
		<img src="{{ asset_('assets/images/left_menu_icons/help.png') }}">
		Support messages
	</a>

	<div class="dropdown">
		<div class="item parent">
			<img src="{{ asset_('assets/images/left_menu_icons/settings.png') }}">
			Settings
			<i class="circle outline icon mx-0"></i>
		</div>
		<div class="children settings">
			<a class="item" href="{{ url('admin/settings/general') }}"><span>General</span></a>
			<a class="item" href="{{ url('admin/settings/mailer') }}"><span>Mailer</span></a>
			<a class="item" href="{{ url('admin/settings/payments') }}"><span>Payments</span></a>
			<a class="item d-none" href="{{ url('admin/settings/adverts') }}"><span>Adverts</span></a>
			<a class="item" href="{{ url('admin/settings/files_host') }}"><span>Files host</span></a>
			<a class="item" href="{{ url('admin/settings/social_login') }}"><span>Social Login</span></a>
			<a class="item" href="{{ url('admin/settings/search_engines') }}"><span>Search engines</span></a>
		</div>
	</div>
	
	<a class="item parent logout" href="{{ route('profile.edit') }}">
		<img src="{{ asset_('assets/images/left_menu_icons/user.png') }}">
		Profile
	</a>

	<a class="item parent logout">
		<img src="{{ asset_('assets/images/left_menu_icons/logout.png') }}">
		Logout
	</a>

	<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">@csrf</form>

</div>