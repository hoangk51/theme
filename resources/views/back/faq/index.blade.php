@extends('back.master')

@section('title', $title)


@section('content')

<div class="row main" id="faqs">

	<div class="ui menu shadowless">		
		<a id="bulk-delete" @click="deleteItems" :href="route+ids.join()" class="item" :class="{disabled: isDisabled}">Delete</a>

		<div class="right menu">
			<a href="{{ route('faq.create') }}" class="item ml-1">Add</a>
		</div>
	</div>
	
	<div class="table wrapper">
		<table class="ui unstackable celled basic table">
			<thead>
				<tr>
					<th>
						<div class="ui fitted checkbox">
						  <input type="checkbox" @change="selectAll">
						  <label></label>
						</div>
					</th>
					<th class="five columns wide">Question</th>
					<th>
						<a href="{{ route('faq', ['orderby' => 'active', 'order' => $items_order]) }}">Active</a>
					</th>
					<th>
						<a href="{{ route('faq', ['orderby' => 'updated_at', 'order' => $items_order]) }}">Updated at</a>
					</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				@foreach($faqs as $faq)
				<tr>
					<td class="center aligned">
						<div class="ui fitted checkbox select">
						  <input type="checkbox" value="{{ $faq->id }}" @change="toogleId({{ $faq->id }})">
						  <label></label>
						</div>
					</td>
					<td>{{ ucfirst($faq->question) }}</td>
					<td class="center aligned">
						<div class="ui toggle fitted checkbox">
						  <input type="checkbox" name="active" @if($faq->active) checked @endif data-id="{{ $faq->id }}" data-status="active" 
						  @click="updateStatus($event)">
						  <label></label>
						</div>
					</td>
					<td class="center aligned">{{ $faq->updated_at }}</td>
					<td class="center aligned one column wide">
						<div class="ui dropdown">
							<i class="bars icon mx-0"></i>
							<div class="menu dropdown left">
								<a href="{{ route('faq.edit', $faq->id) }}" class="item">Edit</a>
								<a @click="deleteItem($event)" href="{{ route('faq.destroy', $faq->id) }}" class="item">Delete</a>
							</div>
						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	
	<div class="ui fluid divider"></div>

	{{ $faqs->appends($base_uri)->onEachSide(1)->links() }}
	{{ $faqs->appends($base_uri)->links('vendor.pagination.simple-semantic-ui') }}

</div>

<script>
	'use strict';

	var app = new Vue({
	  el: '#faqs',
	  data: {
	  	route: '{{ route('faq.destroy', "") }}/',
	    ids: [],
	    isDisabled: true
	  },
	  methods: {
	  	toogleId: function(id)
	  	{
	  		if(this.ids.indexOf(id) >= 0)
	  			this.ids.splice(this.ids.indexOf(id), 1);
	  		else
	  			this.ids.push(id);
	  	},
	  	selectAll: function()
	  	{
	  		$('#faqs tbody .ui.checkbox.select').checkbox('toggle')
	  	},
	  	deleteItems: function(e)
	  	{
	  		var confirmationMsg = 'Are you sure you want to delete the selected faqs(s) ?';

	  		if(!this.ids.length || !confirm(confirmationMsg))
	  		{
	  			e.preventDefault();
	  			return false;
	  		}
	  	},
	  	deleteItem: function(e)
	  	{
	  		if(!confirm('Are you sure you want to delete this faq ?'))
  			{
  				e.preventDefault();
  				return false;
  			}
	  	},
	  	updateStatus: function(e)
	  	{	
	  		var thisEl  = $(e.target);
	  		var id 			= thisEl.data('id');

	  		$.post('{{ route('faq.status') }}', {id: id})
				.done(function(res)
				{
					if(res.success)
					{
						thisEl.checkbox('toggle');
					}
				}, 'json')
				.fail(function()
				{
					alert('Failed')
				})
	  	}
	  },
	  watch: {
	  	ids: function(val)
	  	{
	  		this.isDisabled = !val.length;
	  	}
	  }
	})
</script>
@endsection