@extends('back.master')

@section('title', $title)


@section('content')
<form class="ui form" method="post" action="{{ route('coupons.update', $coupon->id) }}" id="coupon" spellcheck="false">
	@csrf

	<div class="field">
		<button class="ui icon labeled basic button" type="submit">
		  <i class="save outline icon"></i>
		  Update
		</button>
		<a class="ui icon labeled basic button" href="{{ route('coupons') }}">
			<i class="times icon"></i>
			Cancel
		</a>
	</div>
	
	@if($errors->any())
    @foreach ($errors->all() as $error)
		<div class="ui negative fluid small message">
			<i class="times icon close"></i>
			{{ $error }}
		</div>
    @endforeach
	@endif

	<div class="ui fluid divider"></div>

	<div class="one column grid">
		<div class="column">

			<div class="field" id="coupon-code">
				<label>Code</label>
				<div class="ui action input">
				  <input type="text" name="code" placeholder="..." value="{{ $coupon->code }}" autofocus required>
				  <button class="ui teal button" type="button">Generate</button>
				</div>
			</div>

			<div class="field">
				<label>Value ({{ config('payments.currency_code') }})</label>
				<input type="number" step="0.01" name="value" placeholder="..." value="{{ $coupon->value }}" required>
			</div>

			<div class="field">
				<label>Is percentage</label>
				<select name="is_percentage" value="{{ $coupon->is_percentage }}" class="ui dropdown">
					<option value="1">True</option>
					<option value="0" selected>False</option>
				</select>
			</div>

			<div class="field">
				<label>Product</label>
				<div class="ui search selection dropdown">
					<input type="hidden" name="product_id" value="{{ str_replace("'", '', $coupon->product_id) }}">
					<i class="dropdown icon"></i>
					<div class="default text">Products ids</div>
					<div class="menu">
					@foreach(App\Models\Product::where(['active' => 1])->orderBy('id', 'desc')->get() as $product)
						<div class="item" data-value="{{ $product->id }}">{{ $product->name }}</div>
					@endforeach
					</div>
				</div>
			</div>


			<div class="field">
				<label>Users ids</label>
				<div class="ui multiple search selection dropdown">
					<input type="hidden" name="users_ids" value="{{ str_replace("'", '', $coupon->users_ids) }}">
					<i class="dropdown icon"></i>
					<div class="default text">Products ids</div>
					<div class="menu">
					@foreach(App\Models\User::orderBy('id', 'desc')->get() as $user)
						<div class="item" data-value="{{ $user->id }}">{{ $user->email }}</div>
					@endforeach
					</div>
				</div>
			</div>


			<div class="field">
				<label>Starts at</label>
				<input type="datetime-local" name="starts_at" required value="{{ $coupon->starts_at }}">
			</div>
			

			<div class="field">
				<label>Expires at</label>
				<input type="datetime-local" name="expires_at" required value="{{ $coupon->expires_at }}">
			</div>

		</div>
	</div>
</form>

<script>
	$(function()
	{
		'use strict';
		
		$('#coupon').on('submit', function(e)
		{
			if($('#coupon input[name="value"]').val() <= 0)
			{
				$('#coupon input[name="value"]').focus();
				e.preventDefault();
				return false;
			}
		})

		$('#coupon input[name="value"]').on('change', function()
		{
			$(this).toggleClass('error', !($(this).val() > 0));
		})

		$('#coupon-code button').on('click', function()
		{
			$.post('{{ route('coupons.generate') }}', null, null, 'json')
			.done(function(coupon)
			{
				$('#coupon-code input').val(coupon.code);
			})
		})
		
	})
</script>

@endsection