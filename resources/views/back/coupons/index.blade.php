@extends('back.master')

@section('title', $title)


@section('content')

<div class="row main" id="coupons">

	<div class="ui menu shadowless">
		<a id="bulk-delete" @click="deleteItems" :href="route+ids.join()" class="item" :class="{disabled: isDisabled}">Delete</a>

		<div class="right menu">
			<form action="{{ route('coupons') }}" method="get" id="search" class="ui transparent icon input item">
        <input class="prompt" type="text" name="keywords" placeholder="Search ..." required>
        <i class="search link icon" onclick="$('#search').submit()"></i>
			</form>
			<a href="{{ route('coupons.create') }}" class="item ml-1">Add</a>
		</div>
	</div>
	
	<div class="table wrapper">
		<table class="ui unstackable celled basic table">
			<thead>
				<tr>
					<th>
						<div class="ui fitted checkbox">
						  <input type="checkbox" @change="selectAll">
						  <label></label>
						</div>
					</th>
					<th>
						<a href="{{ route('coupons', ['orderby' => 'code', 'order' => $items_order]) }}">Code</a>
					</th>
					<th>
						<a href="{{ route('coupons', ['orderby' => 'used_by', 'order' => $items_order]) }}">Used by</a>
					</th>
					<th>
						<a href="{{ route('coupons', ['orderby' => 'value', 'order' => $items_order]) }}">Value</a>
					</th>
					<th>
						<a href="{{ route('coupons', ['orderby' => 'starts_at', 'order' => $items_order]) }}">Starts at</a>
					</th>
					<th>
						<a href="{{ route('coupons', ['orderby' => 'expires_at', 'order' => $items_order]) }}">Expires at</a>
					</th>
					<th>
						<a href="{{ route('coupons', ['orderby' => 'updated_at', 'order' => $items_order]) }}">Updated at</a>
					</th>
					<th>Actions</th>
				</tr>
			</thead>

			<tbody>
				@foreach($coupons as $coupon)
				<tr id="{{ $coupon->id }}">

					<td class="center aligned">
						<div class="ui fitted checkbox select">
						  <input type="checkbox" value="{{ $coupon->id }}" @change="toogleId({{ $coupon->id }})">
						  <label></label>
						</div>
					</td>

					<td class="center aligned">{{ $coupon->code }}</td>

					<td class="center aligned">{{ $coupon->used_by }}</td>

					<td class="center aligned">
						@if($coupon->is_percentage)
						{{ $coupon->value.'% OFF' }}
						@else
						{{ config('payments.currency_code').' '.number_format($coupon->value, 2) }}
						@endif
					</td>

					<td class="center aligned" >
						{{ (new DateTime($coupon->starts_at))->format("d M Y \a\\t h:i:s A") }}
					</td>

					<td class="center aligned">
						@if($coupon->expires_at < date('Y-m-d H:i:s'))
						<span class="ui basic red label m-0 expired">
							Expired
						</span>
						@else
						{{ (new DateTime($coupon->expires_at))->format("d M Y \a\\t h:i:s A") }}
						@endif
					</td>

					<td class="center aligned">
						{{ (new DateTime($coupon->updated_at))->format("d M Y \a\\t h:i:s A") }}
					</td>

					<td class="center aligned one column wide">
						<div class="ui dropdown">
							<i class="bars icon mx-0"></i>
							<div class="menu dropdown left">
								<a href="{{ route('coupons.edit', $coupon->id) }}" class="item">Edit</a>
								<a @click="deleteItem($event)" href="{{ route('coupons.destroy', $coupon->id) }}" class="item">Delete</a>
							</div>
						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	
	<div class="ui fluid divider"></div>

	{{ $coupons->appends($base_uri)->onEachSide(1)->links() }}
	{{ $coupons->appends($base_uri)->links('vendor.pagination.simple-semantic-ui') }}

</div>

<script>
	'use strict';

	var app = new Vue({
	  el: '#coupons',
	  data: {
	  	route: '{{ route('coupons.destroy', "") }}/',
	    ids: [],
	    isDisabled: true
	  },
	  methods: {
	  	toogleId: function(id)
	  	{
	  		if(this.ids.indexOf(id) >= 0)
	  			this.ids.splice(this.ids.indexOf(id), 1);
	  		else
	  			this.ids.push(id);
	  	},
	  	selectAll: function()
	  	{
	  		$('#coupons tbody .ui.checkbox.select').checkbox('toggle')
	  	},
	  	deleteItems: function(e)
	  	{
	  		var confirmationMsg = 'Are you sure you want to delete the selected coupon(s) ?';
	  		
	  		if(!this.ids.length || !confirm(confirmationMsg))
	  		{
	  			e.preventDefault();
	  			return false;
	  		}
	  	},
	  	deleteItem: function(e)
	  	{
	  		if(!confirm('Are you sure you want to delete this coupon?'))
  			{
  				e.preventDefault();
  				return false;
  			}
	  	}
	  },
	  watch: {
	  	ids: function(val)
	  	{
	  		this.isDisabled = !val.length;
	  	}
	  }
	})
</script>
@endsection