    <!--================================
    START LATEST NEWS
=================================-->
    <section class="latest-news section--padding">
        <!-- start /.container -->
        <div class="container">
            <!-- start row -->
            <div class="row">
                <!-- start col-md-12 -->
                <div class="col-md-12">
                    <div class="section-title">
                        <h1>Latest
                            <span class="highlighted">News</span>
                        </h1>
                        <p>Laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats. Lid
                            est laborum dolo rumes fugats untras.</p>
                    </div>
                </div>
                <!-- end /.col-md-12 -->
            </div>
            <!-- end /.row -->

            <!-- start .row -->
            <div class="row">
                @foreach($items as $post)
                <!-- start .col-md-4 -->
                <div class="col-lg-4 col-md-6">
                    <div class="news">
                        <div class="news__thumbnail">
                            <img src="{{ asset_("storage/posts/{$post->cover}") }}" alt="News Thumbnail">
                        </div>
                        <div class="news__content">
                            <a href="{{ route('home.post', $post->slug) }}" class="news-title">
                                <h4>{{ $post->name }}</h4>
                            </a>
                            <p>{{ mb_substr($post->short_description, 0, 120).'...' }}</p>
                        </div>
                        <div class="news__meta">
                            <div class="date">
                                <span class="lnr lnr-clock"></span>
                                <p>{{ $post->updated_at->diffForHumans() }}</p>
                            </div>

                            <div class="other">
                                <ul>
                                    <li>
                                        <span class="lnr lnr-bubble"></span>
                                        <span>45</span>
                                    </li>
                                    <li>
                                        <span class="lnr lnr-eye"></span>
                                        <span>345</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end /.col-md-4 -->
                @endforeach
            </div>
            <!-- end /.row -->
        </div>
        <!-- end /.container -->
    </section>
    <!--================================
    END LATEST NEWS
=================================-->