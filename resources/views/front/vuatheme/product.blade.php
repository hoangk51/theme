@extends('front.vuatheme.master')

@section('additional_head_tags')
<script type="application/ld+json">
{
	"@context": "http://schema.org",
	"@type": "Product",
	"url": "{{ url()->current() }}",
  "name": "{{ $product->name }}",
  "image": "{{ $meta_data->image }}",
  "description": "{!! $product->short_description !!}",
  @if($product->reviews_count)
  "aggregateRating": {
    "@type": "AggregateRating",
    "ratingValue": "{{ $product->rating }}",
    "reviewCount": "{{ $product->reviews_count }}",
    "bestRating": "5",
    "worstRating": "0"
  },
  "review": [
		@foreach($reviews as $review)
		{
			"@type": "Review",
			"author": "{{ $review->name ?? $review->alias_name }}",
			"datePublished" : "{{ (new DateTime($review->created_at))->format('Y-m-d') }}",
			"description": "{{ $review->content }}",
			"name": "-",
			"reviewRating": {
        "@type": "Rating",
        "bestRating": "5",
        "ratingValue": "{{ $review->rating }}",
        "worstRating": "0"
      }
		}@if(next($reviews)),@endif
		@endforeach
  ],
  @endif
  "category": "{!! $product->category !!}",
  "offers": {
    "@type": "AggregateOffer",
    "lowPrice": "{{ number_format($product->price, 2) }}",
    "highPrice": "{{ number_format($product->price, 2) }}",
    "priceCurrency": "{{ config('payments.currency_code') }}",
    "offerCount": "1"
  },
  "brand": "-",
  "sku": "-",
  "mpn": "{{ $product->id }}"
}
</script>

<script>
	'use strict';

  window.props['product'] = {
  	screenshots: {!! json_encode($product->screenshots) !!},
		id: {{ $product->id }},
		name: '{{ $product->name }}',
		cover: '{{ asset("storage/covers/{$product->cover}") }}',
		thumbnail: '{{ asset("storage/thumbnails/{$product->thumbnail}") }}',
		quantity: 1,
		url: '{{ route('home.product', $product->slug) }}',
		price: {{ $product->price }},
		slug: '{{ $product->slug }}'
  }
</script>
@endsection


@section('body')

    <!--================================
        START BREADCRUMB AREA
    =================================-->
    <section class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb">
                        <ul>
                            <li>
                                <a href="{{route('home')}}">Home</a>
                            </li>
                            <li class="active">
                                <a href="{{route('home.products.category',$category->slug)}}">{{$product->category}}</a>
                            </li>
                        </ul>
                    </div>
                    <h1 class="page-title">{{ ucfirst($product->name) }}</h1>
                </div>
                <!-- end /.col-md-12 -->
            </div>
            <!-- end /.row -->
        </div>
        <!-- end /.container -->
    </section>
    <!--================================
        END BREADCRUMB AREA
    =================================-->


    <!--============================================
        START SINGLE PRODUCT DESCRIPTION AREA
    ==============================================-->
    <section class="single-product-desc">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="item-preview">
                        <div class="item__preview-slider">
                        	@foreach($product->screenshots as $screenshot)
                            <div class="prev-slide">
                                <img src="{{ $screenshot }}" alt="Keep calm this isn't the end of the world, the preview is just missing.">
                            </div>
                           @endforeach
                        </div>
                        <!-- end /.item--preview-slider -->

                        <div class="item__preview-thumb">
                            <div class="prev-thumb">
                                <div class="thumb-slider">
                                   @foreach($product->screenshots as $screenshot)
                                    <div class="item-thumb">
                                        <img width="100" height="100" src="{{ $screenshot }}" alt="This is the thumbnail of the item">
                                    </div>
                                    @endforeach
                                </div>
                                <!-- end /.thumb-slider -->

                                <div class="prev-nav thumb-nav">
                                    <span class="lnr nav-left lnr-arrow-left"></span>
                                    <span class="lnr nav-right lnr-arrow-right"></span>
                                </div>
                                <!-- end /.prev-nav -->
                            </div>

                            <div class="item-action">
                                <div class="action-btns">
                                    <a href="#" class="btn btn--round btn--lg">Live Preview</a>
                                    <a href="#" class="btn btn--round btn--lg btn--icon">
                                        <span class="lnr lnr-heart"></span>Add To Favorites</a>
                                </div>
                            </div>
                            <!-- end /.item__action -->
                        </div>
                        <!-- end /.item__preview-thumb-->


                    </div>
                    <!-- end /.item-preview-->

                    <div class="item-info">
                        <div class="item-navigation">
                            <ul class="nav nav-tabs">
                                <li>
                                    <a href="#product-details" class="active" aria-controls="product-details" role="tab" data-toggle="tab">Item Details</a>
                                </li>
                                <li>
                                    <a href="#product-comment" aria-controls="product-comment" role="tab" data-toggle="tab">Comments </a>
                                </li>
                                <li>
                                    <a href="#product-review" aria-controls="product-review" role="tab" data-toggle="tab">Reviews
                                        <span>({{$comments->count()}})</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#product-support" aria-controls="product-support" role="tab" data-toggle="tab">Support</a>
                                </li>
                                <li>
                                    <a href="#product-faq" aria-controls="product-faq" role="tab" data-toggle="tab">item FAQ</a>
                                </li>
                            </ul>
                        </div>
                        <!-- end /.item-navigation -->

                        <div class="tab-content">
                            <div class="fade show tab-pane product-tab active" id="product-details">
                                <div class="tab-content-wrapper">
                                    {{ $product->short_description }}
                                </div>
                            </div>
                            <!-- end /.tab-content -->

                            <div class="fade tab-pane product-tab" id="product-comment">
                                <div class="thread">
                                    <ul class="media-list thread-list">
                                    	@forelse ($comments as $comment)
                                        <li class="single-thread">
                                            <div class="media">
                                                <div class="media-left">
                                                    <a href="#">
                                                        <img class="media-object" src="{{ asset_("storage/avatars/".$comment->avatar) }}" alt="Commentator Avatar">
                                                    </a>
                                                </div>
                                                <div class="media-body">
                                                    <div>
                                                        <div class="media-heading">
                                                            <a href="author.html">
                                                                <h4>{{ $comment->name ?? $comment->alias_name ?? $comment->fullname }} </h4>
                                                            </a>
                                                            <span>{{ $comment->created_at->diffForHumans() }}</span>
                                                        </div>
                                                        <span class="comment-tag buyer">Purchased</span>
                                                        <a href="#" class="reply-link" 	@click="setReplyTo('{{ $comment->name ?? $comment->alias_name ?? $comment->fullname }}', {{ $comment->id }})">Reply</a>
                                                    </div>
                                                    <p>{!! nl2br($comment->body) !!}</p>
                                                </div>
                                            </div>

                                            <!-- nested comment markup -->
                                            <ul class="children">
                                            	@foreach($comment->children as $child)
                                                <li class="single-thread depth-2">
                                                    <div class="media">
                                                        <div class="media-left">
                                                            <a href="#">
                                                                <img class="media-object" src="{{ asset_("storage/avatars/".$child->avatar) }}" alt="Commentator Avatar">
                                                            </a>
                                                        </div>
                                                        <div class="media-body">
                                                            <div class="media-heading">
                                                                <h4>{{ $child->name ?? $child->alias_name ?? $child->fullname }} </h4>
                                                                <span>{{ $child->created_at->diffForHumans() }}</span>
                                                            </div>
                                                            <span class="comment-tag author">Author</span>
                                                            <p>{!! nl2br($child->body) !!} </p>
                                                        </div>
                                                    </div>

                                                </li>
                                                @endforeach
                                            </ul>

                                            <!-- comment reply -->
                                            <div class="media depth-2 reply-comment">
                                                <div class="media-left">
                                                    <a href="#">
                                                        <img class="media-object" src="images/m2.png" alt="Commentator Avatar">
                                                    </a>
                                                </div>
                                                <div class="media-body">
                                                    <form action="#" class="comment-reply-form">
                                                        <textarea class="bla" name="reply-comment" placeholder="Write your comment..."></textarea>
                                                        <button class="btn btn--md btn--round">Post Comment</button>
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- comment reply -->
                                        </li>
                                        <!-- end single comment thread /.comment-->
                                        @empty
                                        <li class="single-thread">
                                        	<div class="media">
											{{ __('No comments found') }}
											</div>
										</li>
										@endforelse
                                       
                                    </ul>
                                    <!-- end /.media-list -->

                                    <!-- <div class="pagination-area pagination-area2">
                                        <nav class="navigation pagination" role="navigation">
                                            <div class="nav-links">
                                                <a class="page-numbers current" href="#">1</a>
                                                <a class="page-numbers" href="#">2</a>
                                                <a class="page-numbers" href="#">3</a>
                                                <a class="next page-numbers" href="#">
                                                    <span class="lnr lnr-arrow-right"></span>
                                                </a>
                                            </div>
                                        </nav>
                                    </div> -->
                                    <!-- end /.comment pagination area -->

                                    <div class="comment-form-area">
                                        <h4>Leave a comment</h4>
                                        <!-- comment reply -->
                                        <div class="media comment-form">
                                            <div class="media-left">
                                                <a href="#">
                                                    <img class="media-object" src="{{ asset_("storage/avatars/" . (auth()->user()->avatar ?? 'default.jpg')) }}" alt="Commentator Avatar">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <form  method="post" action="{{ route('home.product', $product->slug) }}" class="comment-reply-form">
                                                	<input type="hidden" name="type" value="support" class="none">
							  						<input type="hidden" name="comment_id" :value="replyTo.commentId" class="d-none">
                                                    <textarea name="comment" placeholder="Write your comment..."></textarea>
                                                    <button class="btn btn--sm btn--round">Post Comment</button>
                                                </form>
                                            </div>
                                        </div>
                                        <!-- comment reply -->
                                    </div>
                                    <!-- end /.comment-form-area -->
                                </div>
                                <!-- end /.comments -->
                            </div>
                            <!-- end /.product-comment -->

                            <div class="fade tab-pane product-tab" id="product-review">
                                <div class="thread thread_review">
                                    <ul class="media-list thread-list">
                                    	@if($reviews->count())
	                                    	@foreach($reviews as $review)
	                                        <li class="single-thread">
	                                            <div class="media">
	                                                <div class="media-left">
	                                                    <a href="#">
	                                                        <img class="media-object" src="{{ asset_("storage/avatars/".$review->avatar) }}" alt="Commentator Avatar">
	                                                    </a>
	                                                </div>
	                                                <div class="media-body">
	                                                    <div class="clearfix">
	                                                        <div class="pull-left">
	                                                            <div class="media-heading">
	                                                                <a href="author.html">
	                                                                    <h4>{{ $review->name ?? $review->alias_name ?? $review->fullname }} </h4>
	                                                                </a>
	                                                                <span>{{ $review->created_at->diffForHumans() }}</span>
	                                                            </div>
	                                                            <div class="rating product--rating">
	                                                                <ul>
	                                                                	@for ($i = 0; $i < $review->rating; $i++)
	                                                                    <li>
	                                                                        <span class="fa fa-star"></span>
	                                                                    </li>
	                                                                   @endfor
	                                                                </ul>
	                                                            </div>
	                                                            <span class="review_tag">support</span>
	                                                        </div>
	                                                        <a href="#" class="reply-link">Reply</a>
	                                                    </div>
	                                                    <p>{{ nl2br($review->content) }}</p>
	                                                </div>
	                                            </div>

	                                            <!-- comment reply -->
	                                            <div class="media depth-2 reply-comment">
	                                                <div class="media-left">
	                                                    <a href="#">
	                                                        <img class="media-object" src="{{ asset_("storage/avatars/" . (auth()->user()->avatar ?? 'default.jpg')) }}" alt="Commentator Avatar">
	                                                    </a>
	                                                </div>
	                                                <div class="media-body">
	                                                    <form action="#" class="comment-reply-form">
	                                                    	<input type="hidden" name="type" value="reviews" class="none">
	                                                    	<input type="hidden" name="rating" class="d-none">
	                                                        <textarea class="bla" name="review" placeholder="Write your comment..."></textarea>
	                                                        <button class="btn btn--md btn--round">Post Comment</button>
	                                                    </form>
	                                                </div>
	                                            </div>
	                                            <!-- comment reply -->
	                                        </li>
	                                        <!-- end single comment thread /.comment-->
	                                        @endforeach
                                        @else
                                        <li class="single-thread">
                                        	<div class="media">
											{{ __('No reviews found') }}
											</div>
										</li>
										@endif
                                    </ul>
                                    <!-- end /.media-list -->

                                 <!--    <div class="pagination-area pagination-area2">
                                        <nav class="navigation pagination " role="navigation">
                                            <div class="nav-links">
                                                <a class="page-numbers current" href="#">1</a>
                                                <a class="page-numbers" href="#">2</a>
                                                <a class="page-numbers" href="#">3</a>
                                                <a class="next page-numbers" href="#">
                                                    <span class="lnr lnr-arrow-right"></span>
                                                </a>
                                            </div>
                                        </nav>
                                    </div> -->
                                    <!-- end /.comment pagination area -->
                                </div>
                                <!-- end /.comments -->
                            </div>
                            <!-- end /.product-comment -->

                            <div class="fade tab-pane product-tab" id="product-support">
                                <div class="support">
                                    <div class="support__title">
                                        <h3>Contact the Author</h3>
                                    </div>
                                    <div class="support__form">
                                        <div class="usr-msg">
                                            <p>Please
                                                <a href="login.html">sign in</a> to contact this author.</p>

                                            <form action="#" class="support_form">
                                                <div class="form-group">
                                                    <label for="subj">Support Subject:</label>
                                                    <input type="text" id="subj" class="text_field" placeholder="Enter your subject">
                                                </div>

                                                <div class="form-group">
                                                    <label for="supmsg">Support Query: </label>
                                                    <textarea class="text_field" id="supmsg" name="supmsg">Enter your text...</textarea>
                                                </div>
                                                <button type="submit" class="btn btn--lg btn--round">Submit Now</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end /.product-support -->

                            <div class="fade tab-pane product-tab" id="product-faq">
                                <div class="tab-content-wrapper">
                                    <div class="panel-group accordion" role="tablist" id="accordion">
                                        @if($faq = json_decode($product->faq))
                                            @php $i = 0 @endphp
											@foreach($faq as $qa)
                                            @php $i++ @endphp
											   <div class="panel accordion__single" id="panel-{{ $i }}">
	                                            <div class="single_acco_title">
	                                                <h4>
	                                                    <a data-toggle="collapse" href="#collapse{{ $i }}" class="collapsed" aria-expanded="false" data-target="#collapse1" aria-controls="collapse1">
	                                                        <span>{{ $qa->Q }}</span>
	                                                        <i class="lnr lnr-chevron-down indicator"></i>
	                                                    </a>
	                                                </h4>
	                                            </div>

	                                            <div id="collapse{{ $i }}" class="panel-collapse collapse" aria-labelledby="panel-{{ $i }}" data-parent="#accordion">
	                                                <div class="panel-body">
	                                                    <p>{{ $qa->A }}</p>
	                                                </div>
	                                            </div>
	                                        </div>
											@endforeach
										@else
											{{ __('No Questions / Answers added yet') }}.
										@endif
                                        <!-- end /.accordion__single -->
                                    </div>
                                    <!-- end /.accordion -->
                                </div>

                            </div>
                            <!-- end /.product-faq -->
                        </div>
                        <!-- end /.tab-content -->
                    </div>
                    <!-- end /.item-info -->
                </div>
                <!-- end /.col-md-8 -->

                <div class="col-lg-4">
                    <aside class="sidebar sidebar--single-product">
                        <div class="sidebar-card card-pricing">
                            <div class="price">
                                <h1>
                                    <sup>{{ currency_price($product->price) }}
                                    </sup>
                                    <!-- <sup>$</sup>60 -->
                                </h1>
                            </div>
                            <ul class="pricing-options">
                                <li>
                                    <div class="custom-radio">
                                        <input type="radio" id="opt1" class="" name="filter_opt" checked>
                                        <label for="opt1">
                                            <span class="circle"></span>Single Site License –
                                            <span class="pricing__opt">$20.00</span>
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="custom-radio">
                                        <input type="radio" id="opt2" class="" name="filter_opt">
                                        <label for="opt2">
                                            <span class="circle"></span>2 Sites License –
                                            <span class="pricing__opt">$40.00</span>
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="custom-radio">
                                        <input type="radio" id="opt3" class="" name="filter_opt">
                                        <label for="opt3">
                                            <span class="circle"></span>Multi Site License –
                                            <span class="pricing__opt">$60.00</span>
                                        </label>
                                    </div>
                                </li>
                            </ul>
                            <!-- end /.pricing-options -->

                            <div class="purchase-button">
                                <a href="#" class="btn btn--lg btn--round" @click="buyNow">Purchase Now</a>
                                <a href="#" class="btn btn--lg btn--round cart-btn" @click="addToCart">
                                    <span class="lnr lnr-cart"></span> Add To Cart</a>
                            </div>
                            <!-- end /.purchase-button -->
                        </div>
                        <!-- end /.sidebar--card -->

                        <div class="sidebar-card card--metadata">
                            <ul class="data">
                                <li>
                                    <p>
                                        <span class="lnr lnr-cart pcolor"></span>Total Sales</p>
                                    <span>{{ $product->sales }}</span>
                                </li>
                                <li>
                                    <p>
                                        <span class="lnr lnr-heart scolor"></span>Favorites</p>
                                    <span>240</span>
                                </li>
                                <li>
                                    <p>
                                        <span class="lnr lnr-bubble mcolor3"></span>Comments</p>
                                    <span>{{$comments->count()}}</span>
                                </li>
                            </ul>


                            <div class="rating product--rating">
                                <ul>
                                    <li>
                                        <span class="fa fa-star"></span>
                                    </li>
                                    <li>
                                        <span class="fa fa-star"></span>
                                    </li>
                                    <li>
                                        <span class="fa fa-star"></span>
                                    </li>
                                    <li>
                                        <span class="fa fa-star"></span>
                                    </li>
                                    <li>
                                        <span class="fa fa-star-half-o"></span>
                                    </li>
                                </ul>
                                <span class="rating__count">( {{$product->reviews_count}} Ratings )</span>
                            </div>
                            <!-- end /.rating -->
                        </div>
                        <!-- end /.sidebar-card -->

                        <div class="sidebar-card card--product-infos">
                            <div class="card-title">
                                <h4>Product Information</h4>
                            </div>

                            <ul class="infos">
                            	@if($product->category)
                            	<li>
                                    <p class="data-label">Category</p>
                                    <p class="info">{{ $product->category }}</p>
                                </li>
								@endif

                            	@if($product->release_date)
                                <li>
                                    <p class="data-label">Released</p>
                                    <p class="info">{{ $product->release_date }}</p>
                                </li>
                                @endif
                                @if($product->last_update)
                                <li>
                                    <p class="data-label">Updated</p>
                                    <p class="info">{{ $product->last_update }}</p>
                                </li>
                                @endif
                                
                                 @if($product->version)
                                <li>
                                    <p class="data-label">Version</p>
                                    <p class="info">{{$product->version}}</p>
                                </li>
                                 @endif
                                 @if($product->included_files)
								 <li>
									<p class="data-label">Includes files</p>
									<p class="info">{{ $product->included_files }}</p>
								</li>
								@endif

								@if($product->tags)
								 <li>
									<p class="data-label">Tags</p>
									<p class="info">{{ $product->tags }}</p>
								</li>
								@endif

								@if($product->compatible_browsers)
								 <li>
									<p class="data-label">Compatible browsers</p>
									<p class="info">{{ $product->compatible_browsers }}</p>
								</li>
								@endif

								@if($product->file_size)
								 <li>
									<p class="data-label">File size</p>
									<p class="info">{{ ceil($product->file_size / 1024) }}mb</p>
								</li>
								@endif

								 <li>
									<p class="data-label">Comments</strong></td>
									<p class="info">{{ $product->comments_count }}</p>
								</li>

								@if($product->rating)
								 <li>
									<p class="data-label">Rating</p>
									<td><div class="ui star rating disabled" data-rating="{{ $product->rating }}" data-max-rating="5"></div></p>
								</li>
								@endif

								@if($product->high_resolution)
								 <li>
									<p class="data-label">High resolutoin</p>
									<p class="info">{{ $product->high_resolution ? 'Yes' : 'No' }}</p>
								</li>
								@endif
								<li>
									<p class="data-label">Sales</p>
									<p class="info">{{ $product->sales }}</p>
								</li>
                            </ul>
                        </div>
                        <!-- end /.aside -->

                        <div class="author-card sidebar-card ">
                            <div class="card-title">
                                <h4>Product Information</h4>
                            </div>

                            <div class="author-infos">
                                <div class="author_avatar">
                                    <img src="images/author-avatar.jpg" alt="Presenting the broken author avatar :D">
                                </div>

                                <div class="author">
                                    <h4>AazzTech</h4>
                                    <p>Signed Up: 08 April 2016</p>
                                </div>
                                <!-- end /.author -->

                                <div class="social social--color--filled">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <span class="fa fa-facebook"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="fa fa-twitter"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="fa fa-dribbble"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- end /.social -->

                                <div class="author-btn">
                                    <a href="#" class="btn btn--sm btn--round">View Profile</a>
                                    <a href="#" class="btn btn--sm btn--round">Message</a>
                                </div>
                                <!-- end /.author-btn -->
                            </div>
                            <!-- end /.author-infos -->


                        </div>
                        <!-- end /.author-card -->
                    </aside>
                    <!-- end /.aside -->
                </div>
                <!-- end /.col-md-4 -->
            </div>
            <!-- end /.row -->
        </div>
        <!-- end /.container -->
    </section>
    <!--===========================================
        END SINGLE PRODUCT DESCRIPTION AREA
    ===============================================-->

    <!--============================================
        START MORE PRODUCT ARE
    ==============================================-->
    <section class="more_product_area section--padding">
        <div class="container">
            <div class="row">
                <!-- start col-md-12 -->
                <div class="col-md-12">
                    <div class="section-title">
                        <h1>More Items
                            <!-- <span class="highlighted">by Aazztech</span> -->
                        </h1>
                    </div>
                </div>
                <!-- end /.col-md-12 -->
                @foreach($similar_products as $similar_product)
                <!-- start .col-md-4 -->
                <div class="col-lg-4 col-md-6">
                    <!-- start .single-product -->
                    <div class="product product--card">

                        <div class="product__thumbnail">
                            <img src="{{ asset_("storage/covers/{$similar_product->cover}") }}" alt="Product Image">
                            <div class="prod_btn">
                                <a href="{{ route('home.product', $similar_product->slug) }}" class="transparent btn--sm btn--round">More Info</a>
                                <a href="{{ route('home.product', $similar_product->slug) }}" class="transparent btn--sm btn--round">Live Demo</a>
                            </div>
                            <!-- end /.prod_btn -->
                        </div>
                        <!-- end /.product__thumbnail -->

                        <div class="product-desc">
                            <a href="{{ route('home.product', $similar_product->slug) }}" class="product_title">
                                <h4>{{ $similar_product->name }}</h4>
                            </a>
                            <ul class="titlebtm">
                                <!-- <li>
                                    <img class="auth-img" src="images/auth.jpg" alt="author image">
                                    <p>
                                        <a href="#">AazzTech</a>
                                    </p>
                                </li> -->
                                <!-- <li class="product_cat">
                                    <a href="#">
                                        <img src="images/catph.png" alt="Category Image">{{-- rand_subcategory($similar_product->subcategories, $similar_product->category_name) --}}</a>
                                </li> -->
                            </ul> 

                            <p>{{ $similar_product->short_description }}</p>
                        </div>
                        <!-- end /.product-desc -->

                        <div class="product-purchase">
                            <div class="price_love">
                                <span>
                                	 @if(!$similar_product->price)
									   	Free
								   	@else
								    	{{ currency_price($similar_product->price) }}
								    @endif
                                </span>
                                <p>
                                    <span class="lnr lnr-heart"></span> 48</p>
                            </div>

                            <div class="rating product--rating">
                                <ul>
                                	@for ($i = 0; $i < $similar_product->rating; $i++)
                                    <li>
                                        <span class="fa fa-star"></span>
                                    </li>
                                    @endfor
                                </ul>
                            </div>

                            <div class="sell">
                                <p>
                                    <span class="lnr lnr-cart"></span>
                                    <span>{{ $product->sales }}</span>
                                </p>
                            </div>
                        </div>
                        <!-- end /.product-purchase -->
                    </div>
                    <!-- end /.single-product -->
                </div>
                <!-- end /.col-md-4 -->
                @endforeach
            </div>
            <!-- end /.container -->
        </div>
        <!-- end /.container -->
    </section>
    <!--============================================
        END MORE PRODUCT AREA
    ==============================================-->


    <!--================================
        START CALL TO ACTION AREA
    =================================-->
    <section class="call-to-action bgimage">
        <div class="bg_image_holder">
            <img src="images/calltobg.jpg" alt="">
        </div>
        <div class="container content_above">
            <div class="row">
                <div class="col-md-12">
                    <div class="call-to-wrap">
                        <h1 class="text--white">Ready to Join Our Marketplace!</h1>
                        <h4 class="text--white">Over 25,000 designers and developers trust the MartPlace.</h4>
                        <a href="#" class="btn btn--lg btn--round btn--white callto-action-btn">Join Us Today</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================================
        END CALL TO ACTION AREA
    =================================-->

@endsection