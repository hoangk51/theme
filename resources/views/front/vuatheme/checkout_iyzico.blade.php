<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="UTF-8">
    <meta name="language" content="{{ str_replace('_', '-', app()->getLocale()) }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{{ asset_("storage/images/".config('app.favicon'))}}">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <!-- App CSS -->
    <link rel="stylesheet" href="{{ asset_('assets/default/css/app.css') }}">

    <style>
      body, html {
        height: 100vh !important;
      }
      
      .main.container {
        height: 100%;
        display: contents;
        padding-top: 0 !important;
      }

      .grid {
        height: 100%;
      }

      .form.column {
        width: 350px !important;
      }
    </style>

  </head>

  <body>
    <div class="ui main fluid container pt-0" id="app">
      <div class="ui one column celled middle aligned grid m-0 shadowless">
        <div class="form column mx-auto">
          <div class="ui fluid card">

            <div class="content center aligned p-0">
              <a href="//iyzico.com" target="_blank">
                <img class="ui small image mx-auto p-1" src="{{ asset_("assets/images/iyzico_logo.svg") }}" alt="{{ config('app.name') }}">
              </a>
            </div>

            <div class="content center aligned">
              <h2>{{ __('Iyzico Pre-Checkout') }}</h2>
            </div>

            <div class="content">
              @if(session('IyzicoError'))
              <div class="ui negative fluid small message">
                {{ session('IyzicoError') }}
              </div>
              @endif

              <form class="ui form" action="{{ route('home.checkout.iyzico_init') }}" method="post">
                @csrf 
                
                <div class="two fields">
                  <div class="field">
                    <label>{{ __('First name') }}</label>
                    <input type="text" placeholder="..." name="firstname" value="{{ old('firstname', $user->firstname ?? null) }}" required autocomplete="firstname" autofocus>
              
                    @error('firstname')
                      <div class="ui negative message">
                        <strong>{{ $message }}</strong>
                      </div>
                    @enderror
                  </div>
                  <div class="field">
                    <label>{{ __('Last name') }}</label>
                    <input type="text" placeholder="..." name="lastname" value="{{ old('lastname', $user->lastname ?? null) }}" required autocomplete="lastname">
              
                    @error('lastname')
                      <div class="ui negative message">
                        <strong>{{ $message }}</strong>
                      </div>
                    @enderror
                  </div>
                </div>

                <div class="field">
                  <label>{{ __('ID number') }}</label>
                  <input type="text" placeholder="..." name="id_number" value="{{ old('id_number') }}" required autocomplete="id_number">
            
                  @error('id_number')
                    <div class="ui negative message">
                      <strong>{{ $message }}</strong>
                    </div>
                  @enderror
                </div>

                <div class="two fields">
                  <div class="field">
                    <label>{{ __('City') }}</label>
                    <input type="text" placeholder="..." name="city" value="{{ old('city', $user->city ?? null) }}" required autocomplete="city">
              
                    @error('city')
                      <div class="ui negative message">
                        <strong>{{ $message }}</strong>
                      </div>
                    @enderror
                  </div>
                  <div class="field">
                    <label>{{ __('Country') }}</label>
                    <input type="text" placeholder="..." name="country" value="{{ old('country', $user->country ?? null) }}" required autocomplete="country">
              
                    @error('country')
                      <div class="ui negative message">
                        <strong>{{ $message }}</strong>
                      </div>
                    @enderror
                  </div>
                </div>

                <div class="field">
                  <label>{{ __('Address') }}</label>
                  <input type="text" placeholder="..." name="address" value="{{ old('address') }}" required autocomplete="address">
            
                  @error('address')
                    <div class="ui negative message">
                      <strong>{{ $message }}</strong>
                    </div>
                  @enderror
                </div>

                <div class="field">
                  <label>Email</label>
                  <input type="email" placeholder="..." name="email" value="{{ old('email', $user->email ?? null) }}" required autocomplete="email">
                  <input type="hidden" class="d-none" value="{{ request()->ip() }}" name="ip_address">
                  
                  @error('email')
                    <div class="ui negative message">
                      <strong>{{ $message }}</strong>
                    </div>
                  @enderror
                </div>
            
                <div class="field mb-0">
                  <div class="tow spaced buttons">
                    <button class="ui blue basic fluid button" type="submit">Continue</button>
                    <a class="ui red basic fluid button" href="{{ route('home.checkout.iyzico_cancel') }}">Cancel</a>
                  </div>
                </div>
              </form>
            </div>

            <div class="content center aligned">
              <p>{{ __('This information is required by Iyzico to complete the checkout') }}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>