  
   <!-- ================================
    START MENU AREA
================================= -->
    <!-- start menu-area -->
    <div class="menu-area">
        <!-- start .top-menu-area -->
        <div class="top-menu-area">
            <!-- start .container -->
            <div class="container">
                <!-- start .row -->
                <div class="row">
                    <!-- start .col-md-3 -->
                    <div class="col-lg-3 col-md-3 col-6 v_middle">
                        <div class="logo">
                            <a href="{{route('home')}}">
                                <img src="{{ asset_("storage/images/".config('app.logo')) }}" class="img-fluid">
                            </a>
                        </div>
                    </div>
                    <!-- end /.col-md-3 -->

                    <!-- start .col-md-5 -->
                    <div class="col-lg-8 offset-lg-1 col-md-9 col-6 v_middle">
                        <!-- start .author-area -->
                        <div class="author-area">
                             @guest
                             <a href="{{ route('register') }}" class="author-area__seller-btn inline" style="background-color: #7347c1">Join now</a>

                            <a href="{{ route('login', ['redirect' => url()->current()]) }}" class="author-area__seller-btn inline">Login</a>
                             @endguest
                            
                            <div class="author__notification_area">
                                <ul>
                                    @auth
                                    <li class="has_dropdown">
                                        <div class="icon_wrap">
                                            <span class="lnr lnr-alarm"></span>
                                            <span class="notification_count noti">{{ count(config('notifications', [])) }}</span>
                                        </div>

                                        <div class="dropdowns notification--dropdown">

                                            <div class="dropdown_module_header">
                                                <h4>My Notifications</h4>
                                                <a href="notification.html">View All</a>
                                            </div>

                                            <div class="notifications_module">
                                                 @if(config('notifications'))
                                                @foreach(config('notifications') as $notif)
                                                 <div class="notification" data-id="{{ $notif->id }}"
                                                   data-href="{{ route('home.product', $notif->slug . ($notif->for == 1 ? '#support' : ($notif->for == 2 ? '#reviews' : ''))) }}">
                                                    <div class="notification__info">
                                                        <div class="info_avatar">
                                                            @if($notif->for == 0)
                                                            <img src="{{ asset_("storage/thumbnails/{$notif->image}") }}">
                                                            @else
                                                            <img src="{{ asset_("storage/avatars/{$notif->image}") }}">
                                                            @endif
                                                        </div>
                                                        <div class="info">
                                                            <p>
                                                                {!! $notif->text !!}
                                                            </p>
                                                            <p class="time">{{ \Carbon\Carbon::parse($notif->updated_at)->diffForHumans() }}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <!-- end /.notifications -->

                                                    <div class="notification__icons ">
                                                        <span class="lnr lnr-heart loved noti_icon"></span>
                                                    </div>
                                                    <!-- end /.notifications -->
                                                </div>
                                                @endforeach

                                                @else
                                                
                                                 <div class="notification">
                                                    <div class="notification__info">
                                                    You have 0 new notifications
                                                  </div>
                                                </div>
                                                
                                                @endif

                                            </div>
                                            <!-- end /.dropdown -->
                                        </div>
                                    </li>
                                    
                                     @endauth
                                    <li class="has_dropdown">
                                        <div class="icon_wrap">
                                            <span class="lnr lnr-cart"></span>
                                            <span class="notification_count purch">@{{ cartItems }}</span>
                                        </div>

                                        <div class="dropdowns dropdown--cart">
                                            <div class="cart_area">
                                                <div class="cart_product"  v-for="product in cart">
                                                    <div class="product__info">
                                                        <div class="thumbn">
                                                            <img :src="product.thumbnail" alt="cart product thumbnail">
                                                        </div>

                                                        <div class="info">
                                                            <a class="title" :href="product.url">@{{ product.name }}</a>
                                                           <!--  <div class="cat">
                                                                <a href="#">
                                                                    <img src="images/catword.png" alt="">Wordpress</a>
                                                            </div> -->
                                                        </div>
                                                    </div>

                                                    <div class="product__action">
                                                        <a  @click="removeFromCart(product.id)">
                                                            <span class="lnr lnr-trash"></span>
                                                        </a>
                                                        <p> {{ config('payments.currency_code') }} @{{ product.price }}</p>
                                                    </div>
                                                </div>
                                               
                                              <!--   <div class="total">
                                                    <p>
                                                        <span>Total :</span>$80</p>
                                                </div> -->
                                                <div class="cart_action">
                                                     <a class="go_cart" href="{{ route('home.checkout') }}">View Cart</a> 
                                                    <a class="go_checkout" href="{{ route('home.checkout') }}">Checkout</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <!--start .author__notification_area -->
                            @auth
                            <!--start .author-author__info-->
                            <div class="author-author__info inline has_dropdown">
                                <div class="author__avatar">
                                    <img width="50" height="50"  src="{{ asset_("storage/avatars/". if_null(auth()->user()->avatar, 'default.jpg')) }}" alt="user avatar">

                                </div>
                                <div class="autor__info">
                                    <p class="name">
                                        {{auth()->user()->name}}
                                    </p>
                                    <p class="ammount">$20.45</p>
                                </div>

                                <div class="dropdowns dropdown--author">
                                    <ul>
                                        <li>
                                            <a href="{{ route('profile.edit') }}">
                                                <span class="lnr lnr-user"></span>Profile</a>
                                        </li>
                                          @if(auth_is_admin())
                                        <li>
                                            <a href="{{ route('admin') }}">
                                                <span class="lnr lnr-home"></span> Dashboard</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('settings', 'general') }}">
                                                <span class="lnr lnr-cog"></span> Setting</a>
                                        </li>
                                        
                                        <li>
                                            <a href="{{ route('home.favorites') }}">
                                                <span class="lnr lnr-heart"></span> {{ __('Favourite') }}</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('categories') }}">
                                                <span class="lnr lnr-dice"></span>List category</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('categories.create') }}">
                                                <span class="lnr lnr-chart-bars"></span>Create category</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('products.create') }}">
                                                <span class="lnr lnr-upload"></span>Upload Item</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('products') }}">
                                                <span class="lnr lnr-book"></span>Manage Item</a>
                                        </li>
                                         <li>
                                            <a href="{{ route('posts') }}">
                                                <span class="lnr lnr-dice"></span>List post</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('posts.create') }}">
                                                <span class="lnr lnr-chart-bars"></span>Create post</a>
                                        </li>
                                         <li>
                                            <a href="{{ route('pages') }}">
                                                <span class="lnr lnr-dice"></span>List pages</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('pages.create') }}">
                                                <span class="lnr lnr-chart-bars"></span>Create page</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('support') }}">
                                                <span class="lnr lnr-cart"></span>Support</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('faq') }}">
                                                <span class="lnr lnr-briefcase"></span>Faq</a>
                                        </li>
                                        
                                        @else
                                        <li>
                                            <a href="{{ route('home.favorites') }}">
                                                <span class="lnr lnr-briefcase"></span> {{ __('Collection') }}</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('home.downloads') }}">
                                                <span class="lnr lnr-briefcase"></span> {{ __('Downloads') }}</a>
                                        </li>
                                        @endif
                                        <li>
                                            <a href="#"  @click="logout">
                                                <span class="lnr lnr-exit"></span>Logout</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!--end /.author-author__info-->
                            @endauth
                        </div>
                        <!-- end .author-area -->

                        <!-- author area restructured for mobile -->
                        <div class="mobile_content ">
                            <span class="lnr lnr-user menu_icon"></span>

                            <!-- offcanvas menu -->
                            <div class="offcanvas-menu closed">
                                <span class="lnr lnr-cross close_menu"></span>
                                <div class="author-author__info">
                                    <div class="author__avatar v_middle">
                                        <img src="images/usr_avatar.png" alt="user avatar">
                                    </div>
                                    <div class="autor__info v_middle">
                                        <p class="name">
                                            Jhon Doe
                                        </p>
                                        <p class="ammount">$20.45</p>
                                    </div>
                                </div>
                                <!--end /.author-author__info-->

                                <div class="author__notification_area">
                                    <ul>
                                        <li>
                                            <a href="notification.html">
                                                <div class="icon_wrap">
                                                    <span class="lnr lnr-alarm"></span>
                                                    <span class="notification_count noti">25</span>
                                                </div>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="message.html">
                                                <div class="icon_wrap">
                                                    <span class="lnr lnr-envelope"></span>
                                                    <span class="notification_count msg">6</span>
                                                </div>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="cart.html">
                                                <div class="icon_wrap">
                                                    <span class="lnr lnr-cart"></span>
                                                    <span class="notification_count purch">2</span>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <!--start .author__notification_area -->

                                <div class="dropdowns dropdown--author">
                                    <ul>
                                        <li>
                                            <a href="author.html">
                                                <span class="lnr lnr-user"></span>Profile</a>
                                        </li>
                                        <li>
                                            <a href="dashboard.html">
                                                <span class="lnr lnr-home"></span> Dashboard</a>
                                        </li>
                                        <li>
                                            <a href="dashboard-setting.html">
                                                <span class="lnr lnr-cog"></span> Setting</a>
                                        </li>
                                        <li>
                                            <a href="cart.html">
                                                <span class="lnr lnr-cart"></span>Purchases</a>
                                        </li>
                                        <li>
                                            <a href="favourites.html">
                                                <span class="lnr lnr-heart"></span> Favourite</a>
                                        </li>
                                        <li>
                                            <a href="dashboard-add-credit.html">
                                                <span class="lnr lnr-dice"></span>Add Credits</a>
                                        </li>
                                        <li>
                                            <a href="dashboard-statement.html">
                                                <span class="lnr lnr-chart-bars"></span>Sale Statement</a>
                                        </li>
                                        <li>
                                            <a href="dashboard-upload.html">
                                                <span class="lnr lnr-upload"></span>Upload Item</a>
                                        </li>
                                        <li>
                                            <a href="dashboard-manage-item.html">
                                                <span class="lnr lnr-book"></span>Manage Item</a>
                                        </li>
                                        <li>
                                            <a href="dashboard-withdrawal.html">
                                                <span class="lnr lnr-briefcase"></span>Withdrawals</a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="lnr lnr-exit"></span>Logout</a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="text-center">
                                    <a href="signup.html" class="author-area__seller-btn inline">Become a Seller</a>
                                </div>
                            </div>
                        </div>
                        <!-- end /.mobile_content -->
                    </div>
                    <!-- end /.col-md-5 -->
                </div>
                <!-- end /.row -->
            </div>
            <!-- end /.container -->
        </div>
        <!-- end  -->

        <!-- start .mainmenu_area -->
        <div class="mainmenu">
            <!-- start .container -->
            <div class="container">
                <!-- start .row-->
                <div class="row">
                    <!-- start .col-md-12 -->
                    <div class="col-md-12">
                        <div class="navbar-header">
                            <!-- start mainmenu__search -->
                            <div class="mainmenu__search">
                                <form action="{{ route('home.products.q') }}" method="get" >
                                    <div class="searc-wrap">
                                        <input type="text" placeholder="Search product" name="q">
                                        <button type="submit" class="search-wrap__btn">
                                            <span class="lnr lnr-magnifier"></span>
                                        </button>
                                    </div>
                                </form>
                            </div>
                            <!-- start mainmenu__search -->
                        </div>

                        <nav class="navbar navbar-expand-md navbar-light mainmenu__menu">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
                                aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="navbarNav">
                                <ul class="navbar-nav">
                                    <li class="">
                                        <a href="{{route('home')}}">HOME</a>
                                       
                                    </li>
                                    <li class="">
                                        <a href="{{ route('home.blog') }}">{{ __('Blog') }}</a>
                                    </li>
                                    <li class="has_dropdown">
                                        <a href="#">Categories</a>
                                        <div class="dropdowns dropdown--menu">
                                            <ul>
                                              @foreach($categories as $cate)
                                                <li>
                                                    <a href="{{$cate->link()}}">{{ $cate->name}}</a>
                                                </li>
                                               @endforeach
                                            </ul>
                                        </div>
                                    </li>
                                    <li class="">
                                        <a href="{{ route('home.favorites') }}">{{ __('Collection') }}</a>
                                    </li>
                                    <li class="">
                                        <a href="{{ route('home.support') }}">{{ __('Support') }}</a>
                                    </li>
                                    <li>
                                        <a href="contact.html">contact</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.navbar-collapse -->
                        </nav>
                    </div>
                    <!-- end /.col-md-12 -->
                </div>
                <!-- end /.row-->
            </div>
            <!-- start .container -->
        </div>
        <!-- end /.mainmenu-->
    </div>
    <!-- end /.menu-area -->
    <!--================================
    END MENU AREA
=================================-->