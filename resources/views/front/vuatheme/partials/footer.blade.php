  <!--================================
    START FOOTER AREA
=================================-->
    <footer class="footer-area">
        <div class="footer-big section--padding">
            <!-- start .container -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="info-footer">
                            <div class="info__logo">
                                <img src="{{ asset_("storage/images/".config('app.logo')) }}" alt="{{ config('app.name') }}">
                            </div>
                            <p class="info--text">{{ config('app.description') }}</p>
                            <ul class="info-contact">
                                <li>
                                    <span class="lnr lnr-phone info-icon"></span>
                                    <span class="info">Phone: +6789-875-2235</span>
                                </li>
                                <li>
                                    <span class="lnr lnr-envelope info-icon"></span>
                                    <span class="info">support@aazztech.com</span>
                                </li>
                                <li>
                                    <span class="lnr lnr-map-marker info-icon"></span>
                                    <span class="info">202 New Hampshire Avenue Northwest #100, New York-2573</span>
                                </li>
                            </ul>
                        </div>
                        <!-- end /.info-footer -->
                    </div>
                    <!-- end /.col-md-3 -->

                    <div class="col-lg-5 col-md-6">
                        <div class="footer-menu">
                            <h4 class="footer-widget-title text--white"><strong>F</strong>EATURED CATEGORIES</h4>
                            <ul>
                                @foreach (config('popular_categories', []) as $p_category)
                                <li><a href="{{ route('home.products.category', $p_category->slug) }}">{{ $p_category->name }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                        <!-- end /.footer-menu -->

                        <div class="footer-menu">
                            <h4 class="footer-widget-title text--white"><strong>A</strong>DDITIONAL RESOURCES</h4>
                            <ul>
                                <li><a href="{{ route('home.support') }}">Contact</a></li>
                                <li><a href="{{ route('home.support') }}">FAQ</a></li>
                                @foreach (config('pages', []) as $page)
                                <li><a href="{{ route('home.page', $page->slug) }}">{{ $page->name }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                        <!-- end /.footer-menu -->
                    </div>
                    <!-- end /.col-md-5 -->

                    <div class="col-lg-4 col-md-12">
                        <div class="newsletter">
                            <h4 class="footer-widget-title text--white">Newsletter</h4>
                            <p>Subscribe to get the latest news, update and offer information. Don't worry, we won't send spam!</p>
                            <div class="newsletter__form">
                                <form action="{{ route('home.newsletter', ['redirect' => url()->current()]) }}" method="post">
                                    @csrf
                                    @if(session('newsletter_subscription_msg'))
                                    <p>
                                        {{ session('newsletter_subscription_msg') }}
                                    </p>
                                    @endif
                                    <div class="field-wrapper">
                                        <input name="email" class="relative-field rounded" type="text" placeholder="Enter email">
                                        <button class="btn btn--round" type="submit">Submit</button>
                                    </div>
                                </form>
                            </div>

                            <!-- start .social -->
                            <div class="social social--color--filled">
                                <ul>
                                    <li>
                                        <a href="{{ config('app.facebook') }}">
                                            <span class="fa fa-facebook"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ config('app.twitter') }}">
                                            <span class="fa fa-twitter"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ config('app.youtube') }}">
                                            <span class="fa fa-google-plus"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ config('app.pinterest') }}">
                                            <span class="fa fa-pinterest"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ config('app.twitter') }}">
                                            <span class="fa fa-linkedin"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ config('app.tumblr') }}">
                                            <span class="fa fa-dribbble"></span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- end /.social -->
                        </div>
                        <!-- end /.newsletter -->
                    </div>
                    <!-- end /.col-md-4 -->
                </div>
                <!-- end /.row -->
            </div>
            <!-- end /.container -->
        </div>
        <!-- end /.footer-big -->

        <div class="mini-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="copyright-text">
                            <p>&copy;  {{ date('Y') }}
                                <a href="#">{{ config('app.name') }}</a>. All rights reserved. Created by
                                <a href="#">AazzTech</a>
                            </p>
                        </div>

                        <div class="go_top">
                            <span class="lnr lnr-chevron-up"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--================================
    END FOOTER AREA
=================================-->


@auth
	<form id="logout-form" action="{{ route('logout', ['redirect' => url()->current()]) }}" method="POST" class="d-none">@csrf</form>
@endauth