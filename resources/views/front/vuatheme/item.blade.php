
                <!-- start .col-md-4 -->
                <div class="col-lg-4 col-md-6">
                    <!-- start .single-product -->
                    <div class="product product--card">

                        <div class="product__thumbnail">
                            <img src="{{ asset_("storage/covers/{$item->cover}") }}" alt="Product Image">
                            <div class="prod_btn">
                                <a href="{{ route('home.product', $item->slug) }}" class="transparent btn--sm btn--round">More Info</a>
                                <a href="{{ route('home.product', $item->slug) }}" class="transparent btn--sm btn--round">Live Demo</a>
                            </div>
                            <!-- end /.prod_btn -->
                        </div>
                        <!-- end /.product__thumbnail -->

                        <div class="product-desc">
                            <a href="{{ route('home.product', $item->slug) }}" class="product_title">
                                <h4>{{$item->name}}</h4>
                            </a>
                            <ul class="titlebtm">
                                <li>
                                    <img class="auth-img" src="{{ asset_('assets/vuatheme/images/auth.jpg') }}" alt="author image">
                                    <p>
                                        <a href="#">AazzTech</a>
                                    </p>
                                </li>
                                <li class="product_cat">
                                    <a href="#">
                                        <span class="lnr lnr-book"></span> {{ rand_subcategory($item->subcategories, $item->category_name) }}</a>
                                </li>
                            </ul>

                            <p>{{$item->short_description}}</p>
                        </div>
                        <!-- end /.product-desc -->

                        <div class="product-purchase">
                            <div class="price_love">
                                <span>{{currency_price($item->price)}}</span>
                                <p>
                                    <span class="lnr lnr-heart"></span>{{ $item->views }} </p>
                            </div>
                            <div class="sell">
                                <p>
                                    <span class="lnr lnr-cart"></span>
                                    <span>@if($item->sales){{ $item->sales }} @else 0 @endif</span>
                                </p>
                            </div>
                        </div>
                        <!-- end /.product-purchase -->
                    </div>
                    <!-- end /.single-product -->
                </div>
                <!-- end /.col-md-4 -->