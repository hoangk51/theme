@extends('front.default.master')

@section('additional_head_tags')
<script>
	'use strict';
	
	window.props['countryNames'] = {!! config('app.country_names') !!};
</script>
@endsection

@section('body')
	<div class="ui two columns shadowless celled grid my-0 px-1" id="user-profile">	

		<div class="one column row" id="page-title">
			<div class="fourteen wide column mx-auto">
				<div class="ui unstackable items">
				  <div class="item">
				    <div class="ui rounded tiny image">
				      <img 	src="{{ asset_("storage/avatars/".($user->avatar ?? 'default.jpg').'?v='.time()) }}" 
					      		onclick="$('#user-profile input[name=\'avatar\']').click()" >
				    </div>
				    <div class="content">
							<span class="header">{{ $user->fullname ?? $user->name ?? null }}</span>
				      <div class="description">
				      	<p><strong>Country : </strong>{{ $user->country }}</p>
				      	<p><strong>City : </strong>{{ $user->city }}</p>
				      	<p><strong>Member since : </strong>{{ (new DateTime($user->created_at))->format("d F Y") }}</p>
				      </div>
				    </div>
				  </div>
				</div>
			</div>
		</div>
	
		@if($errors->any())
    @foreach ($errors->all() as $error)
       <div class="ui negative w-100 small message">
       	<i class="times icon close" onclick="this.parentElement.remove()"></i>
       	{{ $error }}
       </div>
    @endforeach
		@endif

		@if(session('profile_updated'))
		<div class="ui positive w-75 mx-auto small message">
			<i class="times icon close" onclick="this.parentElement.remove()"></i>
			{{ session('profile_updated') }}
		</div>
		@endif

		<div class="ui unstackable secondary w-100 menu my-0 py-1 mx-0">
			<a class="item active ml-0" data-tab="personal">Personal</a>
			@if($user->password)
			<a class="item" data-tab="password">Password</a>
			@endif
		</div>

		<div class="one column row">
			<div class="column mx-auto">
				<form class="ui form personal" method="post" action="{{ route('home.profile') }}" enctype="multipart/form-data">
					@csrf

					<input type="file" name="avatar" class="d-none">

					<table class="ui unstackable table borderless radiusless mb-0 personal">
						<tbody>
							<tr>
								<td>Username</td>
								<td><input type="text" name="name" value="{{ old('name', $user->name ?? null) }}"></td>
							</tr>
							<tr>
								<td>First name</td>
								<td><input type="text" name="firstname" value="{{ old('firstname', $user->firstname ?? null) }}"></td>
							</tr>
							<tr>
								<td>Last name</td>
								<td><input type="text" name="lastname" value="{{ old('lastname', $user->lastname ?? null) }}"></td>
							</tr>
							<tr>
								<td>Country</td>
								<td>
									<div class="ui selection dropdown countries">
										<input type="hidden" name="country" value="{{ old('country', $user->country ?? null) }}">
										<i class="dropdown icon"></i>
										<div class="default text">Country</div>
										<div class="menu">
											<a class="item" v-for="countryName in countryNames">
												@{{ countryName }}
											</a>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td>City</td>
								<td><input type="text" name="city" value="{{ old('city', $user->city ?? null) }}"></td>
							</tr>
						</tbody>
					</table>
					
					<table class="ui unstackable table borderless radiusless mb-0 password">
						<tbody>
							<tr>
								<td>Current</td>
								<td><input type="text" name="old_password"></td>
							</tr>
							<tr>
								<td>New</td>
								<td><input type="text" name="new_password"></td>
							</tr>
						</tbody>
					</table>

					<div class="ui divider mt-0"></div>

					<button class="ui basic white button">Save changes</button>
				</form>
			</div>
		</div>
		
	</div>
@endsection