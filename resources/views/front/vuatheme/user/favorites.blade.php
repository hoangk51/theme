@extends('front.default.master')

@section('additional_head_tags')
	<script type="application/javascript">
		'use strict';
		window.props['itemId'] = null;
		var coversBaseUrl = '{{ asset_('storage/covers') }}/';
		var baseUrl = '{{ route('home.product', '/') }}/';
	</script>
@endsection

@section('body')

<div class="ui shadowless celled grid my-0" id="items">

	<div class="one column row rounded-corner">
		<div class="column">
			<h2 class="ui header px-2 py-1"><i class="icon outline large heart"></i> Favorites</h2>
		</div>
	</div>
	
	<div class="sixteen wide column" id="user-favorites">
		<div class="wrapper" v-if="Object.keys(favorites).length" v-cloak>
			<table class="ui single line unstackable table mb-0">
				<tbody>
					<tr v-for="product in favorites">
						<td><img :src="'{{ asset_('storage/covers') }}/' + product.cover" class="ui rounded image"></td>
						<td><a :href="'{{ route('home.product', '/') }}/' + product.slug">@{{ product.name }}</a></td>
						<td>
							<button class="ui basic labeled icon button mx-0 right floated" @click="collectionToggleItem($event, product.id)">
							  <i class="close icon mx-0"></i>
							  Remove
							</button>
						</td>
					</tr>
				</tbody>
			</table>	
		</div>
		
		<div class="ui fluid message" v-else v-cloak>
			Your collection is empty.
		</div>
	</div>

</div>

@endsection