<!DOCTYPE html>
<html lang="en">
	<head>
		{!! config('app.google_analytics') !!}

		<meta charset="UTF-8">
		<meta name="language" content="en-us">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="icon" href="{{ asset("storage/images/".config('app.favicon'))}}">
		
		@include('front.default.partials.meta_data')

		<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
		<!-- App CSS -->
		  <link rel="stylesheet" href="{{ asset('assets/vuatheme/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vuatheme/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vuatheme/css/fontello.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vuatheme/css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vuatheme/css/lnr-icon.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vuatheme/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vuatheme/css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vuatheme/css/trumbowyg.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vuatheme/css/bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vuatheme/style.css') }}">

		<!-- Search engines verification -->
		<meta name="google-site-verification" content="{{ config('app.google') }}">
		<meta name="msvalidate.01" content="{{ config('app.bing') }}">
		<meta name="yandex-verification" content="{{ config('app.yandex') }}">
        
		<script>
			'use strict';
			
			window.props = {
	      product: {},
	      products: {},
	      checkoutRoute: '{{ route('home.checkout') }}',
	      productsRoute: '{{ route('home.products.category', '') }}',
	      currentRouteName: '{{ Route::currentRouteName() }}',
	      trasactionMsg: '{{ session('transaction_response') }}',
	      location: window.location,
	      paymentProcessors: {
	      	paypal: {{ var_export(config('payments.paypal.enabled') ? true : false) }},
	      	stripe: {{ var_export(config('payments.stripe.enabled') ? true : false) }},
					skrill: {{ var_export(config('payments.skrill.enabled') ? true : false) }},
					razorpay: {{ var_export(config('payments.razorpay.enabled') ? true : false) }},
					iyzico: {{ var_export(config('payments.iyzico.enabled') ? true : false) }},
	      },
	      activeScreenshot: null,
	      subcategories: {!! json_encode(config('categories.category_children', [])) !!},
	      categories: {!! json_encode(config('categories.category_parents', [])) !!},
	      workingWithFolders: @if(isFolderProcess()) true @else false @endif,
	    }
		</script>

		@yield('additional_head_tags')
	</head>
<body  class="preload home1 mutlti-vendor">
		<div id="app">
		@include('front.vuatheme.partials.top_menu')
		
		@yield('body')
		@include('front.vuatheme.partials.footer')
		</div>
		<!-- App JS -->
	<script type="application/javascript" src="{{ asset('assets/default/js/app.js') }}"></script>

	    <!--  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA0C5etf1GVmL_ldVAichWwFFVcDfa1y_c"></script> -->
	    <!-- inject:js -->



    <!-- inject:js -->
    <script src="{{ asset('assets/vuatheme/js/vendor/jquery/jquery-1.12.3.js') }}"></script>
    <script src="{{ asset('assets/vuatheme/js/vendor/jquery/popper.min.js') }}"></script>
    <script src="{{ asset('assets/vuatheme/js/vendor/jquery/uikit.min.js') }}"></script>
    <script src="{{ asset('assets/vuatheme/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/vuatheme/js/vendor/chart.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/vuatheme/js/vendor/grid.min.js') }}"></script>
    <script src="{{ asset('assets/vuatheme/js/vendor/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('assets/vuatheme/js/vendor/jquery.barrating.min.js') }}"></script>
    <script src="{{ asset('assets/vuatheme/js/vendor/jquery.countdown.min.js') }}"></script>
    <script src="{{ asset('assets/vuatheme/js/vendor/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('assets/vuatheme/js/vendor/jquery.easing1.3.js') }}"></script>
    <script src="{{ asset('assets/vuatheme/js/vendor/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('assets/vuatheme/js/vendor/slick.min.js') }}"></script>
    <script src="{{ asset('assets/vuatheme/js/vendor/tether.min.js') }}"></script>
    <script src="{{ asset('assets/vuatheme/js/vendor/trumbowyg.min.js') }}"></script>
    <script src="{{ asset('assets/vuatheme/js/vendor/waypoints.min.js') }}"></script>
    <script src="{{ asset('assets/vuatheme/js/dashboard.js') }}"></script>
    <script src="{{ asset('assets/vuatheme/js/main.js') }}"></script>
	  
	  <!--   <script src="{{-- asset('assets/vuatheme/js/map.js') --}}"></script> -->



	</body>
</html>