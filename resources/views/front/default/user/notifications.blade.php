@extends('front.default.master')

@section('body')

<div class="ui shadowless celled grid my-0" id="notifications">

	<div class="one column row" id="page-title">
		<div class="column">
			<h2 class="ui header px-2 py-1"><i class="bell outline large icon"></i> Notifications</h2>
		</div>
	</div>
	
	<div class="sixteen wide column items">
    @if($notifications)
    
    <div class="ui unstackable items">
      @foreach($notifications as $notification)
      <a class="item mx-0 @if(!$notification->read) unread @endif"
          data-id="{{ $notification->id }}"
          data-href="{{ route('home.product', $notification->slug . ($notification->for == 1 ? '#support' : ($notification->for == 2 ? '#reviews' : ''))) }}">

        <div class="ui image">
          @if($notification->for == 0)
          <img src="{{ asset_("storage/thumbnails/{$notification->image}") }}">
          @else
          <img src="{{ asset_("storage/avatars/{$notification->image}") }}">
          @endif
        </div>

        <div class="content pl-1">
          <p>{!! $notification->text !!}</p>
          <time>{{ \Carbon\Carbon::parse($notification->updated_at)->diffForHumans() }}</time>
        </div>
      </a>
      @endforeach
    </div>

    @if($notifications->hasPages())
    <div class="ui fluid divider"></div>

    {{ $notifications->onEachSide(1)->links() }}
	  {{ $notifications->links('vendor.pagination.simple-semantic-ui') }}
    @endif
    @else
    
    <div class="ui fluid message">
			You don't have any notification yet.
    </div>
    @endif
	</div>

</div>

@endsection