@extends('front.default.master')

@section('additional_head_tags')
<script type="application/ld+json">
{
	"@context": "http://schema.org",
	"@type": "Product",
	"url": "{{ url()->current() }}",
  "name": "{{ $product->name }}",
  "image": "{{ $meta_data->image }}",
  "description": "{!! $product->short_description !!}",
  @if($product->reviews_count)
  "aggregateRating": {
    "@type": "AggregateRating",
    "ratingValue": "{{ $product->rating }}",
    "reviewCount": "{{ $product->reviews_count }}",
    "bestRating": "5",
    "worstRating": "0"
  },
  "review": [
		@foreach($reviews as $review)
		{
			"@type": "Review",
			"author": "{{ $review->name ?? $review->alias_name }}",
			"datePublished" : "{{ (new DateTime($review->created_at))->format('Y-m-d') }}",
			"description": "{{ $review->content }}",
			"name": "-",
			"reviewRating": {
        "@type": "Rating",
        "bestRating": "5",
        "ratingValue": "{{ $review->rating }}",
        "worstRating": "0"
      }
		}@if(next($reviews)),@endif
		@endforeach
  ],
  @endif
  "category": "{!! $product->category !!}",
  "offers": {
    "@type": "AggregateOffer",
    "lowPrice": "{{ number_format($product->price, 2) }}",
    "highPrice": "{{ number_format($product->price, 2) }}",
    "priceCurrency": "{{ config('payments.currency_code') }}",
    "offerCount": "1"
  },
  "brand": "-",
  "sku": "-",
  "mpn": "{{ $product->id }}"
}
</script>

<script>
	'use strict';

  window.props['product'] = {
  	screenshots: {!! json_encode($product->screenshots) !!},
		id: {{ $product->id }},
		name: '{{ $product->name }}',
		cover: '{{ asset("storage/covers/{$product->cover}") }}',
		thumbnail: '{{ asset("storage/thumbnails/{$product->thumbnail}") }}',
		quantity: 1,
		url: '{{ route('home.product', $product->slug) }}',
		price: {{ $product->price }},
		slug: '{{ $product->slug }}'
  }
</script>
@endsection


@section('body')
	
<div class="ui two columns shadowless celled grid my-0 px-1" id="item">	

	<div class="one column row" id="page-title">
		<div class="fourteen wide column mx-auto">
			<div class="ui unstackable items">
			  <div class="item">
			    <div class="ui tiny image rounded">
			      <img src="{{ asset_("storage/thumbnails/{$product->thumbnail}") }}">
			    </div>
			    <div class="content">
			      <div class="header">{{ ucfirst($product->name) }}</div>
			      <div class="description">
			        <p>{{ ucfirst($product->short_description) }}</p>
			      </div>
			    </div>
			  </div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="column mx-auto l-side">
			<div class="row menu">
				<div class="ui top secondary menu p-1">
				  <a class="active item ml-0" data-tab="details">{{ __('Details') }}</a>

				  <div class="d-flex tabs">
					  <a class="item" data-tab="support">{{ __('Support') }}</a>
					  <a class="item" data-tab="reviews">{{ __('Reviews') }}</a>
					  <a class="item mr-0" data-tab="faq">{{ __('FAQ') }}</a>
					  <a class="item mr-0" data-tab="similar">{{ __('Similar') }}</a>
					  @if(isFolderProcess())
					  <a class="item mr-0" data-tab="files" @click="getFolderContent">{{ __('Files') }}</a>
					  @endif
				  </div>

				  <div class="item ui dropdown mr-0">

				   <i class="bars icon mx-0"></i>

				   <div class="menu">
					   	<a class="item" data-tab="support">{{ __('Support') }}</a>
						  <a class="item" data-tab="reviews">{{ __('Reviews') }}</a>
						  <a class="item" data-tab="faq">{{ __('FAQ') }}</a>
						  <a class="item" data-tab="similar">{{ __('Similar') }}</a>
						  @if(isFolderProcess())
						  <a class="item" data-tab="files" @click="getFolderContent">{{ __('Files') }}</a>
						  @endif
				   </div>
				  </div>

				  <div class="right menu mobile-only" id="item-r-side-toggler">
				  	<a class="item mr-0">{{ __('Buy') }}</a>
				  </div>
				</div>
			</div>

			<div class="row item">
				<div class="sixteen wide column details">
					<div class="row center aligned cover">
						<div class="ui image w-100">
							<img src="{{ asset_("storage/covers/{$product->cover}") }}" alt="Item cover" class="w-100">
							
							@if($product->preview)
							<div class="actions">
								<a id="live-preview" target="_blank" href="{{ route('home.preview', $product->slug) }}">{{ __('Live Preview') }}</a>
							</div>
							@endif
						</div>
					</div>

					<div class="ui hidden divider"></div>
					
					@if($product->screenshots)
					<div class="ui fluid card">
						<div class="content title">
							<h3 class="small header pb-0">{{ __('Screenshots') }}</h3>
							<h3 class="small header pb-0"><i class="angle down link icon mx-0"></i></h3>
							<span></span>
						</div>
						<div class="content images body">
							<div class="ui grid">
								@foreach($product->screenshots as $screenshot)
								<a class="column">
									<span><img class="screenshot" src="{{ $screenshot }}"></span>
								</a>
								@endforeach
							</div>
						</div>
					</div>
					@endif
					
					@if($product->overview)
					<div class="ui fluid card">
						<div class="content title">
							<h3 class="small header pb-0">{{ __('Overview') }}</h3>
							<h3 class="small header pb-0"><i class="angle down link icon mx-0"></i></h3>
							<span></span>
						</div>
						<div class="content overview body">
							{!! $product->overview !!}
						</div>
					</div>
					@endif
						
					@if($product->features)
					<div class="ui fluid card">
						<div class="content title">
							<h3 class="small header pb-0">{{ __('Features') }}</h3>
							<h3 class="small header pb-0"><i class="angle down link icon mx-0"></i></h3>
							<span></span>
						</div>
						<div class="content features body">
							<ul class="ui list">								
								{!! $product->features !!}
							</ul>
						</div>
					</div>
					@endif
					
					@if($product->requirements)
					<div class="ui fluid card">
						<div class="content title">
							<h3 class="small header pb-0">{{ __('Requirements') }}</h3>
							<h3 class="small header pb-0"><i class="angle down link icon mx-0"></i></h3>
							<span></span>
						</div>
						<div class="content requirements body">
							{!! $product->requirements !!}
						</div>
					</div>
					@endif
				
					@if($product->instructions)
					<div class="ui fluid card">
						<div class="content title">
							<h3 class="small header pb-0">{{ __('Instructions') }}</h3>
							<h3 class="small header pb-0"><i class="angle down link icon mx-0"></i></h3>
							<span></span>
						</div>
						<div class="content instructions body">
							{!! $product->instructions !!}
						</div>
					</div>
					@endif
				</div>
					
				<!-- Support -->
				<div class="sixteen wide column support">

					@if(session('comment_response'))

					<div class="ui fluid shadowless borderless green basic message">
						{{ request()->session()->pull('comment_response', 'default') }}
					</div>

					@elseif(!$comments->count())

					<div class="ui fluid shadowless borderless message">
						{{ __('No comments found') }}.
					</div>

					@endif

					<div class="ui divided unstackable items mt-1">
						<div class="mb-1">
							@foreach($comments as $comment)
							<div class="comments-wrapper">
								<div class="item parent">
									<div class="ui tiny circular image">
										<img src="{{ asset_("storage/avatars/".$comment->avatar) }}">
									</div>

									<div class="content description body">
										<h3>
											{{ $comment->name ?? $comment->alias_name ?? $comment->fullname }} 
											<span class="floated right">{{ $comment->created_at->diffForHumans() }}</span>
										</h3>

										{!! nl2br($comment->body) !!}
										
										<div class="ui hidden divider mt-0"></div>

										<div class="ui form right floated">
											<button class="ui mini blue button basic mr-0"
															@click="setReplyTo('{{ $comment->name ?? $comment->alias_name ?? $comment->fullname }}', {{ $comment->id }})">
												REPLY
											</button>
										</div> 
									</div>
								</div>

								@foreach($comment->children as $child)
								<div class="item children">
									<div class="ui tiny circular image">
										<img src="{{ asset_("storage/avatars/".$child->avatar) }}">
									</div>

									<div class="content description body">
										<h3>
											{{ $child->name ?? $child->alias_name ?? $child->fullname }} 
											<span class="floated right">{{ $child->created_at->diffForHumans() }}</span>
										</h3>

										{!! nl2br($child->body) !!}

										<div class="ui hidden divider mt-0"></div>

										<div class="ui form right floated">
											<button class="ui mini blue button basic mr-0" 
															@click="setReplyTo('{{ $child->name ?? $child->alias_name ?? $child->fullname }}', {{ $comment->id }})">
												REPLY
											</button>
										</div>
									</div>
								</div>
								@endforeach
							</div>
							@endforeach
						</div>
						
						@auth

						<form class="item ui form" method="post" action="{{ route('home.product', $product->slug) }}">
							@csrf

							<div class="ui tiny rounded image">
					    	<img src="{{ asset_("storage/avatars/" . (auth()->user()->avatar ?? 'default.jpg')) }}">
					    	<input type="hidden" name="type" value="support" class="none">
							  <input type="hidden" name="comment_id" :value="replyTo.commentId" class="d-none">
					    </div>
					    	
					    <div class="content pl-1">
								<div class="ui tiny blue basic label mb-1-hf capitalize" v-if="replyTo.userName !== null">
									@{{ replyTo.userName }}
									<i class="delete icon" @click="resetReplyTo"></i>
								</div>

								<textarea rows="5" name="comment" placeholder="Your comment ..."></textarea>

								<button type="submit" class="ui tiny yellow button right floated mt-1-hf">{{ __('SUBMIT') }}</button>
							</div>

						</form>

						@else

						<div class="ui fluid blue shadowless borderless message">
							<a href="{{ url('login') }}"><strong>Sign in</strong></a> to post a comment.
						</div>

						@endauth
					</div>

				</div>

				<!-- Reviews -->
				<div class="sixteen wide column reviews">
					@if(session('review_response'))
					<div class="ui fluid shadowless borderless green basic message">
						{{ request()->session()->pull('review_response', 'default') }}
					</div>
					@elseif(!$reviews->count())
					<div class="ui fluid shadowless borderless message">
						{{ __('This item has not received any review yet') }}.
					</div>
					@endif

					@if($reviews->count())
					<div class="ui divided unstackable items">
						@foreach($reviews as $review)
						<div class="item">
							<div class="ui tiny circular image">
								<img src="{{ asset_("storage/avatars/".$review->avatar) }}">
							</div>

							<div class="content description body">
								<h3>
									{{ $review->name ?? $review->alias_name ?? $review->fullname }} 
									<span class="floated right">{{ $review->created_at->diffForHumans() }}</span>
								</h3>

								<h4 class="mt-1-hf">
									<span class="ui star rating disabled ml-0 floated right" data-rating="{{ $review->rating }}" data-max-rating="5"></span>
								</h4>

								{{ nl2br($review->content) }}
							</div>
						</div>
						@endforeach
					</div>
					@endif

					@auth
					<!-- IF PURCHASED AND NOT REVIEWED YET -->
					@if(!$product->reviewed && $product->purchased)
					
					<div class="ui items borderless">
						<form class="item ui form" method="post" action="{{ route('home.product', $product->slug) }}">
							@csrf
	
							<div class="ui tiny rounded image">
								<img src="{{ asset_("storage/avatars/" . (auth()->user()->avatar ?? 'default.jpg')) }}">
								<input type="hidden" name="type" value="reviews" class="none">
							</div>
								
							<div class="content pl-1">
								<span class="ui star rating active mb-1-hf" data-max-rating="5"></span>
								<input type="hidden" name="rating" class="d-none">
											
								<textarea rows="5" name="review" placeholder="Your review ..."></textarea>
	
								<button type="submit" class="ui tiny yellow button right floated mt-1-hf">SUBMIT</button>
							</div>
						</form>
					</div>
					
					@endif
					@else
				
					<div class="ui fluid blue shadowless borderless message">
						<a href="{{ url('login') }}"><strong>Sign in</strong></a> to review this item.
					</div>

					@endauth
				</div>
					
				<!-- FAQ -->
				<div class="sixteen wide column faq">
					@if($faq = json_decode($product->faq))
					<div class="ui divided list">
						@foreach($faq as $qa)
						<div class="item p-1">
							<div class="header mb-1">Q. {{ $qa->Q }}</div>
							<strong>A.</strong> {{ $qa->A }}
						</div>
						@endforeach
					</div>
					@else
					<div class="ui message">
						{{ __('No Questions / Answers added yet') }}.
					</div>
					@endif
				</div>

				<!-- SIMILAR PRODUCTS -->
				<div class="sixteen wide column similar">
					<div class="ui four doubling cards">
						@foreach($similar_products as $similar_product)

						<div class="card">
						  <div class="content p-0 image">
						  	<a href="{{ route('home.product', $similar_product->slug) }}"><img src="{{ asset_("storage/covers/{$similar_product->cover}") }}"></a>
						  </div>

						  <a href="{{ route('home.product', $similar_product->slug) }}" class="content title py-1-hf">
						  	{{ $similar_product->name }}
						  </a>

						  <div class="content bottom p-1-hf">
						    <div class="left floated">
							    <span title="Add to collection"><i class="bordered heart icon link"></i></span>
							    @if($similar_product->preview)
									<a title="Preview" target="_blank" href="{{ route('home.preview', $similar_product->slug) }}"><i class="bordered tv icon link"></i></a>
									@endif
							    <span title="{{ $similar_product->views }} views"><i class="bordered eye icon"></i></span>
									<span title="Last update {{ (new DateTime($similar_product->updated_at))->format('d M Y') }}">
										<i class="bordered calendar alternate outline icon"></i>
									</span>
						    </div>
						    
						    @if(!$similar_product->price)
							    <div class="price right free floated">
							   		Free
							   	</div>
						   	@else
						    <div class="price right floated">
						    	{{ currency_price($similar_product->price) }}
						    </div>
						    @endif
						  </div>
						</div>

						@endforeach
					</div>
				</div>
	
				@if(isFolderProcess())

				<!-- FILES -->
				<div class="sixteen wide column files">
					<div id="files-list" v-if="folderContent !== null">
						<div class="item" v-for="file in folderContent">
							<div class="file-icon">
								<i class="big icon mx-0" :class="getFolderFileIcon(file)"></i>
							</div>
							<div class="file-name">
								@{{ file.name }}
							</div>
						</div>
					</div>
				</div>

				@endif

			</div>
		</div>

		<div id="vertical-divider" class="desktop-only"></div>
	
		<div class="column mx-auto r-side">
			@if(!$product->is_dir && ($product->is_free || $product->purchased || auth_is_admin()))
				<form action="{{ route('home.download') }}" target="_blank" class="d-none" id="download" method="post">
					@csrf
					<input type="hidden" name="itemId" v-model="itemId">
				</form>
			@endif


			@if(auth_is_admin())

				@if($product->is_dir)
					<a class="ui basic fluid button" target="_blank" href="{{ route('home.product_folder_sync', $product->slug) }}">
						{{ __('Open folder') }}
					</a>
				@else
					<button class="ui basic fluid button" @click="downloadItem({{ $product->id }}, '#download')">
						{{ __('Download') }}
					</button>
				@endif

				<div class="ui hidden divider"></div>

			@else

				@if($product->purchased)

					<div class="ui fluid card free rounded-corner mt-0">
					  <div class="content py-1">
					    <div class="header">{{ __('Purchased') }}</div>
					    <div class="description">
					      <p class="m-0">{{ __('You have already purchased this item') }}.</p>
					    </div>
					  </div>
					  @if(!$product->is_dir)
					  <div class="ui bottom basic attached button" @click="downloadItem({{ $product->id }}, '#download')">
					    {{ __('Download') }}
					  </div>
					  @else
						<a class="ui bottom basic attached button" target="_blank" href="{{ route('home.product_folder_sync', $product->slug) }}">
					    {{ __('Open folder') }}
					  </a>
						@endif
					</div>

					<div class="ui fluid divider"></div>

				@elseif($product->free_lifetime)

					<div class="ui fluid card free rounded-corner mt-0">
					  <div class="content py-1">
					    <div class="header">{{ __('Free item') }}</div>
					    <div class="description">
					      <p class="m-0">{{ __('This item is free lifetime including updates, you can download it and use it as you wish') }}.</p>
					    </div>
					  </div>
					  @if(!$product->is_dir)
					  <div class="ui bottom basic attached button" @click="downloadItem({{ $product->id }}, '#download')">
					    {{ __('Download For Free') }}
					  </div>
					  @else
							@auth()
							<a class="ui bottom basic attached button" href="{{ route('home.product_folder_sync', $product->slug) }}">
						    {{ __('Open folder') }}
						  </a>
					  	@else
							<a class="ui bottom basic attached button" href="{{ route('login', ['redirect' => url()->current()]) }}">
						    {{ __('You must be logged in to download') }}
						  </a>
					  	@endauth
					  @endif
					</div>

					<div class="ui fluid divider"></div>
				
				@elseif($product->free_temporary)

					<div class="ui fluid card free rounded-corner mt-0">
					  <div class="content py-1">
					    <div class="header">{{ __('Download For Free') }}</div>
					    <div class="description">
					      <p class="m-0">{{ __('This item is free for a limited time, you can download it and use it as you wish, neverthless, updates and support are only available for items you purchase') }}.</p>
					    </div>
					  </div>
					  <div class="extra content py-1-hf">
					  	<span class="right floated">
					  		<i class="clock outline icon"></i>
					  		Ends in {{ \Carbon\Carbon::parse($product->free_time->to)->diffInDays() }} day(s)
					  	</span>
					  </div>
					  @if(!$product->is_dir)
					  <div class="ui bottom basic attached button" @click="downloadItem({{ $product->id }}, '#download')">
					    {{ __('Download For Free') }}
					  </div>
					  @else
					  	@auth()
							<a class="ui bottom basic attached button" href="{{ route('home.product_folder_sync', $product->slug) }}">
						    {{ __('Open folder') }}
						  </a>
					  	@else
							<a class="ui bottom basic attached button" href="{{ route('login') }}">
						    {{ __('You must be logged in') }}
						  </a>
					  	@endauth
					  @endif
					</div>

					<div class="ui fluid divider"></div>

				@else

					<div class="ui fluid card paid rounded-corner mt-0">
						<div class="content">
					    <div class="header">
					    	{{ __('PRICE') }} 
					    	<span class="right floated">{{ currency_price($product->price) }}</span>
					    </div>
					  </div>
					  @if(strip_tags($product->notes))
					  <div class="content">
					  	<div class="ui list">
								@foreach(explode("\n", $product->notes) as $note)
								<div class="item">
									<i class="check circle green icon mr-1-hf"></i>{!! $note !!}
								</div>
								@endforeach
							</div>
					  </div>
					  @endif
					  <div class="content">
					  	<div class="ui spaced buttons">
					  		<button class="ui button" @click="buyNow">{{ __('Buy Now') }}</button>
						  	<button class="ui yellow button" @click="addToCart">{{ __('Add To Cart') }}</button>
					  	</div>

							<div class="ui five icons rounded-corner">
								<i class="cc paypal icon"></i>
								<i class="cc mastercard icon"></i>
								<i class="cc visa icon"></i>
								<i class="cc amex icon"></i>
								<i class="cc discover icon fitted"></i>
							</div>
					  </div>
					</div>

				@endif
			@endif

			<table class="ui unstackable table basic item-info">
				@if($product->version)
				<tr>
					<td><strong>Version</strong></td>
					<td>{{ $product->version }}</td>
				</tr>
				@endif

				@if($product->category)
				<tr>
					<td><strong>Category</strong></td>
					<td>{{ $product->category }}</td>
				</tr>
				@endif

				@if($product->release_date)
				<tr>
					<td><strong>Release date</strong></td>
					<td>{{ $product->release_date }}</td>
				</tr>
				@endif
				
				@if($product->last_update)
				<tr>
					<td><strong>Latest update</strong></td>
					<td>{{ $product->last_update }}</td>
				</tr>
				@endif

				@if($product->included_files)
				<tr>
					<td><strong>Includes files</strong></td>
					<td>{{ $product->included_files }}</td>
				</tr>
				@endif

				@if($product->tags)
				<tr>
					<td><strong>Tags</strong></td>
					<td>{{ $product->tags }}</td>
				</tr>
				@endif

				@if($product->compatible_browsers)
				<tr>
					<td><strong>Compatible browsers</strong></td>
					<td>{{ $product->compatible_browsers }}</td>
				</tr>
				@endif

				@if($product->file_size)
				<tr>
					<td><strong>File size</strong></td>
					<td>{{ ceil($product->file_size / 1024) }}mb</td>
				</tr>
				@endif

				<tr>
					<td><strong>Comments</strong></td>
					<td>{{ $product->comments_count }}</td>
				</tr>

				@if($product->rating)
				<tr>
					<td><strong>Rating</strong></td>
					<td><div class="ui star rating disabled" data-rating="{{ $product->rating }}" data-max-rating="5"></div></td>
				</tr>
				@endif

				@if($product->high_resolution)
				<tr>
					<td><strong>High resolutoin</strong></td>
					<td>{{ $product->high_resolution ? 'Yes' : 'No' }}</td>
				</tr>
				@endif

				<tr>
					<td><strong>Sales</strong></td>
					<td>{{ $product->sales }}</td>
				</tr>
			</table>

			<div class="ui fluid divider"></div>

			<div class="ui fluid card share-on">
				<div class="content p-0">
					<h3 class="ui header py-1 px-0">Share on</h3>
				</div>
				<div class="content px-0 borderless">
					<div class="ui spaced buttons">
						<button class="ui icon button" onclick="window.open('https://www.pinterest.com/pin/create/button/?url={{ url()->current() }}&media={{ asset("storage/covers/$product->cover") }}&description={{ $product->short_description }}', 'Pinterest', 'toolbar=0, status=0, width=\'auto\', height=\'auto\'')">
						  <i class="pinterest icon"></i>
						</button>	
						<button class="ui icon button" onclick="window.open('https://twitter.com/intent/tweet?text={{ $product->short_description }}&url={{ url()->current() }}', 'Twitter', 'toolbar=0, status=0, width=\'auto\', height=\'auto\'')">
						  <i class="twitter icon"></i>
						</button>	
						<button class="ui icon button" onclick="window.open('https://facebook.com/sharer.php?u={{ url()->current() }}', 'Facebook', 'toolbar=0, status=0, width=\'auto\', height=\'auto\'')">
						  <i class="facebook icon"></i>
						</button>
						<button class="ui icon button" onclick="window.open('https://www.tumblr.com/widgets/share/tool?canonicalUrl={{ url()->current() }}', 'tumblr', 'toolbar=0, status=0, width=\'auto\', height=\'auto\'')">
						  <i class="tumblr icon"></i>
						</button>
						<button class="ui icon button" onclick="window.open('https://vk.com/share.php?url={{ url()->current() }}', 'VK', 'toolbar=0, status=0, width=\'auto\', height=\'auto\'')">
						  <i class="vk icon"></i>
						</button>
						<button class="ui icon button" onclick="window.open('https://www.linkedin.com/cws/share?url={{ url()->current() }}', 'Linkedin', 'toolbar=0, status=0, width=\'auto\', height=\'auto\'')">
						  <i class="linkedin icon"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="ui modal" id="screenshots"  v-cloak>
	  <div class="image content p-0" v-if="activeScreenshot">
			<div class="left">
				<button class="ui icon button" type="button" @click="slideScreenhots('prev')">
				  <i class="angle big left icon m-0"></i>
				</button>
			</div>

	    <img class="image" :src="activeScreenshot">

	    <div class="right">
		    <button class="ui icon button" type="button" @click="slideScreenhots('next')">
				  <i class="angle big right icon m-0"></i>
				</button>
	    </div>
	  </div>
	</div>
</div>

@endsection