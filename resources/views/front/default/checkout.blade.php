@extends('front.default.master')

@section('additional_head_tags')
	<meta name="robots" content="noindex,nofollow">

	@if(config('payments.stripe.enabled'))
		<script src="https://js.stripe.com/v3/"></script>
		<script>
			'use strict';
			var stripe = Stripe('{{ config('payments.stripe.client_id') }}');
		</script>
	@endif
@endsection

@section('body')
	
<div v-cloak v-if="trasactionMsg === 'processing'">
	<div class="ui active dimmer">
    <div class="ui small text loader">Processing</div>
  </div>
</div>

<div class="ui two columns shadowless celled grid my-0 px-1" id="checkout-page">
	<div class="one column row" id="page-title">
		<div class="fourteen wide column mx-auto">
			<div class="ui unstackable items">
			  <div class="item">
			    <div class="ui tiny image">
			      <img src="{{ asset_("assets/images/shopping-cart.png") }}">
			    </div>
			    <div class="content">
			      <div class="header">Shopping Cart</div>
			      <div class="description">
			        <p>You have <strong v-cloak>@{{ cartItems }}</strong> item(s) in your shopping cart</p>
			      </div>
			    </div>
			  </div>
			</div>
		</div>
	</div>

	<div class="row" v-if="cart.length">
		
		<div class="column mx-auto left">
			<div class="table-wrapper">
				<table class="ui unstackable very basic large table" v-cloak>
					<thead>
						<th class="desktop-only">Image</th>
						<th>Name</th>
						<th>Quantity</th>
						<th class="right aligned unit-price">Unit price</th>
						<th class="right aligned total">Total</th>
						<th class="center aligned">-</th>
					</thead>
			
					<tbody>
						<tr v-for="prd in cart">
							<td class="p-2 desktop-only"><img :src="prd.thumbnail"></td>
			
							<td class="w-100 capitalize">
								<span @click="location.href=prd.url">@{{ prd.name }}</span>
							</td>
			
							<td class="one column wide">
								<input type="text" :value="prd.quantity" @change="editItemQuantity(prd.id, $event.target.value)" placeholder="Qty" :disabled="couponRes.status">
							</td>
						
							<td class="one column wide right aligned">
								{{ config('payments.currency_code') }} @{{ prd.price }}
							</td>
			
							<td class="three column wide right aligned">
								{{ config('payments.currency_code') }} @{{ prd.price * prd.quantity }}
							</td>
			
							<td class="two column wide center aligned">
								<button class="ui small basic icon button" @click="removeFromCart(prd.id)" :disabled="couponRes.status">
		          		<i class="trash alternate outline icon mx-0"></i>
		            </button>
							</td>
						</tr>
					</tbody>
			
					<tfoot>
						<tr>
							<th class="pl-3">Totals</th>
							<th class="desktop-only">-</th>
							<th>
								@{{ 
									cart.reduce(function(c, v){
										return c + v.quantity
									}, 0) 
								}}
							</th>
							<th>-</th>
							<th class="right aligned">
								{{ config('payments.currency_code') }}
								@{{ getTotalAmount() }}
							</th>
							<th>-</th>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>

		<div id="vertical-divider" class="desktop-only"></div>
		
		<div class="column mx-auto right" id="cart-checkout">
			<div class="ui fluid card p-0">

				<div class="content" v-cloak>
					<span v-cloak class="header">
						TOTAL
						<span class="right floated">
							{{ config('payments.currency_code') }}
							@{{ totalAmount }}
						</span>
					</span>
			  </div>

			  <div class="content center aligned borderless" v-cloak>
			    <div class="header">Payment options</div>
			  </div>

			  <div class="content payment-methods borderless" v-cloak>
				  <div class="ui unstackable items">
						
						@if(config('payments.stripe.enabled'))
						<div class="item" :class="{'active': (paymentProcessor == 'stripe')}" @click="setPaymentProcessor('stripe')">
							<div class="content">
								<i class="circle outline icon mx-0" :class="{'check green': (paymentProcessor == 'stripe')}"></i>
							</div>
							<div class="content">
								<strong>Stripe</strong>
								<div class="description">
									Pay with Visa, MasterCard ... and many other Debit and Credit cards.
								</div>
							</div>
							<div class="content right aligned">
								<i class="credit card outline icon mx-0"></i>
							</div>
						</div>
						@endif

						@if(config('payments.skrill.enabled'))
						<div class="item" :class="{'active': (paymentProcessor == 'skrill')}" @click="setPaymentProcessor('skrill')">
							<div class="content">
								<i class="circle outline icon mx-0" :class="{'check green': (paymentProcessor == 'skrill')}"></i>
							</div>
							<div class="content">
								<strong>Skrill</strong>
								<div class="description">
									Pay with Visa, MasterCard, Visa Electron, Maestro, American Express and many more.
								</div>
							</div>
							<div class="content right aligned">
								<i class="credit card outline icon mx-0"></i>
							</div>
						</div>
						@endif
						
						@if(config('payments.paypal.enabled'))
						<div class="item" :class="{'active': (paymentProcessor == 'paypal')}" @click="setPaymentProcessor('paypal')">
							<div class="content">
								<i class="circle outline icon mx-0" :class="{'check green': (paymentProcessor == 'paypal')}"></i>
							</div>
							<div class="content">
								<strong>Paypal</strong>
								<div class="description">
									Pay easily and securily with PayPal
								</div>
							</div>
							<div class="content right aligned">
								<i class="paypal outline icon mx-0"></i>
							</div>
						</div>
						@endif

						@if(config('payments.razorpay.enabled'))
						<div class="item" :class="{'active': (paymentProcessor == 'razorpay')}" @click="setPaymentProcessor('razorpay')">
							<div class="content">
								<i class="circle outline icon mx-0" :class="{'check green': (paymentProcessor == 'razorpay')}"></i>
							</div>
							<div class="content">
								<strong>Razorpay</strong>
								<div class="description">
									Pay with Visa, MasterCard ... and many other Debit and Credit cards.
								</div>
							</div>
							<div class="content right aligned">
								<i class="credit card outline icon mx-0"></i>
							</div>
						</div>
						@endif

						@if(config('payments.iyzico.enabled'))
						<div class="item" :class="{'active': (paymentProcessor == 'iyzico')}" @click="setPaymentProcessor('iyzico')">
							<div class="content">
								<i class="circle outline icon mx-0" :class="{'check green': (paymentProcessor == 'iyzico')}"></i>
							</div>
							<div class="content">
								<strong>Iyzico</strong>
								<div class="description">
									Pay with Visa, MasterCard ... and many more.
								</div>
							</div>
							<div class="content right aligned">
								<i class="credit card outline icon mx-0"></i>
							</div>
						</div>
						@endif

					</div>	
			  </div>

			  <div class="content borderless" v-cloak>
					<form action="{{ route('home.checkout.payment') }}" method="post" id="form-checkout" class="d-none" v-if="cartItems">

						@csrf

						<input type="hidden" name="cart" :value="JSON.stringify(cart)">
						<input type="hidden" name="processor" :value="paymentProcessor">
						<input type="hidden" name="coupon" :value="couponRes.status ? couponRes.coupon.code : ''">

					</form>

					<form id="coupon-form">
						<div class="ui tiny message mb-1" :class="{negative: !couponRes.status, positive: couponRes.status}" v-cloak v-if="couponRes.msg !== undefined">
							@{{ couponRes.msg }}
						</div>

						<div>
							<input type="text" name="coupon-code" placeholder="Coupon" spellcheck="false" :disabled="couponRes.status" 
											value="{{ request()->coupon }}">
							<span>
								<a v-if="!couponRes.status" @click="applyCoupon($event)">Apply</a>
								<a v-else class="reset" @click="removeCoupon()">Reset</a>
							</span>
						</div>
					</form>

					<button type="button" class="ui yellow fluid big button" @click="checkout($event)">Checkout</button>
			  </div>

			</div>
		</div>
	</div>

	<div class="one column row" id="checkout-messages" v-else>
		<div class="column" v-if="trasactionMsg === 'done'">
			<div class="ui positive message" v-cloak>
			  <div class="header mb-1">
			    {{ __('Order complete, thank you!') }}
			  </div>
			  <p>{!! __('Please go to <a href="'.route('home.downloads').'">Downloads</a> section in order to download the items you\'ve just purchased.') !!}</p>
			</div>
		</div>
		
		<div class="column" v-else>
			<div class="ui message" v-cloak>
			  <div class="header">
			    {{ __('Your cart is empty!') }}
			  </div>
			</div>
		</div>
	</div>
</div>

@endsection