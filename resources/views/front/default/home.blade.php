@extends('front.default.master')

@section('additional_head_tags')
<style>
	@if(config('app.search_cover_color'))
	#top-search:before {
		background: {{ config('app.search_cover_color')  }}	
	}
	@endif
	
	@if(config('app.search_cover'))
	#top-search {
		background-image: url('{{ asset_('storage/images/'.config('app.search_cover')) }}')
	}
		@endif
</style>

<script type="application/ld+json">
{
	"@context": "http://schema.org",
	"@type": "WebSite",
	  "name": "{{ $meta_data->title }}",
	  "url": "{{ $meta_data->url }}",
	  "image": "{{ $meta_data->image }}",
	  "keywords": "{{ config('app.keywords') }}"
}
</script>

<script type="application/javascript">
	'use strict';
	window.props['products'] = {!! json_encode($trending_products->merge($featured_products, $free_products, $newest_products)
																	->reduce(function ($carry, $item) {
																	  $carry[$item->id] = $item->only(['id', 'name', 'slug', 'cover']);
																	  return $carry;
																	}, [])) !!};
</script>
@endsection

@section('top-search')
	<div  class="ui bottom attached basic segment" id="top-search">
		<div class="ui middle aligned grid m-0">
			<div class="row">
				<div class="column center aligned">
					
					@if(config('app.search_header'))
					<h1>{{ config('app.search_header') }}</h1>
					<br>
					@endif

					@if(config('app.search_subheader'))
					<h3 class="marquee">{{ config('app.search_subheader') }}</h3>
					@endif
					
					<form class="ui big form fluid search-form" method="get" action="{{ route('home.products.q') }}">
						<div class="ui icon input fluid">
						  <input type="text" name="q" placeholder="Search...">
						  <i class="search link icon"></i>
						</div>
			    </form>
					
					@if(config('home_categories'))
					<div class="categories mt-2 @if(count(config('home_categories')) > 20) large @endif">
				    <div class="ui labels">
				    	@foreach(config('home_categories') as $home_category)
							<a class="ui basic label" href="{{ $home_category->url }}">{{ $home_category->name }}</a>
				    	@endforeach
				    </div>
			    </div>
			    @endif
				</div>
			</div>
		</div>
	</div>
@endsection


@section('body')
	
	<div class="row home-items">
		
		<!--  FEATURED PRODUCTS -->
		@if($featured_products->count())
		<div class="wrapper featured">
			<div class="sixteen wide column mx-auto selection-title">
				<div class="ui menu ml-1 pl-0">
					<a href="{{ route('home.products.selection', 'featured') }}" class="item my-1">
						<strong>F</strong>eatured
					</a>
				</div>
			</div>
			
			<div class="sixteen wide column mx-auto">
				<div class="ui four doubling cards px-1">
					@foreach($featured_products as $featured_product)
					<div class="card">
						
						<div  class="content cover p-0">
							<img src="{{ asset_("storage/covers/{$featured_product->cover}") }}" alt="cover">
							<span class="tag">Featured</span>
							<div class="ui spaced tiny buttons">
								<a href="{{ route('home.product', $featured_product->slug) }}" title="{{ $featured_product->name }}" class="ui button">
									Details
								</a>
								<button class="ui button" @click="addToCartAsync({{ $featured_product->id }})">
									Add to cart	
								</button>
							</div>
						</div>
						
						<div class="content title">
							<div>
							<a href="{{ route('home.product', $featured_product->slug) }}" title="{{ $featured_product->name }}">
						    {{ $featured_product->name }}
							</a>

							<span class="category">
					    {{ rand_subcategory($featured_product->subcategories, $featured_product->category_name) }}
			      	</span>

							<span class="ui star disabled rating mt-1 d-block" data-rating="{{ $featured_product->rating }}" data-max-rating="5"></span>
							</div>
						</div>

						<div class="content bottom p-1-hf">
							<div class="left floated">
								<span title="Add to collection">
									<i class="bordered shadowless heart icon link" 
									 	 @click="collectionToggleItem($event, {{ $featured_product->id }})" 
										 :class="{active: itemInCollection({{ $featured_product->id }})}">
									</i>
								</span>
								@if($featured_product->preview)
								<a title="Preview" target="_blank" href="{{ route('home.preview', $featured_product->slug) }}"><i class="bordered shadowless tv icon link"></i></a>
								@endif
								<span title="{{ $featured_product->views }} views"><i class="bordered shadowless eye icon"></i></span>
								<span title="Last update {{ (new DateTime($featured_product->updated_at))->format('d M Y') }}"><i class="bordered shadowless calendar alternate outline icon mr-0"></i></span>
							</div>

							<div class="price right floated">
								{{ currency_price($featured_product->price) }}
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</div>
		@endif

		
		<!--  TRENDING PRODUCTS -->
		@if($trending_products->count())
		<div class="wrapper">
			<div class="sixteen wide column mx-auto selection-title">
				<div class="ui menu ml-1 pl-0">
					<a href="{{ route('home.products.selection', 'trending') }}" class="item my-1">
						<strong>T</strong>rending
					</a>
				</div>
			</div>

			<div class="sixteen wide column mx-auto">
				<div class="ui four doubling cards px-1">
					@foreach($trending_products as $trending_product)
					<div class="card">
						<div class="content cover p-0">
							<img src="{{ asset_("storage/covers/{$trending_product->cover}") }}" alt="cover">
							<span class="tag">Featured</span>
							<div class="ui spaced tiny buttons">
								<a href="{{ route('home.product', $trending_product->slug) }}" title="{{ $trending_product->name }}" class="ui button">
									Details
								</a>
								<button class="ui button" @click="addToCartAsync({{ $trending_product->id }})">
									Add to cart	
								</button>
							</div>
						</div>

						<div class="content title">
							<div>
							<a href="{{ route('home.product', $trending_product->slug) }}" title="{{ $trending_product->name }}">
						    {{ $trending_product->name }}
							</a>

							<span class="category">
                        {{ rand_subcategory($trending_product->subcategories, $trending_product->category_name) }}
			      	</span>

							<span class="ui star disabled rating mt-1 d-block" data-rating="{{ $trending_product->rating }}" data-max-rating="5"></span>
							</div>
						</div>
						
						<div class="content bottom p-1-hf">
							<div class="left floated">
								<span title="Add to collection">
									<i class="bordered shadowless heart icon link" 
									 	 @click="collectionToggleItem($event, {{ $trending_product->id }})" 
										 :class="{active: itemInCollection({{ $trending_product->id }})}">
									</i>
								</span>
								@if($trending_product->preview)
								<a title="Preview" target="_blank" href="{{ route('home.preview', $trending_product->slug) }}"><i class="bordered shadowless tv icon link"></i></a>
								@endif
								<span title="{{ $trending_product->views }} views"><i class="bordered shadowless eye icon"></i></span>
								<span title="Last update {{ (new DateTime($trending_product->updated_at))->format('d M Y') }}"><i class="bordered shadowless calendar alternate outline icon mr-0"></i></span>
							</div>

							<div class="price right floated">
								{{ currency_price($trending_product->price) }}
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</div>
		@endif
		

		<!--  NEWEST PRODUCTS -->
		@if($newest_products->count())
		<div class="wrapper">
			<div class="sixteen wide column mx-auto selection-title">
				<div class="ui menu ml-1 pl-0">
					<a href="{{ route('home.products.selection', 'newest') }}" class="item my-1">
						<strong>N</strong>ewest
					</a>
				</div>
			</div>

			<div class="sixteen wide column mx-auto">
				<div class="ui four doubling cards px-1">
					@foreach($newest_products as $newest_product)
					<div class="card">
						<div class="content cover p-0">
							<img src="{{ asset_("storage/covers/{$newest_product->cover}") }}" alt="cover">
							<span class="tag">Featured</span>
							<div class="ui spaced tiny buttons">
								<a href="{{ route('home.product', $newest_product->slug) }}" title="{{ $newest_product->name }}" class="ui button">
									Details
								</a>
								<button class="ui button" @click="addToCartAsync({{ $newest_product->id }})">
									Add to cart	
								</button>
							</div>
						</div>

						<div class="content title">
							<div>
							<a href="{{ route('home.product', $newest_product->slug) }}" title="{{ $newest_product->name }}">
						    {{ $newest_product->name }}
							</a>

							<span class="category">
			      		{{ rand_subcategory($newest_product->subcategories, $newest_product->category_name) }}
			      	</span>

							<span class="ui star disabled rating mt-1 d-block" data-rating="{{ $newest_product->rating }}" data-max-rating="5"></span>
							</div>
						</div>
						
						<div class="content bottom p-1-hf">
							<div class="left floated">
								<span title="Add to collection">
									<i class="bordered shadowless heart icon link" 
									 	 @click="collectionToggleItem($event, {{ $newest_product->id }})" 
										 :class="{active: itemInCollection({{ $newest_product->id }})}">
									</i>
								</span>
								@if($newest_product->preview)
								<a title="Preview" target="_blank" href="{{ route('home.preview', $newest_product->slug) }}"><i class="bordered shadowless tv icon link"></i></a>
								@endif
								<span title="{{ $newest_product->views }} views"><i class="bordered shadowless eye icon"></i></span>
								<span title="Last update {{ (new DateTime($newest_product->updated_at))->format('d M Y') }}"><i class="bordered shadowless calendar alternate outline icon mr-0"></i></span>
							</div>

							<div class="price right floated">
								{{ currency_price($newest_product->price) }}
							</div>
						</div>
					</div>
					@endforeach

				</div>
			</div>
		</div>
		@endif
		
		
		<!--  FREE PRODUCTS -->
		@if($free_products->count())
		<div class="wrapper">
			<div class="sixteen wide column mx-auto selection-title">
				<div class="ui menu ml-1 pl-0">
					<a href="{{ route('home.products.selection', 'free') }}" class="item my-1">
						<strong>F</strong>ree
					</a>
				</div>
			</div>

			<div class="sixteen wide column mx-auto">
				<div class="ui four doubling cards px-1">

					@foreach($free_products as $free_product)
					<div class="card">
						<a href="{{ route('home.product', $free_product->slug) }}" class="content cover p-0" title="{{ $free_product->name }}">
							<img src="{{ asset_("storage/covers/{$free_product->cover}") }}" alt="cover">
							<span class="tag">Featured</span>
						</a>

						<div class="content title">
							<div>
								<a href="{{ route('home.product', $free_product->slug) }}" title="{{ $free_product->name }}">
							    {{ $free_product->name }}
								</a>

								<span class="category">
							{{ rand_subcategory($free_product->subcategories, $free_product->category_name) }}
				      	</span>

								<span class="ui star disabled rating mt-1 d-block" data-rating="{{ $free_product->rating }}" data-max-rating="5"></span>
							</div>
						</div>
						
						<div class="content bottom p-1-hf">
							<div class="left floated">
								<span title="Add to collection">
									<i class="bordered shadowless heart icon link" 
									 	 @click="collectionToggleItem($event, {{ $free_product->id }})" 
										 :class="{active: itemInCollection({{ $free_product->id }})}">
									</i>
								</span>
								@if($free_product->preview)
								<a title="Preview" target="_blank" href="{{ route('home.preview', $free_product->slug) }}"><i class="bordered shadowless tv icon link"></i></a>
								@endif
								<span title="{{ $free_product->views }} views"><i class="bordered shadowless eye icon"></i></span>
								<span title="Last update {{ (new DateTime($free_product->updated_at))->format('d M Y') }}"><i class="bordered shadowless calendar alternate outline icon mr-0"></i></span>
							</div>

							<div class="price free right floated @if($free_product->price) paid @endif">
								{{ currency_price($free_product->price) }}
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</div>
		@endif


		<!-- POSTS -->
		@if($posts->count())
		<div class="wrapper posts">
			<div class="sixteen wide column mx-auto selection-title">
				<div class="ui menu ml-1 pl-0">
					<a href="{{ route('home.blog') }}" class="item my-1" href="{{ route('home.blog') }}">
						<strong>P</strong>osts From Our Blog
					</a>
				</div>
			</div>

			<div class="sixteen wide column mx-auto">
				<div class="ui four doubling cards px-1">			
					@foreach($posts as $post)
					<div class="card">
						<div class="content p-0">
							<a href="{{ route('home.post', $post->slug) }}">
								<img src="{{ asset_("storage/posts/{$post->cover}") }}" alt="cover">
							</a>
							<time>{{ $post->updated_at->diffForHumans() }}</time>
						</div>
						<div class="content title">
							<a href="{{ route('home.post', $post->slug) }}">{{ $post->name }}</a>
						</div>
						<div class="content description">
							{{ mb_substr($post->short_description, 0, 120).'...' }}
						</div>
						<div class="content tags">
							@foreach(array_slice(explode(',', $post->tags), 0, 3) as $tag)
							<a class="tag" href="{{ route('home.blog.tag', slug($tag)) }}">{{ trim($tag) }}</a><br>
							@endforeach
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</div>
		@endif

	</div>

@endsection