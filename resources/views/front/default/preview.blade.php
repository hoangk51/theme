<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="language" content="en-us">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" href="{{ asset_("storage/images/".config('app.favicon', 'favicon.png'))}}">
        
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <!-- App CSS -->
    <link rel="stylesheet" href="{{ asset_('assets/default/css/app.css') }}">
  </head>

  <body id="preview">

    <div id="app" class="ui container fluid">
      <div class="ui celled grid m-0 shadowless">

        <div class="row">
          <div id="top-menu" class="ui unstackable secondary menu top attached py-0 preview">
            <a href="/" class="item header logo">
              <img src="{{ asset_("storage/images/".config('app.logo', 'logo.png')) }}" alt="Valexa" class="ui image">
            </a>
          
            <div class="right menu pr-1">
              <a href="{{ route('home.product', $product->slug) }}" class="item">BUY NOW</a>
              <a href="{{ $product->preview }}" class="item">REMOVE FRAME</a>
            </div>
          </div>

          <div class="ui two columns shadowless celled grid m-0">	
            <div class="ui active inverted dimmer">
              <div class="ui text loader">Loading</div>
            </div>

            <iframe src="{{ $product->preview }}" frameborder="0"></iframe>
          </div>
        </div>
        
      </div>
    </div>

    <script>
      'use strict';
      
      document.querySelector('iframe').addEventListener('load', function(e)
      {
        document.querySelector('#app .ui.dimmer').remove();
        e.target.style.visibility = 'visible';
      }, null)
    </script>
  </body>
</html>