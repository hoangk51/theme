<!DOCTYPE html>
<html lang="en">
	<head>
		{!! config('app.google_analytics') !!}

		<meta charset="UTF-8">
		<meta name="language" content="en-us">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="icon" href="{{ asset_("storage/images/".config('app.favicon'))}}">
		
		@include('front.default.partials.meta_data')

		<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
		<!-- App CSS -->
		<link rel="stylesheet" href="{{ asset_('assets/default/css/app.css') }}">
		
		<!-- Search engines verification -->
		<meta name="google-site-verification" content="{{ config('app.google') }}">
		<meta name="msvalidate.01" content="{{ config('app.bing') }}">
		<meta name="yandex-verification" content="{{ config('app.yandex') }}">
        
		<script>
			'use strict';
			
			window.props = {
	      product: {},
	      products: {},
	      checkoutRoute: '{{ route('home.checkout') }}',
	      productsRoute: '{{ route('home.products.category', '') }}',
	      currentRouteName: '{{ Route::currentRouteName() }}',
	      trasactionMsg: '{{ session('transaction_response') }}',
	      location: window.location,
	      paymentProcessors: {
	      	paypal: {{ var_export(config('payments.paypal.enabled') ? true : false) }},
	      	stripe: {{ var_export(config('payments.stripe.enabled') ? true : false) }},
					skrill: {{ var_export(config('payments.skrill.enabled') ? true : false) }},
					razorpay: {{ var_export(config('payments.razorpay.enabled') ? true : false) }},
					iyzico: {{ var_export(config('payments.iyzico.enabled') ? true : false) }},
	      },
	      activeScreenshot: null,
	      subcategories: {!! json_encode(config('categories.category_children', [])) !!},
	      categories: {!! json_encode(config('categories.category_parents', [])) !!},
	      workingWithFolders: @if(isFolderProcess()) true @else false @endif,
	    }
		</script>

		@yield('additional_head_tags')
	</head>
	<body>
		
		<div class="ui main fluid container items-menu-visible" id="app">
			<div class="ui celled grid m-0 shadowless">

				<div class="row">
					@include('front.default.partials.top_menu')
				</div>

				<div class="row">
					@yield('top-search')
				</div>
				
				<div class="row my-1" id="body">
					@yield('body')
				</div>
				
				<div id="blur" @click="toggleMobileMenu" v-if="!menu.mobile.hidden" v-cloak></div>

				<footer id="footer" class="ui doubling stackable four columns grid mt-0 mx-auto px-0">
					@include('front.default.partials.footer')
				</footer>

			</div>
		</div>
		
		<!-- App JS -->
	  <script type="application/javascript" src="{{ asset_('assets/default/js/app.js') }}"></script>

	</body>
</html>