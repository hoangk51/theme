@extends('front.default.master')

@section('additional_head_tags')
<script type="application/ld+json">
{
	"@context": "http://schema.org",
	"@type": "WebSite",
	"image": "{{ $meta_data->image }}",
	"name": "{{ $meta_data->title }}",
  "url": "{{ $meta_data->url }}"
	@if($category->description ?? null)
  ,"description": "{{ $meta_data->description }}"
  @elseif(request()->q)
  ,"potentialAction": {
		"@type": "SearchAction",  
		"target": "{!! route('home.products.q').'?q={query}' !!}",
		"query-input": "required name=query"
	}
  @endif
}
</script>

<script type="application/javascript">
	window.props['products'] = {!! json_encode($products->reduce(function ($carry, $item) {
																	  $carry[$item->id] = $item->only(['id', 'name', 'slug', 'cover']);
																	  return $carry;
																	}, [])) !!};
</script>
@endsection

@section('body')

<div class="ui shadowless celled grid my-0" id="items">
	<div class="one column row">
		<div class="column">
			<h2 class="ui header px-2 py-1">
				@if($category->name ?? null)
			  <i class="tag icon left floated mr-1"></i> {{ $category->name }}
			  @if($category->description)
			  <div class="sub header mt-1">
			  	{{ $category->description }}
			  </div>
			  @endif
			  @elseif(request()->selection)
				{{ ucfirst(request()->selection) }}
				@elseif(request()->q)
				Searching for "{{ request()->q }}"
			  @endif
			</h2>
		</div>
	</div>

	<div class="row">
		<div class="column left">
			
			<div class="categories">
				<div class="title">Categories</div>

				@if(config('categories.category_parents'))
				<div class="ui vertical fluid menu shadowless borderless">
					@foreach(config('categories.category_parents', []) as $category)
					<div class="category @if(request()->category_slug === $category->slug) active @endif">
						<a href="{{ route('home.products.category', $category->slug) }}" class="parent header item">
							<span>{{ $category->name }}</span>
						</a> 
						@if($subcategories = config("categories.category_children.{$category->id}", []))
						<div class="children hide-scroll v-scroll">
							<div class="wrapper">
							@foreach($subcategories as $subcategory)
								<a href="{{ route('home.products.category', $category->slug.'/'.$subcategory->slug) }}" 
									 class="item @if(request()->subcategory_slug === $subcategory->slug) active @endif">
									<span>{{ $subcategory->name }}</span>
								</a>
							@endforeach
							</div>
						</div>
						@endif
					</div>
					@endforeach
				</div>
				@endif
			</div>
			
			<div class="random-items mt-2">
				<div class="title">Random Pick</div>
				
				<div class="items hide-scroll v-scroll">
					<div class="wrapper">
						@foreach($random_products as $random_product)
						<a class="item" href="{{ route('home.product', $random_product->slug) }}">
							<div><img src="{{ asset_("storage/thumbnails/{$random_product->thumbnail}") }}"></div>
							<div><h3>{{ $random_product->name }}</h3></div>
							<div><span>{{ currency_price($random_product->price) }}</span></div>
						</a>
						@endforeach
					</div>
				</div>
			</div>
		</div>
			
		<div id="vertical-divider" class="desktop-only"></div>

		<div class="column right">
			@if($products->count() ?? null)
			<div class="ui filter shadowless borderless menu">
				@if(!request()->selection)
				<div class="ui dropdown item">
					<input type="hidden" value="{{ str_ireplace('_', ' ', request()->query('ob')) }}">
					<div class="default text">Sort by</div>
					<i class="sort amount @if(request()->query('o') == 'asc') up @else down @endif icon ml-1-hf"></i>
					<div class="menu">
						<a href="{{ url_append($sort->release_date) }}" class="item">Release date</a>
						<a href="{{ url_append($sort->price) }}" class="item">Price</a>
						<a href="{{ url_append($sort->popularity) }}" class="item">Popularity</a>
						<a href="{{ url_append($sort->rating) }}" class="item">Rating</a>
					</div>
				</div>
				@endif

				<form class="ui right aligned search item search-form" method="get" action="{{ route('home.products.q') }}">
		      <div class="ui transparent icon input">
		        <input class="prompt" type="text" name="q" placeholder="Search ...">
		        <i class="search link icon"></i>
		      </div>
		    </form>

		    <a class="item icon left-column-toggler"><i class="bars icon mx-0"></i></a>
			</div>
			
			<div class="ui fluid divider"></div>

			<div class="ui four doubling cards">
				@foreach($products as $product)
				
				<div class="card">
				  <div class="content p-0 image">
				  	<a href="{{ route('home.product', $product->slug) }}"><img src="{{ asset_("storage/covers/{$product->cover}") }}"></a>
				  	<span class="tag @if($product->price === 0) free @endif">
				  		<i class="circle icon"></i>
				  		@php $rand_subcategory = rand_subcategory($product->subcategories); @endphp
				  		<a href="{{ route('home.products.category', [$product->category_slug, slug($rand_subcategory ?? '')]) }}">
					  		{{ $rand_subcategory }}
					  	</a>
				  	</span>
				  </div>

				  <a href="{{ route('home.product', $product->slug) }}" class="content title py-1-hf">
				  	{{ $product->name }}
				  </a>

				  <div class="content bottom p-1-hf">
				    <div class="left floated">
					    <span title="Add to collection"><i @click="collectionToggleItem($event, {{ $product->id }})" class="bordered heart icon link" :class="{active: itemInCollection({{ $product->id }})}"></i></span>
					    @if($product->preview)
					    <a href="{{ $product->preview }}" title="Preview"><i class="bordered tv icon link"></i></a>
					    @endif
					    <span title="{{ $product->views }} views"><i class="bordered eye icon"></i></span>
							<span title="Last update {{ (new DateTime($product->updated_at))->format('d M Y') }}">
								<i class="bordered calendar alternate outline icon"></i>
							</span>
				    </div>
				    
				    @if(!$product->price)
					    <div class="price right free floated">
					   		Free
					   	</div>
				   	@else
				    <div class="price right floated">
				    	{{ currency_price($product->price) }}
				    </div>
				    @endif
				  </div>
				</div>

				@endforeach
			</div>
		
			@if($products instanceof \Illuminate\Pagination\LengthAwarePaginator)
			<div class="ui fluid divider"></div>
			{{ $products->appends(request()->query())->onEachSide(1)->links() }}
			{{ $products->appends(request()->query())->links('vendor.pagination.simple-semantic-ui') }}
			@endif

			@else
			<div class="ui message">
				No items found
			</div>
			@endif
		</div>
	</div>
</div>

@endsection