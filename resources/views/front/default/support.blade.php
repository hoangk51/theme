@extends('front.default.master')

@section('additional_head_tags')
<script type="application/ld+json">
{
	"@context": "http://schema.org",
	"@type": "WebPage",
	"image": "{{ $meta_data->image }}",
	"name": "{{ $meta_data->title }}",
  "url": "{{ $meta_data->url }}",
  "description": "Frequently asked questions and support."
}
</script>
@endsection

@section('body')

	<div class="ui two columns shadowless celled grid my-0" id="support">
		<div class="column left faq">
			<div class="title-wrapper">
				<div class="ui shadowless fluid segment">
					<h3>Frequently asked questions</h3>
				</div>
			</div>

			<div class="ui shadowless borderless segments">
				@foreach($faqs as $faq)
			  <div class="ui borderless segment">
			    <p><i class="minus icon"></i>{{ $faq->question }}</p>
			    <div>
			    	{!! $faq->answer !!}
			    </div>
			  </div>
			  @endforeach
			</div>
		</div>
	
		<div id="vertical-divider" class="p-0 desktop-only"></div>

		<div class="column right support">
			<div class="title-wrapper">
				<div class="ui shadowless fluid segment">
					<h3>Still have a question ?</h3>
				</div>
			</div>
			
			@if($errors->any())
		    @foreach ($errors->all() as $error)
				<div class="ui negative fluid small message">
					<i class="times icon close"></i>
					{{ $error }}
				</div>
		    @endforeach
			@endif

			@if(session('support_response'))
			<div class="ui fluid small bold positive message">
				{{ session('support_response') }}
			</div>
			@endif

			<form action="{{ route('home.support') }}" method="post" class="ui form">
				@csrf

				<div class="field">
					<label>Email</label>
					<input type="email" value="{{ old('email', request()->user()->email ?? '') }}" name="email" placeholder="Your email..." required>
				</div>

				<div class="field">
					<label>Subjet</label>
					<input type="text" name="subject" value="{{ old('subject') }}" placeholder="Subject..." required>
				</div>

				<div class="field">
					<label>Question</label>
					<textarea name="message" cols="30" rows="10" placeholder="Your question..." required>{{ old('message') }}</textarea>
				</div>

				<div class="field">
					<button class="ui fluid yellow button" type="submit">Submit</button>
				</div>
			</form>
		</div>
	</div>

@endsection