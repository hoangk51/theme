<div class="ui unstackable secondary menu top attached py-0" id="top-menu">

  <a class="item header logo" href="/">
    <img class="ui image" src="{{ asset_("storage/images/".config('app.logo')) }}" alt="{{ config('app.name') }}">
  </a>

  <form class="item search search-form" method="get" action="{{ route('home.products.q') }}">
    <div class="ui icon input">
      <input type="text" name="q" placeholder="Search...">
      <i class="search link icon"></i>
    </div>
  </form>

  <div class="right menu pr-1"> 
    <a class="item search-icon" @click="toggleMobileSearchBar">
      <i class="search icon"></i>
    </a>

    <a class="item categories" @click="toggleItemsMenu" title="Categories">
      <i class="tags icon"></i>
      <span class="text">{{ __('Categories') }}</span>
    </a>

    <a href="{{ route('home.blog') }}" class="item blog">
      <i class="bold icon"></i>
      <span class="text">{{ __('Blog') }}</span>
    </a>
    
    <a href="{{ route('home.favorites') }}" class="item collection" title="Collection">
      <i class="heart outline icon"></i>
      <span class="text">{{ __('Collection') }}</span>
    </a>

    <a href="{{ route('home.support') }}" class="item help">
      <i class="question circle outline icon"></i>
      <span class="text">{{ __('Support') }}</span>
    </a>
    
    <div class="item notifications dropdown">
      <div class="toggler">
        <div><i class="bell outline icon"></i><span v-cloak>({{ count(config('notifications', [])) }})</span></div>
        <div class="text mt-1-hf">Notifications</div>
      </div>

      <div class="menu">
        <div>
         
          <div class="ui unstackable items">
            @if(config('notifications'))

            @foreach(config('notifications') as $notif)
            <a class="item mx-0"
               data-id="{{ $notif->id }}"
               data-href="{{ route('home.product', $notif->slug . ($notif->for == 1 ? '#support' : ($notif->for == 2 ? '#reviews' : ''))) }}">

              <div class="ui image">
                @if($notif->for == 0)
                <img src="{{ asset_("storage/thumbnails/{$notif->image}") }}">
                @else
                <img src="{{ asset_("storage/avatars/{$notif->image}") }}">
                @endif
              </div>

              <div class="content pl-1">
                <p>{!! $notif->text !!}</p>
                <time>{{ \Carbon\Carbon::parse($notif->updated_at)->diffForHumans() }}</time>
              </div>

            </a>
            @endforeach

            @else
            
            <div class="item mx-0">
              <div class="ui w-100 message">
                You have 0 new notifications
              </div>
            </div>
            
            @endif

            @auth
            <a href="{{ route('home.notifications') }}" class="item mx-0 all">View all</a>
            @endauth
          </div>
          
        </div>
      </div>
    </div>

    @guest
    <a href="{{ route('login', ['redirect' => url()->current()]) }}" class="item">
      <i class="user outline icon"></i>
      <span class="text">{{ __('Account') }}</span>
    </a>
    @endguest

    <div class="item cart dropdown">
      <div class="toggler">
        <div><i class="shopping cart icon"></i><span v-cloak>(@{{ cartItems }})</span></div>
        <div class="text mt-1-hf">Shopping cart</div>
      </div>

      <div class="menu" v-if="Object.keys(cart).length">
        <div>
          <div class="ui unstackable items">

            <div class="item mx-0" v-for="product in cart">
              <div class="ui tiny image"><img :src="product.thumbnail"></div>
              <div class="content pl-1">
                <strong :title="product.name"><a :href="product.url">@{{ product.name }}</a></strong> 
                <div class="subcontent mt-1">
                  <div class="price">
                    {{ config('payments.currency_code') }} @{{ product.price }}
                  </div>
                  <div class="quantity">
                    <input type="text" placeholder="Qty" :value="product.quantity" @change="editItemQuantity(product.id, $event.target.value)" :disabled="couponRes.status">
                  </div>
                  <div class="remove" :disabled="couponRes.status">
                    <i class="trash alternate outline icon mx-0" @click="removeFromCart(product.id)"></i>
                  </div>
                </div>
              </div>
            </div>
            
            <a href="{{ route('home.checkout') }}" class="item mx-0 checkout">Checkout</a>

          </div>
        </div>
      </div>

      <div class="menu" v-else>
        <div class="ui unstackable items">
          <div class="item p-1-hf">
            <div class="ui message w-100 left aligned">
              Your cart is empty
            </div>
          </div>
        </div>
      </div>
    </div>
    
    @auth
    <div class="item ui dropdown user">
        <img src="{{ asset_("storage/avatars/". if_null(auth()->user()->avatar, 'default.jpg')) }}" class="ui avatar image mx-0">
    
        <div class="left menu">
          @if(auth_is_admin())
          <a class="item" href="{{ route('admin') }}">
              <i class="circle blue icon"></i>
              {{ __('cPanel') }}
            </a>

            <a class="item" href="{{ route('profile.edit') }}">
                <i class="user outline icon"></i>
                Profile
            </a>
            <a class="item" href="{{ route('transactions') }}">
                <i class="shopping cart icon"></i>
                {{ __('Transactions') }}
            </a>
            <div class="item">
              <i class="cog icon"></i>
              {{ __('Settings') }}
              <div class="menu settings left d-block w-100">
                  <a href="{{ route('settings', 'general') }}" class="item d-block w-100">General</a>
                  <a href="{{ route('settings', 'search_engines') }}" class="item d-block w-100">Search engines</a>
                  <a href="{{ route('settings', 'payments') }}" class="item d-block w-100">Payments</a>
                  <a href="{{ route('settings', 'advert') }}" class="item d-block w-100">Advertisement</a>
                  <a href="{{ route('settings', 'mailer') }}" class="item d-block w-100">Mailer</a>
                  <a href="{{ route('settings', 'social_login') }}" class="item d-block w-100">Social login</a>
              </div>
            </div>
            <div class="item">
              <i class="file code outline icon"></i>
              {{ __('Products') }}
              <div class="menu left d-block w-100">
                  <a href="{{ route('products') }}" class="item d-block w-100">List</a>
                  <a href="{{ route('products.create') }}" class="item d-block w-100">Create</a>
              </div>
            </div>
            <div class="item" href="">
              <i class="sticky note outline icon"></i>
              {{ __('Pages') }}
              <div class="menu left d-block w-100">
                  <a href="{{ route('pages') }}" class="item d-block w-100">List</a>
                  <a href="{{ route('pages.create') }}" class="item d-block w-100">Create</a>
              </div>
            </div>
            <div class="item" href="">
              <i class="file alternate outline icon"></i>
              {{ __('Posts') }}
              <div class="menu left d-block w-100">
                  <a href="{{ route('posts') }}" class="item d-block w-100">List</a>
                  <a href="{{ route('posts.create') }}" class="item d-block w-100">Create</a>
              </div>
            </div>
            <div class="item" href="">
              <i class="tags icon"></i>
              {{ __('Categories') }}
              <div class="menu left d-block w-100">
                  <a href="{{ route('categories') }}" class="item d-block w-100">List</a>
                  <a href="{{ route('categories.create') }}" class="item d-block w-100">Create</a>
              </div>
            </div>
            <div class="item" href="">
              <i class="question circle icon"></i>
              {{ __('Faq') }}
              <div class="menu left d-block w-100">
                  <a href="{{ route('faq') }}" class="item d-block w-100">List</a>
                  <a href="{{ route('faq.create') }}" class="item d-block w-100">Create</a>
              </div>
            </div>
            <a class="item" href="{{ route('support') }}">
                <i class="comments outline icon"></i>
                {{ __('Support') }}
            </a>
          @else
            @auth
            <a class="item" href="{{ route('home.profile') }}">
                <i class="user outline icon"></i>
                {{ __('Profile') }}
            </a>
            @endauth

            @if(!auth_is_admin())
            <a class="item" href="{{ route('home.favorites') }}">
                <i class="heart outline icon"></i>
                {{ __('Collection') }}
            </a>
            @endif
            
            @auth
            <a class="item" href="{{ route('home.downloads') }}">
                <i class="cloud download icon"></i>
                {{ __('Downloads') }}
            </a>
            @endauth
          @endif
          @auth
          <div class="ui divider my-0"></div>
          <a class="item logout w-100 mx-0" @click="logout">
              <i class="sign out alternate icon"></i>
              {{ __('Sign out') }}
          </a>
          @endauth
        </div>
    </div>
    @endauth

    <a class="header item px-1 mobile-only mr-0" @click="toggleMobileMenu">
      <i class="bars large icon mx-0"></i>
    </a>
  </div>
</div>


<form class="ui form my-0 search-form" 
      id="mobile-search-bar"
      method="get" 
      action="{{ route('home.products.q') }}">
  <div class="ui icon input borderless w-100">
    <input type="text" name="q" placeholder="Search...">
    <i class="search link icon"></i>
  </div>
</form>



<div class="popup items-menu hide-scroll v-scroll" 
     :style="{top: menu.desktop.itemsMenuPopup.top+'px', left: menu.desktop.itemsMenuPopup.left+'px'}" 
     v-cloak>
  
  <div class="wrapper">
    <div v-if="menu.desktop.selectedCategory">
      <a v-for="subcategory in menu.desktop.submenuItems" 
         :href="setProductsRoute(menu.desktop.selectedCategory.slug+'/' + subcategory.slug)">
        <span>@{{ subcategory.name }}</span>
      </a>

      <a class="active" :href="setProductsRoute(menu.desktop.selectedCategory.slug)">
        <span>@{{ menu.desktop.selectedCategory.name }}</span>
      </a>
    </div>
  </div>

</div>




<div id="items-menu">
  <div class="categories hide-scroll vh-scoll">
    <div class="wrapper dragscroll">

    @foreach(config('categories.category_parents', []) as $category)

      <div class="item" @click="setSubMenu($event, {{ $category->id }})">
       <span>{{ $category->name }}</span>
      </div>

    @endforeach

    </div>
  </div>
</div>




<div id="mobile-menu" class="ui vertical menu">

  <div v-bar>
    <div class="wrapper main pt-1-qt" :class="{'d-none': menu.mobile.submenuItems !== null}">

      <div class="parent categories">
        <span class="item">
          <a>
            <i class="tags icon left floated ml-0 mr-1"></i>
            {{ __('Categories') }}
          </a>
        </span>
        <div v-bar>
          <div class="child main">
            @foreach(config('categories.category_parents', []) as $category)
            <span class="item">
              <a @click="setSubMenu($event, {{ $category->id }}, true)">
                {{ $category->name }}
              </a>
            </span>
            @endforeach
          </div>
        </div>
      </div>

      <span class="item">
        <a href=""><i class="bold icon left floated ml-0 mr-1"></i>{{ __('Blog') }}</a>
      </span>
      
      @if($pages = config('pages', []))
      <div class="parent pages">
        <span class="item">
          <a>
            <i class="file outline icon left floated ml-0 mr-1"></i>
            {{ __('Pages') }}
          </a>
        </span>
        <div class="child">
          @foreach($pages as $page)
          @if($page->deletable)
          <span class="item"><a href="{{ route('home.page', $page->slug) }}">{{ $page->name }}</a></span>
          @endif
          @endforeach
        </div>
      </div>
      @endif
      
      @guest
      <span class="item">
        <a href=""><i class="user outline icon left floated ml-0 mr-1"></i>{{ __('Account') }}</a>
      </span>
      @endguest

      @if(!auth_is_admin())
      @auth
      <span class="item">
        <a href="{{ route('home.downloads') }}">
          <i class="cloud download icon left floated ml-0 mr-1"></i>
          {{ __('Downloads') }}
        </a>
      </span>
      @endauth
      <span class="item">
        <a href="{{ route('home.favorites') }}">
          <i class="heart outline icon left floated ml-0 mr-1"></i>
          {{ __('Collection') }}
        </a>
      </span>
      
      @auth
      <div class="d-flex">
        <span class="item">
          <a href="{{ route('home.profile') }}">
            <i class="user outline icon left floated ml-0 mr-1"></i>
            {{ __('Profile') }}
          </a>
        </span>
        
        <span class="item">
          <a @click="logout">
            <i class="sign out alternate icon left floated ml-0 mr-1"></i>
            {{ __('Sign out') }}
          </a>
        </span>
      </div>
      @endauth
      @endif
      
      <span class="item">
        <a href="{{ route('home.support') }}"><i class="question icon left floated ml-0 mr-1"></i>{{ __('Support') }}</a>
      </span>

      <span class="item">
        <a href="{{ route('home.page', 'privacy-policy') }}"><i class="shield alternate icon left floated ml-0 mr-1"></i>{{ __('Privacy policy') }}</a>
      </span>
      <span class="item">
        <a href="{{ route('home.page', 'terms-and-conditions') }}"><i class="square outline icon left floated ml-0 mr-1"></i>{{ __('Terms') }}</a>
      </span>
     
    </div>
  </div>

  <div  class="wrapper sub" 
        :class="{'d-none': menu.mobile.submenuItems === null}" 
        v-if="menu.mobile.selectedCategory" 
        v-cloak>
    <span class="item" @click="mainMenuBack">
      <a><i class="angle icon left floated ml-0 mr-1"></i>{{ __('Back') }}</a>
    </span>

    <div class="parent subcategories">
      <span class="item">
        <a>
          <i class="circle outline icon left floated ml-0 mr-1"></i>
          @{{ menu.mobile.selectedCategory.name }}
        </a>
      </span>
      
      <div v-bar>
        <div class="child main">
          <span class="item" v-for="subcategory in menu.mobile.submenuItems">
            <a :href="setProductsRoute(menu.mobile.selectedCategory.slug+'/'+subcategory.slug)">
              @{{ subcategory.name }}
            </a>
          </span>
          <span class="item">
            <a :href="setProductsRoute(menu.mobile.selectedCategory.slug)">
              @{{ menu.mobile.selectedCategory.name }}
            </a>
          </span>
        </div>
      </div>
      
    </div>
  </div>

</div>