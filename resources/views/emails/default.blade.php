@component('mail::message', ['md5_email' => $data['md5_email']])

{!! $data['message'] !!}

@endcomponent