@component('mail::message', ['md5_email' => $data['md5_email']])

<table style="font-family: arial; max-width: 800px; width: 100%">
  <thead>
    <tr>
      <th colspan="2" style="text-align: left">
        <h1 style="font-size: 1.1rem;margin-top: 0;">Thanks for your order, {{ $data['buyer'] }}</h1>
        <p style="font-size: 1rem;color: #686868 !important; font-weight: normal !important;">Thanks for completing your purchase with {{ config('app.name') }}. 
            The details of your order can be found below.</p>
      </th>
    </tr>
  </thead>
  <tbody>
    @foreach($data['items'] ?? [] as $item)
    <tr>
      <td style="width: 320px; padding: 0">
        <a href="{{ route('home.product', $item->slug) }}">
          <img style="width: 320px; height: auto;" src="{{ asset("storage/covers/{$item->cover}") }}">
        </a>
      </td>
      <td style="padding: 0 0 0 1rem">
        <div class="details" style="font-size: .9rem">
          <h1 style="font-size: 1.1rem !important;margin-top: 0;text-decoration: none !important;">
            <a href="{{ route('home.product', $item->slug) }}">{{ $item->name }}</a>
          </h1>
          <div>
            <strong style="width: 150px; margin-right: 1rem">Price</strong>
            <span style="float: right">{{ $data['currency_symbol'].$item->price }}</span>
          </div>
          <div style="margin-top: .25rem">
            <strong style="width: 150px; margin-right: 1rem">Qty</strong>
            <span style="float: right">{{ $item->quantity }}</span>
          </div>
          <div style="margin-top: .25rem">
            <strong style="width: 150px; margin-right: 1rem">Total</strong>
            <span style="float: right">{{ $data['currency_symbol'].number_format($item->price * $item->quantity, 2) }}</span>
          </div>
        </div>
      </td>
    </tr>
    <tr><td colspan="2" style="text-align: center">- - - - - - - - - - - -</td></tr>
    @endforeach
  </tbody>
  <tfoot>
    <tr style="text-align: right">
      <th style="width: 320px">Discount</th>
      <th>{{ $data['currency_symbol'].$data['discount'] }}</th>
    </tr>
    <tr style="text-align: right">
      <th style="width: 320px">Shipping</th>
      <th>{{ $data['currency_symbol'].$data['shipping'] }}</th>
    </tr>
    <tr style="text-align: right">
      <th style="width: 320px">Fee</th>
      <th>{{ $data['currency_symbol'].$data['fee'] }}</th>
    </tr>
    <tr style="text-align: right">
      <th style="width: 320px">Tax/VAT</th>
      <th>{{ $data['currency_symbol'].$data['tax'] }}</th>
    </tr>
    <tr style="text-align: right">
      <th style="width: 320px">Total</th>
      <th>{{ $data['currency_symbol'].$data['total_amount'] }}</th>
    </tr>
  </tfoot>
</table>

@endcomponent