require('./bootstrap');

window.marquee = require('jquery.marquee');

import Vuebar from 'vuebar';
Vue.use(Vuebar);

require('dragscroll');


const app = new Vue(
{
	el: '#app',
	data: {
		route: props.currentRouteName,
		cartItems: parseInt(localStorage.getItem('cartItems')) || 0,
		cart: JSON.parse(localStorage.getItem('cart')) || [],
		trasactionMsg: props.trasactionMsg || '',
		product: props.product || {},
		products: props.products || {},
		favorites: {},
		activeScreenshot: props.activeScreenshot,
		couponRes: {},
		paymentProcessor: '',
		totalAmount: 0,
		location: props.location,
		paymentProcessors : props.paymentProcessors,
		subcategories: props.subcategories,
		categories: props.categories,
		countryNames: props.countryNames || [],
		itemId: props.itemId || null,
		folderFileName: null,
		replyTo: {userName: null, commentId: null},
		folderContent: null,
		menu: {
			mobile: {
				selectedCategory: null,
				submenuItems: null,
				hidden: true
			},
			desktop: {
				selectedCategory: null,
				submenuItems: null,
				itemsMenuPopup: {top: 97, left: 0}
			}
		}
	},
	methods: {
		setPaymentProcessor: function(method)
		{
			this.paymentProcessor = method;
		},
		checkout: function(e)
		{
			if(this.paymentProcessor)
			{
				e.target.disabled = true;

				if(this.paymentProcessor === 'stripe')
				{
					if(this.paymentProcessors.stripe)
					{
						$.post('/checkout/payment', 
						{
							cart: JSON.stringify(app.cart.reduce(function(c, v)
										{
											return c.concat({
												id: v.id, 
												quantity: v.quantity,
												price: v.price})

										}, [])),
							processor: "stripe",
							coupon: this.couponRes.status ? this.couponRes.coupon.code : null
						}, null, 'json')

						.done(function(data)
						{
							stripe.redirectToCheckout({sessionId: data.id})
							.then(function (result) 
							{
								try
								{
									alert(result.error.message)
								}
								catch(err){}
							});
						})
					}
				}
				else
				{
					$('#form-checkout').submit();
				}
			}
		},
		buyNow: function()
		{
			this.addToCart();

			location.href = props.checkoutRoute || '';
		},
		addToCart: function()
		{
			this.cartItems = (parseInt(localStorage.getItem('cartItems')) || 0)+1;

			var localStorageCart = (JSON.parse(localStorage.getItem('cart')) || []);

			if(localStorageCart.length)
			{
				this.cart = localStorageCart;

				var indexOfProduct = 	this.getProductIndex(this.product['id'], false);

				if(indexOfProduct >= 0)
				{
					this.cart[indexOfProduct]['quantity'] += 1;

					this.updateCartItem(indexOfProduct);
				}
				else
				{
					this.cart.push(this.product);
				}

				this.saveCartChanges();

				return;
			}

			if(this.cart.length)
			{
				var indexOfProduct = 	this.getProductIndex(this.product['id']);

				if(indexOfProduct >= 0)
				{
					this.cart[indexOfProduct]['quantity'] += 1;

					this.updateCartItem(indexOfProduct);
				}
				else
				{
					this.cart.push(this.product);
				}
			}
			else
			{
				this.cart.push(this.product);
			}

			this.saveCartChanges();
		},
		addToCartAsync: function(id)
		{
			if(isNaN(id)) return;

			$.post('/add_to_cart_async', {"item_id": id}, null, 'json')
			.done((data) =>
			{
				app.product = data.product;
				app.addToCart();

				app.$forceUpdate();
			})
		},
		removeFromCart: function(productId)
		{
			if(!confirm('Are you sure you want to remove this item ?'))
				return;

			var indexOfProduct = 	this.getProductIndex(productId);

			this.cartItems = (this.cartItems - this.cart[indexOfProduct]['quantity']);

			this.cart.splice(indexOfProduct, 1);

			this.saveCartChanges();
		},
		editItemQuantity: function(productId, newQuantity)
		{
			var indexOfProduct = 	this.getProductIndex(productId);

			newQuantity = parseInt(newQuantity);

			if(indexOfProduct < 0)
				return;

			if(!(newQuantity > 0))
			{
				this.updateCartItem(indexOfProduct);

				return;
			}

			this.cartItems = (this.cartItems - this.cart[indexOfProduct]['quantity']) + newQuantity;

			this.cart[indexOfProduct]['quantity'] = newQuantity;

			this.updateCartItem(indexOfProduct);

			this.saveCartChanges();
		},
		getProductIndex: function(productId, fromVueCart = true)
		{
			if(fromVueCart)
			{
				return 	this.cart.reduce(function(acc, currval) {
									return acc.concat(currval.id)
								}, []).indexOf(productId);
			}
			else
			{
				var localStorageCart = (JSON.parse(localStorage.getItem('cart')) || []);

				if(!localStorageCart.length)
					return -1;

				return 	localStorageCart.reduce(function(acc, currval) {
									return acc.concat(currval.id)
								}, []).indexOf(productId);
			}
		},
		saveCartChanges: function()
		{
			this.totalAmount = this.getTotalAmount();

			localStorage.setItem('cart', JSON.stringify(this.cart));
			localStorage.setItem('cartItems', this.cartItems);
		},
		updateCartItem: function(indexOfProduct)
		{
			this.cart.splice(indexOfProduct, 1, this.cart[indexOfProduct]);
		},
		applyCoupon: function(event)
		{
			var input  = event.target.parentElement.previousElementSibling;
			var coupon = input.value;
			var _this  = this;

			if(!coupon.length)
				return false;
			
			$.post('/checkout/validate_coupon', 
				{
					coupon: coupon,
					products_ids: JSON.stringify(_this.cart.reduce(function(c, v)
												{
													return c.concat(v.id);
												}, []))
				}, 
				null, 'json')
			.done(function(res)
			{
				_this.couponRes = res;

				if(res.status)
				{
					var total_amount = 	_this.getTotalAmount();

					var discount = 0;

					if(res.coupon.product)
					{
						for(var i in _this.cart)
						{
							if(_this.cart[i].id == res.coupon.product)
							{
								var price = _this.cart[i].price;

								discount = 	res.coupon.is_percentage
														? Number.parseFloat(price * res.coupon.value / 100).toFixed(2)
														: Number.parseFloat(res.coupon.value).toFixed(2);

								break;
							}
						}
					}
					else
					{
						discount = 	res.coupon.is_percentage
												? Number.parseFloat(_this.totalAmount * res.coupon.value / 100).toFixed(2)
												: Number.parseFloat(res.coupon.value).toFixed(2);
					}

					var newTotal = total_amount - discount;

					_this.totalAmount = (newTotal < 0) ? 0 : newTotal;

					_this.removeFromCart = function() { return false };
					_this.editItemQuantity = function() { return false };

					_this.applyCoupon = function() { return false };
				}
			})
		},
		removeCoupon: function()
		{
			location.reload();
		},
		getTotalAmount: function()
		{
			return 	Number.parseFloat(this.cart.reduce(function(c, v){
								return c + (v.price * v.quantity)
							}, 0)).toFixed(2);
		},
		slideScreenhots: function(slideDirection)
		{
			var screenshots 	 = this.product.screenshots;
			var screenshotsLen = this.product.screenshots.length;
			var activeIndex =  screenshots.indexOf(app.activeScreenshot);

			if(slideDirection === 'next')
			{
				if((activeIndex+1) < screenshotsLen)
					this.activeScreenshot = screenshots[activeIndex+1]
				else
					this.activeScreenshot = screenshots[0]
			}
			else
			{
				if((activeIndex-1) >= 0)
					this.activeScreenshot = screenshots[activeIndex-1]
				else
					this.activeScreenshot = screenshots[screenshotsLen-1]
			}
		},
		setSubMenu: function(e, categoryIndex, mobileMenu = false)
		{
			if(categoryIndex === null)
				return;
			
			if(mobileMenu)
			{
				Vue.set(this.menu, 'mobile', Object.assign({... this.menu.mobile}, {
									selectedCategory: this.categories[categoryIndex] || null,
									submenuItems: this.subcategories[categoryIndex]
								}));
			}
			else
			{ 
				var isSame = categoryIndex == getObjectProp(this.menu.desktop.selectedCategory, 'id');

				Vue.set(this.menu, 'desktop', Object.assign({... this.menu.desktop}, {
									selectedCategory: isSame ? null : this.categories[categoryIndex],
									submenuItems: isSame ? null : this.subcategories[categoryIndex],
									itemsMenuPopup: {top: 97, left: e.target.getBoundingClientRect().left}
								}));
			}
		},
		mainMenuBack: function()
		{
			Vue.set(this.menu, 'mobile', Object.assign({... this.menu.mobile}, {
								selectedCategory: null,
								submenuItems: null
							}));
		},
		setProductsRoute: function(categorySlug)
		{
			return `${props.productsRoute}/${categorySlug}`;
		},
		downloadItem: function(itemId, formSelector = '#user-downloads form')
		{
			if(!itemId)
				return;

			this.itemId = itemId;

			this.$nextTick(function()
			{
				$(formSelector).submit();
			})
		},
		downloadFile: function(folderFileName, formSelector)
		{
			if(!folderFileName)
				return;

			this.folderFileName = folderFileName;

			this.$nextTick(function()
			{
				$(formSelector).submit();
			})
		},
		toggleMobileMenu: function()
		{
			Vue.set(this.menu, 'mobile', Object.assign({... this.menu.mobile}, {
								selectedCategory: null,
								submenuItems: null,
								hidden: $('#mobile-menu').isVisible()
							}));

			$('#mobile-menu').transition('fly right', function()
			{
				$('html').toggleClass('overflow-hidden')
			});
		},
		toggleItemsMenu: function()
		{
			$('#items-menu').transition('drop');
		},
		toggleMobileSearchBar: function()
		{
			$('#mobile-search-bar').transition('drop');
		},
		collectionToggleItem: function(e, id)
		{
			if(localStorage.hasOwnProperty('favorites'))
			{
				var favs = JSON.parse(localStorage.getItem('favorites'));

				if(Object.keys(favs).indexOf(String(id)) >= 0)
				{
					var newFavs = Object.keys(favs).reduce((c, v) => {
													if(v != String(id))
														c[v] = favs[v];

													return c;
												}, {});

					localStorage.setItem('favorites', JSON.stringify(newFavs));

					this.favorites = newFavs;

					$(e.target).toggleClass('active', false);
				}
				else
				{
					favs[id] = this.products[id];

					localStorage.setItem('favorites', JSON.stringify(favs));

					$(e.target).toggleClass('active', true);
				}
			}
		},
		itemInCollection: function(id)
		{
			return Object.keys(this.favorites).indexOf(String(id)) >= 0;
		},
		logout: function()
		{
			$('#logout-form').submit();
		},
		setReplyTo: function(userName, commentId)
    {
      this.replyTo = {userName, commentId};

      $('#item .column.l-side .support textarea').focus();
    },
    resetReplyTo: function()
    {
      this.replyTo = {userName: null, commentId: null};
    },
    getFolderContent: function()
    {
    	var _this = this;

    	if(this.folderContent === null)
    	{
    		$.post('/item/product_folder', {"slug": this.product['slug']}, null, 'json')
    		.done(function(data)
    		{
    			if(data.hasOwnProperty('files'))
    			{
    				_this.folderContent = data.files;
    			}
    		})
    	}
    },
    getFolderFileIcon: function(fileObj)
    {
    	var fileMimeType = fileObj.mimeType;

    	if(/(text\/plain|txt)/i.test(fileMimeType))
    	{
    		return 'file alternate outline';
    	}
    	else if(/(image\/.+|\.(png|jpg|jpeg))/i.test(fileMimeType))
    	{
    		return 'file image outline';
    	}
    	else if(/zip|rar|archive|7z/i.test(fileMimeType))
    	{
    		return 'file archive outline';
    	}
    	else
    	{
    		return 'file outline';
    	}
    }
	},
	created: function()
	{
		if(!this.trasactionMsg.length)
		{
			this.cart = this.cart.filter(function(item)
									{
										return item !== null
									});

			if(this.cart.length)
			{
				this.cartItems = this.cart.reduce(function(accumulator, cartItem)
				{
					return accumulator + cartItem.quantity;
				}, 0);
			}
		}
		else
		{
			this.cart = [];
			this.cartItems = 0;

			localStorage.removeItem('cartItems')
			localStorage.removeItem('cart');
		}
	},
	mounted: function()
	{
		if(this.cartItems)
		{
			this.totalAmount = Number.parseFloat(this.cart.reduce(function(c, v){
														return c + (v.price * v.quantity)
													}, 0)).toFixed(2);
		}

		if(!localStorage.hasOwnProperty('favorites'))
		{
			localStorage.setItem('favorites', '{}');
		}
		else
		{
			this.favorites = JSON.parse(localStorage.getItem('favorites'));
		}
	}
});


function getObjectProp(obj, prop)
{
	if(obj === null)
		return null;

	if(obj.hasOwnProperty(prop))
		return obj[prop];

	return null;
}

window.scrollBarWidth = (function()
{
	var outer = document.createElement('div');
	outer.style.visibility = 'hidden';
	outer.style.overflow = 'scroll';
	outer.style.msOverflowStyle = 'scrollbar';
	document.body.appendChild(outer);

	var inner = document.createElement('div');
	outer.appendChild(inner);
	
	var scrollbarWidth = (outer.offsetWidth - inner.offsetWidth);

	outer.parentNode.removeChild(outer);

	return scrollbarWidth;
})();


$.fn.isVisible = function(checkDisplay = false)
{
	var styles 	= getComputedStyle($(this)[0]);
	var visible = styles.visibility === 'visible';

	if(!checkDisplay)
	{
		return visible;
	}
	else
	{
		return visible && styles.display !== 'none';
	}
}

$.fn.replaceClass = function(oldClass, newClass)
{
	if(this.hasClass(oldClass))
		this.removeClass(oldClass).addClass(newClass);
};


$(()=>
{

	$(window).on('click', function(e)
	{
		if(!$(e.target).closest('#items-menu').length)
		{
			Vue.set(app.menu, 'desktop', Object.assign({... app.menu.desktop}, {
								selectedCategory: null,
								submenuItems: null,
								itemsMenuPopup: {left: 0}
							}));
		}

		if(!$(e.target).closest('#top-menu .dropdown.cart').length)
		{
			$('#top-menu .dropdown.cart .menu').replaceClass("visible", "hidden");
		}

		if(!$(e.target).closest('#top-menu .dropdown.notifications').length)
		{
			$('#top-menu .dropdown.notifications .menu').replaceClass("visible", "hidden");
		}

		if(!$(e.target).closest('.search-form').length)
		{
			app.searchResults = [];
		}
	})


	$(document).on('click', '#top-menu .dropdown.notifications .item:not(.all), #notifications .unstackable.items a.item', 
  function()
  {
    var notifId = $(this).data('id');
    var _href = $(this).data('href');
    
    if(isNaN(parseInt(notifId)))
      return;

    $.post('/notifications/read', {notif_id: notifId})
    .done(function()
    {      
      location.href = _href;
    })
    .always(function()
    {
      if(location.href.includes('#'))
        location.reload()
    })
  })

	$('#top-menu .dropdown.cart .toggler').on('click', function()
	{
		$('#top-menu .dropdown.cart .menu').transition('drop');
	})

	$('#top-menu .dropdown.notifications .toggler').on('click', function()
	{
		$('#top-menu .dropdown.notifications .menu').transition('drop');
	})
	
	$(document).on('click', '#top-menu .dropdown.notifications .item:not(.all), #notifications .unstackable.items a.item', 
	function()
	{
		var notifId = $(this).data('id');
		var _href = $(this).data('href');
		
		if(isNaN(parseInt(notifId)))
			return;

		$.post('/notifications/read', {notif_id: notifId})
		.done(function()
		{      
			location.href = _href;
		})
		.always(function()
		{
			if(location.href.includes('#'))
				location.reload()
		})
	})
	
	$('.hide-scroll').each(function()
	{
		$(this).find('.wrapper').css({
			"width": `calc(100% + ${scrollBarWidth + 'px'})`,
			"height": `calc(100% + ${scrollBarWidth + 'px'})`,
			"visibility": "visible"
		})
	})

	$('#items-menu .wrapper.dragscroll').on('scroll', function()
	{
		Vue.set(app.menu, 'desktop', Object.assign({... app.menu.desktop}, {
							selectedCategory: null,
							submenuItems: null,
							itemsMenuPopup: {top: 97, left: 0}
						}));
	})

	$('#user-downloads tbody .image')
	.popup({
		inline     : true,
		hoverable  : true,
		position   : 'bottom left'
		})

	$('.marquee').marquee({
		duration: 10000,
		delayBeforeStart: 2000,
		gap: 50,
		startVisible: true,
		pauseOnHover: true,
		direction: 'left',
		duplicated: true
	});

	$('.search-form .search.link').on('click', function()
	{
		$(this).closest('.search-form').submit();
	})

	$('.search-form').on('submit', function(e)
	{
		if(!$(this).find('input[name="q"]').val().trim().length)
		{
			e.preventDefault();
			return false;
		}
	})

	$('form.newsletter .plane.link').on('click', function()
	{
		$(this).closest('.form.newsletter').submit();
	})

	$('.form.newsletter').on('submit', function(e)
	{
		if(!/^(.+)@(.+)\.([a-z]+)$/.test($(this).find('input[name="email"]').val().trim()))
		{
			e.preventDefault();
			return false;
		}
	})

	

	$('.screenshot').on('click', function()
	{
		app.activeScreenshot = $(this)[0].src;

		$('#screenshots').modal('show');
	})

	$(document).on('click', '.logout', function() {
		$('#logout-form').submit();
	})

	$('#item-r-side-toggler').on('click', function()
	{
		$('#item .r-side').transition('fly right');
	})


	$('#item .l-side .top.menu a.item:not(#item-r-side-toggler a)')
	.on('click', function()
	{
		$('#item .l-side .top.menu a.item').removeClass('active');
		$(this).toggleClass('active', true).transition('tada');

		$('#item .l-side .item > .column').hide()
		.siblings('.column.' + $(this).data('tab')).show();
	})


	$('.left-column-toggler').on('click', function()
	{
		$('#items .left.column').transition({
			animation: 'slide right'
		})
	})


	$('#mobile-menu .categories .items-wrapper').on('click', function()
	{
		$(this).toggleClass('active')
					.siblings('.items-wrapper').removeClass('active');
	})


	$('#items-menu>.item').on('click', function()
	{	
		$(this).toggleClass('active')
					.siblings('.item')
					.removeClass('active');
	})



	$('#item .card .header .link.angle.icon').on('click', function()
	{
		$(this).closest('.card').find('.content.body').toggle();
	})



	$(window).click(function(e)
	{
		if(!$(e.target).closest('#items-menu').length || $(e.target).closest('.search-item').length)
		{
			$('#items-menu>.item').removeClass('active');
		}
	})


	$('.close.icon').on('click', function()
	{
		$(this).closest('.ui.modal').modal('hide');
	})


	$('#user-profile .menu.unstackable .item').on('click', function()
	{
		var tab = $(this).data('tab');

		$(this).toggleClass('active', true)
					.siblings('.item').removeClass('active');
					
		$('#user-profile table.'+tab)
		.show()
		.siblings('.table').hide();
	})



	$('#user-profile input[name="user_avatar"]').on('change', function() {
		var file    = $(this)[0].files[0];
		var reader  = new FileReader();

		if(/^image\/(jpeg|jpg|ico|png|svg)$/.test(file.type))
		{
			reader.addEventListener("load", function() {
				$('#user-profile .user_avatar img').attr('src', reader.result);
			}, false);

			if(file)
			{
				reader.readAsDataURL(file);

				try
				{
					$('input[name="user_avatar_changed"]').prop('checked', true);
				}
				catch(err){}
			}
		}
		else
		{
			alert('File type not allowed!');

			$(this).val('');
		}
	})

	$('.ui.checkbox').checkbox();
	$('.ui.dropdown').dropdown();

	$('.ui.rating.active').rating({
		onRate: function(rate)
		{
			$(this).siblings('input[name="rating"]').val(rate);
		}
	});

	$('.ui.rating.disabled').rating('disable');

	if(app.route === 'home.product')
	{
		if(location.href.indexOf('#') >= 0)
		{
			$(`#item .l-side .top.menu .item[data-tab="${location.href.split('#')[1]}"]`)[0].click();
		}
	}

	$('#support .segments .segment').on('click', function()
	{
		$('p', this).find('i').toggleClass($('div', this).is('visible') ? 'plus minus' : 'minus plus');
		$('div', this).slideToggle();
	})

	$('.message .close')
	.on('click', function() {
		$(this)
			.closest('.message')
			.transition('fade')
		;
	})
})