<?php

	return [
		'google_drive' => [
			'enabled' => 0,
			'client_id' => null,
			'client_secret' => null,
			'refresh_token' => null
		],

		'dropbox' => [
			'enabled' => 0,
			'app_key' => null,
			'app_secret' => null,
			'access_token' => null
		]
	];