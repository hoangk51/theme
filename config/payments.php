<?php

	return [
		'paypal' => [
			'enabled' => 0,
			'mode' => 'sandbox',
			'client_id' => null,
			'secret_id' => null,
			'fee' => 0
		],

		'stripe' => [
			'enabled' => 0,
			'mode' => 'sandbox',
			'client_id' => null,
			'secret_id' => null,
			'fee' => 0
		],

		'currency_code' => 'USD',
		'currency_symbol' => '$',
		'vat'	=> 0
	];