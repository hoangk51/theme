<?php
	define('VALEXA_UPGRADING', true);
	require_once './bootstrap.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Releases</title>
	<meta name="language" content="en-us">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="csrf-token" content="<?= csrf_token() ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="icon" href="<?= asset_('releases/favicon.png') ?>">
	<meta name="robots" content="noindex,nofollow">
	<link rel="stylesheet" href="<?= asset_('releases/tailwind.1.2.0.min.css') ?>">
	<script src="<?= asset_('assets/jquery/jquery-3.4.1.min.js') ?>"></script>
</head>

<body>
	<div class="container mx-auto p-3 max-w-screen-lg">
		<div class="flex items-center bg-gray-100 rounded">
			<div class="w-48 p-3">
				<a href="/"><img class="w-full bg-white rounded h-16" src="<?= asset_('releases/logo.png') ?>"></a>
			</div>
			<div class="flex-1 bg-white w-full rounded mr-3">
				<h1 class="text-3xl h-16 flex items-center content-center px-3">Releases</h1>
			</div>
		</div>

		<div class="bg-gray-100 rounded mt-3 p-3 h-screen">
			<?php if(session('status')): ?>
			<div class="flex items-center bg-blue-500 text-white text-sm font-bold px-4 py-3 mb-3 rounded" role="alert">
			  <svg class="fill-current w-4 h-4 mr-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M12.432 0c1.34 0 2.01.912 2.01 1.957 0 1.305-1.164 2.512-2.679 2.512-1.269 0-2.009-.75-1.974-1.99C9.789 1.436 10.67 0 12.432 0zM8.309 20c-1.058 0-1.833-.652-1.093-3.524l1.214-5.092c.211-.814.246-1.141 0-1.141-.317 0-1.689.562-2.502 1.117l-.528-.88c2.572-2.186 5.531-3.467 6.801-3.467 1.057 0 1.233 1.273.705 3.23l-1.391 5.352c-.246.945-.141 1.271.106 1.271.317 0 1.357-.392 2.379-1.207l.6.814C12.098 19.02 9.365 20 8.309 20z"/></svg>
			  <p><?= session('status') ?></p>
			</div>
			<?php endif ?>
			
			<div class="grid grid-cols-1 lg:grid-cols-5 md:grid-cols-3 sm:grid-cols-2 gap-3">
				<?php foreach ($releases as $release): ?>
				<div>
					<div class="p-3 flex content-center items-center h-32 <?= releaseVersion($release) == env('APP_VERSION') ? 'bg-red-600' : 'bg-gray-800' ?> rounded-tl rounded-tr">
						<h1 class="text-2xl text-center text-white mx-auto">Version <?= releaseVersion($release) ?></h1>
					</div>

					<div class="p-3 rounded-bl rounded-br bg-white">

						<?php if(isNotExecutable(releaseVersion($release))): ?>

						<a href="javascript:void(0)" class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded mb-3 w-full block text-center opacity-50 cursor-not-allowed" disabled>
						  EXECUTE
						</a>

						<?php else: ?>

						<a href="/releases/<?= releaseVersion($release) ?>" class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded mb-3 w-full block text-center">
						  EXECUTE
						</a>

						<?php endif ?>

						<a href="/releases/<?= (string)releaseVersion($release) ?>/CHANGELOG.md" target="_blank" class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded w-full block text-center">
						  CHANGELOG
						</a>
					</div>
				</div>	
				<?php endforeach ?>
			</div>
			
		</div>
	</div>

</body>
</html>