<?php
	
	defined('VALEXA_UPGRADING') OR exit('Access not allowed');

	define('LARAVEL_START', microtime(true));

	if(!file_exists(__DIR__.'/../../vendor/autoload.php'))
		exit('Please run composer install first');

	require __DIR__.'/../../vendor/autoload.php';

	$app = require_once __DIR__.'/../../bootstrap/app.php';

	$app->make('Illuminate\Contracts\Http\Kernel')->handle(Illuminate\Http\Request::capture());

	if(! env('APP_INSTALLED')) abort(403, 'Application not installed yet!');
	
	$releases = glob(base_path('/public/releases/*'), GLOB_ONLYDIR);

	function isNotExecutable($version, $echo_str = '')
	{
		if((float)env('APP_VERSION') >= $version)
		{
			echo $echo_str;
			
			return true;
		}

		return;
	}

	function releaseVersion($release)
	{
		return (float)pathinfo($release, PATHINFO_BASENAME);
	}

	function release_path($path)
	{
		return base_path('public/releases/1.1/'.$path);
	}