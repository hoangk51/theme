require('./bootstrap');

window.authorizeGooglDriveApp = function()
{
	gapi.load('client:auth2', function()
	{
		var clientId 		 = $('input[name="google_drive[client_id]"]').val().trim();
		var clientSecret = $('input[name="google_drive[client_secret]"]').val().trim();

		gapi.auth2.authorize({
      'scope': 'https://www.googleapis.com/auth/drive email',
      'clientId': clientId,
      'response_type': 'code id_token permission',
      'prompt': 'select_account consent'
    }, 
    function(response)
    {
    	if(response.hasOwnProperty('error'))
    	{
    		alert(response.error + ' - ' + response.detail)
    	}
    	else
    	{
        var idToken = response.id_token;
    		var payload = {clientId: clientId, clientSecret: clientSecret, code: response.code};

        $.post(`${googleDriveCurrentUserRoute}`, {"id_token": idToken}, null, 'json')
        .done((res) =>
        {
          $('input[name="google_drive[connected_email]"]').val(res.email);
          $('input[name="google_drive[id_token]"]').val(idToken);
        })
        .fail(() =>
        {
          alert('Google drive "Current user" Request failed');
          return;
        })

    		$.post(googleDriveCode2AcessTokenRoute || '', payload, null, 'json')
    		.done((res) =>
    		{
    			if(res.hasOwnProperty('error'))
    			{
    				alert(res.error);
    				return;
    			}
    			
    			$('input[name="google_drive[refresh_token]"]').val(res.refresh_token);
    		})
    		.fail(() =>
    		{
    			alert('Google drive "Refresh token" Request failed')
    		})
    	}
    })
	});
}

window.authorizeDropBoxApp = function()
{
	var clientId 		 				= $('input[name="dropbox[app_key]"]').val();
	var clientSecret 				= $('input[name="dropbox[app_secret]"]').val();

	var payload	= `client_id=${clientId}&response_type=token&redirect_uri=${dropBoxRedirectUri}&force_reauthentication=true&force_reapprove=true`;

	localStorage.setItem('dropBoxPayload', JSON.stringify({clientId, clientSecret}));

	location.href = `https://www.dropbox.com/oauth2/authorize?${payload}`;
}

window.getDropBoxAccessToken = function()
{
	if(!localStorage.hasOwnProperty('dropBoxPayload'))
		return;

	if(location.href.split('#').length === 2)
	{
		var dropBoxReqResponse = 	Object.fromEntries(location.href.split('#')[1].split('&').map(el => {
																return el.split('=', 2)
															})) || {};

    $.post(`${dropboxCurrentAccount}`, {"access_token": dropBoxReqResponse.access_token}, null, 'json')
    .done((res) =>
    {
      $('input[name="dropbox[current_account]"]').val(res.email);
    })
    .fail(() =>
    {
      alert('"Current dropbox account" Request failed');
      return;
    })

		var reqParams	= JSON.parse(localStorage.getItem('dropBoxPayload')) || {};

		$('input[name="dropbox[access_token]"]').val(dropBoxReqResponse.access_token);

		$('input[name="dropbox[app_key]"]').val(reqParams.clientId || null);
		$('input[name="dropbox[app_secret]"]').val(reqParams.clientSecret || null);

		localStorage.removeItem('dropBoxPayload');
		history.replaceState({}, document.title, "/admin/settings/files_host");
	}
}


window.debounce = function(func, wait, immediate) {
  var timeout;
  return function() {
    var context = this, args = arguments;
    var later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};


window.chunkArr = function(arr, n) 
{
  return  arr.reduce(function(p, cur, i)
              {
                  (p[i/n|0] = p[i/n|0] || []).push(cur);
                  return p;
              },[]);
};


$(function()
{  
	getDropBoxAccessToken();

	$(document).on('click', '.logout', function() {
		$('#logout-form').submit();
	})

  $('.ui.rating').rating('disable');
  
  $('#post .ui.placeholder input').on('change', function()
  { 
    var _this  = $(this); 
    var file   = $(this)[0].files[0];
    var reader = new FileReader();

    reader.addEventListener("load", function() 
    {
      _this.parent().find('img').attr('src', reader.result);

    }, false);

    reader.readAsDataURL(file);

    _this.siblings('.image')
         .toggleClass('ui', true)
         .find('img').show();
  })


	$('.message .close')
  .on('click', function() {
    $(this)
      .closest('.message')
      .transition('fade')
    ;
  })

  
  $('#toggle-r-column').on('click', function()
  {
  	var icon = $('i', this).hasClass('left');

  	$('i', this).removeClass(icon ? 'left' : 'right')
  							.addClass(icon ? 'right' : 'left');

  	$('#product .right.column').toggleClass('visible');
  	$('#product .left.column').toggleClass('blur');
  })


  $('#mobile-menu-toggler').on('click', function()
  {
      $('#content .l-side-wrapper')
      .toggleClass('active')
      .transition('slide right');
  })
  

	$('.ui.dropdown').dropdown();

})