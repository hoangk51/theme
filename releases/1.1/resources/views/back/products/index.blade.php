@extends('back.master')

@section('title', 'Products')


@section('content')

<div class="row main" id="products">

	<div class="ui menu shadowless">		
		<a id="bulk-delete" @click="deleteItems" :href="route+ids.join()" class="item" :class="{disabled: isDisabled}">
			Delete
		</a>

		<div class="right menu">
			<form action="{{ route('products') }}" method="get" id="search" class="ui transparent icon input item">
        <input class="prompt" type="text" name="keywords" placeholder="Search ..." required>
        <i class="search link icon" onclick="$('#search').submit()"></i>
      </form>
			<a href="{{ route('products.create') }}" class="item ml-1">Add</a>
		</div>
	</div>
	
	<div class="table wrapper">
		<table class="ui unstackable celled basic table">
			<thead>
				<tr>
					<th>
						<div class="ui fitted checkbox">
						  <input type="checkbox" @change="selectAll">
						  <label></label>
						</div>
					</th>
					<th class="five columns wide">
						<a href="{{ route('products', ['orderby' => 'name', 'order' => $items_order]) }}">Name</a>
					</th>
					<th>
						<a href="{{ route('products', ['orderby' => 'price', 'order' => $items_order]) }}">Price</a>
					</th>
					<th>
						<a href="{{ route('products', ['orderby' => 'sales', 'order' => $items_order]) }}">Sales</a>
					</th>
					<th>
						<a href="{{ route('products', ['orderby' => 'category', 'order' => $items_order]) }}">Category</a>
					</th>
					<th>
						<a href="{{ route('products', ['orderby' => 'active', 'order' => $items_order]) }}">Active</a>
					</th>
					<th>
						<a href="{{ route('products', ['orderby' => 'trending', 'order' => $items_order]) }}">Trending</a>
					</th>
					<th>
						<a href="{{ route('products', ['orderby' => 'featured', 'order' => $items_order]) }}">Featured</a>
					</th>
					<th>Files</th>
					<th>
						<a href="{{ route('products', ['orderby' => 'updated_at', 'order' => $items_order]) }}">Updated at</a>
					</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				@foreach($products as $product)
				<tr>
					<td class="center aligned">
						<div class="ui fitted checkbox select">
						  <input type="checkbox" value="{{ $product->id }}" @change="toogleId({{ $product->id }})">
						  <label></label>
						</div>
					</td>
					<td><a href="{{ route('home.product', $product->slug) }}">{{ ucfirst($product->name) }}</a></td>
					<td class="center aligned">${{ $product->price }}</td>
					<td class="center aligned">{{ $product->sales }}</td>
					<td class="center aligned">{{ $product->category }}</td>
					<td class="center aligned">
						<div class="ui toggle fitted checkbox" d>
						  <input type="checkbox" name="active" @if($product->active) checked @endif data-id="{{ $product->id }}" data-status="active" @click="updateStatus($event)">
						  <label></label>
						</div>
					</td>
					<td class="center aligned">
						<div class="ui toggle fitted checkbox">
						  <input type="checkbox" name="trending" @if($product->trending) checked @endif data-id="{{ $product->id }}" data-status="trending" @click="updateStatus($event)">
						  <label></label>
						</div>
					</td>
					<td class="center aligned">
						<div class="ui toggle fitted checkbox">
						  <input type="checkbox" name="featured" @if($product->featured) checked @endif data-id="{{ $product->id }}" data-status="featured" @click="updateStatus($event)">
						  <label></label>
						</div>
					</td>
					<td class="center aligned">
						@if($product->is_dir)
							<a href="{{ route('home.product_folder_sync', $product->slug) }}" target="_blank"><i class="cloud large download link grey icon mx-0"></i></a>
						@else
							<i class="cloud large download link grey icon mx-0" @click="downloadFile({{ $product->id }})"></i>
						@endif
					</td>
					<td class="center aligned">{{ $product->updated_at }}</td>
					<td class="center aligned one column wide">
						<div class="ui dropdown">
							<i class="bars large grey icon mx-0"></i>
							<div class="menu dropdown left">
								<a href="{{ route('products.edit', $product->id) }}" class="item">Edit</a>
								<a @click="deleteItem($event)" href="{{ route('products.destroy', $product->id) }}" class="item">Delete</a>
							</div>
						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>

	<div class="ui fluid divider"></div>

	{{ $products->appends($base_uri)->onEachSide(1)->links() }}
	{{ $products->appends($base_uri)->links('vendor.pagination.simple-semantic-ui') }}
	
	<form action="{{ route('home.download') }}" target="_blank" method="post" class="d-none" id="download-file">
		@csrf
		<input type="hidden" name="itemId" v-model="itemId">
	</form>
</div>

<script>
	'use strict';

	var app = new Vue({
	  el: '#products',
	  data: {
	  	route: '{{ route('products.destroy', "") }}/',
	    ids: [],
	    isDisabled: true,
	    itemId: ''
	  },
	  methods: {
	  	toogleId: function(id)
	  	{
	  		if(this.ids.indexOf(id) >= 0)
	  			this.ids.splice(this.ids.indexOf(id), 1);
	  		else
	  			this.ids.push(id);
	  	},
	  	selectAll: function()
	  	{
	  		$('#products tbody .ui.checkbox.select').checkbox('toggle')
	  	},
	  	deleteItems: function(e)
	  	{
	  		var confirmationMsg = 'Are you sure you want to delete the selected product(s) ?';

	  		if(!this.ids.length || !confirm(confirmationMsg))
	  		{
	  			e.preventDefault();
	  			return false;
	  		}
	  	},
	  	deleteItem: function(e)
	  	{
	  		if(!confirm('Are you sure you want to delete this product ?'))
  			{
  				e.preventDefault();
  				return false;
  			}
	  	},
	  	updateStatus: function(e)
	  	{	
	  		var thisEl  = $(e.target);
	  		var id 			= thisEl.data('id');
	  		var status 	= thisEl.data('status');

	  		if(['active', 'trending', 'featured'].indexOf(status) < 0)
	  			return;

	  		$.post('{{ route('products.status') }}', {status: status, id: id})
				.done(function(res)
				{
					if(res.success)
					{
						thisEl.checkbox('toggle');
					}
				}, 'json')
				.fail(function()
				{
					alert('Failed')
				})
	  	},
	  	downloadFile: function(itemId)
	  	{
	  		this.itemId = itemId;

	  		this.$nextTick(function()
	  		{
	  			$('#download-file').submit();
	  		})
	  	}
	  },
	  watch: {
	  	ids: function(val)
	  	{
	  		this.isDisabled = !val.length;
	  	}
	  }
	})


	$('#search').on('submit', function(event)
	{
		if(!$('input', this).val().trim().length)
		{
			e.preventDefault();
			return false;
		}
	})
</script>
@endsection