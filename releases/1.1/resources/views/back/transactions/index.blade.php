@extends('back.master')

@section('title', $title)


@section('content')

<div class="row main" id="transactions">

	<div class="ui menu shadowless">
		<a id="bulk-delete" @click="deleteItems" :href="route+ids.join()" class="item" :class="{disabled: isDisabled}">Delete</a>

		<div class="right menu">
			<form action="{{ route('transactions') }}" method="get" id="search" class="ui transparent icon input item mr-1">
        <input class="prompt" type="text" name="keywords" placeholder="Search ..." required>
        <i class="search link icon" onclick="$('#search').submit()"></i>
      </form>
		</div>
	</div>
	
	<div class="table wrapper">
		<table class="ui unstackable celled basic table">
			<thead>
				<tr>
					<th>
						<div class="ui fitted checkbox">
						  <input type="checkbox" @change="selectAll">
						  <label></label>
						</div>
					</th>
					<th>
						<a href="{{ route('transactions', ['orderby' => 'buyer', 'order' => $items_order]) }}">Buyer</a>
					</th>
					<th>
						<a href="{{ route('transactions', ['orderby' => 'amount', 'order' => $items_order]) }}">Amount</a>
					</th>
					<th>
						<a href="{{ route('transactions', ['orderby' => 'processor', 'order' => $items_order]) }}">Processor</a>
					</th>
					<th>
						<a href="{{ route('transactions', ['orderby' => 'refunded', 'order' => $items_order]) }}">Refunded</a>
					</th>
					<th>
						<a href="{{ route('transactions', ['orderby' => 'refund', 'order' => $items_order]) }}">Refund</a>
					</th>
					<th>
						<a href="{{ route('transactions', ['orderby' => 'updated_at', 'order' => $items_order]) }}">Updated at</a>
					</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				@foreach($transactions as $transaction)
				<tr id="{{ $transaction->transaction_id }}">
					<td class="center aligned">
						<div class="ui fitted checkbox select">
						  <input type="checkbox" value="{{ $transaction->id }}" @change="toogleId({{ $transaction->id }})">
						  <label></label>
						</div>
					</td>

					<td class="left aligned">{{ $transaction->buyer }}</td>

					<td class="center aligned">{{ env('CURRENCY_CODE').' '.number_format($transaction->amount, 2) }}</td>
					
					<td class="center aligned">{{ ucfirst($transaction->processor) }}</td>

					<td class="center aligned" >
						<span class="ui basic {{ $transaction->refunded ? 'red' : 'green' }} label m-0 refunded">
							{{ $transaction->refunded ? 'Yes' : 'No' }}
						</span>
					</td>

					<td class="center aligned refund">
						{{ env('CURRENCY_CODE') }}
						<span>{{ number_format($transaction->refund, 2) }}</span>
					</td>

					<td class="center aligned">{{ $transaction->updated_at }}</td>

					<td class="center aligned one column wide">
						<div class="ui dropdown">
							<i class="bars icon mx-0"></i>
							<div class="menu dropdown left">
								<a href="{{ route('transactions.show', $transaction->id) }}" class="item">Details</a>

								@if($transaction->processor !== 'skrill' && $transaction->refund != $transaction->amount)
									@if($transaction->processor === 'iyzico')
									<a class="item" href="{{ route('transactions.refund_iyzico', $transaction->transaction_id) }}">
										Refund
									</a>
									@else
									<a class="item" @click="refundBox('{{ $transaction->transaction_id }}', '{{ $transaction->processor }}')">
										Refund
									</a>
									@endif

								@endif

								<a class="item" @click="deleteItem($event)" href="{{ route('transactions.destroy', $transaction->id) }}">Delete</a>
							</div>
						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	
	<div class="ui fluid divider"></div>

	{{ $transactions->appends($base_uri)->onEachSide(1)->links() }}
	{{ $transactions->appends($base_uri)->links('vendor.pagination.simple-semantic-ui') }}
	
	<div class="ui tiny modal" id="refund">
    <div class="header">Refund transaction [@{{ transaction_id }}]</div>
    <div class="content">
    	<div class="ui form">
    		<div class="field">
    			<label>Amount to refund</label>
    			<input type="text" placeholder="leave empty for a full refund" @change="setAmountToRefund($event.target.value)" :value="amountToRefund">
    		</div>
    	</div>
    </div>
    <div class="actions">
      <button class="ui small basic red button" @click="cancelRefund">Cancel</button>
      <button class="ui small basic blue button" @click="confirmRefund">Confirm</button>
    </div>
  </div>
	
  <div class="ui dimmer" :class="{active: refundProcessing}">
    <div class="ui loader"></div>
  </div>

</div>

<script>
	'use strict';
	
	var app = new Vue({
	  el: '#transactions',
	  data: {
	  	route: '{{ route('transactions.destroy', "") }}/',
	    ids: [],
	    isDisabled: true,
	    transaction_id: null,
	    amountToRefund: 0,
	    refundProcessing: false,
	  },
	  methods: {
	  	toogleId: function(id)
	  	{
	  		if(this.ids.indexOf(id) >= 0)
	  			this.ids.splice(this.ids.indexOf(id), 1);
	  		else
	  			this.ids.push(id);
	  	},
	  	selectAll: function()
	  	{
	  		$('#transactions tbody .ui.checkbox').checkbox('toggle')
	  	},
	  	deleteItems: function(e)
	  	{
	  		var confirmationMsg = 'Are you sure you want to delete the selected transaction(s) ?';
	  		
	  		if(!this.ids.length || !confirm(confirmationMsg))
	  		{
	  			e.preventDefault();
	  			return false;
	  		}
	  	},
	  	deleteItem: function(e)
	  	{
	  		if(!confirm('Are you sure you want to delete this transaction ?'))
  			{
  				e.preventDefault();
  				return false;
  			}
	  	},
	  	refundBox: function(transaction_id, processor)
	  	{
	  		if(!/^(paypal|stripe|razorpay)$/i.test(processor))
	  			return false;

	  		this.transaction_id = transaction_id;
	  		this.processor = processor;
	  	},
	  	setAmountToRefund: function(amount)
	  	{
	  		this.amountToRefund = amount;

	  		if((isNaN(amount) || amount < 0))
	  			this.amountToRefund = 0;
	  	},
	  	cancelRefund: function()
	  	{
	  		this.amountToRefund = 0;
	  		this.transaction_id = null;
	  		this.processor = null;
	  	},
	  	confirmRefund: function()
	  	{
	  		if(confirm('Are you sure you want to refund this transaction ?'))
	  		{
		  		var data = {'transaction_id': this.transaction_id, 
		  								'amount_to_refund': this.amountToRefund,
		  								'processor': this.processor};

		  		this.refundProcessing = true;
		  		this.amountToRefund = 0;
  				this.transaction_id = null;
  				this.processor = null;

		  		$.post('{{ route('transactions.refund') }}', data, null, 'json')
		  		.done(function(res)
		  		{
		  			if(res.response === 'Done')
		  			{
		  				$('#'+data.transaction_id).find('.refunded')
		  				.removeClass('green').addClass('red')
		  				.text('Yes');

		  				$('#'+data.transaction_id).find('.refund')
		  																	.find('span')
		  																	.text(res.refund);
		  			}
		  			else
		  			{
		  				alert(res.message);
		  			}
		  		})
		  		.always(function()
		  		{
		  			app.refundProcessing = false;
		  		})
	  		}
	  	}
	  },
	  watch: {
	  	ids: function(val)
	  	{
	  		this.isDisabled = !val.length;
	  	},
	  	transaction_id: function(val)
	  	{
	  		$('#refund').modal(!val ? 'hide' : 'show')
	  	}
	  }
	})

	$('.ui.modal').modal({closable: false})
</script>
@endsection