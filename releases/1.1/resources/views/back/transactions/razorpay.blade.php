@extends('back.master')

@section('title', $title)

@section('content')

<div id="transactions" class="srtipe">

	<div class="ui two stackable cards">
		<div class="ui fluid card shadowless">
			<div class="content">
				<h3 class="header">Transaction details</h3>
			</div>
			<div class="content bg-ghostwhite">
				<strong class="status">Status :</strong> 
					<span class="ui basic @if($amount_refunded) red @else green @endif label">
						@if($refunded) 
						{{ strtoupper($refunded.' - '.__('Refund')) }}
						@else
						Complete
						@endif
					</span>
			</div>
			<div class="content bg-ghostwhite">
				<strong>ID :</strong> 
				<span>{{ $details->id }}</span>
			</div>
			<div class="content bg-ghostwhite">
				<strong>@if($created_at == $updated_at) Created @else Updated @endif :</strong> 
				<span>{{ (new DateTime($updated_at))->format("d F Y \a\\t h:i:s A") }}</span>
			</div>
			<div class="content">
				<strong>Total items :</strong> 
				<span>
					<strong>
						{{ $amount + $discount.' '.config('payments.currency_code') }} 
					</strong>
				</span>
			</div>
			<div class="content">
				<strong>Discount :</strong> 
				<span class="minus">
					{{ $discount.' '.config('payments.currency_code') }}
				</span>
			</div>
			<div class="content">
				<strong>Handling :</strong> 
				<span class="plus">
					{{ $handling.' '.config('payments.currency_code')  }}
				</span>
			</div>
			<div class="content">
				<strong>Tax :</strong> 
				<span class="plus">
					{{ $tax.' '.config('payments.currency_code') }}
				</span>
			</div>
			<div class="content">
				<strong>Gross amount :</strong> 
				<span>
					<strong>
						{{ round($amount + $handling + $tax, 2).' '.config('payments.currency_code') }}
					</strong>
				</span>
      </div>
      <div class="content">
				<strong>Razorpay Fee :</strong> 
				<span class="minus">
					{{ $fee.' '.config('payments.currency_code') }}
				</span>
			</div>
			<div class="content">
				<strong>Net amount :</strong> 
				<span>
					<strong>
						{{ round($amount + $handling + $tax - $fee, 2).' '.config('payments.currency_code') }}
					</strong>
				</span>
			</div>
			
			<div class="content bg-ghostwhite">
				<strong>Refunds :</strong> 
				<span>
					<strong>
						{{ $amount_refunded.' '.config('payments.currency_code') }}
					</strong>
				</span>
			</div>

		</div>

		<div class="ui fluid card shadowless">
			<div class="content">
				<h3 class="header">Items</h3>
			</div>

      @foreach($items as $k => $item)
        @php $i = $k + 1; @endphp

        <div class="content title">
          <h4>{{ $i }}. {{ $item->name }}</h4>
        </div>
        <div class="content">
          <strong>Unit amount :</strong> 
          <span>{{ $item->amount.' '.config('payments.currency_code') }}</span>
        </div>
        <div class="content">
          <strong>Quantity :</strong> 
          <span>{{ $item->quantity }}</span>
        </div>
        <div class="content">
          <strong>Description :</strong> 
          <span>{{ $item->description }}</span>
        </div>

			@endforeach
		</div>
	</div>
		
	<div class="ui fluid card shadowless">
		<div class="content">
			<h3 class="header">Buyer</h3>
    </div>
		<div class="content">
			<strong>Email :</strong> 
			<span>{{ $details->payment->email }}</span>
		</div>
	</div>
</div>

@endsection