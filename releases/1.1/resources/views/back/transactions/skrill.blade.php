@extends('back.master')

@section('title', $title)

@section('content')

<div id="transactions" class="srtipe">

	<div class="ui two stackable cards">
		<div class="ui fluid card shadowless">
			<div class="content">
				<h3 class="header">Transaction details</h3>
			</div>
			<div class="content bg-ghostwhite">
				<strong>ID :</strong> 
				<span>{{ $details->transaction_id }}</span>
			</div>
			<div class="content bg-ghostwhite">
				<strong>@if($created_at == $updated_at) Created @else Updated @endif :</strong> 
				<span>{{ (new DateTime($updated_at))->format("d F Y \a\\t h:i:s A") }}</span>
			</div>
			<div class="content">
				<strong>Total items :</strong> 
				<span>
					<strong>
						{{ number_format($details->amount + $details->discount, 2).' '.$details->currency }} 
					</strong>
				</span>
			</div>
			<div class="content">
				<strong>Discount :</strong> 
				<span class="minus">
					{{ number_format($details->discount, 2).' '.$details->currency }}
				</span>
			</div>
		</div>

		<div class="ui fluid card shadowless">
			<div class="content">
				<h3 class="header">Items</h3>
			</div>

			@foreach($details->items as $item)

			<div class="content title">
				<h4>{{ $item->name }}</h4>
			</div>

			<div class="content">
				<strong>Price :</strong> 
				<span>{{ round($item->price, 2).' '.$details->currency }}</span>
			</div>

			@endforeach
		</div>
	</div>

	<div class="ui fluid card shadowless">
		<div class="content">
			<h3 class="header">Buyer</h3>
		</div>
		<div class="content">
			<strong>Name :</strong> 
			<span>{{ $details->lastname . ' ' . $details->firstname }}</span>
		</div>
		<div class="content">
			<strong>Email :</strong> 
			<span>{{ $details->pay_from_email }}</span>
		</div>
	</div>
		
</div>

@endsection