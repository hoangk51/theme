@extends('front.default.master')

@section('additional_head_tags')
	<script type="application/javascript">
		'use strict';
		window.props['itemId'] = null;
	</script>
@endsection

@section('body')

<div class="ui shadowless celled grid my-0" id="items">

	<div class="one column row" id="page-title">
		<div class="fourteen wide column mx-auto">
			<div class="ui unstackable items">
			  <div class="item">
			    <div class="ui tiny image rounded">
			      <img src="{{ asset_("storage/thumbnails/{$product->thumbnail}") }}">
			    </div>
			    <div class="content">
			      <div class="header">{{ ucfirst($product->name) }}</div>
			      <div class="description">
			        <p>{{ ucfirst($product->short_description) }}</p>
			      </div>
			    </div>
			  </div>
			</div>
		</div>
	</div>

	<div class="sixteen wide column" id="user-directory">
		<div class="ui cards">
			@foreach($files_list as $file)
			<div class="card fluid center aligned">
				<div class="content" title="{{ $file->name }}">
					<h3 class="header">{{ $file->name }}</h3>
				</div>
				<div class="content">
					<i class="file huge outline icon mx-0"></i>
				</div>
				<a class="content link" @click="downloadFile('{{ $file->id }}', '#download-file-form')">
					{{ __('Download') }}
				</a>
			</div>
			@endforeach
		</div>
	</div>
	

	<form action="{{ route('home.product_folder_sync_download', $product->slug) }}" target="_blank" id="download-file-form" class="d-none" method="post">
		@csrf
		<input type="hidden" name="slug" value="{{ $product->slug }}">
		<input type="hidden" name="file_name" v-model="folderFileName">
	</form>

</div>

@endsection