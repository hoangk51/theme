@extends('front.default.master')

@section('additional_head_tags')
	<script type="application/javascript">
		'use strict';
		window.props['itemId'] = null;
	</script>
@endsection

@section('body')

<div class="ui shadowless celled grid my-0" id="items">

	<div class="one column row rounded-corner">
		<div class="column">
			<h2 class="ui header px-2 py-1">
				<i class="download large icon"></i> {{ isFolderProcess() ? __('Products') : __('Downloads') }}
			</h2>
		</div>
	</div>

	<div class="sixteen wide column" id="user-downloads">
		@if($products)
		<div class="wrapper">
			<table class="ui single line unstackable table mb-0">
				<thead>
					<tr>
						<th class="center aligned">Cover</th>
						<th>Name</th>
						<th>Rating</th>
						<th>Purchased at</th>
						<th>Last update</th>
						<th class="center aligned">{{ isFolderProcess() ? __('Folder') : __('Download') }}</th>
					</tr>
				</thead>
				<tbody>
					@foreach($products as $product)
					<tr>
						<td>
							<div class="ui image rounded" data-html="<img src='{{ asset_("storage/covers/{$product->cover}") }}'>">
							  <img src="{{ asset_("storage/covers/{$product->cover}") }}" alt="cover">
							</div>
						</td>
						<td>
							<a href="{{ route('home.product', $product->slug) }}">{{ $product->name }}</a>
						</td>
						<td>
							<div class="ui rating star disabled" data-rating="{{ $product->rating }}" data-max-rating="5"></div>
						</td>
						<td>
							{{ (new DateTime($product->purchased_at))->format('jS M Y \a\\t H:i') }}
						</td>
						<td>
							@if($product->last_update)
							{{ (new DateTime($product->last_update))->format('jS M Y \a\\t H:i') }}
							@else
							-
							@endif
						</td>
						<td class="right aligned">
							@if($product->is_dir)
								<a class="ui basic labeled icon button mx-0 right floated" target="_blank" href="{{ route('home.product_folder_sync', $product->slug) }}">
								  <i class="eye icon mx-0"></i> 
								  Open Folder
								</a>
							@else
							<button class="ui basic labeled icon button mx-0 right floated" @click="downloadItem({{ $product->id }})">
							  <i class="cloud download icon mx-0"></i>
							  Download
							</button>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>

		<form action="{{ route('home.download') }}" target="_blank" class="d-none" method="post">
			@csrf
			<input type="hidden" name="itemId" v-model="itemId">
		</form>
		@else

		<div class="ui fluid message">
			You have not purchased any item yet.
		</div>

		@endif
	</div>

</div>

@endsection