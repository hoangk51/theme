<div class="row first">
	<div class="column">
		<img class="ui image mx-auto" src="{{ asset_("storage/images/".config('app.logo')) }}" alt="{{ config('app.name') }}">
		<p class="m-0">
			{{ config('app.description') }}
		</p>
	</div>
	<div class="column">
		<h4><strong>F</strong>EATURED CATEGORIES</h4>
		<ul class="p-0">
			@foreach (config('popular_categories', []) as $p_category)
			<li><a href="{{ route('home.products.category', $p_category->slug) }}">{{ $p_category->name }}</a></li>
			@endforeach
		</ul>
	</div>
	<div class="column">
		<h4><strong>A</strong>DDITIONAL RESOURCES</h4>
		<ul class="p-0">
			<li><a href="{{ route('home.support') }}">Contact</a></li>
			<li><a href="{{ route('home.support') }}">FAQ</a></li>
			@foreach (config('pages', []) as $page)
			<li><a href="{{ route('home.page', $page->slug) }}">{{ $page->name }}</a></li>
			@endforeach
		</ul>
	</div>
	<div class="column">
		<h4><strong>N</strong>EWSLETTER</h4>
		<form class="ui form newsletter" action="{{ route('home.newsletter', ['redirect' => url()->current()]) }}" method="post">
			@csrf
			<p>Subscribe to our newsletter to receive news, updates, free stuff and new releases by email.</p>

			@if(session('newsletter_subscription_msg'))
			<div class="ui fluid small message inverted p-1-hf">
				{{ session('newsletter_subscription_msg') }}
			</div>
			@endif

			<div class="ui icon input fluid">
				<input type="text" name="email" placeholder="email...">
				<i class="paper plane outline link icon"></i>
			</div>
		</form>
		<div class="social-icons mx-auto justify-content-center mt-1">
			<a class="ui circular basic icon button" href="{{ config('app.facebook') }}">
				<i class="large blue facebook icon"></i>
			</a>
			<a class="ui circular basic icon button" href="{{ config('app.twitter') }}">
				<i class="large blue twitter icon"></i>
			</a>
			<a class="ui circular basic icon button" href="{{ config('app.pinterest') }}">
				<i class="large blue pinterest icon"></i>
			</a>
			<a class="ui circular basic icon button" href="{{ config('app.youtube') }}">
				<i class="large blue youtube icon"></i>
			</a>
			<a class="ui circular basic icon button mr-0" href="{{ config('app.tumblr') }}">
				<i class="large blue tumblr icon"></i>
			</a>
		</div>
	</div>
</div>

<div class="row last">
	<div class="sixteen wide column">
		<div class="ui secondary stackable menu">
			<a class="item" href="{{ route('home.page', 'privacy-policy') }}" title="Privacy policy">Privacy</a>
			<a class="item" href="{{ route('home.page', 'terms-and-conditions') }}" title="Terms & Conditions">Terms</a>
			<a class="item" href="{{ route('home.blog') }}">Blog</a>
			<a class="item" href="{{ route('home.support') }}">Help</a>

			<div class="right menu">
				<span class="item">{{ config('app.name') }} © {{ date('Y') }} all right reserved</span>
			</div>
		</div>
	</div>
</div>

@auth
	<form id="logout-form" action="{{ route('logout', ['redirect' => url()->current()]) }}" method="POST" class="d-none">@csrf</form>
@endauth