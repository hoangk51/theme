# Changelog

## [1.1] - 2020-03-05
### Added
- Razorpay payment gateway
- Skrill payment gateway
- Iyzico payment gateway
- Option to work with folders for products.
- Mimetypes config file to get the right extension for downloaded files.

### Fixed
- Installation process (Quoting .env vars during the installation).
- Downloading files via dropbox.
- Product thumbnails display size.
- Sitemap delete method.
- Subcategories for products.

### Removed
- Routes cache file.