<?php

namespace App\Listeners;

use App\Events\TransactionComplete;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class TransactionCompleteNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TransactionComplete  $event
     * @return void
     */
    public function handle(TransactionComplete $event)
    {
        $mail = new \App\Mail\NewOrder($event->transaction);

        \Mail::to([auth()->user()->email ?? $event->transaction['email']])->send($mail);
    }
}