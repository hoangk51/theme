<?php
	
	function auth_is_admin()
	{
		return preg_match('/^(superadmin|admin)$/i', request()->user()->role ?? null);
	}


	function if_null($str, $replacement)
	{
		if(is_null($str) || (strtolower((string)$str) === "null"))
			return $replacement;

		return $str;
	}


	function array_has_values($haystack, $needle, $all = false)
	{
		$haystack_values = array_map('strtolower', array_values($haystack));

		$found = 	array_filter($needle, function($v, $k) use($haystack_values)
							{
								return in_array(strtolower($v), $haystack_values);
							}, 
							ARRAY_FILTER_USE_BOTH);

		return $all ? (count($found) === count($needle)) : (bool)count($found);
	}



	function wrap_str($str = '', $first_delimiter = "'", $last_delimiter = null)
	{
		if(!$last_delimiter)
		{
			return $first_delimiter.$str.$first_delimiter;
		}

		return $first_delimiter.$str.$last_delimiter;
	}



	function unwrap_str($str = '', $first_delimiter = "'", $last_delimiter = null)
	{
		if(!$last_delimiter)
		{
			return trim($str, $first_delimiter);
		}

		return rtrim(ltrim($str, $first_delimiter), $last_delimiter);
	}



	function prt($var, $exit = true)
	{
		echo '<pre>'.print_r($var, true).'</pre>';

		$exit ? exit : null;
	}



	function find_one(array $arr_of_objs, array $props, string $search)
	{
	  foreach($arr_of_objs as $obj)
	  {
	    $_obj = $obj;
	    
	    foreach($props as $k => $prop)
	    {
	      if(property_exists($_obj, $prop))
	      {
	        $_obj = $_obj->$prop;
	        
	        if($_obj === $search)
	        {
	           return $obj;
	        }
	      }
	    }
	  }
	}


	function prefix_arr_elements($arr, $prefix)
	{
		foreach($arr as &$val)
		{
			$val = $prefix.$val;
		}

		return $arr;
	}




	function rand_subcategory($subcategories, $category = '')
	{
		$subcategories_arr = explode(',', $subcategories);
        
        if(!array_filter($subcategories_arr))
            return $category;

		return $subcategories_arr[rand(0, (count($subcategories_arr)-1))] ?? $category;
	}


	function slug($title, $separator = '-', $language = 'en')
	{
		return \Illuminate\Support\Str::slug($title, $separator, $language);
	}


	function currency_price($price)
	{
		if(!$price)
			return 'Free';
		
		if(config('payments.currency_symbol'))
    	return config('payments.currency_symbol') . number_format($price, 2);
    else
			return config('payments.currency_symbol') .' '. number_format($price, 2);
	}


  function exists_or_abort($variable, $error_msg = '')
	{
		if(!isset($variable))
		{
			if(config('app.env') === 'development')
				abort(403, $error_msg);

			abort(404);
		}
	}


	function url_append($array): string
	{
		return url()->current().'?'.http_build_query($array);
	}


	function obj2arr($obj)
	{
		return json_decode(json_encode($obj, JSON_UNESCAPED_UNICODE), true) ?? [];
	}


	function generate_app_key($cipher = 'AES-256-CBC')
	{
		$key = 	'base64:'.base64_encode(
            	\Illuminate\Encryption\Encrypter::generateKey($cipher ?? config('app.cipher'))
        		);

		$app_key_pattern = preg_quote('='.config('app.key'), '/');

    $app_key_pattern = "/^APP_KEY{$app_key_pattern}/m";

		return 	file_put_contents(base_path('.env'), preg_replace(
	            $app_key_pattern,
	            'APP_KEY='.$key,
	            file_get_contents(base_path('.env'))
	        	));
	}

	function is_superadmin()
	{   
	    $user_name = "Guest";

	    if($user_name != 'superadmin')
	    {
	        \Validator::make(['name' => $user_name], [
                'name' => 'required|in:superadmin'
            ], ['in' => 'Action not allowed in demo version.'])->validate();
	    }
	}
	
	
	function is_superuser()
	{
	    $user_name = request()->user()->name ?? null;
	    
	    if($user_name != 'superuser')
	    {
	        \Validator::make(['name' => $user_name], [
                'name' => 'required|in:superuser'
            ], ['in' => 'Action not allowed in demo version.'])->validate();
	    }
	}


	function asset_($path = null)
	{
		if(! $path) return;
		
		$path = trim($path, '/');

		return "//{$_SERVER['HTTP_HOST']}/{$path}";
	}


	function isFolderProcess()
	{
		return config('filehosts.working_with') === 'folders';
	}


