<?php

	namespace App\Libraries;


	class Stripe 
	{

		public function __construct()
		{
			exists_or_abort(config('payments.stripe.enabled'), 'Stripe is not enabled');
		}


		// Create checkout session
		public function create_checkout_session(array $cart = [], 
																					  array $discount)
		{
			/*
				API DOC URL : https://stripe.com/docs/api/checkout/sessions/create
				------------------------------------------------------------------
				curl https://api.stripe.com/v1/checkout/sessions \
				  -u sk_test_WlDtXCea4H8cKRFk7bgzW3xq00hcfmF73r: \
				  -d customer=cus_123 \
				  -d payment_method_types[]=card \
				  -d line_items[][name]=T-shirt \
				  -d line_items[][description]="Comfortable cotton t-shirt" \
				  -d line_items[][images][]="https://example.com/t-shirt.png" \
				  -d line_items[][amount]=500 \
				  -d line_items[][currency]=eur \
				  -d line_items[][quantity]=1 \
				  -d success_url="https://example.com/success" \
				  -d cancel_url="https://example.com/cancel"
				------------------------------------------------------------------
			*/

			$item_total = round(array_reduce($cart, function($carry, $prod)
                    {                                
                      return $carry + ($prod->quantity * $prod->price);
                    }, 0), 2);


      $tax_total =  round(config('payments.vat')
                          ? ($item_total - $discount['value']) * (config('payments.vat') / 100)
                          : 0, 2);


      $total_quantity = array_reduce($cart, function($carry, $prod)
		                    {                                
		                      return $carry + $prod->quantity;
		                    }, 0);

      $line_items 		= [];
      $items_names 		= [];
      $_cart 					= [];

      foreach($cart as $prod)
      {
      	if($prod->quantity > 1)
      	{
      		for($i=0; $i<$prod->quantity; $i++)
  				{	
  					array_push($_cart, $prod);
  				}
      	}
      	else
      	{
      		array_push($_cart, $prod);
      	}
      }

      foreach($_cart as &$prod)
      {
      	$items_names[] = $prod->name;
      	$quantity = $prod->quantity;
      	$amount 	= $prod->price;
      	$item 		= [
		                  'name' => $prod->name,
		                  'description' => $prod->category,
		                  'amount' => $amount*100,
		                  'currency' => config('payments.currency_code'),
		                  'quantity' => 1
		                ];

		    if($discount['value'])
		    {
		    	if(!isset($discount['product']))
		    	{
		    		$item['amount'] = round(($amount - ($discount['value']/$total_quantity))*100);
		    	}
		    	else
		    	{
		    		if($prod->id == $discount['product'])
        		{
        			if($quantity > 1 && in_array($prod->name, $items_names))
        			{
        				if(isset($prod->coupon_applied))
        				{
        					$item['amount'] = round($amount*100);
        				}
        				else
        				{
        					$prod->coupon_applied = true;
        					$item['amount'] = round(($amount - $discount['value'])*100);
        				}
        			}
        			else
        			{
        				$item['amount'] = round(($amount - $discount['value'])*100);
        			}
        		}
		    	}
		    }

		    $line_items[] = $item;
      }


		  $payload = [
		  	'payment_method_types' => ['card'],
		  	'success_url' => route('home.checkout.save', ['stripe_sess_id' => 'CHECKOUT_SESSION_ID']),
		  	'cancel_url' => route('home.checkout'),
		  	'mode' => 'payment',
		  	'payment_intent_data' => [
		  		'capture_method' => 'automatic'
		  	],
		  	'submit_type' => 'pay',
		  	'line_items' => $line_items
		  ];

		  if($tax_total)
		  {
		  	$payload['line_items'][] = [
		  		'name' => 'VAT',
		  		'description' => config('payments.vat').'%',
          'amount' => round($tax_total*100),
          'currency' => config('payments.currency_code'),
          'quantity' => 1
		  	];
		  }

		  if($handling = config('payments.stripe.fee'))
		  {
		  	$payload['line_items'][] = [
		  		'name' => 'Fee',
		  		'description' => 'Handling',
          'amount' => round($handling*100),
          'currency' => config('payments.currency_code'),
          'quantity' => 1
		  	];
		  }

		  $headers = [ 
				'Content-Type: application/x-www-form-urlencoded',
				'Authorization: Bearer ' . config('payments.stripe.secret_id'),
			];

			$ch = curl_init();

			$post_query = str_replace('CHECKOUT_SESSION_ID', 
																'{CHECKOUT_SESSION_ID}', 
																http_build_query($payload));

			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_query);
			curl_setopt($ch, CURLOPT_URL, 'https://api.stripe.com/v1/checkout/sessions');
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			$result = curl_exec($ch);

			curl_close($ch);

			return $result;
		}




		// Retrieve checkout session
		public function get_checkout_session(string $cs = '')
		{
			/*
				API DOC URL : https://stripe.com/docs/api/checkout/sessions/retrieve
				--------------------------------------------------------------------
				curl https://api.stripe.com/v1/checkout/sessions/cs_test_hnVLmJSlnLAeHlPXDMAh0gGbDEfjYEucRfIbMlKdeaHSZGHnE2mrCY4O \
					-u sk_test_WlDtXCea4H8cKRFk7bgzW3xq00hcfmF73r:
				--------------------------------------------------------------------
			*/

			$cs OR die();

			$ch = curl_init();

			 $headers = [
				'Content-Type: application/x-www-form-urlencoded',
				'Authorization: Bearer ' . config('payments.stripe.secret_id'),
			];

			$expand = http_build_query(['expand' => ['payment_intent']]);

			curl_setopt($ch, CURLOPT_URL, "https://api.stripe.com/v1/checkout/sessions/{$cs}?{$expand}");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			$result = curl_exec($ch);
			
			curl_close($ch);

			return $result;
		}



		// Retrieve paymeny intents
		public function get_payment_intents(string $pi_id = '')
		{
			/*
				API DOC URL : https://stripe.com/docs/api/payment_intents/retrieve
				--------------------------------------------------------------------
				curl https://api.stripe.com/v1/payment_intents/pi_1FE4IjHCAt5CXcX72ncX42q9 \
				  -u sk_test_WlDtXCea4H8cKRFk7bgzW3xq00hcfmF73r:
				--------------------------------------------------------------------
			*/

			$pi_id OR die();

			$ch = curl_init();

			 $headers = [
				'Content-Type: application/x-www-form-urlencoded',
				'Authorization: Bearer ' . config('payments.stripe.secret_id'),
			];

			curl_setopt($ch, CURLOPT_HTTPGET, 1);
			curl_setopt($ch, CURLOPT_URL, "https://api.stripe.com/v1/payment_intents/{$pi_id}");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			$result = curl_exec($ch);

			curl_close($ch);

			return $result;
		}



		// Retrieve customer
		public function get_customer($cus)
		{
			/*
				API DOC URL : https://stripe.com/docs/api/customers/retrieve
				--------------------------------------------------------------------
				curl https://api.stripe.com/v1/customers/cus_Flz46Wq3HGZFTJ \
				  -u sk_test_WlDtXCea4H8cKRFk7bgzW3xq00hcfmF73r:
				--------------------------------------------------------------------
			*/

			$cus OR die();

			$ch = curl_init();

			 $headers = [
				'Content-Type: application/x-www-form-urlencoded',
				'Authorization: Bearer ' . config('payments.stripe.secret_id'),
			];

			curl_setopt($ch, CURLOPT_HTTPGET, 1);
			curl_setopt($ch, CURLOPT_URL, "https://api.stripe.com/v1/customers/{$cus}");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			$result = curl_exec($ch);

			curl_close($ch);

			return $result;
		}




		// Create a charge 
		public function create_charge($stripeToken)
		{
			/*
				API DOC URL : https://stripe.com/docs/api/charges/create
				-----------------------------------------------------
				curl https://api.stripe.com/v1/charges \
				  -u sk_test_WlDtXCea4H8cKRFk7bgzW3xq00hcfmF73r: \
				  -d amount=2000 \
				  -d currency=eur \
				  -d source=tok_amex \
				  -d description="Charge for example@example.com"
				-----------------------------------------------------
			*/

			$coupon 	= json_decode($this->create_coupon(null, 9, 'once'));
			$customer = json_decode($this->create_customer($stripeToken, null, $coupon->id));

			$headers = [
				'Content-Type: application/x-www-form-urlencoded',
				'Authorization: Bearer ' . config('payments.stripe.secret_id')
			];

			$payload = [
				'amount' => 42.99*100,
				'currency' => 'USD',
				'description' => 'Charge for mr X',
				'customer' => $customer->id
			];


			$ch = curl_init();

			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payload));
			curl_setopt($ch, CURLOPT_URL, 'https://api.stripe.com/v1/charges');
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			$result = curl_exec($ch);

			curl_close($ch);
			
			return $result;
		}
	


		// Create a customer
		public function create_customer($source = null, 
																		$description = '', 
																		$coupon = null, 
																		$tax_id_data = [])
		{
			/*
				API DOC URL : https://stripe.com/docs/api/customers/create
				---------------------------------------------------------
				curl https://api.stripe.com/v1/customers \
				  -u sk_test_WlDtXCea4H8cKRFk7bgzW3xq00hcfmF73r: \
				  -d description="Customer for jenny.rosen@example.com" \
				  -d source=tok_amex
				  -d coupn=qsfsfqsf
				---------------------------------------------------------
			*/

			$payload = [];

			if($description)
				$payload['description'] = $description;

			if($source)
				$payload['source'] = $source;

			if($coupon)
				$payload['coupon'] = $coupon;

			if($tax_id_data)
				$payload['tax_id_data'] = $tax_id_data;

			$headers = [
				'Content-Type: application/x-www-form-urlencoded',
				'Authorization: Bearer ' . config('payments.stripe.secret_id')
			];

			$ch = curl_init();

			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_URL, 'https://api.stripe.com/v1/customers');

			if($payload)
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payload));
			
			$result = curl_exec($ch);

			curl_close($ch);
			
			return $result;
		}




		// Create a Tax
		public function create_tax(	$display_name, 
																$description = '', 
																$percentage, 
																$jurisdiction = '', 
																$inclusive = false)
		{
			/*
				API DOC URL : https://stripe.com/docs/api/tax_rates/create
				------------------------------------------------------
				curl https://api.stripe.com/v1/tax_rates \
				  -u sk_test_WlDtXCea4H8cKRFk7bgzW3xq00hcfmF73r: \
				  -d display_name=VAT \
				  -d description="VAT Germany" \
				  -d jurisdiction=DE \
				  -d percentage="19.0" \
				  -d inclusive=false
				------------------------------------------------------
			*/

			$payload = [
				'display_name' => $display_name,
				'description' => $description,
				'percentage' => $percentage,
				'jurisdiction' => $jurisdiction,
				'inclusive' => $inclusive
			];

			$headers = [
				'Content-Type: application/x-www-form-urlencoded',
				'Authorization: Bearer ' . config('payments.stripe.secret_id')
			];

			$ch = curl_init();

			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_URL, 'https://api.stripe.com/v1/customers');
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payload));
			
			$result = curl_exec($ch);

			curl_close($ch);
			
			return $result;
		}




		// Create a coupon
		public function create_coupon($amount_off = null, 
																	$percent_off = null,
																	$duration = 'once'
																)
		{
			/*
				API DOC URL : https://stripe.com/docs/api/coupons/create
				----------------------------------------------------
				curl https://api.stripe.com/v1/coupons \
				  -u sk_test_WlDtXCea4H8cKRFk7bgzW3xq00hcfmF73r: \
				  -d percent_off=5 \
				  -d duration=once
				----------------------------------------------------
			*/

			$payload = ['duration' => $duration];

			if($amount_off)
				$payload['amount_off'] = $amount_off;
			elseif($percent_off)
				$payload['percent_off'] = $percent_off;

			$headers = [
				'Content-Type: application/x-www-form-urlencoded',
				'Authorization: Bearer ' . config('payments.stripe.secret_id')
			];

			$ch = curl_init();

			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_URL, 'https://api.stripe.com/v1/coupons');
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payload));
			
			$result = curl_exec($ch);

			curl_close($ch);
			
			return $result;
		}




		// Create a card token
		public function create_card_token(array $card)
		{
			/*
				API DOC URL : https://stripe.com/docs/api/tokens/create_card
				------------------------------------------------------------
				curl https://api.stripe.com/v1/tokens \
				  -u sk_test_WlDtXCea4H8cKRFk7bgzW3xq00hcfmF73r: \
				  -d card[number]=4000002500003155 \
				  -d card[exp_month]=12 \
				  -d card[exp_year]=2050 \
				  -d card[cvc]=123
				------------------------------------------------------------
			*/

			$payload = [
				'number' => $card['number'] ?? null,
				'exp_month' => $card['exp_month'] ?? null,
				'exp_year' => $card['exp_year'] ?? null,
				'cvc' => $card['cvc'] ?? null
			];

			$headers = [
				'Content-Type: application/x-www-form-urlencoded',
				'Authorization: Bearer ' . config('payments.stripe.secret_id')
			];

			$ch = curl_init();

			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_URL, 'https://api.stripe.com/v1/tokens');
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payload));
			
			$result = curl_exec($ch);

			curl_close($ch);
			
			return json_decode($result)->id ?? null;
		}




		// Create payment intents
		public function create_payment_intents($stripeToken)
		{
			/*
				API DOC URL : https://stripe.com/docs/api/payment_intents/create
				----------------------------------------------------------------
				curl https://api.stripe.com/v1/payment_intents \
				  -u sk_test_WlDtXCea4H8cKRFk7bgzW3xq00hcfmF73r: \
				  -X POST \
				  -d amount=33.58 \
				  -d currency=usd \
				  -d payment_method_types[]=card
				----------------------------------------------------------------
			*/

			$coupon 	= json_decode($this->create_coupon(null, 5, 'once'));
			$customer = json_decode($this->create_customer($stripeToken, null, $coupon->id));

			$payload = [
				'amount' => 33.58*100,
				'currency' => 'usd',
				'confirm' => 'true',
				'return_url' => 'https://imarket.co/checkout',
				'customer' => $customer->id
			];

			$headers = [
				'Content-Type: application/x-www-form-urlencoded',
				'Authorization: Bearer ' . config('payments.stripe.secret_id')
			];

			$ch = curl_init();

			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_URL, 'https://api.stripe.com/v1/payment_intents');
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payload));
			
			$result = curl_exec($ch);

			curl_close($ch);

			$this->delete_customer($customer->id);
			
			return $result;
		}





		// Delete a customer
		public function delete_customer($customer_id)
		{
			/*
				API DOC URL : https://stripe.com/docs/api/customers/delete
				----------------------------------------------------------
				curl https://api.stripe.com/v1/customers/cus_FkJ3U2SRoptyhl \
				  -u sk_test_WlDtXCea4H8cKRFk7bgzW3xq00hcfmF73r: \
				  -X DELETE
				----------------------------------------------------------
			*/

			$headers = [
				'Authorization: Bearer ' . config('payments.stripe.secret_id')
			];

			$ch = curl_init();

			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_URL, "https://api.stripe.com/v1/{$customer_id}");
			
			$result = curl_exec($ch);

			curl_close($ch);
			
			return $result;
		}




		public function get_balance_transaction(string $txn)
		{
			/*
				API DOC URL : https://stripe.com/docs/api/balance/balance_retrieve
				------------------------------------------------------------------
				curl https://api.stripe.com/v1/balance_transactions/txn_1FJCmcHCAt5CXcX7aUqGNqx2 \
				  -u sk_test_WlDtXCea4H8cKRFk7bgzW3xq00hcfmF73r:
  			------------------------------------------------------------------
			*/
  		
  		$headers = [
				'Authorization: Bearer ' . config('payments.stripe.secret_id')
			];

			$ch = curl_init();

			curl_setopt($ch, CURLOPT_HTTPGET, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_URL, "https://api.stripe.com/v1/balance_transactions/{$txn}");
			
			$result = curl_exec($ch);

			curl_close($ch);
			
			return $result;
		}



		public function refund_transaction(string $charge, float $amount = 0)
		{
			/*
				API DOC URL : https://stripe.com/docs/api/refunds/create
				------------------------------------------------------------------
				curl https://api.stripe.com/v1/refunds \
				  -u sk_test_WlDtXCea4H8cKRFk7bgzW3xq00hcfmF73r: \
				  -d charge=ch_1FDZiGHCAt5CXcX7YYow7zS4
  			------------------------------------------------------------------
			*/
  		
  		$headers = [
  			'Content-Type: application/x-www-form-urlencoded',
				'Authorization: Bearer ' . config('payments.stripe.secret_id')
			];

			$payload = ['charge' => $charge];

			if($amount)
				$payload['amount'] = ceil($amount*100);


			$ch = curl_init();

			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payload));
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_URL, "https://api.stripe.com/v1/refunds");
			
			$result = curl_exec($ch);

			curl_close($ch);
			
			return $result;
		}
	}

	