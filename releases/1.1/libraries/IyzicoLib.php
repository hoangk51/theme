<?php

	namespace App\Libraries;

  use Illuminate\Http\Request;
  use App\User;
	use Illuminate\Support\Facades\{DB, Cache, Session};
  use Iyzipay;
  use Iyzipay\Model\{PaymentGroup, Currency, Locale, Buyer, Address, BasketItem, BasketItemType, CheckoutFormInitialize};

  require_once(app_path('Libraries/iyzipay-php-2.0.46/IyzipayBootstrap.php'));

  \IyzipayBootstrap::init();

	class IyzicoLib
	{
    public static function getOptions()
    {
      $options = new Iyzipay\Options();

      $options->setApiKey(config('payments.iyzico.client_id'));
      $options->setSecretKey(config('payments.iyzico.secret_id'));
      
      if(config('payments.iyzico.mode') === 'sandbox')
      {
        $options->setBaseUrl("https://sandbox-api.iyzipay.com");
      }
      else
      {
        $options->setBaseUrl("https://api.iyzipay.com");
      }

      return $options;
    }




    public static function init_payment(array $cart, array $discount, User $user, object $buyerInf)
    {
      exists_or_abort(config('payments.iyzico.enabled'), 'Iyzico is not enabled');


      $item_total = 0;
    
      $total_quantity = array_reduce($cart, function($carry, $prod)
		                    {                                
		                      return $carry + $prod->quantity;
		                    }, 0);


      $basketItems 		= [];
      $items_names 		= [];
      $_cart 					= [];

      foreach($cart as $prod)
      {
      	if($prod->quantity > 1)
      	{
      		for($i=0; $i<$prod->quantity; $i++)
  				{	
  					array_push($_cart, $prod);
  				}
      	}
      	else
      	{
      		array_push($_cart, $prod);
      	}
      }


      foreach($_cart as &$prod)
      {
      	$items_names[] = $prod->name;
        $quantity = $prod->quantity;
        $amount 	= $prod->price;
        $item     = Self::basketItem([
                      'id'        => $prod->id,
                      'name'      => $prod->name,
                      'category1' => $prod->category ?? 'Default',
                      'itemType'  => BasketItemType::VIRTUAL,
                      'price'     => round($amount, 2),
                    ]);

		    if($discount['value'])
		    {
		    	if(!isset($discount['product']))
		    	{
            $item->setPrice(round($amount - ($discount['value']/$total_quantity), 2));
		    	}
		    	else
		    	{
		    		if($prod->id == $discount['product'])
        		{
        			if($quantity > 1 && in_array($prod->name, $items_names))
        			{
        				if(isset($prod->coupon_applied))
        				{
                  $item->setPrice(round($amount, 2));
        				}
        				else
        				{
                  $prod->coupon_applied = true;
                  
                  $item->setPrice(round($amount - $discount['value'], 2));
        				}
        			}
        			else
        			{
                $item->setPrice(round($amount - $discount['value'], 2));
        			}
        		}
          }
        }
        
        $item_total += $item->getPrice();

		    $basketItems[] = $item;
      }


      $tax_total  =  round(config('payments.vat')
                          ? ($item_total - $discount['value']) * (config('payments.vat') / 100)
                          : 0, 2);


      if($handling_fee = config('payments.razorpay.fee'))
      {
        $basketItems[]  = Self::basketItem([
                            'id'        => "FEE1",
                            'name'      => __('Handling Fee'),
                            'category1' => 'Fee',
                            'itemType'  => BasketItemType::VIRTUAL,
                            'price'     => (double)$handling_fee
                          ]);

        $item_total += (double)$handling_fee;
      }
      

      if($tax_total)
		  {
        $basketItems[]  = Self::basketItem([
                            'id'        => "VAT".round(config('payments.vat')),
                            'name'      => __('VAT'),
                            'category1' => 'Tax',
                            'itemType'  => BasketItemType::VIRTUAL,
                            'price'     => (double)$tax_total
                          ]);

        $item_total += (double)$tax_total;
      }

      $request = new Iyzipay\Request\CreateCheckoutFormInitializeRequest();
      $request->setLocale(Locale::EN);
      $request->setConversationId($user->id . time());
      $request->setPrice($item_total);
      $request->setPaidPrice($item_total);
      $request->setCurrency(config('payments.currency_code'));
      $request->setBasketId("B".time());
      $request->setPaymentGroup(PaymentGroup::PRODUCT);
      $request->setCallbackUrl(route('home.checkout.iyzico_save'));
      $request->setEnabledInstallments([2, 3, 6, 9]);

      $buyer = new Buyer();
      $buyer->setId('B'.$user->id);
      $buyer->setName($buyerInf->firstname);
      $buyer->setSurname($buyerInf->lastname);
      $buyer->setEmail($buyerInf->email);
      $buyer->setIdentityNumber($buyerInf->id_number);
      $buyer->setRegistrationAddress($buyerInf->address);
      $buyer->setIp($buyerInf->ip_address);
      $buyer->setCity($buyerInf->city);
      $buyer->setCountry($buyerInf->country);

      $request->setBuyer($buyer);

      $billingAddress = new Address();
      $billingAddress->setContactName($buyerInf->firstname . $buyerInf->lastname);
      $billingAddress->setCity($buyerInf->city);
      $billingAddress->setCountry($buyerInf->country);
      $billingAddress->setAddress($buyerInf->ip_address);

      $request->setBillingAddress($billingAddress);

      $request->setBasketItems($basketItems);

      return CheckoutFormInitialize::create($request, Self::getOptions());
    }





    public static function validate_payment(string $token = null)
    {
      if(! $token) abort(404);

      $request = new \Iyzipay\Request\RetrieveCheckoutFormRequest();

      $request->setLocale(Locale::EN);

      $request->setToken($token);

      $checkoutForm = Iyzipay\Model\CheckoutForm::retrieve($request, Self::getOptions());

      if($checkoutForm->getPaymentStatus() !== 'SUCCESS')
        abort(403, $checkoutForm->getErrorMessage() ?? 'Error');

      return $checkoutForm;
    }


    

    public static function basketItem($attributes)
    {
      extract($attributes);

      $basketItem = new BasketItem();

      $basketItem->setId($id ?? null);
      $basketItem->setPrice($price ?? null);
      $basketItem->setName($name ?? null);
      $basketItem->setCategory1($category1 ?? null);
      $basketItem->setCategory2($category2 ?? null);
      $basketItem->setItemType($itemType ?? null);
      $basketItem->setSubMerchantKey($merchantKey ?? null);
      $basketItem->setSubMerchantPrice($MerchantPrice ?? null);

      return $basketItem;
    }



    public static function getPaymentRequest($paymentId)
    {
      $request = new \Iyzipay\Request\RetrievePaymentRequest();

      $request->setLocale(Locale::EN);
      $request->setPaymentId($paymentId);

      return \Iyzipay\Model\Payment::retrieve($request, Self::getOptions());
    }

  }

