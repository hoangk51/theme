<?php
	
	define('VALEXA_UPGRADING', true);

	require_once('../bootstrap.php');


	if(!is_null(env('APP_VERSION')) && env('APP_VERSION') > 1.1)
		abort(403, __('Trying to upgrade to a lower version.'));

	$replacements = [
		'helpers.php' => 'app/helpers.php',
		'Kernel.php' => 'app/Http/Kernel.php',

		// Assets
		'assets/paypal_icon.png' => 'public/assets/images/paypal_icon.png',
		'assets/skrill_icon.png' => 'public/assets/images/skrill_icon.png',
		'assets/razorpay_icon.png' => 'public/assets/images/razorpay_icon.png',
		'assets/iyzico_icon.png' => 'public/assets/images/iyzico_icon.png',
		'assets/iyzico_logo.svg' => 'public/assets/images/iyzico_logo.svg',
		'assets/folder.png' => 'public/assets/images/folder.png',
		'assets/back/app.css' => 'public/assets/admin/css/app.css',
		'assets/back/app.js' => 'public/assets/admin/js/app.js',
		'assets/front/app.css' => 'public/assets/default/css/app.css',
		'assets/front/app.js' => 'public/assets/default/js/app.js',

		// Config
		'config/mimetypes.php' => 'config/mimetypes.php',

		// Controllers
		'controllers/CheckoutController.php' => 'app/Http/Controllers/CheckoutController.php',
		'controllers/HomeController.php' => 'app/Http/Controllers/HomeController.php',
		'controllers/Back/ProductsController.php' => 'app/Http/Controllers/Back/ProductsController.php',
		'controllers/Back/PagesController.php' => 'app/Http/Controllers/Back/PagesController.php',
		'controllers/Back/PostsController.php' => 'app/Http/Controllers/Back/PostsController.php',
		'controllers/Back/SettingsController.php' => 'app/Http/Controllers/Back/SettingsController.php',
		'controllers/Back/TransactionsController.php' => 'app/Http/Controllers/Back/TransactionsController.php',
		'controllers/Auth/LoginController.php' => 'app/Http/Controllers/Auth/LoginController.php',


		// Libraries
		'libraries/DropBox.php' => 'app/Libraries/DropBox.php',
		'libraries/GoogleDrive.php' => 'app/Libraries/GoogleDrive.php',
		'libraries/IyzicoLib.php' => 'app/Libraries/IyzicoLib.php',
		'libraries/Razorpay.php' => 'app/Libraries/Razorpay.php',
		'libraries/Sitemap.php' => 'app/Libraries/Sitemap.php',
		'libraries/Skrill.php' => 'app/Libraries/Skrill.php',
		

		// Middlewares
		'middleware/ValidateListFolder.php' => 'app/Http/Middleware/ValidateListFolder.php',
		'middleware/VerifyCsrfToken.php' => 'app/Http/Middleware/VerifyCsrfToken.php',
		'middleware/PaymentMethodValidation.php' => 'app/Http/Middleware/PaymentMethodValidation.php',

		// Migrations
		'migrations/2019_08_12_003219_create_products_table.php' => 'database/migrations/2019_08_12_003219_create_products_table.php',
		'migrations/2019_11_01_151956_create_settings_table.php' => 'database/migrations/2019_11_01_151956_create_settings_table.php',
		'migrations/2020_02_23_233443_create_skrill_transactions_table.php' => 'database/migrations/2020_02_23_233443_create_skrill_transactions_table.php',

		// Models
		'models/Product.php' => 'app/Models/Product.php',
		'models/Skrill_Transaction.php' => 'app/Models/Skrill_Transaction.php',

		// Resources - Assets
		'resources/js/back.js' => 'resources/js/back.js',
		'resources/js/front.js' => 'resources/js/front.js',
		'resources/sass/back.scss' => 'resources/sass/back.scss',
		'resources/sass/front.scss' => 'resources/sass/front.scss',

		// Resources - Views
		'resources/views/auth/login.blade.php' => 'resources/views/auth/login.blade.php',

		'resources/views/back/posts/index.blade.php' => 'resources/views/back/posts/index.blade.php',
		'resources/views/back/products/create_with_folders.blade.php' => 'resources/views/back/products/create_with_folders.blade.php',
		'resources/views/back/products/edit_with_folders.blade.php' => 'resources/views/back/products/edit_with_folders.blade.php',
		'resources/views/back/products/edit.blade.php' => 'resources/views/back/products/edit.blade.php',
		'resources/views/back/products/index.blade.php' => 'resources/views/back/products/index.blade.php',
		'resources/views/back/settings/files_host.blade.php' => 'resources/views/back/settings/files_host.blade.php',
		'resources/views/back/settings/payments.blade.php' => 'resources/views/back/settings/payments.blade.php',
		'resources/views/back/settings/mailer.blade.php' => 'resources/views/back/settings/mailer.blade.php',
		'resources/views/back/transactions/skrill.blade.php' => 'resources/views/back/transactions/skrill.blade.php',
		'resources/views/back/transactions/iyzico.blade.php' => 'resources/views/back/transactions/iyzico.blade.php',
		'resources/views/back/transactions/iyzico_refund.blade.php' => 'resources/views/back/transactions/iyzico_refund.blade.php',
		'resources/views/back/transactions/razorpay.blade.php' => 'resources/views/back/transactions/razorpay.blade.php',
		'resources/views/back/transactions/index.blade.php' => 'resources/views/back/transactions/index.blade.php',
		
		'resources/views/front/partials/footer.blade.php' => 'resources/views/front/default/partials/footer.blade.php',
		'resources/views/front/partials/top_menu.blade.php' => 'resources/views/front/default/partials/top_menu.blade.php',

		'resources/views/front/user/downloads.blade.php' => 'resources/views/front/default/user/downloads.blade.php',
		'resources/views/front/user/product_folder.blade.php' => 'resources/views/front/default/user/product_folder.blade.php',

		'resources/views/front/blog.blade.php' => 'resources/views/front/default/blog.blade.php',
		'resources/views/front/checkout.blade.php' => 'resources/views/front/default/checkout.blade.php',
		'resources/views/front/checkout_iyzico.blade.php' => 'resources/views/front/default/checkout_iyzico.blade.php',
		'resources/views/front/home.blade.php' => 'resources/views/front/default/home.blade.php',
		'resources/views/front/master.blade.php' => 'resources/views/front/default/master.blade.php',
		'resources/views/front/post.blade.php' => 'resources/views/front/default/post.blade.php',
		'resources/views/front/product.blade.php' => 'resources/views/front/default/product.blade.php',

		'resources/views/mail/html/message.blade.php' => 'resources/views/vendor/mail/html/message.blade.php',
		
		// Routes
		'routes/web.php' => 'routes/web.php',

		// Listeners
		'listeners/TransactionCompleteNotification.php' => 'app/Listeners/TransactionCompleteNotification.php',
	];

	if(file_exists(base_path('bootstrap/cache/routes.php')))
	{
		unlink(base_path('bootstrap/cache/routes.php'));
	}

	foreach($replacements as $source => $destination)
	{
		copy(release_path($source), base_path($destination));
	}
	
	if(! \DB::select("SHOW COLUMNS FROM products LIKE 'is_dir'"))
	{
		\DB::statement("ALTER TABLE products ADD COLUMN is_dir TINYINT(1) DEFAULT 0");
		\DB::statement("ALTER TABLE products ADD INDEX is_dir (is_dir)");
	}

	/** SETTING .ENV VARS STARTS **/
  $env =  array_reduce(file(base_path('.env'), FILE_IGNORE_NEW_LINES|FILE_SKIP_EMPTY_LINES), 
          function($carry, $item)
          {
            list($key, $val) = explode('=', $item, 2);

            $carry[$key] = $val;

            return $carry;
          }, []);

  $env['APP_VERSION'] = 1.1;

  foreach($env as $k => &$v)
    $v = "{$k}={$v}";

  file_put_contents(base_path('.env'), implode("\r\n", $env));
	/** SETTING .ENV VARS ENDS **/


	$settings = \App\Models\Setting::select('files_host')->first()->files_host;

	$settings = json_decode($settings) ?? (object)[];

	\App\Models\Setting::first()->update(['files_host' => json_encode([
													    "google_drive"  => $settings->google_drive,
													    "dropbox"       => $settings->dropbox,
													    "working_with"  => 'files'
													  ])]);

	\DB::update("UPDATE transactions SET transaction_id = id WHERE transaction_id IS NULL || transaction_id = ''");

	if(! \DB::select("SHOW INDEXES FROM transactions WHERE key_name = 'processor_2'"))
  {
		\DB::statement("ALTER TABLE transactions add UNIQUE processor_2 (transaction_id, processor)");
	}

	\DB::statement("CREATE TABLE IF NOT EXISTS skrill_transactions (
		id INT(11) AUTO_INCREMENT,
		transaction_id VARCHAR(255) NOT NULL,
		country VARCHAR(255) DEFAULT NULL,
		amount FLOAT NOT NULL,
		firstname VARCHAR(255) DEFAULT NULL,
		lastname VARCHAR(255) DEFAULT NULL,
		IP_country VARCHAR(255) DEFAULT NULL,
		md5sig VARCHAR(255) NOT NULL,
		merchant_id VARCHAR(255) NOT NULL,
		payment_type VARCHAR(255) NOT NULL,
		pay_from_email VARCHAR(255) NOT NULL,
		pay_to_email VARCHAR(255) NOT NULL,
		currency VARCHAR(255) NOT NULL,
		customer_id VARCHAR(255) NOT NULL,
		user_id INT(11) NOT NULL,
		payment_instrument_country VARCHAR(255) NOT NULL,
		PRIMARY KEY id (id), 
		UNIQUE transaction_id (transaction_id)
	) ENGINE=innodb DEFAULT charset=utf8 COLLATE=utf8_unicode_ci");


	if(! file_exists(app_path('Libraries/iyzipay-php-2.0.46')))
	{
		\File::copyDirectory(release_path('libraries/iyzipay-php-2.0.46'), app_path('Libraries/iyzipay-php-2.0.46'));
	}


	header("location: /releases");