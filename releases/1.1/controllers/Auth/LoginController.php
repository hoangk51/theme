<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\HomeBaseController;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use DB;
use Auth;
use Session;


class LoginController extends HomeBaseController
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/';



    public function redirectTo()
    {
        if(Auth::user()->role === 'admin')
        {
          return '/admin';
        }

        return request()->redirect ?? '/';
    }

    

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware('guest')->except('logout');
    }



    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    { 
        $this->meta_data->url = route('login');
        $this->meta_data->title = 'Login';
        
        return view('auth.login', ['meta_data' => $this->meta_data]);
    }




    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    /*protected function authenticated(Request $request, $user)
    {
      $this->redirectTo = $redirectTo;
    }*/



    /**
     * Redirect the user to the authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider)
    {   
        return  Socialite::driver($provider)->redirect();
    }



    /**
     * Obtain the user information from the provider.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {
      $userSocial = Socialite::driver($provider)->user();

      if($userSocial->accessTokenResponseBody ?? null)
      {
        $userSocial->email = $userSocial->accessTokenResponseBody['email'];
      }

      $user = \App\User::where(['email' => $userSocial->email])->first();

      if(!$user)
      {
          $user = new \App\User;

          $user->name               = $userSocial->name ?? explode('@', $userSocial->email)[0];
          $user->email              = $userSocial->email;
          $user->provider_id        = $userSocial->id;
          $user->provider           = $provider;
          $user->email_verified_at  = date('Y-m-d H:i:s');
          $user->avatar             = $this->copy_avatar($userSocial);

          $user->save();
      }
      else
      {
        if((string)$userSocial->id !== (string)$user->provider_id)
        {
          $user->firstname    = $userSocial->firstname ?? null;
          $user->lastname     = $userSocial->lastname ?? null;
          $user->name         = $userSocial->name ?? null;
          $user->provider_id  = $userSocial->id;
          $user->provider     = $provider;
          $user->avatar       = $this->copy_avatar($userSocial, true, $user);

          $user->save();
        }
      }

      Auth::login($user, true);

      return redirect($this->redirectTo());
    }



    private function copy_avatar($userSocial, $update = false, $user = null)
    {
        if(!isset($userSocial->avatar))
            return;

        if(!$update)
        {
          $avatar_id =  DB::select("SHOW TABLE STATUS LIKE 'users'")[0]
                        ->Auto_increment;
        }
        else
        {
          if(!$user)
            exit('User id is missing');

          @unlink(storage_path("app/public/avatars/{$user->avatar}"));

          $avatar_id = $user->id; 
        }

        $avatar = file_get_contents($userSocial->avatar);

        $avatar_mime = (new \finfo(FILEINFO_MIME))->buffer($avatar);

        preg_match('/image\/(?P<type>\w+);.+/', $avatar_mime, $matches);

        $file_name = "{$avatar_id}.{$matches['type']}";

        if(copy($userSocial->avatar, storage_path("app/public/avatars/{$file_name}")))
        {
            return $file_name;
        }
    }



    /**
     * The user has logged out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    protected function loggedOut(Request $request)
    {
      return redirect($request->redirect ?? '/');
    }
}
