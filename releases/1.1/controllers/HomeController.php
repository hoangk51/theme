<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\DB;
use App\Libraries\{DropBox, GoogleDrive, IyzicoLib};


class HomeController extends HomeBaseController
{
    private static $product_columns = ['products.id', 'products.name', 'products.views', 'products.preview',
                                       'products.slug', 'products.updated_at', 'products.active',
                                       'products.cover', 'products.price', 'products.last_update', 
                                       'categories.name as category_name', 'categories.slug as category_slug',
                                       'GROUP_CONCAT(subcategories.name) AS subcategories'];


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      Parent::__construct();
    }



    public function index()
    {
        $posts = \App\Models\Post::useIndex('primary')->where('active', 1)->orderBy('id', 'DESC')->limit(4)->get();
        
        Self::init_notifications();

        $trending_products  = $this->trending_products(false, 8);
        
        $featured_products  = $this->featured_products(false, 8);

        $free_products      = $this->free_products(false, 8);

        $newest_products    = $this->newest_products(false, 8);

        $meta_data          = $this->meta_data;

        $this->set_home_categories(35);

        return view('front.default.home', compact('posts', 'trending_products', 'newest_products',
                                          'featured_products', 'free_products', 'meta_data'));
    }

    

    // Single page
    public function page($slug)
    {
        if(!$page = \App\Models\Page::useIndex('slug', 'active')->where(['slug' => $slug, 'active' => 1])->first())
          abort(404);

        Self::init_notifications();

        $page->setTable('pages')->increment('views', 1);

        $meta_data = $this->meta_data;

        $meta_data->title = $page->name;
        $meta_data->description = $page->short_description;
        $meta_data->url = route('home.page', $page->slug);

        return view('front.default.page',compact('page', 'meta_data'));
    }


    // Products per category
    public function products(Request $request)
    {
      $categories         = config('categories.category_parents', []);
      $subcategories      = config('categories.category_children', []);
      $category           = $active_category = $active_subcategory = (object)[];
      $meta_data          = $this->meta_data;

      $query_rule = ['ob' => ['required_with:o', 'regex:/^(release_date|price|popularity|rating)$/i'], 
                     'o'  => ['required_with:ob', 'regex:/^(asc|desc)$/i']];

      if(\Validator::make($request->query(), $query_rule)->fails())
        abort(404);

      Self::init_notifications();

      $orderBy  = $request->query('ob') ?? 'updated_at';
      $order    = $request->query('o') ?? 'desc';
      $orderRev = $order === 'asc' ? 'desc' : 'asc';

      if($orderBy === 'popularity') $orderBy = 'products.trending';
      elseif($orderBy === 'rating') $orderBy = 'rating';
      else $orderBy = "products.{$orderBy}";

      $sort = ['release_date' => ['ob' => 'release_date', 'o' => $orderRev],
               'price'        => ['ob' => 'price', 'o' => $orderRev],
               'popularity'   => ['ob' => 'popularity', 'o' => $orderRev],
               'rating'       => ['ob' => 'rating', 'o' => $orderRev]];

      if($request->category_slug)
      {
        $category_slug = $request->category_slug;

        $active_category =  array_filter($categories, function($category) use ($category_slug)
                            {
                              return $category->slug === strtolower($category_slug);
                            });

        if(!$active_category) abort(404);

        $active_category = array_shift($active_category);

        if($subcategory_slug = $request->subcategory_slug)
        {
          if(!isset($subcategories[$active_category->id])) abort(404);

          $active_subcategory =   array_filter($subcategories[$active_category->id], 
                                  function($subcategory) use ($subcategory_slug)
                                  {
                                    return $subcategory->slug === strtolower($subcategory_slug);
                                  });

          if(!$active_subcategory = array_shift($active_subcategory)) abort(404);  
        }

        if(!$subcategory_slug)
        {
          $category->name        = $active_category->name;
          $category->description = \App\Models\Category::useIndex('primary')->select('description')
                                                        ->where('id', $active_category->id)->first()->description;

          $products = Product::useIndex('category', 'active')
                                        ->selectRaw(implode(',', Self::$product_columns).', AVG(reviews.rating) AS rating')
                                        ->leftJoin('categories', 'categories.id', '=', 'products.category')
                                        ->leftJoin('categories as subcategories', function($join) use ($active_category)
                                        {
                                          $join->on('products.subcategories', 
                                                    'REGEXP', DB::raw('concat("\'", subcategories.id, "\'")'))
                                               ->where('subcategories.parent', '=', $active_category->id);
                                        })
                                        ->leftJoin('reviews', 'reviews.product_id', '=', 'products.id')
                                        ->where(['category' => $active_category->id, 'active' => 1])
                                        ->groupBy('products.id', 'products.name', 'products.views', 'products.preview',
                                                   'products.slug', 'products.updated_at', 'products.active',
                                                   'products.cover', 'products.price', 'products.last_update', 
                                                   'categories.name', 'categories.slug', 'rating')
                                        ->orderBy($orderBy, $order);
       
          $meta_data->url = route('home.products.category', ['category_slug' => $category_slug]);
        }
        else
        {
          $category->name        = $active_subcategory->name;
          $category->description = \App\Models\Category::useIndex('primary')->select('description')
                                                        ->where('id', $active_subcategory->id)->first()->description;

          $products = Product::useIndex('category', 'subcategories', 'active')
                                ->selectRaw(implode(',', Self::$product_columns).', AVG(reviews.rating) AS rating')
                                ->leftJoin('categories', 'categories.id', '=', 'products.category')
                                ->leftJoin('categories as subcategories', function($join) use ($active_category)
                                {
                                  $join->on('products.subcategories', 
                                            'REGEXP', DB::raw('concat("\'", subcategories.id, "\'")'))
                                       ->where('subcategories.parent', '=', $active_category->id);
                                })
                                ->leftJoin('reviews', 'reviews.product_id', '=', 'products.id')
                                ->where(['category' => $active_category->id, 'active' => 1])
                                ->whereRaw("subcategories LIKE '%{$active_subcategory->id}%'")
                                ->groupBy('products.id', 'products.name', 'products.views', 'products.preview',
                                                   'products.slug', 'products.updated_at', 'products.active',
                                                   'products.cover', 'products.price', 'products.last_update', 
                                                   'categories.name', 'categories.slug', 'rating')
                                ->orderBy($orderBy, $order);

          $meta_data->url = route('home.products.category', ['category_slug'    => $category_slug, 
                                                             'subcategory_slug' => $subcategory_slug]);
        }

        $meta_data->title       = config('app.name').' - '.$category->name;
        $meta_data->description = $category->description;
      }
      elseif($request->selection)
      {
        $products = call_user_func("Self::{$request->selection}_products", true);

        $meta_data->title       = config('app.name').' - '.ucfirst($request->selection);
        $meta_data->description = config('app.description');
        $meta_data->url         = route('home.products.selection', $request->selection);
      }
      elseif($request->q)
      {
        $q = $request->q; 

        $products = Product::useIndex('description', 'active')
                            ->selectRaw(implode(',', Self::$product_columns).', products.short_description, 
                              AVG(reviews.rating) AS rating')
                            ->leftJoin('transactions', 'products_ids', 'REGEXP', DB::raw('concat("\'", products.id, "\'")'))
                            ->leftJoin('categories', 'categories.id', '=', 'products.category')
                            ->leftJoin('categories as subcategories', 'products.subcategories', 'REGEXP', DB::raw('concat("\'", subcategories.id, "\'")'))
                            ->leftJoin('reviews', 'reviews.product_id', '=', 'products.id')
                            ->groupBy('products.id', 'products.name', 'products.views', 'products.preview',
                                       'products.slug', 'products.updated_at', 'products.active',
                                       'products.cover', 'products.price', 'products.last_update', 
                                       'categories.name', 'categories.slug', 'rating', 'products.short_description')
                            ->whereRaw("active = 1 AND (
                                products.name LIKE ? OR products.slug LIKE ? OR 
                                products.short_description LIKE ? OR products.overview LIKE ? OR
                                products.features LIKE ? OR products.tags LIKE ?
                              )", ["%{$q}%", "%{$q}%", "%{$q}%", "%{$q}%", "%{$q}%", "%{$q}%"])
                            ->orderBy($orderBy, $order);

        foreach($sort as &$v)
        {
          $v['q'] = $q;
          krsort($v);
        }

        $meta_data->title       = config('app.name').' - Searching for '.ucfirst($request->q);
        $meta_data->description = config('app.description');
        $meta_data->url         = route('home.products.q').'?q='.$request->q;
      }
      else
      {
        abort(404);
      }

      $products = $request->selection === 'newest'
                  ? $products->limit(16)->get()
                  : $products->paginate(config('app.items_per_page', 12));

      $random_products = Product::useIndex('primary')->select('name', 'slug', 'thumbnail', 'price')
                                ->where('active', 1)
                                ->orderByRaw('rand()')
                                ->limit(10)->get();

      $sort = json_decode(json_encode($sort));

      return view('front.default.products', compact('active_category', 
                                                    'active_subcategory',
                                                    'products',
                                                    'sort',
                                                    'random_products',
                                                    'category',
                                                    'meta_data'));
    }



    // Single product
    public function product(Request $request)
    {
      $user_id = request()->user()->id ?? 'null';

      $product = Product::by_slug($request->slug, $user_id);

      if(!$product) abort(404);

      $meta_data  = $this->meta_data;

      $product->is_free = $product->free_temporary || $product->free_lifetime;
      
      if($request->method() === 'POST')
      {
        $type = $request->input('type');

        $approved = auth_is_admin() ? 1 : 0;

        if($type === 'reviews')
        {
          if(!$product->purchased) abort(404);

          $rating  = $request->input('rating');
          $review  = $request->input('review');

          if(!ctype_digit($rating)) return redirect()->route('home.product', $request->slug.'#'.$request->input('type'));

          DB::insert("INSERT INTO reviews (product_id, user_id, rating, content, approved) VALUES (?, ?, ?, ?, ?) 
                      ON DUPLICATE KEY UPDATE rating = ?, content = ?", 
                      [$product->id, $user_id, $rating, $review, $approved, $rating, $review]);

          if(!auth_is_admin())
            $request->session()->put(['review_response' => 'Your review is waiting for approval. Thank you!']);
        }
        elseif($type === 'support')
        {
          if(!$comment = $request->input('comment')) return redirect()->route('home.product', $request->slug.'#'.$request->input('type'));

          $comment = strip_tags($comment);

          if($request->comment_id) // parent
          {
            DB::insert("INSERT INTO comments (product_id, user_id, body, approved, parent) SELECT ?, ?, ?, ?, ? 
                        WHERE (SELECT COUNT(*) FROM comments WHERE comments.id = ? 
                               AND comments.parent IS NULL AND comments.product_id = ?) > 0", 
                        [$product->id, $user_id, $comment, $approved, $request->comment_id,  $request->comment_id, $product->id]);
          }
          else
          {
            DB::insert("INSERT INTO comments (product_id, user_id, body, approved) VALUES (?, ?, ?, ?)", 
                        [$product->id, $user_id, $comment, $approved]); 
          }

          if(!auth_is_admin())
            $request->session()->put(['comment_response' => 'Your comment is waiting for approval. Thank you!']);
        }

        return redirect()->route('home.product', $request->slug.'#'.$request->input('type'));
      }

      Self::init_notifications();

      DB::update('UPDATE products USE INDEX(primary) SET views = views+1 WHERE id = ?', [$product->id]);

      $reviews = \App\Models\Review::useIndex('product_id', 'approved')
                          ->selectRaw("reviews.*, users.name, SUBSTR(users.email, 1, LOCATE('@', users.email)-1) as alias_name, CONCAT(users.firstname, ' ', users.lastname) AS fullname, IFNULL(users.avatar, 'default.jpg') AS avatar")
                          ->leftJoin('users', 'users.id', '=', 'reviews.user_id')
                          ->where(['reviews.product_id' => $product->id, 'reviews.approved' => 1])
                          ->orderBy('created_at', 'DESC')->get();

      $comments = \App\Models\Comment::useIndex('product_id', 'approved')
                          ->selectRaw("comments.*, users.name, SUBSTR(users.email, 1, LOCATE('@', users.email)-1) as alias_name, CONCAT(users.firstname, ' ', users.lastname) AS fullname, IFNULL(users.avatar, 'default.jpg') AS avatar, IF(users.role = 'admin', 1, 0) as is_admin, 
                            IF((SELECT COUNT(transactions.id) FROM transactions WHERE transactions.user_id = comments.user_id AND transactions.products_ids REGEXP CONCAT('\'', comments.product_id, '\'')) > 0, 1, 0) as item_purchased")
                          ->leftJoin('users', 'users.id', '=', 'comments.user_id')
                          ->where(['comments.product_id' => $product->id, 'comments.approved' => 1])
                          ->orderBy('id', 'ASC')->get();


      $similar_products = Product::useIndex('primary', 'category', 'active')
                                  ->selectRaw(implode(',', Self::$product_columns))
                                  ->leftJoin('categories', 'categories.id', '=', 'products.category')
                                  ->leftJoin('categories as subcategories', 'products.subcategories', 'REGEXP', DB::raw('CONCAT("\'", subcategories.id, "\'")'))
                                  ->where(['products.category' => $product->category_id, 
                                           'active' => 1])
                                  ->where('products.id', '!=', $product->id)
                                  ->groupBy('products.id', 'products.name', 'products.views', 'products.preview',
                                           'products.slug', 'products.updated_at', 'products.active',
                                           'products.cover', 'products.price', 'products.last_update', 
                                           'category_name', 'category_slug')
                                  ->orderByRaw('rand()')
                                  ->limit(8)->get();

      if($parents = $comments->where('parent', null)->sortByDesc('id')) // parents comments only
      {
        $children = $comments->where('parent', '!=', null); // children comments only

        // Append children comments to their parents
        $parents->map(function (&$item, $key) use ($children)
        {
          $item->children = $children->where('parent', $item->id)->sortBy('created_at');
        });
      }
      

      if($product->screenshots)
        $product->screenshots = prefix_arr_elements(explode(',', $product->screenshots), 
                                                    asset("storage/screenshots\\"));

      if($product->free_temporary)
        $product->free_time = json_decode($product->free);

      $meta_data->title         = $product->name;
      $meta_data->description   = $product->short_description;
      $meta_data->image         = asset("storage/covers/{$product->cover}");

      return view('front.default.product', [
                  'title'     => ucfirst($product->name),
                  'product'   => $product,
                  'reviews'   => $reviews,
                  'comments'  => $parents, // Parents comments with their children in
                  'similar_products' => $similar_products,
                  'meta_data' => $meta_data
                ]);
    }




     // Preview product
     public function preview(Request $request)
     {
       $product = Product::useIndex('slug', 'active')->select('preview', 'slug')
                                                     ->where(['slug' => $request->slug, 'active' => 1])
                                                     ->first();
       if(!$product) abort(404);
 
       return view('front.default.preview', ['product' => $product]);
     }



    // Trending products
    public static function trending_products(bool $returnQueryBuilder, $limit = 15)
    {
      $products = Product::useIndex('trending', 'active')
                          ->selectRaw(implode(',', Self::$product_columns).', is_dir, products.trending, 
                            count(transactions.id) as sales, IFNULL((SELECT ROUND(AVG(rating)) FROM reviews WHERE product_id = products.id), 0) AS rating')
                          ->leftJoin('transactions', 'products_ids', 'REGEXP', DB::raw('concat("\'", products.id, "\'")'))
                          ->leftJoin('categories', 'categories.id', '=', 'products.category')
                          ->leftJoin('categories as subcategories', 'products.subcategories', 'REGEXP', DB::raw('concat("\'", subcategories.id, "\'")'))
                          ->groupBy('products.id', 'products.name','products.views','products.preview','products.slug','products.updated_at','products.active',
                                    'products.cover','products.price','products.last_update', 'categories.name', 'categories.slug', 'products.trending','is_dir')
                          ->havingRaw("active = 1 AND (trending = 1 OR sales > 0)")
                          ->orderByRaw('trending, sales DESC');

      return $returnQueryBuilder ? $products : $products->limit($limit)->get();                                
    }



    // Featured products
    private static function featured_products(bool $returnQueryBuilder, $limit = 15)
    {
      $products = Product::useIndex('featured', 'active')
                          ->selectRaw(implode(',', Self::$product_columns).', is_dir, products.featured, IFNULL((SELECT ROUND(AVG(rating)) FROM reviews WHERE product_id = products.id), 0) AS rating')
                          ->leftJoin('categories', 'categories.id', '=', 'products.category')
                          ->leftJoin('categories as subcategories', 'products.subcategories', 'REGEXP', DB::raw('concat("\'", subcategories.id, "\'")'))
                          ->where(['featured' => 1, 'active' => 1])
                          ->groupBy('products.id', 'products.name','products.views','products.preview','products.slug','products.updated_at','products.active',
                                    'products.cover','products.price','products.last_update', 'categories.name', 'categories.slug', 'products.featured', 'is_dir');
      
      return $returnQueryBuilder ? $products : $products->limit($limit)->get();   
    }



    // Newest products
    private static function newest_products(bool $returnQueryBuilder, $limit = 15)
    {
      $products = Product::useIndex('primary', 'active')
                          ->selectRaw(implode(',', Self::$product_columns).', is_dir, IFNULL((SELECT ROUND(AVG(rating)) FROM reviews WHERE product_id = products.id), 0) AS rating')
                          ->leftJoin('categories', 'categories.id', '=', 'products.category')
                          ->leftJoin('categories as subcategories', 'products.subcategories', 'REGEXP', DB::raw('concat("\'", subcategories.id, "\'")'))
                          ->where('active', 1)
                          ->groupBy('products.id', 'products.name','products.views','products.preview','products.slug','products.updated_at','products.active', 'is_dir', 'products.cover','products.price','products.last_update', 'categories.name', 'categories.slug')
                          ->orderBy('id', 'DESC');
      
      return $returnQueryBuilder ? $products : $products->limit($limit)->get();   
    }



    // Free products
    private static function free_products(bool $returnQueryBuilder, $limit = 15)
    {
      $products = Product::useIndex('price', 'free', 'active')
                          ->selectRaw(implode(',', Self::$product_columns).', is_dir, IFNULL((SELECT ROUND(AVG(rating)) FROM reviews WHERE product_id = products.id), 0) AS rating')
                          ->leftJoin('categories', 'categories.id', '=', 'products.category')
                          ->leftJoin('categories as subcategories', 'products.subcategories', 'REGEXP', DB::raw('concat("\'", subcategories.id, "\'")'))
                          ->where('active', 1)
                          ->where(function ($query)
                          {
                            $query->where('price', 0)
                                  ->orWhereRaw("CURRENT_DATE between substr(free, 10, 10) and substr(free, 28, 10)");
                          })
                          ->groupBy('products.id', 'products.name','products.views','products.preview','products.slug','products.updated_at','products.active', 'is_dir', 'products.cover','products.price','products.last_update', 'categories.name', 'categories.slug');

      return $returnQueryBuilder ? $products : $products->limit($limit)->get();   
    }




    // Blog 
    public function blog(Request $request)
    {
      $filter     = [];
      $meta_data  = $this->meta_data;

      $meta_data->description = config('app.blog.description');
      $meta_data->title       = config('app.blog.title');
      $meta_data->image       = asset('storage/images/'.(config('app.blog_cover') ?? 'blog_cover.jpg'));

      if($request->category)
      {
        if(!$category = \App\Models\Category::useIndex('slug')->where('slug', $request->category)->first())
          abort(404);

        $posts = \App\Models\Post::useIndex('category')->where(['category' => $category->id, 'active' => 1]);

        $filter = ['name' => 'Category', 'value' => $category->name];

        $meta_data->title       = config('app.name').' Blog - '.$category->name;
        $meta_data->description = $category->description;
        $meta_data->url         = route('home.blog.category', $category->slug);
      }
      elseif($request->tag)
      {
        $posts = \App\Models\Post::useIndex('tags')->where(function ($query) use ($request) {
                                                        $tag = str_replace('-', ' ', $request->tag);

                                                        $query->where('tags', 'LIKE', "%{$request->tag}%")
                                                              ->orWhere('tags', 'like', "%{$tag}%");
                                                   })
                                                   ->where('active', 1);

        $filter = ['name' => 'Tag', 'value' => $request->tag];

        $meta_data->title = config('app.name').' Blog - '.$request->tag;
        $meta_data->url   = route('home.blog.tag', $request->tag);
      }
      elseif($request->q)
      {
        $request->tag = str_replace('-', ' ', $request->tag);
        $posts = \App\Models\Post::useIndex('search', 'active')->where(function ($query) use ($request) {
                                                         $query->where('name', 'like', "%{$request->q}%")
                                                               ->orWhere('tags', 'like', "%{$request->q}%")
                                                               ->orWhere('short_description', 'like', "%{$request->q}%")
                                                               ->orWhere('content', 'like', "%{$request->q}%")
                                                               ->orWhere('slug', 'like', "%{$request->q}%");
                                                     })
                                                     ->where('active', 1);

        $filter = ['name' => 'Search', 'value' => $request->q];

        $meta_data->title = config('app.name').' Blog - Searching for '.$request->q;
        $meta_data->url   = route('home.blog.q', $request->q);
      }
      else
      {
        $posts = \App\Models\Post::useIndex('primary')->where('active', 1);

        $meta_data->url = route('home.blog');
      }

      $posts = $posts->paginate(config('app.items_per_page', 15));

      if($filter) settype($filter, 'object');

      $random_posts = \App\Models\Post::useIndex('primary')->where('active', 1)->orderByRaw('rand()')->limit(8)->get();

      $posts_categories = \App\Models\Category::useIndex('`for`')->select('name', 'slug')
                                                                 ->where('categories.for', 0)->get();

      Self::init_notifications();

      return view('front.default.blog', compact('posts_categories', 'random_posts', 'posts', 'filter', 'meta_data'));
    }



    // Blog Post
    public function post(string $slug)
    {
      $post = \App\Models\Post::useIndex('slug')->select('posts.*', 'categories.name AS category')
                                                ->leftJoin('categories', 'categories.id', '=', 'posts.category')
                                                ->where(['posts.slug' => $slug, 'posts.active' => 1])->first();

      if(!$post) abort(404);

      $meta_data  = $this->meta_data;

      $meta_data->description = $post->short_description;
      $meta_data->title       = $post->name;
      $meta_data->image       = asset('storage/posts/'.$post->cover);
      $meta_data->url         = route('home.blog', $post->slug);

      $post->setTable('posts')->increment('views', 1);

      $random_posts = \App\Models\Post::useIndex('primary')->where('id', '!=', $post->id)
                                                           ->orderByRaw('rand()')->limit(8)->get();

      $posts_categories = \App\Models\Category::useIndex('`for`')->select('name', 'slug')
                                                                 ->where('categories.for', 0)->get();

      Self::init_notifications();

      return view('front.default.post', compact('post', 'posts_categories', 'random_posts', 'meta_data'));
    }


    // Checkout
    public function checkout()
    {
      $meta_data = $this->meta_data;

      $meta_data->title = config('app.name').' - Checkout';
      $meta_data->url = route('home.checkout');
      $meta_data->description = 'Checkout';
      
      Self::init_notifications();

      return view('front.default.checkout', [
        'title' => 'Checkout',
        'meta_data' => $meta_data
      ]);
    }


    // Profile
    public function profile(Request $request)
    {
      $user = \App\User::find($request->user()->id);

      if($request->method() === 'POST')
      {        
        $user->name       = $request->input('name', $user->name);
        $user->firstname  = $request->input('firstname', $user->firstname);
        $user->lastname   = $request->input('lastname', $user->lastname);
        $user->country    = $request->input('country', $user->country);
        $user->city       = $request->input('city', $user->city);


        if($request->old_password && $request->new_password)
        {
          \Validator::make($request->all(), [
            'old_password' => [
              function ($attribute, $value, $fail) 
              {
                  if(! \Hash::check($value, auth()->user()->password)) {
                      $fail($attribute.' is incorrect.');
                  }
              }
            ],
          ])->validate();
        
          $user->password = \Illuminate\Support\Facades\Hash::make($request->new_password);
        }

        if($avatar = $request->file('avatar'))
        {          
          $request->validate(['avatar' => 'image']);

          $ext  = $avatar->extension();
          $file = $avatar->storeAs('public/avatars/', "{$user->id}.{$ext}");

          $user->avatar = str_ireplace('public/avatars/', '', $file);
        }

        $user->save();

        $request->session()->flash('profile_updated', 'Done!');

        return redirect()->route('home.profile');
      }

      $user = (object)$user->getAttributes();

      $user->fullname = null;

      if($user->firstname && $user->lastname)
      {
        $user->fullname = $user->firstname . ' ' . $user->lastname;
      }

      $meta_data = $this->meta_data;

      Self::init_notifications();

      return view('front.default.user.profile', compact('user', 'meta_data'));
    }


    // Downloads
    public function downloads(Request $request)
    {
      $products = DB::select('SELECT id, name, slug, cover, is_dir, thumbnail, file_host, file_name, products.last_update, 
                    (SELECT MAX(created_at) FROM transactions WHERE products_ids REGEXP CONCAT("\'", products.id, "\'") AND user_id = ?) as purchased_at, 
                    IFNULL((SELECT ROUND(AVG(rating)) FROM reviews WHERE reviews.product_id = products.id), 0) as rating FROM products
                    WHERE (SELECT GROUP_CONCAT(products_ids) FROM transactions 
                    WHERE user_id = ? AND is_dir = ? GROUP BY user_id, products.id, products.name, products.slug, products.cover, products.thumbnail, products.file_host, products.file_name, products.last_update, products.is_dir) REGEXP CONCAT("\'", id, "\'")', 
                    [$request->user()->id, $request->user()->id, isFolderProcess() ? 1 : 0]);
                    
      $meta_data = $this->meta_data;

      Self::init_notifications();

      return view("front.default.user.downloads", compact('products', 'meta_data'));
    }


    // Download
    public function download(Request $request)
    {
      set_time_limit(0);

      $request->validate(['itemId' => 'required|numeric']);

      $item = DB::select("SELECT slug, file_host, file_name, 
                          IF(price = 0 OR CURRENT_DATE between substr(free, 10, 10) and substr(free, 28, 10), 1, 0) AS is_free
                          FROM products USE INDEX(primary, price, active)
                          WHERE products.id = ? AND products.active = 1
                          GROUP BY products.id, products.slug, products.file_host, products.file_name, is_free", [$request->itemId])[0] ?? [];

      if(!$item) abort(404);

      if(!$item->is_free && !auth_is_admin())
      {
        $item_purchased = DB::table('transactions')
                              ->whereRaw('products_ids REGEXP ?', [wrap_str($request->itemId, "'")])
                              ->where('user_id', $request->user()->id)
                              ->exists();

        if(!$item_purchased) abort(404);
      }
      
      if(!$item->file_name) exit('File is not ready yet.');

      if($item->file_host === 'local')
      {
        $extension = pathinfo($item->file_name, PATHINFO_EXTENSION);
        
        if(file_exists(storage_path("app/downloads/{$item->file_name}")))
            return response()->download(storage_path("app/downloads/{$item->file_name}"), "{$item->slug}.{$extension}");
      }
      elseif($item->file_host === 'dropbox')
      {
        try
        {
          return DropBox::download($item->file_name, $item->slug);
        }
        catch(\Exception $e)
        {
          
        }
      }
      elseif($item->file_host === 'google')
      {
        try
        {
          return GoogleDrive::download($item->file_name, $item->slug);
        }
        catch(\Exception $e)
        {

        }
      }
    }



    // Favorites
    public function favorites(Request $request)
    {
        Self::init_notifications();

        return view('front.default.user.favorites', ['meta_data' => $this->meta_data]);
    }




    // Support
    public function support(Request $request)
    {
      if($request->method() === 'POST')
      {
        $request->validate([
          'email' => 'required|email|bail',
          'subject' => 'required|bail',
          'message' => 'required'
        ]);

        $user_email = $request->input('email');

        $email = \App\Models\Support_Email::insertIgnore(['email' => $user_email]);

        if(!($email->id ?? null))
        {
          $email = \App\Models\Support_Email::where('email', $user_email)->first();
        }

        $support = new \App\Models\Support();

        $support->email_id = $email->id;
        $support->subject  = strip_tags($request->input('subject'));
        $support->message  = strip_tags($request->input('message'));

        $support->save();

        if(config('mail.alt'))
        {
          \Mail::raw($support->message, function($message) use($support, $user_email)
          {
            $message->to(config('mail.alt'))->setSubject($support->subject)
                                            ->addFrom($user_email, explode('@', $user_email)[0])
                                            ->addReplyTo($user_email);
          });
        }

        $request->session()->flash('support_response', 'Message sent successfully !');

        return redirect()->route('home.support');
      }

      $faqs = \App\Models\Faq::useIndex('active')->where('active', 1)->get();

      $meta_data = $this->meta_data;

      $meta_data->url   = route('home.support');
      $meta_data->title = config('app.name').' - Support & FAQ';

      Self::init_notifications();

      return view('front.default.support', compact('faqs', 'meta_data'));
    }



    // List Product folder To Preview Its Files (POST)
    public function product_folder_async(Request $request)
    {
      if(!($item_slug = $request->input('slug')) || config('filehosts.working_with') !== 'folders') abort(404);

      $item = Product::useIndex('primary')->select('file_name', 'file_host')
                                          ->where('slug', $item_slug)->first() ?? abort(404);

      if($item->file_host === 'google')
      {
        $files_list = \App\Libraries\GoogleDrive::list_folder($item->file_name)->original['files_list'] ?? [];
      }
      elseif($item->file_host === 'dropbox')
      {
        $files_list = \App\Libraries\DropBox::list_folder($item->file_name)->original['files_list'] ?? [];
      }
      elseif($item->file_host === 'local')
      {
        $files      = glob(storage_path("app/downloads/{$item->file_name}/*"));
        $files_list = ['files' => []];

        foreach($files as &$file)
        {
          $files_list['files'][] = ['name' => \File::basename($file), 'mimeType' => \File::extension($file)];
        }
      }
      else
      {
        $files_list = [];
      }

      return response()->json($files_list);
    }



    // List Product folder To Download Its Files (GET)
    public function product_folder_sync(Request $request)
    {
      if(config('filehosts.working_with') !== 'folders') abort(404);

      $user_id = $request->user()->id;

      $product = Product::by_slug($request->slug, $user_id);

      if(!$product) abort(404);

      $product->is_free = $product->free_temporary || $product->free_lifetime;

      if(!$product->is_free && !$product->purchased && !auth_is_admin()) abort(404);

      $meta_data = $this->meta_data;

      $meta_data->image       = asset_("storage/covers/{$product->cover}");
      $meta_data->title       = $product->name;
      $meta_data->description = $product->short_description;

      if($product->file_host === 'google')
      {
        $files_list = GoogleDrive::list_folder($product->file_name)->original['files_list']->files ?? [];
      }
      elseif($product->file_host === 'dropbox')
      {
        $files_list = DropBox::list_folder($product->file_name)->original['files_list']['files'] ?? [];
      }
      elseif($product->file_host === 'local')
      {
        $files      = glob(storage_path("app/downloads/{$product->file_name}/*"));

        foreach($files as &$file)
        {
          $files_list[] = (object)['id' => \File::basename($file), 'name' => \File::basename($file), 'mimeType' => \File::extension($file)];
        }
      }
      else
      {
        $files_list = [];
      }

      return view('front.default.user.product_folder',  ['product'     => $product,
                                                         'title'       => ucfirst($product->name),
                                                         'files_list'  => $files_list,
                                                         'meta_data'   => $meta_data]);
    }



    // Download Folder File (POST)
    public function product_folder_sync_download(Request $request)
    {
      if(config('filehosts.working_with') !== 'folders' || !$request->file_name) abort(404);

      $product = Product::by_slug($request->slug, $request->user()->id);

      if(!$product) abort(404);

      $product->is_free = $product->free_temporary || $product->free_lifetime;

      if(!$product->is_free && !$product->purchased && !auth_is_admin()) abort(404);


      if($product->file_host === 'local')
      {        
        if(file_exists(storage_path("app/downloads/{$product->file_name}/{$request->file_name}")))
        {
          return response()->download(storage_path("app/downloads/{$product->file_name}/{$request->file_name}"), "{$product->slug}");
        }
      }
      elseif($product->file_host === 'dropbox')
      {
        try
        {
          return \App\Libraries\DropBox::download($request->file_name, $product->slug);
        }
        catch(\Exception $e)
        {
          
        }
      }
      elseif($product->file_host === 'google')
      {
        try
        {
          return \App\Libraries\GoogleDrive::download($request->file_name, $product->slug);
        }
        catch(\Exception $e)
        {

        }
      }
    }


    // Newsletter
    public function subscribe_to_newsletter(Request $request)
    {
      $subscription = \App\Models\Newsletter_Subscriber::insertIgnore(['email' => strip_tags($request->email)]);

      $request->session()->flash('newsletter_subscription_msg', ($subscription->id ?? null) 
                                                                ? 'Subscription done !'
                                                                : 'You are already subscribed to our newsletter !');

      return redirect(($request->redirect ?? '/') . '#footer');
    }


    // Newsletter / unsubscribe
    public function unsubscribe_from_newsletter(Request $request)
    {
      if(!\App\Models\Newsletter_Subscriber::whereRaw("MD5(email) = '{$request->md5_email}'")->delete())
        return redirect()->route('home');

      return view('front.default.message', ['message' => 'You have been unsubscribed successfully.']);
    }



    private function set_home_categories($limit = 20)
    {
      $categories    = config('categories.category_parents');
      $subcategories = config('categories.category_children');

      if($categories && $subcategories)
      {
        $_categories  = [];

        foreach($categories as $category)
        {
          if(!key_exists($category->id, array_keys($subcategories)))
            continue;

          foreach($subcategories[$category->id] as $subcategory)
          {
            $_categories[] = (object)['name' => $subcategory->name, 
                                      'url' => route('home.products.category', [$category->slug, $subcategory->slug])];
          }
        }

        shuffle($_categories);

        $_categories = array_slice($_categories, 0, $limit);

        \Config::set('home_categories', $_categories);
      }
    }


    public function notifications(Request $request)
    {
      Self::init_notifications();

      $notifications = \App\Models\Notification::useIndex('users_ids', 'updated_at')
                            ->selectRaw("products.name, products.slug, notifications.updated_at, notifications.id, `for`,
                                          CASE
                                            WHEN `for` = 0 THEN IFNULL(products.thumbnail, 'default.png')
                                            WHEN `for` = 1 OR `for` = 2 THEN IFNULL(users.avatar, 'default.jpg')
                                          END AS `image`,
                                          CASE notifications.`for`
                                            WHEN 0 THEN CONCAT('New version is available for <strong>', products.name, '</strong>')
                                            WHEN 1 THEN CONCAT('Your comment has been approved for <strong>', products.name, '</strong>')
                                            WHEN 2 THEN CONCAT('Your review has been approved for <strong>', products.name, '</strong>')
                                          END AS `text`,
                                          IF(users_ids LIKE CONCAT('\'%|', ?,':0|%\''), 0, 1) AS `read`", [\Auth::id()])
                            ->leftJoin('products', 'products.id', '=', 'notifications.product_id')
                            ->leftJoin('users', 'users.id', '=', DB::raw(\Auth::id()))
                            ->where('users_ids', 'REGEXP', '\|'.\Auth::id().':(0|1)\|')
                            ->orderBy('updated_at', 'desc')
                            ->paginate(15);

      return view('front.default.user.notifications', ['notifications' => $notifications, 'meta_data' => $this->meta_data]);
    }
    

    public function notifications_read(Request $request)
    {
      if(!ctype_digit($request->notif_id)) abort(404);

      $user_id = \Auth::id();

      return \DB::update("UPDATE notifications SET users_ids = REPLACE(users_ids, CONCAT('|', ?, ':0|'), CONCAT('|', ?, ':1|')) 
                          WHERE users_ids REGEXP CONCAT('/|', ? ,':0|/') AND id = ?",
                          [$user_id, $user_id, $user_id, $request->notif_id]);
    }


    public function add_to_cart_async(Request $request)
    {
      if(!ctype_digit($request->item_id)) abort(404);

      $product = Product::useIndex('primary', 'active')->select('products.id', 'products.name', 'products.slug', 'products.cover', 
                                                                'products.thumbnail', 'products.price', 'categories.name as category_name')
                                                       ->leftJoin('categories', 'categories.id', '=', 'products.category')
                                                       ->where(['products.active' => 1, 'products.id' => $request->item_id])
                                                       ->first();

      $product->thumbnail = asset("storage/thumbnails/{$product->thumbnail}");
      $product->cover     = asset("storage/covers/{$product->cover}");
      $product->url       = route('home.product', $product->slug);
      $product->quantity  = 1;

      return response()->json(['product' => $product]);
    }

                                    
    protected static function init_notifications()
    {
      if($user_id = \Auth::id())
      {
        $notifications = \DB::select("SELECT products.name, products.slug, notifications.updated_at, notifications.id, `for`,
                                        CASE
                                          WHEN `for` = 0 THEN IFNULL(products.thumbnail, 'default.png')
                                          WHEN `for` = 1 OR `for` = 2 THEN IFNULL(users.avatar, 'default.jpg')
                                        END AS `image`,
                                        CASE notifications.`for`
                                          WHEN 0 THEN CONCAT('New release is available for <strong>', products.name, '</strong>')
                                          WHEN 1 THEN CONCAT('Your comment has been approved for <strong>', products.name, '</strong>')
                                          WHEN 2 THEN CONCAT('Your review has been approved for <strong>', products.name, '</strong>')
                                        END AS `text`
                                       FROM notifications USE INDEX (users_ids, updated_at)
                                       LEFT JOIN products ON products.id = notifications.product_id
                                       LEFT JOIN users ON users.id = ?
                                       WHERE users_ids REGEXP CONCAT('/|', ? ,':0|/')
                                       ORDER BY updated_at DESC
                                       LIMIT 5", [$user_id, $user_id]);

        config(['notifications' => $notifications]);
      }
    }
    
    


    // App installation
    public function install(Request $request)
    {
      if(config('app.installed')) abort(404);

      if($request->method() === 'POST')
      {
        /** CREATE DATABASE CONNECTION STARTS **/
          $db_params = $request->input('database');

          \Config::set("database.connections.mysql", array_merge(config('database.connections.mysql'), $db_params));

          try 
          {
            DB::connection()->getPdo();
          }
          catch (\Exception $e)
          {
            $validator = \Validator::make($request->all(), [])
                         ->errors()->add('Database', $e->getMessage());

            return redirect()->back()->withErrors($validator)->withInput();
          }
        /** CREATE DATABASE CONNECTION ENDS **/


        /** CREATE DATABASE TABLES STARTS **/
          $classes = ['PasswordResets', 'Products', 'Categories', 'Pages', 'Users', 'Transactions', 'Coupons', 
                      'Settings', 'Posts', 'Comments', 'Reviews', 'Faqs', 'Support', 'SupportEmail', 
                      'Newsletter_Subscribers', 'Notifications', 'Transactions'];

          foreach($classes as $classe)
          {
            call_user_func(["\Migrations\Create{$classe}Table", "up"]);
          }
        /** CREATE DATABASE TABLES ENDS **/


        /** INIT PAGES TABLE START **/
          \Migrations\InitPagesTable::up();
        /** INIT PAGES TABLE END **/



        /** SETTING .ENV VARS STARTS **/
          $env =  array_reduce(file(base_path('.env'), FILE_IGNORE_NEW_LINES|FILE_SKIP_EMPTY_LINES), 
                    function($carry, $item)
                    {
                      list($key, $val) = explode('=', $item, 2);

                      $carry[$key] = $val;

                      return $carry;
                    }, []);

          $env['DB_HOST']       = wrap_str($db_params['host']);
          $env['DB_DATABASE']   = wrap_str($db_params['database']);
          $env['DB_USERNAME']   = wrap_str($db_params['username']);
          $env['DB_PASSWORD']   = wrap_str($db_params['password']);
          $env['APP_NAME']      = wrap_str($request->input('site.name'));
          $env['APP_URL']       = "{$_SERVER['REQUEST_SCHEME']}://{$_SERVER['HTTP_HOST']}";
          $env['APP_INSTALLED'] = 'true';

          foreach($env as $k => &$v)
            $v = "{$k}={$v}";

          file_put_contents(base_path('.env'), implode("\r\n", $env));
        /** SETTING .ENV VARS ENDS **/

        
        /** CREATE SYMLINK FOR STORAGE/PUBLIC FOLDER STARTS **/
          if(!is_dir(public_path('storage')))
            app()->make('files')->link(storage_path('app/public'), public_path('storage'));
        /** CREATE SYMLINK FOR STORAGE/PUBLIC FOLDER ENDS **/


        /** CREATE ADMIN USER STARTS **/
          $request->validate([
            'admin.username' => 'required',
            'admin.email' => 'required|email',
            'admin.password' => 'required|max:255'
          ]);

          if(!$user = \App\User::where('email', $request->input('admin.email'))->first())
          {
            $user = new \App\User;

            $user->name = $request->input('admin.username');
            $user->email = $request->input('admin.email');
            $user->password = \Hash::make($request->input('admin.password'));
            $user->email_verified_at = date('Y-m-d');
            $user->role = 'admin';

            // Avatar
            if($avatar_file = $request->file('admin.avatar'))
            {
              $user_auto_inc_id = DB::select("SHOW TABLE STATUS LIKE 'users'")[0]->Auto_increment;

              $ext    = $avatar_file->extension();
              $avatar = $avatar_file->storeAs('public/avatars', "{$user_auto_inc_id}.{$ext}");

              $user->avatar = str_ireplace('public/avatars/', '', $avatar);
            }

            $user->save();
          }
        /** CREATE ADMIN USER END **/


        $settings = \App\Models\Setting::first();

        /** GENERAL SETTINGS STARTS **/
        //----------------------------
          $general_settings = json_decode($settings->general);

          $general_settings->name           = $request->input('site.name');
          $general_settings->title          = $request->input('site.title');
          $general_settings->description    = $request->input('site.description');
          $general_settings->items_per_page = $request->input('site.items_per_page');
          $general_settings->timezone       = $request->input('site.timezone');

          // Cover
          if($cover_file = $request->file('site.cover'))
          {
            $ext    = $cover_file->extension();
            $cover  = $cover_file->storeAs('public/images', "cover.{$ext}");

            $general_settings->cover = str_ireplace('public/images/', '', $cover);
          }

          // Logo
          if($logo_file = $request->file('site.logo'))
          {
            $ext   = $logo_file->extension();
            $logo  = $logo_file->storeAs('public/images', "logo.{$ext}");

            $general_settings->logo = str_ireplace('public/images/', '', $logo);
          }

          // Favicon
          if($favicon_file = $request->file('site.favicon'))
          {
            $ext      = $favicon_file->extension();
            $favicon  = $favicon_file->storeAs('public/images', "favicon.{$ext}");

            $general_settings->favicon = str_ireplace('public/images/', '', $favicon);
          }

          $settings->general = json_encode($general_settings);
        /** GENERAL SETTINGS ENDS **/


        /** MAILER SETTINGS STARTS **/
        //----------------------------
          $mailer_settings = json_decode($settings->mailer);

          $mailer_settings->mail = json_encode($request->input('mailer.mail'));

          if($request->input('mailer.imap.enabled'))
          {
            $mailer_settings->imap = json_encode($request->input('mailer.imap'));
          }

          $mailer_settings = json_encode($mailer_settings);
        /** MAILER SETTINGS ENDS **/


        /** PAYMENTS SETTINGS STARTS **/
        //----------------------------
          $payments_settings = json_decode($settings->payments);

          if($request->input('payments.paypal.enabled'))
          {
            $payments_settings->paypal = json_encode($request->input('payments.paypal'));
          }

          if($request->input('payments.stripe.enabled'))
          {
            $payments_settings->stripe = json_encode($request->input('payments.stripe'));
          }

          $payments_settings = json_encode($payments_settings);
        /** PAYMENTS SETTINGS ENDS **/


        $settings->save();

        $login_token = base64_encode(encrypt($user->email).'|'.encrypt($request->input('admin.password')));

        $login_url = url("admin_login/{$login_token}");

        \Cache::put('login_token', $login_token, now()->addMinutes(5));
        
        $html_message = <<<MSG
          <span style="color: #56DB3A;">
            Installation done successfully
          </span>
          <br>
          <a href="{$login_url}">
            Access Admin Dashboard
          </a>
MSG;

        return view('front.default.message', ['message' => $html_message]);
      }

      generate_app_key();

      $request->session()->flush();
      
      return view('install');
    }
}
