<?php

namespace App\Http\Controllers\Back;

use App\Libraries\{GoogleDrive, DropBox};
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\Models\{Category, Product};
use Illuminate\Database\Query\Builder;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\{DB, Storage, Validator};


class ProductsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      if(!file_exists(public_path('products.xml')))
        \App\Libraries\Sitemap::create('products');

      $validator =  Validator::make($request->all(),
                    [
                      'orderby' => ['regex:/^(name|price|sales|category|active|trending|featured|updated_at)$/i', 'required_with:order'],
                      'order' => ['regex:/^(asc|desc)$/i', 'required_with:orderby']
                    ]);

      if($validator->fails()) abort(404);

      $base_uri = [];

      if($keywords = $request->keywords)
      {
        $base_uri = ['keywords' => $request->keywords];

        $products = Product::useIndex('description')
                                ->selectRaw('products.id, products.name, products.slug, products.trending, 
                                             products.featured, products.active, products.price, 
                                             products.updated_at, count(transactions.id) as sales,
                                             categories.name as category, products.is_dir')
                                ->leftJoin('transactions', 'products_ids', 'LIKE', DB::raw('concat("\'%", products.id, "%\'")'))
                                ->leftJoin('categories', 'categories.id', '=', 'products.category')
                                ->where('products.name', 'like', "%{$keywords}%")
                                ->orWhere('products.slug', 'like', "%{$keywords}%")
                                ->orWhere('products.overview', 'like', "%{$keywords}%")
                                ->orWhere('products.features', 'like', "%{$keywords}%")
                                ->orWhere('products.tags', 'like', "%{$keywords}%")
                                ->orWhere('products.short_description', 'like', "%{$keywords}%")
                                ->groupBy('products.id', 'products.name', 'products.slug', 'products.trending', 
                                            'products.featured', 'products.active', 'products.price', 
                                            'products.updated_at', 'categories.name', 'products.is_dir')
                                ->orderBy('id', 'DESC');
      }
      else
      {
        if($request->orderby)
        {
          $base_uri = ['orderby' => $request->orderby, 'order' => $request->order];
        }
        
        $index = $request->orderby ?? 'primary';
        $index = $index === 'sales' ? 'primary' : $index;
        
        $products = Product::useIndex($index)
                                ->selectRaw('products.id, products.name, products.slug, products.trending, 
                                             products.featured, products.active, products.price, 
                                             products.updated_at, count(transactions.id) as sales,
                                             categories.name as category, products.is_dir')
                                ->leftJoin('transactions', 'products_ids', 'LIKE', DB::raw('concat("\'%", products.id, "%\'")'))
                                ->leftJoin('categories', 'categories.id', '=', 'products.category')
                                ->groupBy('products.id', 'products.name', 'products.slug', 'products.trending', 
                                            'products.featured', 'products.active', 'products.price', 
                                            'products.updated_at', 'categories.name', 'products.is_dir')
                                ->orderBy($request->orderby ?? 'id', $request->order ?? 'DESC');
      }

      $products = $products->paginate(15);

      $items_order = $request->order === 'desc' ? 'asc' : 'desc';

      return View('back.products.index', ['title' => 'Products',
                                          'products' => $products,
                                          'items_order' => $items_order,
                                          'base_uri' => $base_uri]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      extract(Category::products());

      $view = isFolderProcess() ? 'create_with_folders' : 'create';

      return view("back.products.{$view}", ['title' => 'Create product',
                                           'category_children' => $category_children,
                                           'category_parents' => $category_parents]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product  = new Product;

        $product_id = Product::get_auto_increment();

        $request->validate([
            'name'              => 'bail|required|unique:products|max:255',
            'price'             => 'bail|required|numeric|min:0',
            'cover'             => 'required|image',
            'overview'          => 'required',
            'short_description' => 'required',
            'category'          => 'required|numeric',
            'file_host'         => ['regex:/^(local|dropbox|google)$/i']
        ]);

        if($subcategories = $request->input('subcategories'))
        {
          $product->subcategories = implode(',', array_map('wrap_str', explode(',', $subcategories)));
        }

        $product->name                = $request->input('name');
        $product->slug                = Str::slug($product->name, '-');
        $product->short_description   = $request->input('short_description');
        $product->overview            = $request->input('overview');
        $product->features            = $request->input('features');
        $product->requirements        = $request->input('requirements');
        $product->instructions        = $request->input('instructions');
        $product->category            = $request->input('category');
        $product->price               = $request->input('price');
        $product->notes               = $request->input('notes');
        $product->version             = $request->input('version');
        $product->preview             = $request->input('preview');
        $product->release_date        = $request->input('release_date');
        $product->last_update         = $request->input('last_update');
        $product->included_files      = $request->input('included_files');
        $product->tags                = $request->input('tags');
        $product->software            = $request->input('software');
        $product->db                  = $request->input('database');
        $product->compatible_browsers = $request->input('compatible_browsers');
        $product->compatible_os       = $request->input('compatible_os');
        $product->file_host           = $request->input('file_host');
        $product->high_resolution     = $request->input('high_resolution');
        $product->free                = null;

        if(isFolderProcess())
          $product->is_dir = 1;
        
        if($faq = $request->input('faq'))
        $product->faq = json_encode(array_map(function($v)
                        {
                          $qa = preg_split('/a:/i', $v);
                          return ['Q' => trim($qa[0] ?? ''), 'A' => trim($qa[1] ?? '')];
                        }, array_filter(preg_split('/q:/i', $faq))) ?? []);


        if($product->price)
        {
          if($free = array_filter($request->input('free')))
            $product->free = json_encode($free);
        }

        // Main file | folder
        if($product->file_host === 'local')
        {
          $request->validate(['file' => 'required']); 

          if(! isFolderProcess())
          {
            $request->validate(['file' => 'mimes:zip,rar,7z']); 
          }
          
          $file = $request->file('file');

          if(isFolderProcess())
          {
            $files = $file;

            if(!\Storage::exists(storage_path("downloads/{$product_id}")))
            {
              \Storage::makeDirectory(storage_path("downloads/{$product_id}"), 0711, true, true);
            }

            foreach($files as $file)
            {
              $filename = $file->getClientOriginalName();

              $file->storeAs("downloads/{$product_id}", $filename);
            }
            
            $product->file_name = $product_id;
          }
          else
          {
            $product->file_size = $file->getSize();

            $ext  = $file->extension();
            $file = $file->storeAs('downloads', "{$product_id}.{$ext}");

            $product->file_name = str_ireplace('downloads/', '', $file);
          }
        }
        else
        {
          $request->validate([
            'file_name' => 'required'
          ]);

          $product->file_name = $request->input('file_name');
        }


        // Cover
        $ext    = $request->file('cover')->extension();
        $cover  = $request->file('cover')
                          ->storeAs('public/covers', "{$product_id}.{$ext}");

        $product->cover = str_ireplace('public/covers/', '', $cover);


        // Thumbnail
        if($thumbnail = $request->file('thumbnail'))
        {
            $ext        = $thumbnail->extension();
            $thumbnail  = $thumbnail->storeAs('public/thumbnails', "{$product_id}.{$ext}");
    
            $product->thumbnail = str_ireplace('public/thumbnails/', '', $thumbnail);
        }


        // Screenshots
        if($screenshots = $request->file('screenshots'))
        {
            $schs = [];

            foreach($screenshots as $k => $screenshot)
            {
                $ext  = $screenshot->extension();
                $name = "{$product_id}-{$k}.{$ext}";
                $sch  = $screenshot->storeAs('public/screenshots', $name);

                $schs[] = str_ireplace('public/screenshots/', '', $sch);
            }

            $product->screenshots = implode(',', $schs);
        }

        $product->save();

        $sitemap_url_data = ['loc' => route('home.product', $product->slug), 'lastmod' => $product->updated_at];

        \App\Libraries\Sitemap::append($sitemap_url_data, 'products');

        return redirect()->route('products');
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
      if(!$product = Product::find($id))
        abort(404);
      
      $product->free = json_decode($product->free);

      if($faq = json_decode($product->faq))
      {
        $t = '';

        foreach($faq as $v)
        {
          $t .= "Q: {$v->Q}\nA: {$v->A}\n";
        }

        $product->faq = $t;
      }

      extract(Category::products());

      $view = isFolderProcess() ? 'edit_with_folders' : 'edit';

      if($product->subcategories)
      {
        $subcategories = explode(',', $product->subcategories);
        $subcategories = array_map('unwrap_str', $subcategories);

        $product->subcategories = implode(',', $subcategories);
      }

      return view("back.products.{$view}",  ['title' => "Edit product - {$product->name}",
                                             'product' => $product,
                                             'category_children' => $category_children,
                                             'category_parents' => $category_parents]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $product  = Product::find($id);

        $request->validate([ 
            'name'              => ['bail', 'required', 'max:255', Rule::unique('products')->ignore($id)],
            'price'             => 'bail|required|numeric|min:0',
            'overview'          => 'required',
            'short_description' => 'required',
            'category'          => 'required|numeric'
        ]);
        
        if($subcategories = $request->input('subcategories'))
        {
          $subcategories = explode(',', $subcategories);
          $subcategories = array_map('unwrap_str', $subcategories);

          $product->subcategories = implode(',', array_map('wrap_str', $subcategories));
        }
        
        $product->name                = $request->input('name');
        $product->slug                = Str::slug($product->name, '-');
        $product->short_description   = $request->input('short_description');
        $product->overview            = $request->input('overview');
        $product->features            = $request->input('features');
        $product->requirements        = $request->input('requirements');
        $product->instructions        = $request->input('instructions');
        $product->category            = $request->input('category');
        $product->price               = $request->input('price');
        $product->notes               = $request->input('notes');
        $product->version             = $request->input('version');
        $product->preview             = $request->input('preview');
        $product->release_date        = $request->input('release_date');
        $product->last_update         = $request->input('last_update');
        $product->included_files      = $request->input('included_files');
        $product->tags                = $request->input('tags');
        $product->software            = $request->input('software');
        $product->db                  = $request->input('database');
        $product->compatible_browsers = $request->input('compatible_browsers');
        $product->compatible_os       = $request->input('compatible_os');
        $product->high_resolution     = $request->input('high_resolution');
        $product->free                = null;

        if(isFolderProcess())
          $product->is_dir = 1;

        if($product->price)
        {
          if($free = array_filter($request->input('free')))
            $product->free = json_encode($free);
        }


        if($faq = $request->input('faq'))
        $product->faq                 = json_encode(array_map(function($v)
                                        {
                                          $qa = preg_split('/a:/i', $v);
                                          return ['Q' => trim($qa[0] ?? ''), 'A' => trim($qa[1] ?? '')];
                                        }, array_filter(preg_split('/q:/i', $faq))) ?? []);


        if($request->input('file_host'))
        {
          $request->validate([
            'file_host' => ['regex:/^(local|dropbox|google)$/i']
          ]);
          
          $product->file_host = $request->input('file_host');

          // Main file | folder
          if($file = $request->file('file'))
          {
            $request->validate(['file' => 'required']); 

            if(! isFolderProcess())
            {
              $request->validate(['file' => 'mimes:zip,rar,7z']); 
            }
            
            if(isFolderProcess())
            {
              $files = $file;

              \File::deleteDirectory(storage_path("app/downloads/{$id}"));

              if(!\Storage::exists(storage_path("downloads/{$id}")))
              {
                \Storage::makeDirectory(storage_path("downloads/{$id}"), 0711, true, true);
              }

              foreach($files as $file)
              {
                $filename = $file->getClientOriginalName();

                $file->storeAs("downloads/{$id}", $filename);
              }
              
              $product->file_name = $id;
            }
            else
            {
              $product->file_size = $file->getSize();

              $ext  = $file->extension();
              $file = $file->storeAs('downloads', "{$id}.{$ext}");

              $product->file_name = str_ireplace('downloads/', '', $file);
            }
          }
          else
          {
            $request->validate([
              'file_name' => 'required'
            ]);

            $product->file_name = $request->input('file_name');
          }
        }


        // Cover
        if($cover = $request->file('cover'))
        {
          $ext    = $cover->extension();
          $cover  = $cover->storeAs('public/covers', "{$id}.{$ext}");

          $product->cover = str_ireplace('public/covers/', '', $cover);
        }

        // Thumbnail
        if($thumbnail = $request->file('thumbnail'))
        {
          $ext        = $thumbnail->extension();
          $thumbnail  = $thumbnail->storeAs('public/thumbnails', "{$id}.{$ext}");

          $product->thumbnail = str_ireplace('public/thumbnails/', '', $thumbnail);
        }


        // Screenshots
        if($screenshots = $request->file('screenshots'))
        {
            $schs = glob(storage_path("app/public/screenshots/{$id}-*.*"));

            foreach($schs as $sch)
            {
              unlink($sch);
            }

            $product->screenshots = [];

            $schs = [];

            foreach($screenshots as $k => $screenshot)
            {
                $ext  = $screenshot->extension();
                $name = "{$id}-{$k}.{$ext}";
                $sch  = $screenshot->storeAs('public/screenshots', $name);

                $schs[] = str_ireplace('public/screenshots/', '', $sch);
            }

            $product->screenshots = implode(',', $schs);
        }

        $product->updated_at = date('Y-m-d H:i:s');

        $product->save();

        $sitemap_url_data = ['loc' => route('home.product', $product->slug), 'lastmod' => $product->updated_at];

        \App\Libraries\Sitemap::append($sitemap_url_data, 'products');

        if($request->notify_buyers)
        {
          DB::insert("INSERT INTO notifications (`product_id`, `for`, `users_ids`) 
                      SELECT ?, 0, CONCAT('|', GROUP_CONCAT(CONCAT(user_id, ':0') SEPARATOR '|'), '|')
                      FROM transactions USE INDEX (products_ids) WHERE products_ids LIKE CONCAT('\'%', ?, '%\'')", [$product->id, $product->id]);
        }

        return redirect()->route('products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $ids
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $ids)
    {
      $ids   = explode(',', $ids);
      $slugs = Product::useIndex('primary')->selectRaw("CONCAT(?, '/', `slug`) slug", [route('home.product', '')])
                                           ->whereIn('id', $ids)->get();

      if($slugs)
      {
        \App\Libraries\Sitemap::delete('products', array_column($slugs->toArray(), 'slug'));

        $working_with_folders = isFolderProcess();

        if(Product::destroy($ids))
        {
          foreach($ids as $id)
          {
            @$this->unlink_files($id, $working_with_folders);
          }
        }
      }

      return redirect()->route('products');
    }




    public function status(Request $request)
    {
      $res = DB::update("UPDATE products USE INDEX(primary) SET {$request->status} = IF({$request->status} = 1, 0, 1) WHERE id = ?", 
                      [$request->id]);

      if($request->status === 'active')
      {
        $product = Product::find($request->id);

        if(! $product->active)
        {
          \App\Libraries\Sitemap::delete('products', $product->slug);
        }
        else
        {
          $sitemap_url_data = ['loc' => route('home.product', $product->slug), 'lastmod' => date('Y-m-d H:i:s')];
          
          \App\Libraries\Sitemap::append($sitemap_url_data, 'products');
        }
      }

      return response()->json(['success' => (bool)$res ?? false]);
    }




    // Unlink "main file", "screenshots" and "cover"
    private function unlink_files(int $product_id, $folder = false)
    {
      try
      {
        if($folder)
        {
          \File::deleteDirectory(storage_path("app/downloads/{$product_id}"));
        }
        else
        {
          \File::delete(glob(storage_path("app/downloads/{$product_id}.*")));
        }

        $screenshots = glob(storage_path("app/public/screenshots/{$product_id}-*.*"));
        $cover       = glob(storage_path("app/public/covers/{$product_id}.*"));
        $thumbnail   = glob(storage_path("app/public/thumbnails/{$product_id}.*"));

        \File::delete(array_merge($screenshots, $cover, $thumbnail));
      }
      catch(Exception $e)
      {
        
      }
    }



    public function list_files(Request $request)
    {
      return call_user_func("\App\Libraries\\$request->files_host::list_files", $request);
    }



    public function list_folders(Request $request)
    {
      return call_user_func("\App\Libraries\\$request->files_host::list_folders", $request);
    }
    
    
    // Search products for newsletter selections
    public function newsletter(Request $request)
    {
        $products = \App\Models\Product::useIndex('name')
          ->select('id', 'name', 'slug', 'short_description', 'cover', 'price')
          ->where('name', 'like', "%{$request->keywords}%")
          ->where('active', '=', '1')
          ->limit(50)->get();

    return response()->json(['products' => $products]);
    }
}