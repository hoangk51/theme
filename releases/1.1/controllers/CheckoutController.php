<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\{Stripe, PayPalCheckout, Skrill, Razorpay, IyzicoLib, GoogleDrive, DropBox};
use App\Models\{Transaction, Coupon, Product, Google_Drive_Permission, Skrill_Transaction};
use Illuminate\Support\Facades\{DB, Session, Cache, Validator};


class CheckoutController extends Controller
{

    public function payment(Request $request)
    {
      $cart         = $this->validate_request($request);
      $products_ids = array_column($cart, 'id');

      $request->products_ids = json_encode($products_ids, true);
      $coupon                = $this->validate_coupon($request)->original;
      $discount              = ['value' => 0];

      if($coupon['status'] ?? null)
      {
        $total_amount = array_reduce($cart, function($c, $p)
                        {
                          return $c + ($p->price * $p->quantity);
                        }, 0);
        
        if($product = $coupon['coupon']['product'])
        {
          foreach($cart as $item)
          {
            if($item->id == $product)
            {
              $price = $item->price;

              $discount['value'] =  $coupon['coupon']['is_percentage']
                                    ? number_format($price * ($coupon['coupon']['value'] / 100), 2)
                                    : number_format($coupon['coupon']['value'], 2);
              $discount['product'] = $product;

              break;
            }
          }
        }
        else
        {
          $discount['value'] =  $coupon['coupon']['is_percentage']
                                ? number_format($total_amount * ($coupon['coupon']['value'] / 100), 2)
                                : number_format($coupon['coupon']['value'], 2);
        }
      }

      $temp_storage = ['cart'         => $cart, 
                       'products_ids' => $products_ids, 
                       'coupon'       => ['code' => $coupon['coupon']['code'] ?? '', 'value' => $discount['value']]];


      if(preg_match('/^(paypal|stripe|razorpay|iyzico)$/i', $request->processor))
      {
        Session::put($temp_storage);   
      }

      if($request->processor === 'paypal')
      {
        $paypal = new PayPalCheckout();

        if(!$created_order = json_decode($paypal->create_order($cart, $discount)))
          abort(404);

        if($created_order->message ?? null)
        {
          $error_details = $created_order->details[0];

          exists_or_abort(null, "{$error_details->issue} - {$error_details->description}");
        }

        session(['payment_processor' => 'paypal']);

        foreach($created_order->links as $link)
        {
          if($link->rel === 'approve')
          {
            header("location: $link->href");
          }
        }
      }
      elseif($request->processor === 'stripe')
      {
        if($response = json_decode((new Stripe)->create_checkout_session($cart, $discount)))
        {
          session(['payment_processor' => 'stripe']);

          return response()->json(['id' => $response->id]);
        }
      }
      elseif($request->processor === 'skrill')
      {
        $user_id = auth()->user()->id ?? abort(404);

        if($sid = Skrill::checkout_session_id($cart, $discount, $user_id))
        {
          Cache::put("skrill_{$user_id}", array_merge($temp_storage, ['payment_processor' => 'skrill', 'skrill_sig' => $sid, 'user_id' => $user_id]), now()->addMinutes(5));

          header("location: https://pay.skrill.com/app/?sid={$sid}");
        }
      }
      elseif($request->processor === 'razorpay')
      {
        if($url = Razorpay::create_payment_link($cart, $discount, auth()->user()))
        {
          session(['payment_processor' => 'razorpay']);

          header("location: {$url}");
        }
      }
      elseif($request->processor === 'iyzico')
      {
        Session::put(['discount' => $discount]);
        
        return redirect()->route('home.checkout.iyzico_pre_chackout');
      }
    }



    public function save(Request $request)
    {
      $products_ids      = Session::pull('products_ids');
      $payment_processor = Session::pull('payment_processor');
      $coupon            = Session::pull('coupon');
      $cart              = Session::pull('cart');
      
      $transaction = new Transaction;

      if($payment_processor === 'paypal')
      {
        if(!$request->token) abort(404);

        $capture = (new PayPalCheckout)->capture_order($request->token);

        $response = json_decode($capture);

        if(property_exists($response, 'name'))
          return redirect()->route('home');

        $transaction->order_id          = $response->order_id;
        $transaction->transaction_id    = $response->purchase_units[0]->payments->captures[0]->id;
        $transaction->reference_id      = $response->purchase_units[0]->reference_id;
        $transaction->amount            = $response->gross_total_amount->value;
      }
      elseif($payment_processor === 'stripe')
      {
        if(!$stripe_session_id = $request->stripe_sess_id) abort(404);

        if(!$response = json_decode((new Stripe)->get_checkout_session($stripe_session_id))) abort(404);
        
        $transaction->transaction_id = Transaction::get_auto_increment();
        $transaction->amount   = number_format($response->payment_intent->amount/100, 2);
        $transaction->cs_token = $stripe_session_id;
      }
      elseif($payment_processor === 'razorpay')
      {
        $validator = Validator::make($request->all(), [
          "razorpay_payment_id"       => "required|string",
          "razorpay_invoice_id"       => "required|string",
          "razorpay_invoice_status"   => "required|string|in:paid",
          "razorpay_invoice_receipt"  => "nullable|string",
          "razorpay_signature"        => "required|string"
        ]);
      
        if($validator->fails())
        {
          exists_or_abort(null, $validator->errors());
        }

        extract($request->all());

        $expected_sig = hash_hmac('sha256', 
                                  "{$razorpay_invoice_id}|{$razorpay_invoice_receipt}|{$razorpay_invoice_status}|{$razorpay_payment_id}", 
                                  config('payments.razorpay.secret_id'));

        if(! hash_equals($expected_sig, $razorpay_signature)) abort(404);
        
        $payment = Razorpay::fetch_payment($razorpay_payment_id);

        if(property_exists($payment, 'error'))
        {
          exists_or_abort(null, $payment->error->code.' : '.$payment->error->description);
        }
        
        $transaction->transaction_id = $razorpay_payment_id;
        $transaction->order_id       = $razorpay_invoice_id;
        $transaction->amount         = number_format($payment->amount/100, 2);
      }

      $transaction->discount          = $coupon['value'] ?? 0;
      $transaction->user_id           = auth()->user()->id;
      $transaction->updated_at        = date('Y-m-d H:i:s');
      $transaction->products_ids      = implode(',', array_map('wrap_str', $products_ids));
      $transaction->processor         = $payment_processor;
      $transaction->items_count       = array_sum(array_column($cart, 'quantity'));

      $transaction->save();

      DB::update("UPDATE coupons SET used_by = IF(used_by IS NULL, ?, CONCAT_WS(',', used_by, ?)) WHERE code = ?", ["'{$transaction->user_id}'", "'{$transaction->user_id}'", $coupon['code']]);

      Session::flash('transaction_response', 'done');

      $notif_data = $this->notification_data($cart, $coupon['value'] ?? 0, $payment_processor);

      event(new \App\Events\TransactionComplete($notif_data));

      return redirect()->route('home.checkout');
    }

    
    
    public function save_skrill(Request $request)
    {
      $validator = Validator::make($request->all(), [
                      "transaction_id" => "required|string",
                      "country" => "required|string",
                      "mb_amount" => "required|numeric",
                      "amount" => "required|numeric",
                      "firstname" => "string|nullable",
                      "IP_country" => "required|string",
                      "md5sig" => "required|string",
                      "merchant_id" => "required|string",
                      "lastname" => "string|nullable",
                      "payment_type" => "string|nullable",
                      "mb_transaction_id" => "required|string",
                      "mb_currency" => "required|string",
                      "pay_from_email" => "required|email",
                      "sha2sig" => "required|string",
                      "pay_to_email" => "required|email",
                      "currency" => "required|string",
                      "customer_id" => "required|string",
                      "user_id" => ["required", 
                                   function ($attribute, $value, $fail) {
                                      $skrill_cache = Cache::get("skrill_{$value}");
                      
                                      if(isset($skrill_cache['user_id']) && $skrill_cache['user_id'] != $value)
                                      {
                                          $fail('Invalid user id.');
                                      }
                                   }],
                      "payment_instrument_country" => "required|string",
                      "status" => "in:2"
                  ]);
                  
      if($validator->fails() || 
         is_null(Cache::get("skrill_{$request->user_id}")) || 
         $request->pay_to_email !== config('payments.skrill.merchant_account'))
      {
        exists_or_abort(null, $validator->errors());
      }
      
      // $cart - $products_ids - $coupon - $payment_processor - $user_id - $sig
      extract(Cache::pull("skrill_{$request->user_id}"));
      
      if(! $user = \App\User::find($user_id))
        abort(404);
        
      $md5_sig  = md5(implode('', [$request->post('merchant_id'),
                                   $request->post('transaction_id'),
                                   strtoupper(md5(config('payments.skrill.mqiapi_secret_word'))),
                                   $request->post('mb_amount'),
                                   $request->post('mb_currency'),
                                   $request->post('status')]));
    
      if(strtoupper($md5_sig) !== $request->post('md5sig'))
      {
        abort(404);
      }
        
      $transaction = [
        'transaction_id' => $request->post('mb_transaction_id'),
        'amount' => $request->post('mb_amount'),
        'discount' => $coupon['value'] ?? 0,
        'user_id' => $user_id,
        'updated_at' => date('Y-m-d H:i:s'),
        'products_ids' => implode(',', array_map('wrap_str', $products_ids)),
        'processor' => $payment_processor,
        'items_count' => array_sum(array_column($cart, 'quantity'))
      ];
      

      $new_transaction = DB::insert("INSERT IGNORE INTO transactions (transaction_id, amount, discount, user_id, updated_at, 
                                     products_ids, processor, items_count) 
                                     VALUES (?,?,?,?,?,?,?,?)", array_values($transaction));
        
      if($new_transaction)
      {
          $user_data = ['name' => $user->name,
                        'email' => $user->email];
          
          $notif_data = $this->notification_data($cart, $coupon['value'] ?? 0, $payment_processor, $user_data);

          event(new \App\Events\TransactionComplete($notif_data));
          
          $request_vars = $request->all();

          unset($request_vars['status'], $request_vars['mb_amount'], $request_vars['mb_transaction_id'], 
                $request_vars['mb_currency'], $request_vars['sha2sig']);

          Skrill_Transaction::create($request_vars);

          Cache::put("skrill_response_{$request->user_id}", ['transaction_response' => 'done']);

          if($coupon['code'])
          {
            DB::update("UPDATE coupons SET used_by = IF(used_by IS NULL, ?, CONCAT_WS(',', used_by, ?)) WHERE code = ?", 
                      ["'{$transaction->user_id}'", "'{$transaction->user_id}'", $coupon['code']]);
          }
      }

      header("HTTP/1.1 200 OK");
    }



    public function save_skrill_complete()
    {
      sleep(5);

      $user_id = auth()->user()->id;

      if($skrill_response = Cache::pull("skrill_response_{$user_id}"))
      {
        if(($skrill_response['transaction_response'] ?? null) === 'done')
        {
            Session::flash('transaction_response', 'done');
        }
      }

      return redirect()->route('home.checkout');
    }



    public function iyzico_save(Request $request)
    {
      if(! $token = $request->post('token')) abort(404);

      $checkoutForm = IyzicoLib::validate_payment($token);

      $products_ids      = Session::pull('products_ids');
      $payment_processor = Session::pull('payment_processor');
      $coupon            = Session::pull('coupon');
      $discount          = Session::pull('discount');
      $cart              = Session::pull('cart');
      
      $transaction                    = new Transaction;
      $transaction->order_id          = $checkoutForm->getToken();
      $transaction->transaction_id    = $checkoutForm->getPaymentId();
      $transaction->discount          = $coupon['value'] ?? 0;
      $transaction->user_id           = auth()->user()->id;
      $transaction->updated_at        = date('Y-m-d H:i:s');
      $transaction->products_ids      = implode(',', array_map('wrap_str', $products_ids));
      $transaction->processor         = 'iyzico';
      $transaction->items_count       = array_sum(array_column($cart, 'quantity'));
      $transaction->amount            = number_format(array_reduce($checkoutForm->getPaymentItems(), function($carry, $paymentItem)
                                        {                                
                                          return $carry + $paymentItem->getMerchantPayoutAmount();
                                        }, 0), 2);
      
      $transaction->save();

      DB::update("UPDATE coupons SET used_by = IF(used_by IS NULL, ?, CONCAT_WS(',', used_by, ?)) WHERE code = ?", ["'{$transaction->user_id}'", "'{$transaction->user_id}'", $coupon['code']]);

      Session::flash('transaction_response', 'done');

      $notif_data = $this->notification_data($cart, $coupon['value'] ?? 0, $payment_processor);

      event(new \App\Events\TransactionComplete($notif_data));

      return redirect()->route('home.checkout');
    }

    

    public function iyzico_pre_checkout(Request $request)
    {
      if(! $request->session()->has(['cart', 'products_ids', 'coupon', 'discount']))
        abort(404);

      return View('front.default.checkout_iyzico', ['user' => auth()->user()]);
    }


    
    public function iyzico_init(Request $request)
    {
      if(! $request->session()->has(['cart', 'products_ids', 'coupon', 'discount']))
        abort(404);

      $request->validate([
                'firstname' => 'string|required',
                'lastname'  => 'string|required',
                'id_number' => 'string|required',
                'city'      => 'string|required',
                'country'   => 'string|required',
                'address'   => 'string|required',
                'email'     => 'email|required'
              ]);

      $buyer = (object) $request->post();

      if(! $response = IyzicoLib::init_payment(session('cart'), session('discount'), auth()->user(), $buyer))
        abort(404);
      
      if($response->getErrorCode())
      {
        Session::flash('IyzicoError', $response->getErrorMessage());

        return back()->withInputs($request->all());
      }

      $paymentPageUrl = $response->getPaymentPageUrl();

      if(! $paymentPageUrl)
        abort(404);

      session(['payment_processor' => 'iyzico', 'iyzico_token' => $response->getToken()]);

      header("location: {$paymentPageUrl}");
    }


    public function iyzico_cancel(Request $request)
    {
      $request->session()->forget(['discount', 'cart', 'products_ids', 'coupon']);

      return redirect()->route('home.checkout');
    }



    private function notification_data(array $cart, $coupon, $payment_processor, array $buyer = [])
    {
      $data = [
        'items'     => $cart,
        'buyer'     => $buyer['name'] ?? auth()->user()->name ?? auth()->user()->email ?? (auth()->user()->firstname.' '.auth()->user()->lastname),
        'md5_email' => md5($buyer['email'] ?? auth()->user()->email),
        'email'     => $buyer['email'] ?? auth()->user()->email,
        'items_total_amount' => number_format(array_reduce($cart, function($carry, $item)
                                {
                                  return $carry + ($item->quantity * $item->price);
                                }, 0), 2),
        'discount'           => number_format(($coupon ?? 0), 2),
        'shipping'           => number_format(0, 2),
        'fee'                => number_format(config("payments.{$payment_processor}.fee", 0), 2),
        'currency_symbol'    => config('payments.currency_symbol', '$'),
        'currency_code'      => config('payments.currency_code', 'USD')
      ];

      $data['tax'] =  number_format(config('payments.vat')
                          ? (round(($data['items_total_amount'] - $data['discount']) * config('payments.vat') / 100, 2))
                          : 0, 2);
      
      $data['total_amount'] = number_format(($data['items_total_amount'] - $data['discount']) 
                                            + ($data['tax'] + $data['shipping'] + $data['fee']), 2);
      return $data;
    }


    


    private function validate_request(Request $request)
    {
      $cart   = json_decode($request->cart);
      $ids    = array_column($cart, 'id');

      $products = DB::table('products')
                  ->select('products.id', 
                           'products.name', 
                           'products.slug', 
                           'products.price',
                           'products.cover', 
                           'categories.name as category')
                  ->leftJoin('categories', 'categories.id', '=', 'products.category')
                  ->whereIn('products.id', $ids)
                  ->get();

      if(!$products)
        abort(404);

      foreach($products as $product)
      {
        foreach($cart as &$item)
        {
          if($product->id == $item->id)
          {
            unset($item->url, $item->thumbnail, $item->screenshots);

            $item->price      = $product->price;
            $item->category   = $product->category;
            $item->name       = $product->name;
            $item->cover      = $product->cover;
            $item->slug       = $product->slug;
          }
        }
      }

      unset($products);

      return $cart;
    }



    public function validate_coupon(Request $request)
    {      
      if(!$coupon = $request->coupon)
        return response()->json(['status' => false, 'msg' => 'Invalid coupon 1']);


      if(!$products_ids = json_decode($request->products_ids))
      {
        return response()->json(['status' => false, 'msg' => 'Missing/Invalid parameter (products_ids).']);
      }


      foreach($products_ids as $product_id)
      {
        if(!is_numeric($product_id))
        {
          return response()->json(['status' => false, 'msg' => 'Misformatted request.']);
        }
      }


      $user_id = auth()->user()->id;

      if(!$_coupon = Coupon::where('code', $coupon)->get()->first())
      {
        return response()->json(['status' => false, 'msg' => 'Coupon unavailable.']);
      }


      if($_coupon->expires_at < date('Y-m-d H:i:s'))
      {
        return response()->json(['status' => false, 'msg' => 'Coupon expired.']);
      }


      if($_coupon->starts_at >= date('Y-m-d H:i:s'))
      {
        return response()->json(['status' => false, 'msg' => 'Coupon not available yet.']);
      }


      if(is_numeric($_coupon->product_id))
      {
        if(!in_array($_coupon->product_id, $products_ids))
        {
          return response()->json(['status' => false, 'msg' => 'This coupon is not for the selected product(s).']);
        }
      }


      if($_users_ids = array_filter(explode(',', str_replace("'", '', $_coupon->users_ids))))
      {
        if(!in_array($user_id, $_users_ids))
        {
          return response()->json(['status' => false, 'msg' => 'You are not allowed to use this coupon.']);
        }
      }


      if($_used_by = array_filter(explode(',', str_replace("'", '', $_coupon->used_by))))
      {
        if(in_array($user_id, $_used_by))
        {
          return response()->json(['status' => false, 'msg' => 'Coupon already used.']);
        }
      }

      $product_id = is_numeric($_coupon->product_id) ? (int)$_coupon->product_id : null;


      return response()->json([
                                'status' => true, 
                                'msg' => 'Coupon applied',
                                'coupon' => [
                                             'value' => $_coupon->value,
                                             'is_percentage' => $_coupon->is_percentage ? true : false,
                                             'product' => $product_id,
                                             'expires_at' => $_coupon->expires_at,
                                             'code' => $coupon
                                           ]
                              ]);
    }

}
