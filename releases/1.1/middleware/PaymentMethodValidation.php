<?php

namespace App\Http\Middleware;

use Closure;
use Validator;
use Illuminate\Http\Request;

class PaymentMethodValidation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {   
      $validator =  Validator::make($request->all(), [
                      'processor' => ['required', 'regex:/^(paypal|stripe|skrill|razorpay|iyzico)$/i', 'bail'],
                      'cart' => ['required', 'regex:/^\[.+\]$/i']
                    ]);

      if($validator->fails()) abort(404);
      
      return $next($request);
    }
}
