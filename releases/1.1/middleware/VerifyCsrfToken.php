<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/logout',
        '/admin/*',
        '/checkout/payment',
        '/blog/search',
        '/products/search',
        '/checkout/validate_coupon',
        '/item/*',
        '/install',
        '/notifications/read',
        '/add_to_cart_async',
        '/item/product_folder',
        '/downloads/dropbox_preview_url',
        '/downloads/*/download',
        '/checkout/save_skrill',
        '/checkout/iyzico_save'
    ];
}
