<?php

namespace App\Models;

use App\Database\EloquentModel as Model;
use Illuminate\Support\Facades\DB;


class Skrill_Transaction extends Model
{
  protected $guarded = [];
  protected $table = 'skrill_transactions';
  public $timestamps = false;
}
