<?php

namespace App\Models;

use App\Database\EloquentModel as Model;
use JustBetter\PaginationWithHavings\PaginationWithHavings;
use Illuminate\Support\Facades\DB;


class Product extends Model
{
    use PaginationWithHavings;


    protected static function home()
    {
      if(!$trending_products = Self::home_filter("products.trending = 1"))
      {
        $trending_products = Self::home_filter("products.views > 0 AND products.price > 0", 
                                                8, "products.views DESC");
      }

      $featured_products = Self::home_filter("products.featured = 1 AND products.price > 0");
      $free_products     = Self::home_filter("products.price = 0");
      $newest_products   = Self::home_filter("products.price > 0", 8, "products.id DESC");

      return compact('trending_products', 'featured_products', 'free_products', 'newest_products');
    }



    private static function home_filter($where, $limit = 8, $order = "products.updated_at DESC")
    {
      return DB::select("SELECT products.id, products.`name`, products.slug, products.cover, 
                         products.price, GROUP_CONCAT(categories.name) AS subcategories
                         FROM products
                         LEFT JOIN categories ON products.subcategories REGEXP CONCAT('(\'', categories.id, '\')')
                         WHERE $where AND products.active = 1
                         GROUP BY products.id
                         ORDER BY $order
                         LIMIT ?", [$limit]);
    }



    protected static function by_slug($slug, $user_id = 'null')
    {
      DB::statement(DB::raw("SET @purchased = 0, @reviewed = 0"));

      $product = DB::select("SELECT products.*, IFNULL(products.thumbnail, 'default.png') AS thumbnail, categories.name as category, categories.id as category_id, 
                    (SELECT COUNT(transactions.id) FROM transactions WHERE transactions.products_ids REGEXP CONCAT(\"'\", products.id, \"'\")) AS sales,
                    (SELECT COUNT(comments.id) FROM comments WHERE comments.product_id = products.id AND comments.approved = 1) AS comments_count,
                    (SELECT COUNT(reviews.id) FROM reviews WHERE product_id = products.id) AS reviews_count,
                    IFNULL((SELECT ROUND(AVG(rating)) FROM reviews WHERE product_id = products.id), 0) AS rating,
                    (CASE WHEN ? IS NOT NULL THEN @purchased := (SELECT COUNT(*) FROM transactions WHERE user_id = ? AND products_ids REGEXP CONCAT(\"'\", products.id, \"'\")) END) AS purchased,
                    (CASE WHEN ? IS NOT NULL THEN @reviewed := (SELECT COUNT(*) FROM reviews WHERE user_id = ? AND product_id = products.id) END) AS reviewed,
                    IF(price = 0, 1, 0) AS free_lifetime,
                    IF(CURRENT_DATE between substr(free, 10, 10) and substr(free, 28, 10), 1, 0) AS free_temporary
                    FROM products USE INDEX(slug, active)
                    LEFT JOIN categories ON categories.id = products.category
                    WHERE products.slug = ? AND products.active = 1
                    GROUP BY products.id, products.name, products.slug, products.short_description, products.overview, products.features, 
                    products.requirements, products.price, products.notes, products.active, products.category, products.subcategories, 
                    products.cover, products.thumbnail, products.screenshots, products.version, products.release_date, products.last_update, 
                    products.included_files, products.tags, products.preview, products.software, products.db, products.compatible_browsers, 
                    products.compatible_os, products.high_resolution, products.documentation, products.instructions, products.file_name, 
                    products.file_size, products.file_host, products.created_at, products.deleted_at, products.updated_at, products.free, 
                    products.featured, products.trending, products.views, products.faq, category_id, comments_count, reviews_count,
                    rating, purchased, reviewed, free_lifetime, free_temporary, categories.name, products.is_dir", [$user_id, $user_id, $user_id, $user_id, $slug]);

      return array_shift($product);
    }



}
