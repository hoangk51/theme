<?php

namespace Migrations;

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;


class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public static function up()
    {        
        DB::statement("
          CREATE TABLE IF NOT EXISTS `settings` (
            `id` int(11) NOT NULL AUTO_INCREMENT UNIQUE,
            `general` longtext,
            `mailer` longtext,
            `payments` longtext,
            `search_engines` longtext,
            `adverts` longtext,
            `files_host` longtext,
            `social_login` longtext,
            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `deleted_at` timestamp NULL DEFAULT NULL
          ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
        ");

        Self::init();
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP TABLE IF EXISTS settings");
    }



    private static function init()
    {
      DB::statement("TRUNCATE TABLE `settings`");

      DB::insert("INSERT IGNORE INTO settings (general, mailer, payments, search_engines, adverts, files_host, social_login) 
                  VALUES (?, ?, ?, ?, ?, ?, ?)",
                  [
                    '{"name":"","title":"","description":"","email":"","keywords":"","items_per_page":"12","favicon":"favicon.png","logo":"logo.png","search_cover":"search_cover.jpeg","search_cover_color":"linear-gradient(to top, #fff, rgba(255, 255, 255, 0.8), #fff)","env":"development","debug":"1","timezone":"","facebook":"#","twitter":"#","pinterest":"#","youtube":"#","tumblr":"#","search_header":"Get HTML Templates, PHP Scripts, Graphics and Codes starting from $9.99","search_subheader":"","cover":"cover.png","blog":{"title":"","description":""},"blog_cover":"blog_cover.png"}',

                    '{"mail":{"username":"","password":"","host":"","port":"465","encryption":"ssl","reply_to":""},"imap":{"enabled":"","username":"","password":"","host":"","port":"143","encryption":"tls","validate_cert":"false","account":"default","protocol":"imap"}}',

                    '{"paypal":{"enabled":"","mode":"sandbox","client_id":"","secret_id":"","fee":"0"},"stripe":{"enabled":"","mode":"sandbox","client_id":"","secret_id":"","fee":"0"},"skrill":{"enabled":"","merchant_account":"","mqiapi_secret_word":"","mqiapi_password":"","fee":"0"},"vat":"0","currency_code":"USD","currency_symbol":"$"}',

                    '{"google":"","bing":"","yandex":"","google_analytics":"","robots":"follow, index"}',

                    '{"responsive_ad":"","auto_ad":"","in_feed_ad":"","link_ad":"","ad_728x90":"","ad_468x60":"","ad_250x250":"","ad_320x100":""}',

                    '{"google_drive":{"enabled":"","api_key":"","client_id":"","client_secret":"","refresh_token":"","connected_email":"","id_token":""},"dropbox":{"enabled":"","app_key":"","app_secret":"","access_token":"","current_account":""},"working_with":"files"}',

                    '{"google":{"enabled":"","client_id":"","client_secret":"","redirect":""},"github":{"enabled":"","client_id":"","client_secret":"","redirect":""},"linkedin":{"enabled":"","client_id":"","client_secret":"","redirect":""},"facebook":{"enabled":"","client_id":"","client_secret":"","redirect":""},"vkontakte":{"enabled":"","client_id":"","client_secret":"","redirect":""},"twitter":{"enabled":"","client_id":"","client_secret":"","redirect":""}}'
                  ]);
    }
}
