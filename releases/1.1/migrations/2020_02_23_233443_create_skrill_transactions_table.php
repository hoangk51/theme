<?php

namespace Migrations;

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSkrillTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public static function up()
    {
        \DB::statement("CREATE TABLE IF NOT EXISTS skrill_transactions (
          id INT(11) AUTO_INCREMENT,
          transaction_id VARCHAR(255) NOT NULL,
          country VARCHAR(255) DEFAULT NULL,
          amount FLOAT NOT NULL,
          firstname VARCHAR(255) DEFAULT NULL,
          lastname VARCHAR(255) DEFAULT NULL,
          IP_country VARCHAR(255) DEFAULT NULL,
          md5sig VARCHAR(255) NOT NULL,
          merchant_id VARCHAR(255) NOT NULL,
          payment_type VARCHAR(255) NOT NULL,
          pay_from_email VARCHAR(255) NOT NULL,
          pay_to_email VARCHAR(255) NOT NULL,
          currency VARCHAR(255) NOT NULL,
          customer_id VARCHAR(255) NOT NULL,
          user_id INT(11) NOT NULL,
          payment_instrument_country VARCHAR(255) NOT NULL,
          PRIMARY KEY id (id), 
          UNIQUE transaction_id (transaction_id)
        ) ENGINE=innodb DEFAULT charset=utf8 COLLATE=utf8_unicode_ci");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skrill_transactions');
    }
}
