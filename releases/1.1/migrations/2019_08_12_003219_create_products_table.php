<?php

namespace Migrations;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public static function up()
    {
        DB::statement("
          CREATE TABLE IF NOT EXISTS `products` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
          `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
          `short_description` text COLLATE utf8_unicode_ci,
          `overview` longtext COLLATE utf8_unicode_ci NOT NULL,
          `features` text COLLATE utf8_unicode_ci,
          `requirements` text COLLATE utf8_unicode_ci,
          `price` float DEFAULT '0',
          `notes` text COLLATE utf8_unicode_ci,
          `active` tinyint(1) DEFAULT '1',
          `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
          `subcategories` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
          `cover` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
          `thumbnail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
          `screenshots` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
          `version` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
          `release_date` date DEFAULT NULL,
          `last_update` date DEFAULT NULL,
          `included_files` text COLLATE utf8_unicode_ci,
          `tags` text COLLATE utf8_unicode_ci,
          `preview` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
          `software` text COLLATE utf8_unicode_ci,
          `db` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
          `compatible_browsers` text COLLATE utf8_unicode_ci,
          `compatible_os` text COLLATE utf8_unicode_ci,
          `high_resolution` tinyint(1) DEFAULT '0',
          `documentation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
          `instructions` text COLLATE utf8_unicode_ci,
          `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
          `file_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
          `file_host` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'local',
          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
          `deleted_at` timestamp NULL DEFAULT NULL,
          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
          `free` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
          `featured` tinyint(1) DEFAULT '0',
          `trending` tinyint(1) DEFAULT '0',
          `views` int(11) DEFAULT '0',
          `faq` text COLLATE utf8_unicode_ci,
          `is_dir` tinyint(1) DEFAULT '0',
          PRIMARY KEY (`id`),
          UNIQUE KEY `slug` (`slug`),
          UNIQUE KEY `name` (`name`),
          KEY `category` (`category`),
          KEY `subcategories` (`subcategories`),
          KEY `documentation` (`documentation`),
          KEY `release_date` (`release_date`),
          KEY `created_at` (`created_at`),
          KEY `deleted_at` (`deleted_at`),
          KEY `updated_at` (`updated_at`),
          KEY `active` (`active`),
          KEY `file_name` (`file_name`),
          KEY `file_host` (`file_host`),
          KEY `price` (`price`),
          KEY `featured` (`featured`),
          KEY `trending` (`trending`),
          KEY `free` (`free`),
          KEY `is_dir` (`is_dir`),
          FULLTEXT KEY `description` (`overview`,`features`,`requirements`,`tags`,`short_description`,`name`,`slug`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP TABLE IF EXISTS `products`");
    }
}
